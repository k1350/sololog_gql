# CHANGELOG

## 1.5.2

### Change

- ブログ表示部分のletter-spacingを広めにする
- 古いCSSプロパティだけが記述されている部分に新しい記述も併記

## 1.5.1

### Change

- 記事投稿・編集フォームでテキストエリアの自動リサイズを廃止
- API 内部コードで不要なカスタムエラーを削除
## 1.4.3

### Fix

- アカウント削除時に不要なヘッダーが表示されていた問題を修正

### Change

- API 内部コードのリファクタリング
- API 内部コードで、ブログや記事が無いときの戻り値を nil, nil にしないよう変更

## 1.4.2

### Fix

- 不要なログ書き出しを削除

## 1.4.1

### Fix

- 記事内のリンクが content という文字列になるバグを修正

### Change

- more ボタン押下時カーソル位置に挿入し、more の後ろにカーソルを戻す
- 記事編集フォームを共通化

## 1.4.0

### Change

- Next.js の app directory を導入

## 1.3.3

### Change

- Storybook を導入
- ソースコードの整理

## 1.3.2

### Fix

- Safari や Firefox でログインできない問題に対応

### Change

- 初回デプロイ時、sololog_lambda をテスト実行して初期鍵を生成する運用に変更
- ライセンス関係の整理方法を変更

## 1.3.1

### Fix

- ライセンス関係の整理

## 1.3.0

### Breaking Change

- データを論理削除するのをやめ、物理削除に変更
  - SKIP MIGRATION を指定している環境においては、`sololog_back/app/migration/migrations/0008_delete_deleted_at.sql` の `+migrate Up` に記載の SQL 文を delete 文から実行すること

## 1.2.3

### Change

- モバイルビューでもブログ説明・プロフィールをページ上部に表示するように戻し、ブログ説明が長い場合は「もっと見る」表示にする

### Fix

- Fargate Spot 化対応で LoadBalancer の deregistrationDelay を 120 秒未満に変更する対応がもれていたので修正
- クエリストリングが undefined の状態でサーバーにリクエストを送っている問題を修正

### Refactor

- AWS SDK for Go V2 に移行

## 1.2.2

### Fix

- cdk deploy の前後関係が定義できておらず、初回デプロイに失敗することがあったので修正

## 1.2.1

### Change

- バックエンドは Fargate と Fargate Spot を併用する

## 1.2.0

### Add

- ダークモードを追加

### Change

- cdk deploy の前後関係の調整

## 1.1.3

### Fix

- CloudWatch Logs に書き出されるログが複数行にわたって書き出されないよう修正

## 1.1.2

### Fix

- MySQL で bad connection エラーが出ることがある点に対して対策

### Change

- 投稿内の URL を自動的にリンクにするよう変更

## 1.1.1

### Fix

- Android でテキストエリアのリサイズができない問題を修正

### Change

- more ボタン押下後テキストエリアにフォーカスするよう変更
- モバイルビューではブログ説明・プロフィールをページ下部に表示するよう変更
  投稿ボタンの位置調整

## 1.1.0

### Fix

- Public なブログで投稿ボタンが投稿権限のない人にも表示されるバグを修正

## 1.0.0

First Release
