import { PublishOption } from '@/graph/types.generated'

export interface Blog {
  id: string
  blogKey: string
  name?: string | null
  description?: string | null
  publishOption: PublishOption
}
