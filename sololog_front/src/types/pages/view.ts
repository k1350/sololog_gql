import { ArticlePaginationInput, BlogByBlogKeyInput, PublishOption } from '@/graph/types.generated'

export interface BlogData {
  blogKey: string
  author?: string | null
  name?: string | null
  description?: string | null
  publishOption: PublishOption
  passwordHint: string
  links?: BlogLink[] | null
}

export interface BlogLink {
  id: string
  name: string
  url: string
}

export interface ArticleData {
  content?: string | null
  publishOption: PublishOption
  createdAt?: string | null
  updatedAt?: string | null
}

export interface ArticleEdgeData {
  __typename?: 'ArticleEdge' | undefined
  cursor: string
  node?:
    | {
        __typename?: 'Article' | undefined
        id: string
        articleKey: string
        content?: string | null
        publishOption: PublishOption
        createdAt?: string | null
        updatedAt?: string | null
      }
    | null
    | undefined
}
