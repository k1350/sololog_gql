'use client'

import { Box } from '@chakra-ui/react'

type ViewLayoutProps = Required<{
  readonly children: React.ReactElement
}>

export const ViewLayout = ({ children }: ViewLayoutProps) => (
  <main>
    <Box mb='2rem' maxW='1280px' mr='auto' ml='auto' pr='10px' pl='10px'>
      {children}
    </Box>
  </main>
)
