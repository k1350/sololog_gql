'use client'

import { Box } from '@chakra-ui/react'

type ArticleViewLayoutProps = Required<{
  readonly children: React.ReactElement
}>

export const ArticleViewLayout = ({ children }: ArticleViewLayoutProps) => (
  <main>
    <Box mb='2rem' maxW='768px' mr='auto' ml='auto' pr='10px' pl='10px'>
      {children}
    </Box>
  </main>
)
