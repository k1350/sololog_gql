'use client'

import { LockIcon, SettingsIcon, UnlockIcon, WarningTwoIcon } from '@chakra-ui/icons'
import {
  Box,
  Center,
  Container,
  Divider,
  Button,
  MenuButton,
  MenuList,
  MenuItem,
  Menu,
  Flex,
  Icon,
} from '@chakra-ui/react'
import { SkipNavLink, SkipNavContent } from '@chakra-ui/skip-nav'
import Link from 'next/link'
import { memo, useCallback, useContext } from 'react'
import { MdHome, MdOutlineSupervisorAccount } from 'react-icons/md'
import ColorModeButton from '@/components/ui/ColorModeButton'
import { CONSTANT } from '@/constants'
import { useLogoutMutation } from '@/graph/logout.generated'
import { AuthContext } from '@/lib/auth/authContext'
import { useCurrentUser } from '@/lib/auth/currentUserContext'

type LayoutProps = Required<{
  readonly children: React.ReactElement
}>

export const Layout = ({ children }: LayoutProps) => (
  <>
    <Header />
    <main>
      <Box mb='2rem' maxW='768px' mr='auto' ml='auto' pr='10px' pl='10px'>
        {children}
      </Box>
    </main>
  </>
)

function HeaderInnerComponent() {
  const { user, role, isAuthChecking } = useCurrentUser()
  const authContext = useContext(AuthContext)
  const { mutateAsync } = useLogoutMutation({
    onError: async (e: any) => {
      // エラーはコンソールに書いておくがログアウト処理は継続する
      console.error(e)
    },
  })

  const logout = useCallback(async () => {
    try {
      await mutateAsync({})
    } catch (e) {
      // サーバー側でログアウトを記録できなくてもフロント側のログアウトはやる
      console.log(e)
    }

    authContext.signOut().catch((error) => {
      alert(`ログアウトに失敗しました。エラーコード：${error.code}`)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mutateAsync])

  if (isAuthChecking) {
    return null
  }

  return (
    <nav>
      <SkipNavLink>Skip to content</SkipNavLink>
      <Container maxW='md'>
        <Center>
          <Flex gap={4}>
            {user ? (
              <>
                <Link href='/home'>
                  <Button leftIcon={<Icon as={MdHome} />} variant='ghost'>
                    ブログ一覧
                  </Button>
                </Link>
                <Menu>
                  <MenuButton
                    as={Button}
                    aria-label='Other Menu'
                    leftIcon={<SettingsIcon />}
                    variant='ghost'
                  >
                    その他
                  </MenuButton>
                  <MenuList>
                    {role === CONSTANT.ROLE.ADMIN ? (
                      <Link href='/admin'>
                        <MenuItem icon={<Icon as={MdOutlineSupervisorAccount} />}>
                          管理者メニュー
                        </MenuItem>
                      </Link>
                    ) : (
                      <></>
                    )}
                    <MenuItem icon={<UnlockIcon />} onClick={logout}>
                      ログアウト
                    </MenuItem>
                    <Link href='/delete'>
                      <MenuItem icon={<WarningTwoIcon />}>アカウント削除</MenuItem>
                    </Link>
                  </MenuList>
                </Menu>
              </>
            ) : (
              <>
                <Link href='/login'>
                  <Button leftIcon={<LockIcon />} variant='ghost'>
                    ログイン
                  </Button>
                </Link>
              </>
            )}
            <ColorModeButton />
          </Flex>
        </Center>
      </Container>
      <Divider />
      <SkipNavContent />
    </nav>
  )
}
const Header = memo(HeaderInnerComponent)
