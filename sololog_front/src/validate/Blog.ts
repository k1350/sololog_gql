import { CONSTANT } from '@/constants'

export const AuthorValidator = {
  required: CONSTANT.ERROR_MESSAGE.REQUIRED,
  maxLength: {
    value: 50,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(50),
  },
}

export const NameValidator = {
  required: CONSTANT.ERROR_MESSAGE.REQUIRED,
  maxLength: {
    value: 255,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(255),
  },
}

export const DescriptionValidator = {
  maxLength: {
    value: 500,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(500),
  },
}

export const PublishOptionValidator = {
  required: CONSTANT.ERROR_MESSAGE.REQUIRED,
}

export const PasswordValidator = (showPasswordInput: boolean) => {
  return {
    validate: (value: string) => {
      if (showPasswordInput && value === '') {
        return CONSTANT.ERROR_MESSAGE.REQUIRED
      }
    },
    pattern: {
      value: /^[a-zA-Z\d!@#%^&*]+$/i,
      message: CONSTANT.ERROR_MESSAGE.PASSWORD,
    },
    minLength: {
      value: 1,
      message: CONSTANT.ERROR_MESSAGE.MIN_LENGTH(1),
    },
    maxLength: {
      value: 255,
      message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(255),
    },
  }
}

export const PasswordHintValidator = {
  maxLength: {
    value: 100,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(100),
  },
}

export const LinkNameValidator = {
  maxLength: {
    value: 255,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(255),
  },
}

export const LinkUrlValidator = (linkName: string) => {
  return {
    maxLength: {
      value: 3000,
      message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(3000),
    },
    validate: (value: string) => {
      if (linkName !== '' && value === '') {
        return CONSTANT.ERROR_MESSAGE.LINK_URL_REQUIRED
      }
    },
    pattern: {
      value: /^http([s]?):\/\/.+$/,
      message: CONSTANT.ERROR_MESSAGE.LINK_URL_REGEX,
    },
  }
}
