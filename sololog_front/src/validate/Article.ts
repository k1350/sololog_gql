import { CONSTANT } from '@/constants'

export const ArticleValidator = {
  required: CONSTANT.ERROR_MESSAGE.REQUIRED,
  maxLength: {
    value: 40000,
    message: CONSTANT.ERROR_MESSAGE.MAX_LENGTH(40000),
  },
}
