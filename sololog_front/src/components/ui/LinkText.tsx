'use client'

import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Text, useColorModeValue } from '@chakra-ui/react'
import theme from '@/theme'

type LinkTextProps = {
  children: React.ReactNode
  isExternal?: boolean
}

export const LinkText = (props: LinkTextProps) => {
  const color = useColorModeValue(theme.colors.linkColor.lightMode, theme.colors.linkColor.darkMode)
  if (props.isExternal) {
    return (
      <Text as='u' color={color} wordBreak={'break-word'} overflowWrap={'anywhere'}>
        {props.children}&nbsp;
        <ExternalLinkIcon mx='2px' data-testid='test-external-link-icon' />
      </Text>
    )
  }
  return (
    <Text as='u' color={color} wordBreak={'break-word'} overflowWrap={'anywhere'}>
      {props.children}
    </Text>
  )
}
