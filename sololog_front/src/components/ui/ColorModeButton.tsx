'use client'

import { MoonIcon, SunIcon } from '@chakra-ui/icons'
import { IconButton, useColorMode } from '@chakra-ui/react'

export default function ColorModeButton() {
  const { colorMode, toggleColorMode } = useColorMode()
  return (
    <IconButton
      aria-label='DarkMode Switch'
      icon={colorMode === 'light' ? <MoonIcon /> : <SunIcon />}
      onClick={toggleColorMode}
    />
  )
}
