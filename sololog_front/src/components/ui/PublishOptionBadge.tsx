'use client'

import { Badge } from '@chakra-ui/react'
import { PublishOption } from '@/graph/types.generated'

interface PublishOptionBadgeProps {
  publishOption: PublishOption
  mr?: number
}

const PublishOptionBadge = (props: PublishOptionBadgeProps) => {
  const { publishOption, mr } = props

  switch (publishOption) {
    case 'PUBLIC':
      return (
        <Badge variant='solid' colorScheme='blue' mr={mr}>
          {publishOption}
        </Badge>
      )
    case 'PASSWORD':
      return (
        <Badge variant='outline' colorScheme='blue' mr={mr}>
          {publishOption}
        </Badge>
      )
    default:
      return (
        <Badge variant='subtle' colorScheme='gray' mr={mr}>
          {publishOption}
        </Badge>
      )
  }
}

export default PublishOptionBadge
