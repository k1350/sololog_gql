'use client'

import { Button, Text } from '@chakra-ui/react'
import Link, { type LinkProps } from 'next/link'

type NextLinkProps = LinkProps & {
  children: React.ReactNode
  leftIcon?: React.ReactElement<any, string | React.JSXElementConstructor<any>>
  outline?: boolean
}

const NextLinkButton = ({ href, children, leftIcon, outline, ...otherProps }: NextLinkProps) => {
  return (
    <Link href={href} {...otherProps}>
      {outline ? (
        <Button colorScheme='blue' variant='outline' leftIcon={leftIcon}>
          <Text>{children}</Text>
        </Button>
      ) : (
        <Button colorScheme='blue' leftIcon={leftIcon}>
          <Text>{children}</Text>
        </Button>
      )}
    </Link>
  )
}

export default NextLinkButton
