'use client'

import Link, { type LinkProps } from 'next/link'
import { LinkText } from '@/components/ui/LinkText'

type NextLinkProps = LinkProps & {
  children: React.ReactNode
  isExternal?: boolean
}

const NextLink = ({ href, children, isExternal, ...otherProps }: NextLinkProps) => {
  return (
    <Link href={href} {...otherProps}>
      <LinkText isExternal={isExternal}>{children}</LinkText>
    </Link>
  )
}

export default NextLink
