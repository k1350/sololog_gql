'use client'

import { createContext, useContext } from 'react'
import useCurrentUserBase, { type UserType } from '@/hooks/useCurrentUser'
import useRequireLoginBase from '@/hooks/useRequireLogin'

export type CurrentUserContextType = {
  useUser(): {
    user: UserType
    isAuthChecking: boolean
    role?: string
  }
  useRequireLogin(): {
    user: UserType
    isAuthChecking: boolean
  }
}

export const currentUserContextDefaultValue = {
  useUser: useCurrentUserBase,
  useRequireLogin: useRequireLoginBase,
}

export const CurrentUserContext = createContext<CurrentUserContextType>(
  currentUserContextDefaultValue,
)

export const useCurrentUser = () => useContext(CurrentUserContext).useUser()
export const useRequireLogin = () => useContext(CurrentUserContext).useRequireLogin()
