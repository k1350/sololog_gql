'use client'

import {
  getAuth,
  signOut as signOutBase,
  type NextOrObserver,
  type User,
  type ErrorFn,
  type CompleteFn,
  type Unsubscribe,
  onAuthStateChanged as onAuthStateChangedBase,
  signInWithRedirect as signInWithRedirectBase,
  type PopupRedirectResolver,
  type UserCredential,
  getRedirectResult as getRedirectResultBase,
  type OAuthCredential,
  GoogleAuthProvider,
  type AuthCredential,
  reauthenticateWithCredential as reauthenticateWithCredentialBase,
} from 'firebase/auth'
import { createContext } from 'react'
import { app, provider } from '@/config'

export type AuthContextType = {
  signOut(): Promise<void>
  onAuthStateChanged(
    nextOrObserver: NextOrObserver<User>,
    error?: ErrorFn,
    completed?: CompleteFn,
  ): Unsubscribe
  signInWithRedirect(resolver?: PopupRedirectResolver): Promise<never>
  getRedirectResult(resolver?: PopupRedirectResolver): Promise<UserCredential | null>
  credentialFromResult(userCredential: UserCredential): OAuthCredential | null
  reauthenticateWithCredential(user: User, credential: AuthCredential): Promise<UserCredential>
}

export const authContextDefaultValue = {
  signOut: async () => {
    const auth = getAuth(app)
    return signOutBase(auth)
  },
  onAuthStateChanged: (
    nextOrObserver: NextOrObserver<User>,
    error?: ErrorFn,
    completed?: CompleteFn,
  ) => {
    const auth = getAuth(app)
    return onAuthStateChangedBase(auth, nextOrObserver, error, completed)
  },
  signInWithRedirect: (resolver?: PopupRedirectResolver) => {
    const auth = getAuth(app)
    return signInWithRedirectBase(auth, provider, resolver)
  },
  getRedirectResult: (resolver?: PopupRedirectResolver) => {
    const auth = getAuth(app)
    return getRedirectResultBase(auth, resolver)
  },
  credentialFromResult: GoogleAuthProvider.credentialFromResult,
  reauthenticateWithCredential: reauthenticateWithCredentialBase,
}

export const AuthContext = createContext<AuthContextType>(authContextDefaultValue)
