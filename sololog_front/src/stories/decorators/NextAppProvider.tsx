import type { DecoratorFunction } from '@storybook/csf'
import type { ReactRenderer } from '@storybook/react'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { type AuthCredential, OperationType, type User, type UserCredential } from 'firebase/auth'
import { sampleFirebaseUserData } from '../data/firebase'
import { CONSTANT } from '@/constants'
import { Layout } from '@/layout/Layout'
import { ViewLayout } from '@/layout/ViewLayout'
import { AuthContext } from '@/lib/auth/authContext'
import { CurrentUserContext } from '@/lib/auth/currentUserContext'

const authContextValue = {
  signOut: async () => {},
  onAuthStateChanged: () => {
    return () => {}
  },
  signInWithRedirect: () => {
    throw new Error()
  },
  getRedirectResult: async () => {
    return null
  },
  credentialFromResult: (_: UserCredential) => {
    return null
  },
  reauthenticateWithCredential: async (_u: User, _a: AuthCredential) => {
    return {
      user: sampleFirebaseUserData,
      providerId: 'dummy',
      operationType: OperationType.REAUTHENTICATE,
    }
  },
}

export const UserContextProviderFactory = (
  login = true,
  user?: User,
  role?: string,
): DecoratorFunction<ReactRenderer, any> => {
  const queryClient = new QueryClient()
  const userContextValue = {
    useUser: () => {
      return login
        ? {
            user: user ?? sampleFirebaseUserData,
            isAuthChecking: false,
            role: role ?? CONSTANT.ROLE.ADMIN,
          }
        : {
            user: null,
            isAuthChecking: false,
            role: undefined,
          }
    },
    useRequireLogin: () => {
      return login
        ? {
            user: user ?? sampleFirebaseUserData,
            isAuthChecking: false,
          }
        : {
            user: null,
            isAuthChecking: false,
          }
    },
  }
  const Decorator: DecoratorFunction<ReactRenderer, any> = (Story) => (
    <QueryClientProvider client={queryClient}>
      <AuthContext.Provider value={authContextValue}>
        <CurrentUserContext.Provider value={userContextValue}>
          <Story />
        </CurrentUserContext.Provider>
      </AuthContext.Provider>
    </QueryClientProvider>
  )
  return Decorator
}

export const LayoutDecorator: DecoratorFunction<ReactRenderer, any> = (Story) => (
  <Layout>
    <Story />
  </Layout>
)

export const ViewLayoutDecorator: DecoratorFunction<ReactRenderer, any> = (Story) => (
  <ViewLayout>
    <Story />
  </ViewLayout>
)

const NextAppProvider: DecoratorFunction<ReactRenderer, any> = (Story, StoryContext) =>
  UserContextProviderFactory()(() => LayoutDecorator(Story, StoryContext), StoryContext)

export default NextAppProvider
