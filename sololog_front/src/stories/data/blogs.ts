import { Blog, PublishOption } from '@/graph/types.generated'
import type { Blog as HomeBlog } from '@/types/pages/home'
import type { BlogData } from '@/types/pages/view'

export const sampleHomeBlogs: HomeBlog[] = [
  {
    id: 'b-1',
    blogKey: 'blog-1',
    name: 'ブログ1',
    description: 'ブログ1説明\n改行',
    publishOption: PublishOption.Public,
  },
  {
    id: 'b-2',
    blogKey: 'blog-2',
    name: 'ブログ2',
    description: null,
    publishOption: PublishOption.Password,
  },
  {
    id: 'b-3',
    blogKey: 'blog-3',
    name: 'ブログ3',
    description: 'ブログ3説明\n改行',
    publishOption: PublishOption.Private,
  },
]

export const sampleEditPublicBlog: Blog = {
  __typename: 'Blog',
  author: 'あお',
  blogKey: 'blog-1',
  createdAt: '2023-02-06T23:16:34Z',
  description: '説明\n改行',
  id: 'b-1',
  links: [
    {
      id: '01GRMZMW8RYY5VXV4HM2H31XDP',
      name: '技術ブログ',
      url: 'https://1.example.com',
    },
    {
      id: '01GRMZMW8RYY5VXV4HM495NKZH',
      name: '創作系個人サイト',
      url: 'https://2.example.com',
    },
  ],
  name: 'テストブログ',
  passwordHint: '',
  publishOption: PublishOption.Public,
  updatedAt: '2023-02-07T03:45:24Z',
}

export const sampleEditPasswordBlog: Blog = {
  ...sampleEditPublicBlog,
  passwordHint: 'test1234',
  publishOption: PublishOption.Password,
}

export const sampleViewPublicBlog: BlogData = {
  blogKey: 'blog-1',
  author: 'あお',
  name: '公開ブログ',
  description: '説明\n改行',
  publishOption: PublishOption.Public,
  passwordHint: '',
  links: [
    {
      id: '01GRMZMW8RYY5VXV4HM2H31XDP',
      name: '技術ブログ',
      url: 'https://1.example.com',
    },
    {
      id: '01GRMZMW8RYY5VXV4HM495NKZH',
      name: '創作系個人サイト',
      url: 'https://2.example.com',
    },
  ],
}

export const sampleViewPasswordBlog: BlogData = {
  blogKey: 'blog-1',
  publishOption: PublishOption.Password,
  passwordHint: 'パスワードのヒント',
}

export const sampleViewPrivateBlog: BlogData = {
  blogKey: 'blog-1',
  publishOption: PublishOption.Private,
  passwordHint: '',
}
