import { PublishOption } from '@/graph/types.generated'
import type { ArticleData, ArticleEdgeData } from '@/types/pages/view'

export const sampleArticleEdge: ArticleEdgeData = {
  __typename: 'ArticleEdge',
  cursor: 'art-1',
  node: {
    __typename: 'Article',
    id: 'a-1',
    articleKey: 'art-1',
    content: 'test',
    publishOption: PublishOption.Public,
    createdAt: '2023-02-07T03:45:24Z',
    updatedAt: '2023-02-07T03:45:24Z',
  },
}

export const articleConnection = {
  __typename: 'ArticleConnection',
  xJwtToken: null,
  pageInfo: {
    __typename: 'PageInfo',
    startCursor: 'art-1',
    endCursor: 'art-2',
    hasNextPage: false,
    hasPreviousPage: true,
  },
  edges: [
    sampleArticleEdge,
    {
      ...sampleArticleEdge,
      cursor: 'art-2',
      node: {
        ...sampleArticleEdge.node,
        id: 'a-2',
        articleKey: 'art-2',
        content: 'もあ https://example.com [[more]]テスト\n改行',
      },
    },
  ],
}

export const articlePublicData: ArticleData = {
  content: 'テスト https://example.com[[more]]改行\n改行',
  publishOption: PublishOption.Public,
  createdAt: '2023-02-07T03:45:24Z',
  updatedAt: '2023-02-07T03:45:24Z',
}

export const articlePasswordData: ArticleData = {
  publishOption: PublishOption.Password,
}
