import type { IdTokenResult, User } from 'firebase/auth'

export const sampleFirebaseUserData: User = {
  email: 'test@example.com',
  emailVerified: true,
  isAnonymous: false,
  metadata: {},
  providerData: [],
  refreshToken: '',
  tenantId: null,
  delete: function (): Promise<void> {
    throw new Error('Function not implemented.')
  },
  getIdToken: function (forceRefresh?: boolean | undefined): Promise<string> {
    throw new Error('Function not implemented.')
  },
  getIdTokenResult: function (forceRefresh?: boolean | undefined): Promise<IdTokenResult> {
    throw new Error('Function not implemented.')
  },
  reload: function (): Promise<void> {
    throw new Error('Function not implemented.')
  },
  toJSON: function (): object {
    throw new Error('Function not implemented.')
  },
  displayName: 'テストユーザー',
  phoneNumber: null,
  photoURL: null,
  providerId: 'google',
  uid: 'test',
}
