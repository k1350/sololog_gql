import type { Meta, StoryObj } from '@storybook/react'
import { LayoutDecorator, UserContextProviderFactory } from '../../decorators/NextAppProvider'
import LoginPageContent from '@/app/login/LoginPageContent'

const meta: Meta<typeof LoginPageContent> = {
  title: 'pages/login/index',
  component: LoginPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof LoginPageContent>

export const Default: Story = {
  args: {
    onClickLogin: () => {},
  },
  decorators: [LayoutDecorator, UserContextProviderFactory(false)],
}
