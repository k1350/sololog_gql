import type { Meta, StoryObj } from '@storybook/react'
import { UserContextProviderFactory } from '../../decorators/NextAppProvider'
import LoginLoadingPageContent from '@/app/login/loading/LoginLoadingPageContent'

const meta: Meta<typeof LoginLoadingPageContent> = {
  title: 'pages/login/loading',
  component: LoginLoadingPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof LoginLoadingPageContent>

export const Default: Story = {
  decorators: [UserContextProviderFactory(false)],
}
