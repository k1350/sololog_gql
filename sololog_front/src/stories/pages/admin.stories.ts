import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../decorators/NextAppProvider'
import AdminPageContent from '@/app/admin/AdminPageContent'

const meta: Meta<typeof AdminPageContent> = {
  title: 'pages/admin',
  component: AdminPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof AdminPageContent>

export const Default: Story = {
  args: {
    isLoading: false,
    isError: false,
    mutate: async () => {
      return {}
    },
    isUpdating: false,
    allowRegister: false,
  },
  decorators: [NextAppProvider],
}
