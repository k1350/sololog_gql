import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../../decorators/NextAppProvider'
import BlogCreatePageContent from '@/app/blogs/new/BlogCreatePageContent'

const meta: Meta<typeof BlogCreatePageContent> = {
  title: 'pages/blogs/new',
  component: BlogCreatePageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof BlogCreatePageContent>

export const Default: Story = {
  args: {
    isLoading: false,
    isError: false,
    canShowForm: true,
    maxBlogLinks: 3,
    errorMessage: undefined,
  },
  decorators: [NextAppProvider],
}
