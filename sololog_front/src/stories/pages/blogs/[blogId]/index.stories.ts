import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../../../decorators/NextAppProvider'
import BlogEditPageContent from '@/app/blogs/[blogId]/BlogEditPageContent'
import { sampleEditPasswordBlog, sampleEditPublicBlog } from '@/stories/data/blogs'

const meta: Meta<typeof BlogEditPageContent> = {
  title: 'pages/blogs/[blogId]/index',
  component: BlogEditPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof BlogEditPageContent>

export const Default: Story = {
  args: {
    blogId: 'b-1',
    isLoading: false,
    isError: false,
    blog: sampleEditPublicBlog,
    maxBlogLinks: 3,
    errorMessage: undefined,
  },
  decorators: [NextAppProvider],
}

export const Password: Story = {
  args: {
    blogId: 'b-1',
    isLoading: false,
    isError: false,
    blog: sampleEditPasswordBlog,
    maxBlogLinks: 3,
    errorMessage: undefined,
  },
  decorators: [NextAppProvider],
}
