import type { Meta, StoryObj } from '@storybook/react'
import { sampleHomeBlogs } from '../data/blogs'
import NextAppProvider from '../decorators/NextAppProvider'
import HomePageContent from '@/app/home/HomePageContent'

const meta: Meta<typeof HomePageContent> = {
  title: 'pages/home',
  component: HomePageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof HomePageContent>

export const Default: Story = {
  args: {
    isError: false,
    isMaxBlogsError: false,
    blogs: sampleHomeBlogs,
    maxBlogs: 5,
    errorMessage: undefined,
    maxBlogsErrorMessage: undefined,
  },
  decorators: [NextAppProvider],
}
