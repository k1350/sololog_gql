import type { Meta, StoryObj } from '@storybook/react'
import { rest } from 'msw'
import {
  UserContextProviderFactory,
  ViewLayoutDecorator,
} from '../../../decorators/NextAppProvider'
import BlogViewPageContent from '@/app/view/[blogKey]/BlogViewPageContent'
import { CONSTANT } from '@/constants'
import { articleConnection } from '@/stories/data/articles'
import {
  sampleViewPasswordBlog,
  sampleViewPrivateBlog,
  sampleViewPublicBlog,
} from '@/stories/data/blogs'

const meta: Meta<typeof BlogViewPageContent> = {
  title: 'pages/view/[blogKey]/index',
  component: BlogViewPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof BlogViewPageContent>

export const Default: Story = {
  args: {
    blogKey: 'blog-1',
    blogByBlogKey: sampleViewPublicBlog,
    jwt: null,
    blogId: undefined,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                articles: articleConnection,
              },
            }),
          )
        }),
      ],
    },
  },
}

export const PasswordInput: Story = {
  args: {
    blogKey: 'blog-1',
    blogByBlogKey: sampleViewPasswordBlog,
    jwt: null,
    blogId: undefined,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
}

export const PasswordInputError: Story = {
  args: {
    blogKey: 'blog-1',
    blogByBlogKey: sampleViewPasswordBlog,
    jwt: 'test',
    blogId: undefined,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                blogByBlogKey: null,
              },
              errors: [
                { message: CONSTANT.SERVER_ERROR_MESSAGE.FORBIDDEN, path: ['blogByBlogKey'] },
              ],
            }),
          )
        }),
      ],
    },
  },
}

export const PasswordInputReauthenticate: Story = {
  args: {
    blogKey: 'blog-1',
    blogByBlogKey: sampleViewPasswordBlog,
    jwt: 'test',
    blogId: undefined,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                blogByBlogKey: null,
              },
              errors: [
                {
                  message: CONSTANT.SERVER_ERROR_MESSAGE.BLOG_AUTHENTICATION_EXPIRED,
                  path: ['blogByBlogKey'],
                },
              ],
            }),
          )
        }),
      ],
    },
  },
}

export const Author: Story = {
  args: {
    blogKey: 'blog-1',
    blogByBlogKey: sampleViewPublicBlog,
    jwt: null,
    blogId: 'b-1',
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(true)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                articles: articleConnection,
                isAuthor: true,
              },
            }),
          )
        }),
      ],
    },
  },
}
