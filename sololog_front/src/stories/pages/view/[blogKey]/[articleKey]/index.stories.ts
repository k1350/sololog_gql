import type { Meta, StoryObj } from '@storybook/react'
import { rest } from 'msw'
import {
  UserContextProviderFactory,
  ViewLayoutDecorator,
} from '../../../../decorators/NextAppProvider'
import ArticleViewPageContent from '@/app/view/[blogKey]/[articleKey]/ArticleViewPageContent'
import { CONSTANT } from '@/constants'
import { articlePasswordData, articlePublicData } from '@/stories/data/articles'

const meta: Meta<typeof ArticleViewPageContent> = {
  title: 'pages/view/[blogKey]/[articleKey]/index',
  component: ArticleViewPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof ArticleViewPageContent>

export const Default: Story = {
  args: {
    blogKey: 'blog-1',
    articleKey: 'art-1',
    articleByArticleKey: articlePublicData,
    jwt: null,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
}

export const PasswordInput: Story = {
  args: {
    blogKey: 'blog-1',
    articleKey: 'art-1',
    articleByArticleKey: articlePasswordData,
    jwt: null,
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
}

export const PasswordInputError: Story = {
  args: {
    blogKey: 'blog-1',
    articleKey: 'art-1',
    articleByArticleKey: articlePasswordData,
    jwt: 'test',
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                articleByArticleKey: null,
              },
              errors: [
                { message: CONSTANT.SERVER_ERROR_MESSAGE.FORBIDDEN, path: ['blogByBlogKey'] },
              ],
            }),
          )
        }),
      ],
    },
  },
}

export const PasswordInputReauthenticate: Story = {
  args: {
    blogKey: 'blog-1',
    articleKey: 'art-1',
    articleByArticleKey: articlePasswordData,
    jwt: 'test',
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                articleByArticleKey: null,
              },
              errors: [
                {
                  message: CONSTANT.SERVER_ERROR_MESSAGE.BLOG_AUTHENTICATION_EXPIRED,
                  path: ['articleByArticleKey'],
                },
              ],
            }),
          )
        }),
      ],
    },
  },
}

export const NotFound: Story = {
  args: {
    blogKey: 'blog-1',
    articleKey: 'art-1',
    articleByArticleKey: articlePasswordData,
    jwt: 'test',
  },
  decorators: [ViewLayoutDecorator, UserContextProviderFactory(false)],
  parameters: {
    msw: {
      handlers: [
        rest.post(`${process.env.NEXT_PUBLIC_API_BASE_URL}`, (_, res, ctx) => {
          return res(
            ctx.json({
              data: {
                articleByArticleKey: null,
              },
              errors: [{ message: '', path: ['articleByArticleKey'] }],
            }),
          )
        }),
      ],
    },
  },
}
