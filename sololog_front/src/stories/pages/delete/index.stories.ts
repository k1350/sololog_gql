import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../../decorators/NextAppProvider'
import DeletePageContent from '@/app/delete/DeletePageContent'

const meta: Meta<typeof DeletePageContent> = {
  title: 'pages/delete/index',
  component: DeletePageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof DeletePageContent>

export const Default: Story = {
  args: {
    onClickLogin: () => {},
  },
  decorators: [NextAppProvider],
}
