import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../../decorators/NextAppProvider'
import ReauthenticatePageContent from '@/app/delete/loading/ReauthenticatePageContent'

const meta: Meta<typeof ReauthenticatePageContent> = {
  title: 'pages/delete/loading',
  component: ReauthenticatePageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof ReauthenticatePageContent>

export const Default: Story = {
  decorators: [NextAppProvider],
}
