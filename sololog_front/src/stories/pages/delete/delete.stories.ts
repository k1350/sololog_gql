import type { Meta, StoryObj } from '@storybook/react'
import NextAppProvider from '../../decorators/NextAppProvider'
import DeleteUserPageContent from '@/app/delete/delete/DeleteUserPageContent'

const meta: Meta<typeof DeleteUserPageContent> = {
  title: 'pages/delete/delete',
  component: DeleteUserPageContent,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof DeleteUserPageContent>

export const Default: Story = {
  args: {
    onClickDelete: () => {},
    isLoading: false,
  },
  decorators: [NextAppProvider],
}
