import type { Meta, StoryObj } from '@storybook/react'
import NotFound from '@/app/not-found'

const meta: Meta<typeof NotFound> = {
  title: 'pages/not-found',
  component: NotFound,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}

export default meta
type Story = StoryObj<typeof NotFound>

export const Default: Story = {}
