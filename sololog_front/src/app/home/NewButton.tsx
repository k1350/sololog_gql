'use client'

import { PlusSquareIcon } from '@chakra-ui/icons'
import { Button, Flex, Text } from '@chakra-ui/react'
import NextLinkButton from '@/components/ui/NextLinkButton'

type NewButtonProps = {
  dataLength: number
  maxBlogs: number
}

export default function NewButton({ dataLength, maxBlogs }: NewButtonProps) {
  if (maxBlogs > dataLength) {
    return (
      <Flex>
        <NextLinkButton href={`/blogs/new`} leftIcon={<PlusSquareIcon />}>
          新規作成
        </NextLinkButton>
      </Flex>
    )
  }

  return (
    <Flex>
      <Button colorScheme='blue' leftIcon={<PlusSquareIcon />} as='a' isDisabled>
        <Text>新規作成</Text>
      </Button>
      <Text ml={2} fontSize='sm'>
        作成可能なブログは {maxBlogs} 件までです。
      </Text>
    </Flex>
  )
}
