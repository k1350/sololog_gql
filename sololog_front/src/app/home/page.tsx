'use client'

import { Spinner } from '@chakra-ui/react'
import HomePageContent from './HomePageContent'
import { useHomeQuery } from '@/graph/home.generated'
import { useMaxBlogsQuery } from '@/graph/maxBlogs.generated'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

export default function HomePage() {
  const { isAuthChecking, user: currentUser } = useRequireLogin()
  const { data, isLoading, isError, error } = useHomeQuery()
  const {
    data: maxBlogsData,
    isLoading: isMaxBlogsLoading,
    isError: isMaxBlogsError,
    error: maxBlogsError,
  } = useMaxBlogsQuery(undefined, {
    staleTime: Infinity,
  })

  if (isAuthChecking || !currentUser || isLoading || isMaxBlogsLoading) {
    return <Spinner size='xl' />
  }

  return (
    <HomePageContent
      isError={isError}
      isMaxBlogsError={isMaxBlogsError}
      blogs={data?.blogs}
      maxBlogs={maxBlogsData?.masterParameter?.maxBlogs}
      errorMessage={error?.message}
      maxBlogsErrorMessage={maxBlogsError?.message}
    />
  )
}
