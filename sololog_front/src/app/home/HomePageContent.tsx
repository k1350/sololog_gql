'use client'

import { Heading, Stack, useColorModeValue, Box, SimpleGrid } from '@chakra-ui/react'
import NewButton from '@/app/home/NewButton'
import NextLink from '@/components/ui/NextLink'
import PublishOptionBadge from '@/components/ui/PublishOptionBadge'
import theme from '@/theme'
import type { Blog } from '@/types/pages/home'

type HomePageContentProps = {
  isError: boolean
  isMaxBlogsError: boolean
  blogs?: Blog[]
  maxBlogs?: number
  errorMessage?: string
  maxBlogsErrorMessage?: string
}

export default function HomePageContent({
  isError,
  isMaxBlogsError,
  blogs,
  maxBlogs,
  errorMessage,
  maxBlogsErrorMessage,
}: HomePageContentProps) {
  return (
    <>
      <Heading as='h2' mt={4} mb={4}>
        ブログ一覧
      </Heading>
      <div>
        {isError || blogs === undefined ? (
          <span>エラーが発生しました： {errorMessage ?? ''}</span>
        ) : isMaxBlogsError || maxBlogs === undefined ? (
          <span>エラーが発生しました： {maxBlogsErrorMessage ?? ''}</span>
        ) : (
          <Stack spacing={4} direction='column'>
            <NewButton dataLength={blogs.length} maxBlogs={maxBlogs} />
            <BlogList data={blogs} />
          </Stack>
        )}
      </div>
    </>
  )
}

type BlogListProps = {
  data: Blog[]
}

function BlogList({ data }: BlogListProps) {
  const descriptionColor = useColorModeValue(
    theme.colors.mutedTextColor.lightMode,
    theme.colors.mutedTextColor.darkMode,
  )

  return (
    <SimpleGrid minChildWidth='300px' spacing='10px'>
      {data.map((blog) => (
        <Box key={blog.id} maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' p={5}>
          <PublishOptionBadge publishOption={blog.publishOption} />
          <Box mt='1' fontWeight='semibold' as='h3' fontSize='3xl'>
            <NextLink href={`/blogs/${blog.id}`}>{blog.name ?? ''}</NextLink>
          </Box>
          <Box color={descriptionColor} noOfLines={[1, 2, 3]}>
            {blog?.description}
          </Box>
          <Box mt='2'>
            <NextLink
              href={`${process.env.NEXT_PUBLIC_FRONT_URL_BASE as string}view/${blog.blogKey}`}
              isExternal={true}
            >
              公開ページを見る
            </NextLink>
          </Box>
        </Box>
      ))}
    </SimpleGrid>
  )
}
