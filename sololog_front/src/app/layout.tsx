import { Providers } from './providers'

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang='ja'>
      <head>
        <meta name='robots' content='noindex' />
        <link rel='shortcut icon' href='/favicon.svg' />
      </head>
      <body>
        <Providers>{children}</Providers>
      </body>
    </html>
  )
}
