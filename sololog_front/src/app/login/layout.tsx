import type { Metadata } from 'next'
import { Layout as BasicLayout } from '@/layout/Layout'

export const metadata: Metadata = {
  title: 'ログイン | ソロログ',
}

export default function Layout({ children }: { children: React.ReactElement }) {
  return <BasicLayout>{children}</BasicLayout>
}
