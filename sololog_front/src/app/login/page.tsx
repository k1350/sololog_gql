'use client'

import { Spinner } from '@chakra-ui/react'
import { useQueryClient } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { useEffect } from 'react'
import LoginPageContent from './LoginPageContent'
import { useCurrentUser } from '@/lib/auth/currentUserContext'

export default function LoginPage() {
  const { isAuthChecking, user: currentUser } = useCurrentUser()
  const router = useRouter()
  const queryClient = useQueryClient()

  useEffect(() => {
    // クエリの全キャッシュを破棄する
    queryClient.resetQueries()
  }, [queryClient])

  useEffect(() => {
    // ログイン済だったらホームに飛ぶ
    if (currentUser) {
      router.push('/home')
    }
  }, [currentUser, router])

  if (isAuthChecking || currentUser) {
    return <Spinner size='xl' />
  }

  return <LoginPageContent onClickLogin={() => router.push('/login/loading')} />
}
