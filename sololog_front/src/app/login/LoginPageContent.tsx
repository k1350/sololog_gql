'use client'

import { Heading } from '@chakra-ui/react'
import styles from '@/app/login/page.module.scss'

type LoginPageContentProps = {
  onClickLogin: () => void
}
export default function LoginPageContent({ onClickLogin }: LoginPageContentProps) {
  return (
    <>
      <Heading as='h1' size='4xl' mt={2} mb={2}>
        ログイン
      </Heading>
      <button onClick={onClickLogin} className={styles.login}>
        <img
          src='/assets/images/btn_google_signin_light_normal_web@2x.png'
          alt='Google Login Button'
          width='382'
          height='92'
        />
      </button>
    </>
  )
}
