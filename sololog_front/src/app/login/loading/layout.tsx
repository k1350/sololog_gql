import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'ログイン | ソロログ',
}

export default function Layout({ children }: { children: React.ReactElement }) {
  return children
}
