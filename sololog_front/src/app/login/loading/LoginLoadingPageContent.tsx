'use client'

import { Spinner } from '@chakra-ui/react'

export default function LoginLoadingPageContent() {
  return (
    <>
      <Spinner size='xl' />
    </>
  )
}
