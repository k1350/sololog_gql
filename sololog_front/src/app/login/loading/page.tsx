'use client'

import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/navigation'
import { useEffect, useState, useCallback, useContext } from 'react'
import LoginLoadingPageContent from './LoginLoadingPageContent'
import { CONSTANT } from '@/constants'
import { useLoginMutation } from '@/graph/guest/login.generated'
import { LoginInput } from '@/graph/types.generated'
import { AuthContext } from '@/lib/auth/authContext'
import { useCurrentUser } from '@/lib/auth/currentUserContext'

export default function LoginLoadingPage() {
  const authContext = useContext(AuthContext)
  const { isAuthChecking, user: currentUser } = useCurrentUser()
  const [isError, setIsError] = useState(false)
  const router = useRouter()
  const toast = useToast()

  const { mutateAsync } = useLoginMutation({
    onError: async (e: any) => {
      console.error(e)
      setIsError(true)
      const isNotAllowedRegister = e.message === CONSTANT.SERVER_ERROR_MESSAGE.REGISTER_NOT_ALLOWED
      const msg = isNotAllowedRegister
        ? '現在、新規登録を受け付けていません'
        : 'ログインに失敗しました'
      toast({
        title: msg,
        status: 'error',
        isClosable: true,
      })
      await authContext.signOut().catch((error) => {
        alert(`ログアウトに失敗しました。エラーコード：${error.code}`)
      })
      router.push('/login')
    },
    onSuccess: () => {
      const unsubscribe = authContext.onAuthStateChanged(async (user) => {
        if (user) {
          // カスタムクレームを更新するためにユーザーを読み込みなおしてからトークンリフレッシュ
          await user.reload()
          await user.getIdTokenResult(true)
          router.push('/home')
        } else {
          setIsError(true)
          alert('ログインに失敗しました')
          router.push('/login')
        }
      })
      unsubscribe()
    },
  })

  const mutateLogin = useCallback(async () => {
    const input: LoginInput = {
      token: (await currentUser?.getIdToken()) ?? '',
    }
    await mutateAsync({
      input: input,
    })
  }, [mutateAsync, currentUser])

  useEffect(() => {
    if (isAuthChecking) {
      return
    }
    const login = async () => {
      const result = await authContext.getRedirectResult()
      if (!result?.user) {
        if (!currentUser && !isError) {
          await authContext.signInWithRedirect()
        }
      } else {
        if (!isError) {
          mutateLogin()
        }
      }
    }
    login()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthChecking, currentUser, isError, mutateLogin])

  return <LoginLoadingPageContent />
}
