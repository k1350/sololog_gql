'use client'

import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons'
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Text,
  Grid,
  Button,
  Input,
  FormHelperText,
  Flex,
  InputGroup,
  InputRightElement,
  Checkbox,
  useColorModeValue,
} from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import type { FieldError, FieldErrors, Merge, UseFormRegisterReturn } from 'react-hook-form'
import theme from '@/theme'

type PasswordInputProps = {
  hasPassword: boolean
  showPasswordInput: boolean
  isLargerThan600: boolean
  gridTemplate: string
  register: UseFormRegisterReturn
  errors: FieldError | Merge<FieldError, FieldErrors<any>> | undefined
  unregister?: () => void
  clearError?: () => void
}

export default function PasswordInput({
  hasPassword,
  showPasswordInput,
  isLargerThan600,
  gridTemplate,
  register,
  errors,
  unregister,
  clearError,
}: PasswordInputProps) {
  const [showPassword, setShowPassword] = useState(false)
  const handleClick = () => setShowPassword(!showPassword)
  const [hidePassword, setHidePassword] = useState(true)
  const handleHidePasswordCheck = (value: boolean) => {
    if (value) {
      if (unregister !== undefined) {
        unregister()
      }
      if (clearError !== undefined) {
        clearError()
      }
    }
    setHidePassword(value)
  }

  useEffect(() => {
    if (!hasPassword) {
      setHidePassword(false)
    }
  }, [hasPassword])
  const alertColor = useColorModeValue(
    theme.colors.alertColor.lightMode,
    theme.colors.alertColor.darkMode,
  )

  if (showPasswordInput) {
    return (
      <Grid templateColumns={isLargerThan600 ? gridTemplate : '1fr'} gap={1} mb={4}>
        {hasPassword ? (
          <>
            <span></span>
            <Checkbox
              defaultChecked
              onChange={(event) => handleHidePasswordCheck(event.target.checked)}
            >
              現在のパスワードと同じ
            </Checkbox>
          </>
        ) : (
          <></>
        )}
        {hidePassword ? (
          <></>
        ) : (
          <>
            <span></span>
            <FormControl isInvalid={Boolean(errors)} mb={2}>
              <FormLabel htmlFor='password'>
                <Flex>
                  <Text fontWeight='bold'>パスワード</Text>
                  <Text color={alertColor}>*</Text>
                </Flex>
              </FormLabel>
              <InputGroup size='md'>
                <Input
                  id='password'
                  pr='4.5rem'
                  type={showPassword ? 'text' : 'password'}
                  {...register}
                />
                <InputRightElement width='4.5rem'>
                  <Button h='1.75rem' size='sm' onClick={handleClick}>
                    {showPassword ? <ViewOffIcon /> : <ViewIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
              <FormErrorMessage>{errors && errors.message?.toString()}</FormErrorMessage>
              <FormHelperText ml={4}>
                <ul>
                  <li>1 文字以上 255 文字以下</li>
                  <li>半角英数字と記号が使用できます</li>
                  <li>使用できる記号は!@#%\^&\*</li>
                </ul>
              </FormHelperText>
            </FormControl>
          </>
        )}
      </Grid>
    )
  }
  return (
    <>
      <span></span>
      <FormControl mb={2}></FormControl>
    </>
  )
}
