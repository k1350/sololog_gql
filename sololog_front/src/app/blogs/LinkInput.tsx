'use client'

import { FormErrorMessage, FormLabel, FormControl, Text, Input } from '@chakra-ui/react'
import {
  type FieldError,
  type FieldErrors,
  type Merge,
  UseFormRegisterReturn,
} from 'react-hook-form'

type LinkInputProps = {
  id: string
  label: string
  register: UseFormRegisterReturn
  errors: FieldError | Merge<FieldError, FieldErrors<any>> | undefined
}

export default function LinkInput({ id, label, register, errors }: LinkInputProps) {
  return (
    <FormControl isInvalid={Boolean(errors)} mb={2}>
      <FormLabel htmlFor={id} mr={2}>
        <Text fontWeight='bold'>{label}</Text>
      </FormLabel>
      <Input id={id} {...register}></Input>
      {errors && errors.message ? (
        <FormErrorMessage>{errors.message.toString()}</FormErrorMessage>
      ) : (
        <></>
      )}
    </FormControl>
  )
}
