'use client'

import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Text,
  Input,
  Grid,
  Flex,
  FormHelperText,
  useColorModeValue,
} from '@chakra-ui/react'
import type { FieldError, FieldErrors, Merge, UseFormRegisterReturn } from 'react-hook-form'
import theme from '@/theme'

type TextFieldInputProps = {
  id: string
  label: string
  helperText: string
  isLargerThan600: boolean
  gridTemplate: string
  register: UseFormRegisterReturn
  errors: FieldError | Merge<FieldError, FieldErrors<any>> | undefined
  isRequired?: boolean
}

export default function TextFieldInput({
  id,
  label,
  helperText,
  isLargerThan600,
  gridTemplate,
  register,
  errors,
  isRequired,
}: TextFieldInputProps) {
  const alertColor = useColorModeValue(
    theme.colors.alertColor.lightMode,
    theme.colors.alertColor.darkMode,
  )
  return (
    <FormControl isInvalid={Boolean(errors)} mb={4}>
      <Grid templateColumns={isLargerThan600 ? gridTemplate : '1fr'} gap={1} mb={2}>
        <FormLabel htmlFor={id}>
          <Flex>
            <Text fontWeight='bold'>{label}</Text>
            {isRequired && <Text color={alertColor}>*</Text>}
          </Flex>
        </FormLabel>
        <Input id={id} {...register} />
        {errors ? (
          <>
            <span></span>
            <FormErrorMessage>{errors.message?.toString()}</FormErrorMessage>
          </>
        ) : (
          <>
            <span></span>
            <FormHelperText>{helperText}</FormHelperText>
          </>
        )}
      </Grid>
    </FormControl>
  )
}
