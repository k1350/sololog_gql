'use client'

import { CheckIcon, CloseIcon } from '@chakra-ui/icons'
import { Button, ButtonGroup, Center } from '@chakra-ui/react'
import NextLinkButton from '@/components/ui/NextLinkButton'

type SaveCancelButtonGroupProps = {
  isLoading: boolean
  cancelLinkTo: string
}

export default function SaveCancelButtonGroup({
  isLoading,
  cancelLinkTo,
}: SaveCancelButtonGroupProps) {
  return (
    <Center mt={5}>
      <ButtonGroup justifyContent='center' size='sm'>
        <Button type='submit' colorScheme='blue' isLoading={isLoading} leftIcon={<CheckIcon />}>
          保存する
        </Button>
        <NextLinkButton href={cancelLinkTo} outline={true} leftIcon={<CloseIcon />}>
          キャンセル
        </NextLinkButton>
      </ButtonGroup>
    </Center>
  )
}
