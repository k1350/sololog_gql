'use client'

import { CloseIcon, DeleteIcon } from '@chakra-ui/icons'
import {
  Alert,
  AlertDescription,
  Button,
  FormControl,
  FormLabel,
  IconButton,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  AlertIcon,
  useToast,
} from '@chakra-ui/react'
import { useRouter } from 'next/navigation'
import { useRef, useState } from 'react'
import {
  type DeleteBlogMutationVariables,
  useDeleteBlogMutation,
} from '@/graph/deleteBlog.generated'

type DeleteButtonProps = {
  blogId: string
}

export default function DeleteButton({ blogId }: DeleteButtonProps) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [text, setText] = useState('')
  const [isDisabledDelete, setIsDisabledDelete] = useState(true)
  const initialRef = useRef(null)

  const router = useRouter()
  const toast = useToast()
  const { mutateAsync, isLoading } = useDeleteBlogMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '削除に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '削除しました',
        status: 'success',
        isClosable: true,
      })
      router.push('/home')
    },
  })

  const mutate = async (variables: DeleteBlogMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  const onChangeInput = (value: string) => {
    setText(value)
    if (value === 'delete') {
      setIsDisabledDelete(false)
    } else {
      setIsDisabledDelete(true)
    }
  }

  const onClickDelete = () => {
    mutate({ input: blogId })
  }

  return (
    <>
      <IconButton
        colorScheme='red'
        aria-label={'delete blog'}
        icon={<DeleteIcon />}
        onClick={onOpen}
      />
      <Modal isOpen={isOpen} onClose={onClose} initialFocusRef={initialRef}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>ブログ削除</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <Alert status='error' mb={2}>
              <AlertIcon />
              <AlertDescription>
                ブログを削除するとすべての投稿が失われ、元に戻すことはできません。
              </AlertDescription>
            </Alert>
            <FormControl>
              <FormLabel>
                ブログを削除する場合は &quot;delete&quot;
                と入力してから「削除する」ボタンを押してください。
              </FormLabel>
              <Input
                id='confirm-delete'
                value={text}
                onChange={(event) => onChangeInput(event.target.value)}
                ref={initialRef}
                placeholder='delete'
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme='red'
              mr={3}
              leftIcon={<DeleteIcon />}
              onClick={onClickDelete}
              isDisabled={isDisabledDelete || isLoading}
              isLoading={isLoading}
            >
              削除する
            </Button>
            <Button colorScheme='blue' variant='outline' onClick={onClose} leftIcon={<CloseIcon />}>
              キャンセル
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}
