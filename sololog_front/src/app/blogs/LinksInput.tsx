'use client'

import { FormLabel, FormControl, Text, Grid, FormHelperText, Box } from '@chakra-ui/react'

type LinksInputProps = {
  isLargerThan600: boolean
  gridTemplate: string
  linksRender: JSX.Element[]
}

export default function LinksInput({
  isLargerThan600,
  gridTemplate,
  linksRender,
}: LinksInputProps) {
  return (
    <FormControl mb={4}>
      <Grid templateColumns={isLargerThan600 ? gridTemplate : '1fr'} gap={1} mb={2}>
        <FormLabel>
          <Text fontWeight='bold'>リンク</Text>
        </FormLabel>
        <Box>{linksRender}</Box>
        <span></span>
        <FormHelperText ml={4}>
          <ul>
            <li>リンクタイトルは最大 50 文字、URL は最大 3000 文字</li>
            <li>リンクタイトルを入力した場合は URL は必須入力です</li>
          </ul>
        </FormHelperText>
      </Grid>
    </FormControl>
  )
}
