'use client'

import { Spinner } from '@chakra-ui/react'
import { notFound, useRouter } from 'next/navigation'
import { useEffect, useState } from 'react'
import BlogCreatePageContent from './BlogCreatePageContent'
import { useMasterParameterQuery } from '@/graph/masterParameter.generated'
import { usePageBlogsNewQuery } from '@/graph/pageBlogsNew.generated'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

export default function BlogCreatePage() {
  const { isAuthChecking, user: currentUser } = useRequireLogin()
  const { data, isLoading: isDataLoading, isError, error } = usePageBlogsNewQuery()
  const {
    data: masterParameterData,
    isLoading: isMasterParameterLoading,
    isError: isMasterParameterError,
    error: masterParameterError,
  } = useMasterParameterQuery(undefined, {
    staleTime: Infinity,
  })
  const [canShowForm, setCanShowForm] = useState(false)

  const router = useRouter()

  useEffect(() => {
    if (!data || !masterParameterData?.masterParameter?.maxBlogs || canShowForm) {
      return
    }
    // 最大数のブログを作成済ならホームに飛ばす
    if (data.blogs.length >= masterParameterData.masterParameter.maxBlogs) {
      router.push('/home')
    } else {
      setCanShowForm(true)
    }
  }, [data, masterParameterData, router, canShowForm])

  if (isAuthChecking || !currentUser || masterParameterData?.masterParameter === undefined) {
    return <Spinner size='xl' />
  }

  if (masterParameterData.masterParameter === null) {
    notFound()
  }

  return (
    <BlogCreatePageContent
      isLoading={isDataLoading || isMasterParameterLoading}
      isError={isError || isMasterParameterError}
      canShowForm={canShowForm}
      maxBlogLinks={masterParameterData.masterParameter.maxBlogLinks}
      errorMessage={error?.message ?? masterParameterError?.message}
    />
  )
}
