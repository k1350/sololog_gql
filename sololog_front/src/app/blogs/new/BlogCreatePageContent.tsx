'use client'

import {
  Heading,
  Spinner,
  useToast,
  Text,
  Divider,
  useMediaQuery,
  useColorModeValue,
} from '@chakra-ui/react'
import { useRouter } from 'next/navigation'
import { useState } from 'react'
import {
  FormProvider,
  useForm,
  type FieldError,
  type FieldErrorsImpl,
  type Merge,
  useFieldArray,
  useFormContext,
} from 'react-hook-form'
import LinkInput from '@/app/blogs/LinkInput'
import LinksInput from '@/app/blogs/LinksInput'
import PasswordInput from '@/app/blogs/PasswordInput'
import PublishOptionInput from '@/app/blogs/PublishOptionInput'
import SaveCancelButtonGroup from '@/app/blogs/SaveCancelButtonGroup'
import TextFieldInput from '@/app/blogs/TextFieldInput'
import TextareaInput from '@/app/blogs/TextareaInput'
import { CreateBlogMutationVariables, useCreateBlogMutation } from '@/graph/createBlog.generated'
import { type BlogLinkInput, type CreateBlogInput, PublishOption } from '@/graph/types.generated'
import type { Link } from '@/models/BlogCreate'
import theme from '@/theme'
import {
  AuthorValidator,
  NameValidator,
  DescriptionValidator,
  PublishOptionValidator,
  PasswordValidator,
  LinkNameValidator,
  LinkUrlValidator,
  PasswordHintValidator,
} from '@/validate/Blog'

type BlogCreatePageContentProps = {
  isLoading: boolean
  isError: boolean
  canShowForm: boolean
  maxBlogLinks: number
  errorMessage?: string
}

type CreateFormData = {
  author: string
  description: string
  links: Link[]
  name: string
  password: string
  passwordHint: string
  publishOption: PublishOption
}

export default function BlogCreatePageContent({
  isLoading,
  isError,
  canShowForm,
  maxBlogLinks,
  errorMessage,
}: BlogCreatePageContentProps) {
  const methods = useForm<CreateFormData>({
    defaultValues: {
      author: '',
      name: '',
      description: undefined,
      publishOption: PublishOption.Public,
      passwordHint: undefined,
      links: Array(maxBlogLinks)
        .fill(0)
        .map(() => ({
          id: '',
          name: '',
          url: '',
        })),
    },
  })
  const router = useRouter()
  const toast = useToast()
  const { mutateAsync, isLoading: isUpdating } = useCreateBlogMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '保存に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '保存しました',
        status: 'success',
        isClosable: true,
      })
      router.push('/home')
    },
  })

  const mutate = async (variables: CreateBlogMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  const onSubmit = async (formData: CreateFormData) => {
    const input: CreateBlogInput = {
      author: formData.author,
      name: formData.name,
      publishOption: formData.publishOption,
      links: [],
    }
    if (formData.description !== '') {
      input.description = formData.description
    }
    if (formData.publishOption === PublishOption.Password) {
      input.password = formData.password
      if (formData.passwordHint !== '') {
        input.passwordHint = formData.passwordHint
      }
    }
    formData.links.forEach((value: Link) => {
      if (value.url !== '') {
        const link: BlogLinkInput = {
          url: value.url,
        }
        if (value.name !== '') {
          link.name = value.name
        }
        input.links?.push(link)
      }
    })
    await mutate({ input })
  }

  return (
    <>
      <Heading as='h2' mt={4} mb={4}>
        ブログ作成
      </Heading>
      {isLoading || !canShowForm ? (
        <Spinner size='xl' />
      ) : isError ? (
        <span>Error: {errorMessage ?? ''}</span>
      ) : (
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <BlogCreateForm maxBlogLinks={maxBlogLinks} isUpdating={isUpdating} />
          </form>
        </FormProvider>
      )}
    </>
  )
}

type BlogCreateFormProps = {
  maxBlogLinks: number
  isUpdating: boolean
}

function BlogCreateForm({ maxBlogLinks, isUpdating }: BlogCreateFormProps) {
  const [isLargerThan600] = useMediaQuery('(min-width: 600px)')
  const gridTemplate = '7em 1fr'
  const {
    register,
    watch,
    formState: { errors },
  } = useFormContext()
  const { fields } = useFieldArray({
    name: 'links',
  })
  const [showPasswordInput, setShowPasswordInput] = useState(false)
  const alertColor = useColorModeValue(
    theme.colors.alertColor.lightMode,
    theme.colors.alertColor.darkMode,
  )

  const onChangePublishOption = (value: string) => {
    if (value === PublishOption.Password) {
      setShowPasswordInput(true)
    } else {
      setShowPasswordInput(false)
    }
  }

  return (
    <>
      <Text color={alertColor} mb={2}>
        * は必須入力
      </Text>
      <TextFieldInput
        id='author'
        label='著者名'
        helperText='最大 50 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('author', AuthorValidator)}
        errors={errors.author}
        isRequired={true}
      />
      <TextFieldInput
        id='name'
        label='ブログ名'
        helperText='最大 255 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('name', NameValidator)}
        errors={errors.name}
        isRequired={true}
      />
      <TextareaInput
        id='description'
        label='ブログ説明'
        helperText='最大 500 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('description', DescriptionValidator)}
        errors={errors.description}
      />
      <PublishOptionInput
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('publishOption', PublishOptionValidator)}
        errors={errors.publishOption}
        onChange={onChangePublishOption}
      />
      <PasswordInput
        hasPassword={false}
        showPasswordInput={showPasswordInput}
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('password', PasswordValidator(showPasswordInput))}
        errors={errors.password}
      />
      {showPasswordInput && (
        <TextFieldInput
          id='passwordHint'
          label='パスワードのヒント'
          helperText='最大 100 文字'
          isLargerThan600={isLargerThan600}
          gridTemplate={gridTemplate}
          register={register('passwordHint', PasswordHintValidator)}
          errors={errors.passwordHint}
        />
      )}
      <LinksInput
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        linksRender={fields.map((field, index) => (
          <div key={field.id}>
            <LinkInput
              id={`links.${index}.name`}
              label='リンクタイトル'
              register={register(`links.${index}.name`, LinkNameValidator)}
              errors={
                (
                  (errors.links as Merge<FieldError, FieldErrorsImpl<any>> | undefined)?.[
                    index
                  ] as Partial<
                    FieldErrorsImpl<{
                      [x: string]: any
                    }>
                  >
                )?.name
              }
            ></LinkInput>
            <LinkInput
              id={`links.${index}.url`}
              label='URL'
              register={register(
                `links.${index}.url`,
                LinkUrlValidator(watch(`links.${index}.name`)),
              )}
              errors={
                (
                  (errors.links as Merge<FieldError, FieldErrorsImpl<any>> | undefined)?.[
                    index
                  ] as Partial<
                    FieldErrorsImpl<{
                      [x: string]: any
                    }>
                  >
                )?.url
              }
            ></LinkInput>
            {index !== maxBlogLinks - 1 ? <Divider mb={2} /> : <></>}
          </div>
        ))}
      />
      <SaveCancelButtonGroup isLoading={isUpdating} cancelLinkTo={'/home'} />
    </>
  )
}
