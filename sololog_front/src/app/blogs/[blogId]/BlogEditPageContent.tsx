'use client'

import {
  Flex,
  Heading,
  Spinner,
  useToast,
  Text,
  Divider,
  useMediaQuery,
  useColorModeValue,
} from '@chakra-ui/react'
import { useQueryClient } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { useState, useEffect } from 'react'
import {
  FormProvider,
  useForm,
  type FieldError,
  type FieldErrorsImpl,
  type Merge,
  useFieldArray,
  useFormContext,
} from 'react-hook-form'
import DeleteButton from '@/app/blogs/DeleteButton'
import LinkInput from '@/app/blogs/LinkInput'
import LinksInput from '@/app/blogs/LinksInput'
import PasswordInput from '@/app/blogs/PasswordInput'
import PublishOptionInput from '@/app/blogs/PublishOptionInput'
import SaveCancelButtonGroup from '@/app/blogs/SaveCancelButtonGroup'
import TextFieldInput from '@/app/blogs/TextFieldInput'
import TextareaInput from '@/app/blogs/TextareaInput'
import { useGetBlogByIdQuery } from '@/graph/getBlogByID.generated'
import {
  type Blog,
  PublishOption,
  type UpdateBlogInput,
  type UpdateBlogLinkInput,
} from '@/graph/types.generated'
import { UpdateBlogMutationVariables, useUpdateBlogMutation } from '@/graph/updateBlog.generated'
import type { Link } from '@/models/BlogEdit'
import theme from '@/theme'
import {
  AuthorValidator,
  NameValidator,
  DescriptionValidator,
  PublishOptionValidator,
  PasswordValidator,
  LinkNameValidator,
  LinkUrlValidator,
  PasswordHintValidator,
} from '@/validate/Blog'

type EditFormData = {
  author: string
  description: string
  links: Link[]
  name: string
  password?: string
  passwordHint?: string
  publishOption: PublishOption
}

type BlogEditPageContentProps = {
  blogId: string
  isLoading: boolean
  isError: boolean
  blog: Blog
  maxBlogLinks: number
  errorMessage?: string
}

export default function BlogEditPageContent({
  blogId,
  isLoading,
  isError,
  blog,
  maxBlogLinks,
  errorMessage,
}: BlogEditPageContentProps) {
  const methods = useForm<EditFormData>({
    defaultValues: {
      author: blog.author ?? '',
      name: blog.name ?? '',
      description: blog.description ?? '',
      publishOption: blog.publishOption,
      passwordHint: blog.passwordHint,
      links: Array(maxBlogLinks)
        .fill(0)
        .map((_, i) => ({
          id: blog.links?.[i]?.id ?? '',
          name: blog.links?.[i]?.name ?? '',
          url: blog.links?.[i]?.url ?? '',
        })),
    },
  })

  const router = useRouter()
  const queryClient = useQueryClient()
  const toast = useToast()
  const { mutateAsync, isLoading: isUpdating } = useUpdateBlogMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '保存に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '保存しました',
        status: 'success',
        isClosable: true,
      })
      queryClient.invalidateQueries(useGetBlogByIdQuery.getKey({ id: blogId }))
      router.push('/home')
    },
  })
  const mutate = async (variables: UpdateBlogMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  const onSubmit = async (formData: EditFormData) => {
    const input: UpdateBlogInput = {
      id: blogId,
      author: formData.author,
      name: formData.name,
      description: formData.description,
      publishOption: formData.publishOption,
      links: [],
    }
    if (formData.publishOption === PublishOption.Password) {
      if (formData.password !== undefined && formData.password !== '') {
        input.password = formData.password
      }
      if (formData.passwordHint !== undefined) {
        input.passwordHint = formData.passwordHint
      }
    }
    formData.links.forEach((value: Link) => {
      if (value.url !== '') {
        const link: UpdateBlogLinkInput = {
          name: value.name,
          url: value.url,
        }
        if (value.id !== '') {
          link.id = value.id
        }
        input.links?.push(link)
      }
    })
    await mutate({ input })
  }

  return (
    <>
      <Flex justifyContent={'space-between'} mt={4} mb={4}>
        <Heading as='h2'>ブログ編集</Heading>
        {blog && <DeleteButton blogId={blogId} />}
      </Flex>
      {isLoading ? (
        <Spinner size='xl' />
      ) : isError ? (
        <span>Error: {errorMessage ?? ''}</span>
      ) : (
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <BlogEditForm
              initialPublishOption={blog.publishOption}
              maxBlogLinks={maxBlogLinks}
              isUpdating={isUpdating}
            ></BlogEditForm>
          </form>
        </FormProvider>
      )}
    </>
  )
}

type BlogEditFormProps = {
  initialPublishOption: PublishOption
  maxBlogLinks: number
  isUpdating: boolean
}

function BlogEditForm({ initialPublishOption, maxBlogLinks, isUpdating }: BlogEditFormProps) {
  const [isLargerThan600] = useMediaQuery('(min-width: 600px)')
  const gridTemplate = '7em 1fr'
  const {
    register,
    unregister,
    watch,
    clearErrors,
    formState: { errors },
  } = useFormContext()
  const { fields } = useFieldArray({
    name: 'links',
  })
  const [showPasswordInput, setShowPasswordInput] = useState(false)
  const alertColor = useColorModeValue(
    theme.colors.alertColor.lightMode,
    theme.colors.alertColor.darkMode,
  )
  const onChangePublishOption = (value: string) => {
    if (value === PublishOption.Password) {
      setShowPasswordInput(true)
    } else {
      setShowPasswordInput(false)
    }
  }

  useEffect(() => {
    if (initialPublishOption === PublishOption.Password) {
      setShowPasswordInput(true)
    }
  }, [initialPublishOption])

  return (
    <>
      <Text color={alertColor} mb={2}>
        * は必須入力
      </Text>
      <TextFieldInput
        id='author'
        label='著者名'
        helperText='最大 50 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('author', AuthorValidator)}
        errors={errors.author}
        isRequired={true}
      />
      <TextFieldInput
        id='name'
        label='ブログ名'
        helperText='最大 255 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('name', NameValidator)}
        errors={errors.name}
        isRequired={true}
      />
      <TextareaInput
        id='description'
        label='ブログ説明'
        helperText='最大 500 文字'
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('description', DescriptionValidator)}
        errors={errors.description}
      />
      <PublishOptionInput
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('publishOption', PublishOptionValidator)}
        errors={errors.publishOption}
        onChange={onChangePublishOption}
      />
      <PasswordInput
        hasPassword={initialPublishOption === PublishOption.Password}
        showPasswordInput={showPasswordInput}
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        register={register('password', PasswordValidator(showPasswordInput))}
        unregister={() => {
          unregister('password')
        }}
        clearError={() => clearErrors('password')}
        errors={errors.password}
      />
      {showPasswordInput && (
        <TextFieldInput
          id='passwordHint'
          label='パスワードのヒント'
          helperText='最大 100 文字'
          isLargerThan600={isLargerThan600}
          gridTemplate={gridTemplate}
          register={register('passwordHint', PasswordHintValidator)}
          errors={errors.passwordHint}
        />
      )}
      <LinksInput
        isLargerThan600={isLargerThan600}
        gridTemplate={gridTemplate}
        linksRender={fields.map((field, index) => (
          <div key={field.id}>
            <LinkInput
              id={`links.${index}.name`}
              label='リンクタイトル'
              register={register(`links.${index}.name`, LinkNameValidator)}
              errors={
                (
                  (errors.links as Merge<FieldError, FieldErrorsImpl<any>> | undefined)?.[
                    index
                  ] as Partial<
                    FieldErrorsImpl<{
                      [x: string]: any
                    }>
                  >
                )?.name
              }
            ></LinkInput>
            <LinkInput
              id={`links.${index}.url`}
              label='URL'
              register={register(
                `links.${index}.url`,
                LinkUrlValidator(watch(`links.${index}.name`)),
              )}
              errors={
                (
                  (errors.links as Merge<FieldError, FieldErrorsImpl<any>> | undefined)?.[
                    index
                  ] as Partial<
                    FieldErrorsImpl<{
                      [x: string]: any
                    }>
                  >
                )?.url
              }
            ></LinkInput>
            {index !== maxBlogLinks - 1 ? <Divider mb={2} /> : <></>}
          </div>
        ))}
      />
      <SaveCancelButtonGroup isLoading={isUpdating} cancelLinkTo={`/home`} />
    </>
  )
}
