'use client'

import path from 'path'
import { Spinner } from '@chakra-ui/react'
import { notFound, usePathname } from 'next/navigation'
import BlogEditPageContent from './BlogEditPageContent'
import { useGetBlogByIdQuery } from '@/graph/getBlogByID.generated'
import { useMaxBlogLinksQuery } from '@/graph/maxBlogLinks.generated'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

type Props = {
  params: { blogId: string }
}

export default function BlogEditPage({ params }: Props) {
  const { isAuthChecking, user: currentUser } = useRequireLogin()

  const blogId = params.blogId

  const {
    data: blogData,
    isLoading: blogIsLoading,
    isError: blogIsError,
    error: blogError,
  } = useGetBlogByIdQuery({ id: blogId })
  const {
    data: maxBlogLinksData,
    isLoading: maxBlogLinksIsLoading,
    isError: maxBlogLinksIsError,
    error: maxBlogLinksError,
  } = useMaxBlogLinksQuery(undefined, { staleTime: Infinity })

  if (
    isAuthChecking ||
    !currentUser ||
    blogIsLoading ||
    maxBlogLinksIsLoading ||
    blogData?.blogById === undefined ||
    maxBlogLinksData?.masterParameter === undefined
  ) {
    return <Spinner size='xl' />
  }

  if (blogData.blogById === null || maxBlogLinksData.masterParameter === null) {
    notFound()
  }

  return (
    <BlogEditPageContent
      blogId={blogId}
      isLoading={blogIsLoading || maxBlogLinksIsLoading}
      isError={blogIsError || maxBlogLinksIsError}
      blog={blogData.blogById}
      maxBlogLinks={maxBlogLinksData.masterParameter.maxBlogLinks}
      errorMessage={blogError?.message ?? maxBlogLinksError?.message}
    />
  )
}
