'use client'

import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Text,
  Grid,
  Flex,
  Select,
  useColorModeValue,
} from '@chakra-ui/react'
import type { FieldError, FieldErrors, Merge, UseFormRegisterReturn } from 'react-hook-form'
import { PublishOption } from '@/graph/types.generated'
import theme from '@/theme'

type PublishOptionInputProps = {
  isLargerThan600: boolean
  gridTemplate: string
  register: UseFormRegisterReturn
  errors: FieldError | Merge<FieldError, FieldErrors<any>> | undefined
  onChange: (value: string) => void
}

export default function PublishOptionInput({
  isLargerThan600,
  gridTemplate,
  register,
  errors,
  onChange,
}: PublishOptionInputProps) {
  const alertColor = useColorModeValue(
    theme.colors.alertColor.lightMode,
    theme.colors.alertColor.darkMode,
  )
  return (
    <FormControl isInvalid={Boolean(errors)} mb={2}>
      <Grid templateColumns={isLargerThan600 ? gridTemplate : '1fr'} gap={1} mb={2}>
        <FormLabel htmlFor='publishOption'>
          <Flex>
            <Text fontWeight='bold'>公開状態</Text>
            <Text color={alertColor}>*</Text>
          </Flex>
        </FormLabel>
        <Select id='publishOption' {...register} onChange={(event) => onChange(event.target.value)}>
          <option value={PublishOption.Public}>PUBLIC</option>
          <option value={PublishOption.Password}>PASSWORD</option>
          <option value={PublishOption.Private}>PRIVATE</option>
        </Select>
        <span></span>
        <FormErrorMessage>{errors && errors.message?.toString()}</FormErrorMessage>
      </Grid>
    </FormControl>
  )
}
