'use client'

import { useSearchParams } from 'next/navigation'
import { useEffect, useMemo, useState } from 'react'
import { CONSTANT } from '@/constants'
import type { ArticlePaginationInput } from '@/graph/types.generated'

export function usePagination() {
  const searchParams = useSearchParams()

  const after = searchParams?.get('a')
  const before = searchParams?.get('b')

  const initialPaginationInput: ArticlePaginationInput = useMemo(() => {
    return after
      ? {
          first: CONSTANT.ARTICLES_LIMIT,
          after,
        }
      : before
      ? {
          last: CONSTANT.ARTICLES_LIMIT,
          before,
        }
      : {
          last: CONSTANT.ARTICLES_LIMIT,
        }
  }, [after, before])

  const [paginationInput, setPaginationInput] =
    useState<ArticlePaginationInput>(initialPaginationInput)

  useEffect(() => {
    if (after) {
      setPaginationInput({
        first: CONSTANT.ARTICLES_LIMIT,
        after,
      })
    } else if (before) {
      setPaginationInput({
        last: CONSTANT.ARTICLES_LIMIT,
        before,
      })
    } else {
      setPaginationInput({
        last: CONSTANT.ARTICLES_LIMIT,
      })
    }
  }, [after, before])

  return paginationInput
}
