'use client'

import { Text } from '@chakra-ui/react'
import { useEffect } from 'react'

export default function Error() {
  useEffect(() => {
    document.title = 'エラー'
  }, [])
  return (
    <>
      <Text>読み込みエラーが発生しました。お手数ですがしばらく後にアクセスし直してください。</Text>
    </>
  )
}
