'use client'

import { Box, Grid, Heading, Spinner, Text, useMediaQuery } from '@chakra-ui/react'
import { useQueryClient } from '@tanstack/react-query'
import Link from 'next/link'
import { notFound } from 'next/navigation'
import { useEffect, useState } from 'react'
import ArticleList from '@/app/view/[blogKey]/ArticleList'
import Error from '@/app/view/[blogKey]/Error'
import NewButton from '@/app/view/[blogKey]/NewButton'
import Pagination from '@/app/view/[blogKey]/Pagination'
import PasswordError from '@/app/view/[blogKey]/PasswordError'
import PasswordInput from '@/app/view/[blogKey]/PasswordInput'
import ReauthenticateView from '@/app/view/[blogKey]/ReauthenticateView'
import TabsView from '@/app/view/[blogKey]/TabsView'
import { usePagination } from '@/app/view/[blogKey]/usePagination'
import ColorModeButton from '@/components/ui/ColorModeButton'
import PublishOptionBadge from '@/components/ui/PublishOptionBadge'
import { CONSTANT } from '@/constants'
import { type ArticlesQueryVariables, useArticlesQuery } from '@/graph/articles.generated'
import { useGuestBlogByBlogKeyQuery } from '@/graph/guest/getGuestBlogByBlogKey.generated'
import { useGuestArticlesQuery } from '@/graph/guest/guestArticles.generated'
import { PublishOption } from '@/graph/types.generated'
import type { BlogData } from '@/types/pages/view'

type BlogViewPageContentProps = {
  blogKey: string
  blogByBlogKey: BlogData
  jwt: string | null
  blogId?: string
}

export default function BlogViewPageContent({
  blogKey,
  blogByBlogKey,
  jwt,
  blogId,
}: BlogViewPageContentProps) {
  switch (blogByBlogKey.publishOption) {
    case PublishOption.Public:
      return <Blog data={blogByBlogKey} blogKey={blogKey} blogId={blogId} />
    case PublishOption.Password:
      // 非公開の情報が取れていたら著者自身の閲覧
      if (blogByBlogKey.author !== undefined && blogByBlogKey.author !== null) {
        return <Blog data={blogByBlogKey} blogKey={blogKey} blogId={blogId} />
      }
      // 著者以外
      if (jwt != null) {
        return <GuestView blogKey={blogKey} xJwtToken={jwt} />
      }
      return <Password blogKey={blogKey} passwordHint={blogByBlogKey.passwordHint} />
    default:
      // 非公開の情報が取れていたら著者自身の閲覧
      if (blogByBlogKey.author !== undefined && blogByBlogKey.author !== null) {
        return <Blog data={blogByBlogKey} blogKey={blogKey} blogId={blogId} />
      }
      notFound()
  }
}

type PasswordProps = {
  blogKey: string
  passwordHint?: string
}

function Password({ blogKey, passwordHint }: PasswordProps) {
  const [text, setText] = useState('')
  const [isSend, setIsSend] = useState(false)

  const onChangeInput = (value: string) => {
    setText(value)
  }

  const onClickSend = () => {
    setIsSend(true)
  }

  if (!isSend) {
    return (
      <>
        {passwordHint && passwordHint !== '' && <Text>ヒント:&nbsp;{passwordHint}</Text>}
        <PasswordInput text={text} onChangeInput={onChangeInput} onClickSend={onClickSend} />
      </>
    )
  }

  return <GuestView blogKey={blogKey} password={text} />
}

type GuestViewProps = {
  blogKey: string
  password?: string
  xJwtToken?: string
}

function GuestView({ blogKey, password, xJwtToken }: GuestViewProps) {
  const { data, isError, error, isLoading } = useGuestBlogByBlogKeyQuery(
    {
      input: { blogKey, password, xJwtToken },
    },
    { retry: false },
  )

  useEffect(() => {
    if (data?.blogByBlogKey?.xJwtToken) {
      sessionStorage.setItem(blogKey, data.blogByBlogKey.xJwtToken)
    }
  }, [blogKey, data])

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    switch (error.message) {
      case CONSTANT.SERVER_ERROR_MESSAGE.BLOG_AUTHENTICATION_EXPIRED:
        return <ReauthenticateView blogKey={blogKey} />
      case CONSTANT.SERVER_ERROR_MESSAGE.FORBIDDEN:
        return <PasswordError />
    }
  }

  if (isError || !data.blogByBlogKey) {
    return <Error />
  }

  return (
    <Blog data={data.blogByBlogKey} blogKey={blogKey} password={password} xJwtToken={xJwtToken} />
  )
}

type BlogProps = {
  data: BlogData
  blogKey: string
  blogId?: string
  password?: string
  xJwtToken?: string
}

function Blog({ data, blogKey, blogId, password, xJwtToken }: BlogProps) {
  useEffect(() => {
    if (data.name) {
      document.title = data.name
    }
  }, [data.name])

  const [isPC] = useMediaQuery('(min-width: 769px)')

  const gridTemplateColumns = isPC ? '30% 0px 1fr' : '1fr'

  return (
    <>
      <a href='#skip' style={{ display: 'block', width: 0, height: 0, overflow: 'hidden' }}>
        Skip to content
      </a>
      <Box mb={1}>
        <PublishOptionBadge publishOption={data.publishOption} mr={2} />
        <ColorModeButton />
      </Box>
      <Heading as='h1' size='xl' mb={2} letterSpacing={'wider'}>
        {data.name}
      </Heading>
      <Grid gridTemplateColumns={gridTemplateColumns} columnGap={1}>
        <TabsView
          author={data.author ?? ''}
          description={data.description ?? ''}
          links={data.links ?? []}
        />
        <div id='skip' tabIndex={-1}></div>
        <Box>
          {blogId !== undefined ? (
            <Articles blogKey={blogKey} blogId={blogId} />
          ) : (
            <GuestArticles blogKey={blogKey} password={password} xJwtToken={xJwtToken} />
          )}
        </Box>
      </Grid>
    </>
  )
}

type ArticlesProps = {
  blogKey: string
  blogId: string
}

function Articles({ blogKey, blogId }: ArticlesProps) {
  const [isPC] = useMediaQuery('(min-width: 769px)')

  const paginationInput = usePagination()
  const queryClient = useQueryClient()

  const queryKey: ArticlesQueryVariables = {
    input: { blogKey: blogKey },
    paginationInput,
    blogKey: blogKey,
  }
  const { data, isError, isLoading } = useArticlesQuery(queryKey, { staleTime: 1000 })

  const onCreate = () => {
    if (!data?.articles) {
      return
    }
    if (!data.articles.pageInfo.hasNextPage) {
      queryClient.invalidateQueries(useArticlesQuery.getKey(queryKey))
    }
  }

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    return (
      <Text>読み込みエラーが発生しました。お手数ですがしばらく後にアクセスし直してください。</Text>
    )
  }

  if (!data.articles) {
    return (
      <Text>読み込みエラーが発生しました。お手数ですがしばらく後にアクセスし直してください。</Text>
    )
  }

  return (
    <Box>
      <ArticleList
        blogKey={blogKey}
        data={data.articles.edges}
        canEdit={data.isAuthor}
        queryKey={queryKey}
      />
      <Pagination
        hasPreviousPage={data.articles.pageInfo.hasNextPage}
        hasNextPage={data.articles.pageInfo.hasPreviousPage}
        startCursor={data?.articles?.pageInfo.startCursor ?? undefined}
        endCursor={data?.articles?.pageInfo.endCursor ?? undefined}
        marginBottom={data.isAuthor || !isPC ? 10 : 0}
      />
      {data.isAuthor && <NewButton blogId={blogId} onCreate={onCreate} />}
    </Box>
  )
}

type GuestArticlesProps = {
  blogKey: string
  password?: string
  xJwtToken?: string
}

function GuestArticles({ blogKey, password, xJwtToken }: GuestArticlesProps) {
  const [isPC] = useMediaQuery('(min-width: 769px)')
  const paginationInput = usePagination()

  const { data, isError, error, isLoading } = useGuestArticlesQuery(
    {
      input: { blogKey, password, xJwtToken },
      paginationInput,
    },
    { retry: false, staleTime: 1000 },
  )
  useEffect(() => {
    if (data?.articles?.xJwtToken) {
      sessionStorage.setItem(blogKey, data.articles.xJwtToken)
    }
  }, [blogKey, data])

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    switch (error.message) {
      case CONSTANT.SERVER_ERROR_MESSAGE.BLOG_AUTHENTICATION_EXPIRED:
      case CONSTANT.SERVER_ERROR_MESSAGE.FORBIDDEN:
        return <ReauthenticateView blogKey={blogKey} />
    }
  }

  if (isError || !data.articles) {
    return (
      <Text>読み込みエラーが発生しました。お手数ですがしばらく後にアクセスし直してください。</Text>
    )
  }

  return (
    <Box>
      <ArticleList blogKey={blogKey} data={data.articles.edges} canEdit={false} />
      <Pagination
        hasPreviousPage={data.articles.pageInfo.hasNextPage}
        hasNextPage={data.articles.pageInfo.hasPreviousPage}
        startCursor={data?.articles?.pageInfo.startCursor ?? undefined}
        endCursor={data?.articles?.pageInfo.endCursor ?? undefined}
        marginBottom={!isPC ? 10 : 0}
      />
    </Box>
  )
}
