'use client'

import { EditIcon } from '@chakra-ui/icons'
import {
  Button,
  Text,
  useDisclosure,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  ModalHeader,
  useToast,
  useMediaQuery,
} from '@chakra-ui/react'
import { useRef } from 'react'
import ArticleForm from '@/app/view/[blogKey]/ArticleForm'
import {
  type CreateArticleMutationVariables,
  useCreateArticleMutation,
} from '@/graph/createArticle.generated'
import type { CreateArticleInput } from '@/graph/types.generated'

type NewButtonProps = {
  blogId: string
  onCreate: () => void
}

export default function NewButton({ blogId, onCreate }: NewButtonProps) {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <Button
        colorScheme='blue'
        leftIcon={<EditIcon />}
        position={'fixed'}
        bottom={4}
        right={6}
        onClick={onOpen}
      >
        <Text>投稿する</Text>
      </Button>
      <CreateFormModal isOpen={isOpen} onClose={onClose} blogId={blogId} onCreate={onCreate} />
    </>
  )
}

type CreateFormModalProps = {
  blogId: string
  isOpen: boolean
  onCreate: () => void
  onClose: () => void
}

function CreateFormModal({ blogId, isOpen, onCreate, onClose }: CreateFormModalProps) {
  const [isMobile] = useMediaQuery('(max-width: 704px)')
  const initialRef = useRef<HTMLTextAreaElement | null>(null)
  const toast = useToast()
  const { mutateAsync, isLoading } = useCreateArticleMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '投稿に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '投稿しました',
        status: 'success',
        isClosable: true,
      })
      onCreate()
      onClose()
    },
  })

  const mutate = async (variables: CreateArticleMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  const onSubmit = async (content: string) => {
    const input: CreateArticleInput = {
      blogId,
      content,
    }
    await mutate({ input })
  }

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      initialFocusRef={initialRef}
      closeOnOverlayClick={false}
      size={isMobile ? 'full' : 'xl'}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>記事投稿</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={6}>
          <ArticleForm update={onSubmit} isLoading={isLoading} />
        </ModalBody>
      </ModalContent>
    </Modal>
  )
}
