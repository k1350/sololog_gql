'use client'

import { CheckIcon, CloseIcon, TriangleUpIcon, TriangleDownIcon } from '@chakra-ui/icons'
import {
  Button,
  FormControl,
  Textarea,
  FormHelperText,
  FormErrorMessage,
  Center,
  ButtonGroup,
  IconButton,
  HStack,
} from '@chakra-ui/react'
import { useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import { CONSTANT } from '@/constants'
import { ArticleValidator } from '@/validate/Article'

type ArticleFormProps = {
  update: (content: string) => Promise<void>
  isLoading: boolean
  defaultValue?: string
  onClose?: () => void
}

export default function ArticleForm({
  update,
  isLoading,
  defaultValue,
  onClose,
}: ArticleFormProps) {
  const [height, setHeight] = useState<number>(5)
  const initialRef = useRef<HTMLTextAreaElement | null>(null)
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    reset,
    formState: { errors, isValid },
  } = useForm({
    defaultValues: { article: defaultValue ?? '' },
  })

  const onSubmit = async (formData: any) => {
    await update(formData.article)
    reset()
  }

  const watchArticle = watch('article')
  const onClickCont = () => {
    if (!initialRef.current) {
      return
    }
    const selectionStart = initialRef.current.selectionStart
    const beforeValue = watchArticle.substring(0, selectionStart)
    const afterValue = watchArticle.substring(selectionStart)
    const value = `${beforeValue}${CONSTANT.ARTICLE_MORE}${afterValue}`
    setValue('article', value, { shouldValidate: true })
    initialRef.current.focus()
    initialRef.current.selectionEnd = selectionStart + CONSTANT.ARTICLE_MORE.length
  }
  const { ref, ...rest } = register('article', ArticleValidator)

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl isInvalid={Boolean(errors.article)} mb={4}>
        <Textarea
          mb={2}
          {...rest}
          ref={(e) => {
            ref(e)
            initialRef.current = e
          }}
          resize='none'
          boxSizing='border-box'
          height={`${height}rem`}
        />
        <HStack spacing='10px'>
          <Button mt={2} colorScheme='blue' variant={'outline'} onClick={onClickCont} mb={2}>
            {CONSTANT.ARTICLE_MORE}挿入
          </Button>
          <IconButton
            aria-label='Reduce textarea rows'
            icon={<TriangleUpIcon />}
            onClick={() => setHeight(height - 1)}
            isDisabled={height <= 5}
          />
          <IconButton
            aria-label='Increase textarea rows'
            icon={<TriangleDownIcon />}
            onClick={() => setHeight(height + 2)}
            isDisabled={height >= 15}
          />
        </HStack>
        {errors.article ? (
          <FormErrorMessage>{errors.article.message?.toString()}</FormErrorMessage>
        ) : (
          <FormHelperText ml={5}>
            <ul>
              <li>最大 40000 字</li>
              <li>{CONSTANT.ARTICLE_MORE}キーワードを入れるとそれ以降が折りたたまれます</li>
              <li>
                {CONSTANT.ARTICLE_MORE}
                キーワードは最初の一つだけ有効で、二つ目以降は無視されます
              </li>
            </ul>
          </FormHelperText>
        )}
      </FormControl>
      <Center>
        {defaultValue === undefined ? (
          <Button
            colorScheme='blue'
            leftIcon={<CheckIcon />}
            type='submit'
            isDisabled={!isValid}
            isLoading={isLoading}
          >
            投稿する
          </Button>
        ) : (
          <ButtonGroup justifyContent='center' size='sm'>
            <Button
              type='submit'
              colorScheme='blue'
              leftIcon={<CheckIcon />}
              isDisabled={!isValid}
              isLoading={isLoading}
            >
              保存する
            </Button>
            <Button
              colorScheme={'blue'}
              variant='outline'
              leftIcon={<CloseIcon />}
              onClick={onClose}
            >
              キャンセル
            </Button>
          </ButtonGroup>
        )}
      </Center>
    </form>
  )
}
