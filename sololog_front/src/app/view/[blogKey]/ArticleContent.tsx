'use client'

import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { memo } from 'react'
import reactStringReplace from 'react-string-replace'
import NextLink from '@/components/ui/NextLink'
import { CONSTANT } from '@/constants'
import theme from '@/theme'

type ArticleContentProps = {
  content: string
}

const regExp = /(https?:\/\/\S+)/g

function ArticleContentInnerComponent({ content }: ArticleContentProps) {
  const bgColor = useColorModeValue(
    theme.colors.secondaryBgColor.lightMode,
    theme.colors.secondaryBgColor.darkMode,
  )
  const moreIndex = content.indexOf(CONSTANT.ARTICLE_MORE)
  if (moreIndex > -1) {
    const beforeMore = content.substring(0, moreIndex)
    const afterMore = content.substring(moreIndex + 8)
    return (
      <Box>
        <Text wordBreak={'break-word'} overflowWrap={'anywhere'} whiteSpace={'pre-wrap'} letterSpacing={'wider'}>
          {reactStringReplace(beforeMore, regExp, (match, i) => (
            <AutoLinkContent key={match + i} content={match} />
          ))}
        </Text>
        <Accordion allowToggle>
          <AccordionItem>
            <h2>
              <AccordionButton bg={bgColor}>
                <Box flex='1' textAlign='left'>
                  <Text letterSpacing={'wider'}>続きを読む</Text>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <Text wordBreak={'break-word'} overflowWrap={'anywhere'} whiteSpace={'pre-wrap'} letterSpacing={'wider'}>
                {reactStringReplace(afterMore, regExp, (match, i) => (
                  <AutoLinkContent key={match + i} content={match} />
                ))}
              </Text>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Box>
    )
  }

  return (
    <Text wordBreak={'break-word'} overflowWrap={'anywhere'} whiteSpace={'pre-wrap'} letterSpacing={'wider'}>
      {reactStringReplace(content, regExp, (match, i) => (
        <AutoLinkContent key={match + i} content={match} />
      ))}
    </Text>
  )
}

function AutoLinkContent({ content }: ArticleContentProps) {
  return <NextLink href={encodeURI(content)}>{content}</NextLink>
}

const ArticleContent = memo(ArticleContentInnerComponent)

export default ArticleContent
