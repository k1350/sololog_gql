'use client'

import { Spinner } from '@chakra-ui/react'
import { notFound } from 'next/navigation'
import { useEffect, useState } from 'react'
import BlogViewPageContent from './BlogViewPageContent'
import Error from '@/app/view/[blogKey]/Error'
import { useAuthBlogByBlogKeyQuery } from '@/graph/getAuthBlogByBlogKey.generated'

type BlogViewPageProps = {
  params: { blogKey: string }
}

export default function BlogViewPage({ params }: BlogViewPageProps) {
  const blogKey = params.blogKey
  const [jwt, setJwt] = useState<string | null>(null)
  useEffect(() => {
    setJwt(sessionStorage.getItem(blogKey))
  }, [blogKey])
  const { data, isError, isLoading } = useAuthBlogByBlogKeyQuery({
    input: { blogKey: blogKey },
  })

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    return <Error />
  }

  if (!data.blogByBlogKey) {
    notFound()
  }

  return (
    <BlogViewPageContent
      blogKey={blogKey}
      blogByBlogKey={data.blogByBlogKey}
      jwt={jwt}
      blogId={data.blogByBlogKey.id}
    />
  )
}
