'use client'

import { LinkIcon } from '@chakra-ui/icons'
import { Grid } from '@chakra-ui/react'
import NextLink from '@/components/ui/NextLink'
import type { BlogLink } from '@/types/pages/view'

type BlogLinksProps = {
  data: BlogLink[]
}

export default function BlogLinks({ data }: BlogLinksProps) {
  if (data.length == 0) {
    return null
  }

  return (
    <Grid gap={2}>
      {data.map((link) => (
        <Grid key={link.id} templateColumns='2em 1fr' gap={4}>
          <LinkIcon alignSelf={'start'} m={1} />
          {link.name == '' ? (
            <NextLink href={encodeURI(link.url)} isExternal={true}>
              {link.url}
            </NextLink>
          ) : (
            <NextLink href={encodeURI(link.url)} isExternal={true}>
              {link.name}
            </NextLink>
          )}
        </Grid>
      ))}
    </Grid>
  )
}
