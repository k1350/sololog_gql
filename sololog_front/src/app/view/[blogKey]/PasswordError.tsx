'use client'

import { Text, Button, VStack } from '@chakra-ui/react'
import { useEffect } from 'react'

export default function PasswordError() {
  useEffect(() => {
    document.title = 'エラー'
  }, [])

  return (
    <>
      <VStack spacing={4} align='center'>
        <Text>パスワードが違います。</Text>
        <Button colorScheme={'blue'} onClick={() => location.reload()}>
          再入力する
        </Button>
      </VStack>
    </>
  )
}
