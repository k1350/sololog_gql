import { ViewLayout } from '@/layout/ViewLayout'

export default function Layout({ children }: { children: React.ReactElement }) {
  return <ViewLayout>{children}</ViewLayout>
}
