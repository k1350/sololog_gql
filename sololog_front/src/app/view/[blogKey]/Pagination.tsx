'use client'

import { Center, ButtonGroup, Button } from '@chakra-ui/react'
import { usePathname } from 'next/navigation'
import NextLinkButton from '@/components/ui/NextLinkButton'

type PaginationProps = {
  hasNextPage: boolean
  hasPreviousPage: boolean
  startCursor?: string
  endCursor?: string
  marginBottom?: number
}

export default function Pagination({
  hasNextPage,
  hasPreviousPage,
  startCursor,
  endCursor,
  marginBottom,
}: PaginationProps) {
  const pathname = usePathname()

  return (
    <>
      <Center mt={2} mb={2}>
        <ButtonGroup justifyContent='center' size='sm'>
          {endCursor && hasPreviousPage ? (
            <NextLinkButton href={pathname + `?a=${endCursor}`} outline>
              前へ
            </NextLinkButton>
          ) : (
            <Button colorScheme={'blue'} variant='outline' isDisabled>
              前へ
            </Button>
          )}
          {startCursor && hasNextPage ? (
            <NextLinkButton href={pathname + `?b=${startCursor}`} outline>
              次へ
            </NextLinkButton>
          ) : (
            <Button colorScheme={'blue'} variant='outline' isDisabled>
              次へ
            </Button>
          )}
        </ButtonGroup>
      </Center>
      <Center marginBottom={marginBottom}>
        <NextLinkButton href={pathname ?? ''} outline>
          ブログトップに戻る
        </NextLinkButton>
      </Center>
    </>
  )
}
