'use client'

import { Text, Button, VStack } from '@chakra-ui/react'
import { useEffect } from 'react'

type ReauthenticateViewProps = {
  blogKey: string
}

export default function ReauthenticateView({ blogKey }: ReauthenticateViewProps) {
  useEffect(() => {
    sessionStorage.removeItem(blogKey)
  }, [blogKey])

  return (
    <>
      <VStack spacing={4} align='center'>
        <Text>パスワード認証の有効期限が切れました。再度パスワード認証が必要です。</Text>
        <Button colorScheme={'blue'} onClick={() => location.reload()}>
          再入力する
        </Button>
      </VStack>
    </>
  )
}
