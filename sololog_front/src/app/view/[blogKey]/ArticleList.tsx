'use client'

import { SimpleGrid, Text, Box } from '@chakra-ui/react'
import ArticleCard from '@/app/view/[blogKey]/ArticleCard'
import EditableArticleCard from '@/app/view/[blogKey]/EditableArticleCard'
import type { ArticlesQueryVariables } from '@/graph/articles.generated'
import type { ArticleEdge, Edge, Maybe } from '@/graph/types.generated'

type ArticlesProps = {
  blogKey: string
  data: (Maybe<Edge>[] & Maybe<ArticleEdge>[]) | null | undefined
  canEdit: boolean
  queryKey?: ArticlesQueryVariables
}

export default function ArticleList({ blogKey, data, canEdit, queryKey }: ArticlesProps) {
  if (!data || data.length === 0) {
    return (
      <Box borderWidth='1px' borderRadius='lg' p={5}>
        <Text>記事がありません</Text>
      </Box>
    )
  }

  const edges = data as ArticleEdge[]

  if (canEdit && queryKey) {
    return (
      <>
        <SimpleGrid gap={2}>
          {[...edges].reverse().map((article) => (
            <EditableArticleCard
              blogKey={blogKey}
              key={article.cursor}
              data={article.node}
              queryKey={queryKey}
            />
          ))}
        </SimpleGrid>
      </>
    )
  }

  return (
    <>
      <SimpleGrid gap={2}>
        {[...edges].reverse().map((article) => (
          <ArticleCard blogKey={blogKey} key={article.cursor} data={article.node} />
        ))}
      </SimpleGrid>
    </>
  )
}
