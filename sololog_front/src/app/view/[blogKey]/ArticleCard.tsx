'use client'

import { Box } from '@chakra-ui/react'
import ArticleContent from '@/app/view/[blogKey]/ArticleContent'
import CreatedAtLink from '@/app/view/[blogKey]/CreatedAtLink'
import type { Article } from '@/graph/types.generated'

type ArticleCardProps = {
  blogKey: string
  data: Article | null | undefined
}

export default function ArticleCard({ blogKey, data }: ArticleCardProps) {
  if (!data) {
    return null
  }

  return (
    <Box borderWidth='1px' borderRadius='lg' p={5}>
      <ArticleContent content={data.content ?? ''} />
      <CreatedAtLink blogKey={blogKey} data={data} mt={2} />
    </Box>
  )
}
