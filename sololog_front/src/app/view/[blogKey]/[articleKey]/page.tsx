'use client'

import { Spinner } from '@chakra-ui/react'
import { notFound } from 'next/navigation'
import { useEffect, useState } from 'react'
import ArticleViewPageContent from './ArticleViewPageContent'
import Error from '@/app/view/[blogKey]/Error'
import { useAuthArticleByArticleKeyQuery } from '@/graph/authArticleByArticleKey.generated'

type ArticleViewPageProps = {
  params: {
    blogKey: string
    articleKey: string
  }
}

export default function ArticleViewPage({ params }: ArticleViewPageProps) {
  const blogKey = params.blogKey
  const articleKey = params.articleKey
  const [jwt, setJwt] = useState<string | null>(null)
  useEffect(() => {
    setJwt(sessionStorage.getItem(blogKey))
  }, [blogKey])

  const { data, isError, isLoading } = useAuthArticleByArticleKeyQuery({
    input: { blogKey: blogKey, articleKey: articleKey },
  })

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    return <Error />
  }

  if (!data.articleByArticleKey) {
    notFound()
  }

  return (
    <ArticleViewPageContent
      blogKey={blogKey}
      articleKey={articleKey}
      articleByArticleKey={data.articleByArticleKey}
      jwt={jwt}
    />
  )
}
