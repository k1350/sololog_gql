'use client'

import { ArrowBackIcon } from '@chakra-ui/icons'
import { Spinner, Box, Center } from '@chakra-ui/react'
import { notFound } from 'next/navigation'
import { useEffect, useState } from 'react'
import ArticleContent from '@/app/view/[blogKey]/ArticleContent'
import CreatedAtText from '@/app/view/[blogKey]/CreatedAtText'
import Error from '@/app/view/[blogKey]/Error'
import PasswordError from '@/app/view/[blogKey]/PasswordError'
import PasswordInput from '@/app/view/[blogKey]/PasswordInput'
import ReauthenticateView from '@/app/view/[blogKey]/ReauthenticateView'
import NextLinkButton from '@/components/ui/NextLinkButton'
import { CONSTANT } from '@/constants'
import { useGuestArticleByArticleKeyQuery } from '@/graph/guest/guestArticleByArticleKey.generated'
import { PublishOption } from '@/graph/types.generated'
import type { ArticleData } from '@/types/pages/view'

type ArticleViewPageContentProps = {
  blogKey: string
  articleKey: string
  articleByArticleKey: ArticleData
  jwt: string | null
}

export default function ArticleViewPageContent({
  blogKey,
  articleKey,
  articleByArticleKey,
  jwt,
}: ArticleViewPageContentProps) {
  switch (articleByArticleKey.publishOption) {
    case PublishOption.Public:
      return <ArticleView blogKey={blogKey} data={articleByArticleKey} />
    case PublishOption.Password:
      // 非公開の情報が取れていたら著者自身の閲覧
      if (articleByArticleKey.content !== undefined && articleByArticleKey.content !== null) {
        return <ArticleView blogKey={blogKey} data={articleByArticleKey} />
      }
      // 著者以外
      if (jwt != null) {
        return <GuestArticleView blogKey={blogKey} articleKey={articleKey} xJwtToken={jwt} />
      }
      return <ArticlePassword blogKey={blogKey} articleKey={articleKey} />
    default:
      // 非公開の情報が取れていたら著者自身の閲覧
      if (articleByArticleKey.content !== undefined && articleByArticleKey.content !== null) {
        return <ArticleView blogKey={blogKey} data={articleByArticleKey} />
      }
      notFound()
  }
}

type ArticlePasswordProps = {
  blogKey: string
  articleKey: string
}

function ArticlePassword({ blogKey, articleKey }: ArticlePasswordProps) {
  const [text, setText] = useState('')
  const [isSend, setIsSend] = useState(false)

  const onChangeInput = (value: string) => {
    setText(value)
  }

  const onClickSend = () => {
    setIsSend(true)
  }

  if (!isSend) {
    return <PasswordInput text={text} onChangeInput={onChangeInput} onClickSend={onClickSend} />
  }

  return <GuestArticleView blogKey={blogKey} articleKey={articleKey} password={text} />
}

type ArticleViewProps = {
  blogKey: string
  data: ArticleData
}

function ArticleView({ blogKey, data }: ArticleViewProps) {
  useEffect(() => {
    if (data.content) {
      document.title = data.content
    }
  }, [data.content])

  return (
    <>
      <Box borderWidth='1px' borderRadius='lg' p={5} mb={2}>
        <ArticleContent content={data.content ?? ''} />
        <CreatedAtText createdAt={data.createdAt ?? ''} updatedAt={data.updatedAt ?? ''} mt={2} />
      </Box>
      <Center>
        <NextLinkButton
          href={`${process.env.NEXT_PUBLIC_FRONT_URL_BASE as string}view/${blogKey}`}
          leftIcon={<ArrowBackIcon />}
          outline={true}
        >
          ブログに戻る
        </NextLinkButton>
      </Center>
    </>
  )
}

type GuestArticleViewProps = {
  blogKey: string
  articleKey: string
  password?: string
  xJwtToken?: string
}

function GuestArticleView({ blogKey, articleKey, password, xJwtToken }: GuestArticleViewProps) {
  const { data, isError, error, isLoading } = useGuestArticleByArticleKeyQuery(
    {
      input: { blogKey, articleKey, password, xJwtToken },
    },
    { retry: false },
  )
  useEffect(() => {
    if (data?.articleByArticleKey?.xJwtToken) {
      sessionStorage.setItem(blogKey, data.articleByArticleKey.xJwtToken)
    }
  }, [blogKey, data])
  useEffect(() => {
    if (data?.articleByArticleKey?.content) {
      document.title = data?.articleByArticleKey?.content
    }
  }, [data?.articleByArticleKey?.content])

  if (isLoading) {
    return <Spinner size='xl' />
  }

  if (isError) {
    switch (error.message) {
      case CONSTANT.SERVER_ERROR_MESSAGE.BLOG_AUTHENTICATION_EXPIRED:
        return <ReauthenticateView blogKey={blogKey} />
      case CONSTANT.SERVER_ERROR_MESSAGE.FORBIDDEN:
        return <PasswordError />
    }
  }

  if (isError) {
    return <Error />
  }

  if (!data.articleByArticleKey) {
    notFound()
  }

  return (
    <>
      <Box borderWidth='1px' borderRadius='lg' p={5} mb={2}>
        <ArticleContent content={data.articleByArticleKey.content ?? ''} />
        <CreatedAtText
          createdAt={data.articleByArticleKey.createdAt ?? ''}
          updatedAt={data.articleByArticleKey.updatedAt ?? ''}
          mt={2}
        />
      </Box>
      <Center>
        <NextLinkButton
          href={`${process.env.NEXT_PUBLIC_FRONT_URL_BASE as string}view/${blogKey}`}
          leftIcon={<ArrowBackIcon />}
          outline={true}
        >
          ブログに戻る
        </NextLinkButton>
      </Center>
    </>
  )
}
