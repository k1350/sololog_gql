import { ArticleViewLayout } from '@/layout/ArticleViewLayout'

export default function Layout({ children }: { children: React.ReactElement }) {
  return <ArticleViewLayout>{children}</ArticleViewLayout>
}
