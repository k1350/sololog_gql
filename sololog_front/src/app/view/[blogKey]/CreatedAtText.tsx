'use client'

import { Text, useColorModeValue } from '@chakra-ui/react'
import dayjs from 'dayjs'
import theme from '@/theme'

type CreatedAtTextProps = {
  createdAt: string
  updatedAt: string
  mt?: number
  underline?: boolean
}

export default function CreatedAtText({ createdAt, updatedAt, mt, underline }: CreatedAtTextProps) {
  const color = useColorModeValue(
    theme.colors.mutedTextColor.lightMode,
    theme.colors.mutedTextColor.darkMode,
  )
  const edited = createdAt !== updatedAt ? '（編集済）' : ''
  const formattedCreatedAt = dayjs(createdAt).format('YYYY/MM/DD HH:mm:ss')
  if (underline) {
    return (
      <Text fontSize='xs' color={color} mt={mt ?? 0} as='u'>
        <time dateTime={createdAt}>{formattedCreatedAt}</time>
        {edited}
      </Text>
    )
  }
  return (
    <Text fontSize='xs' color={color} mt={mt ?? 0}>
      <time dateTime={createdAt}>{formattedCreatedAt}</time>
      {edited}
    </Text>
  )
}
