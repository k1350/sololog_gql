'use client'

import Link from 'next/link'
import { memo } from 'react'
import CreatedAtText from '@/app/view/[blogKey]/CreatedAtText'
import type { Article } from '@/graph/types.generated'

type CreatedAtLinkProps = {
  blogKey: string
  data: Article
  mt?: number
}

function CreatedAtLinkInnerComponent({ blogKey, data, mt }: CreatedAtLinkProps) {
  return (
    <Link
      href={`${process.env.NEXT_PUBLIC_FRONT_URL_BASE as string}view/${blogKey}/${data.articleKey}`}
    >
      <CreatedAtText
        createdAt={data.createdAt ?? ''}
        updatedAt={data.updatedAt ?? ''}
        mt={mt}
        underline={true}
      />
    </Link>
  )
}

const CreatedAtLink = memo(CreatedAtLinkInnerComponent)
export default CreatedAtLink
