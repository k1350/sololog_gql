'use client'

import { CheckIcon } from '@chakra-ui/icons'
import { Button, FormControl, FormLabel, Input, VStack } from '@chakra-ui/react'
import { useEffect } from 'react'

type PasswordInputProps = {
  text: string
  onChangeInput: (value: string) => void
  onClickSend: () => void
}

export default function PasswordInput({ text, onChangeInput, onClickSend }: PasswordInputProps) {
  useEffect(() => {
    document.title = 'パスワード入力'
  }, [])

  return (
    <>
      <VStack spacing={4} align='center'>
        <FormControl>
          <FormLabel>パスワードを入力してください</FormLabel>
          <Input
            type='password'
            value={text}
            onChange={(event) => onChangeInput(event.target.value)}
          />
        </FormControl>
        <Button
          colorScheme='blue'
          leftIcon={<CheckIcon />}
          isDisabled={text === ''}
          onClick={onClickSend}
        >
          送信する
        </Button>
      </VStack>
    </>
  )
}
