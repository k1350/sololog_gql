'use client'

import { HamburgerIcon, EditIcon, DeleteIcon, CheckIcon, CloseIcon } from '@chakra-ui/icons'
import {
  Box,
  Flex,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useDisclosure,
  Button,
  useToast,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/react'
import { useQueryClient } from '@tanstack/react-query'
import { useRef, useState } from 'react'
import ArticleContent from '@/app/view/[blogKey]/ArticleContent'
import ArticleForm from '@/app/view/[blogKey]/ArticleForm'
import CreatedAtLink from '@/app/view/[blogKey]/CreatedAtLink'
import {
  type ArticlesQuery,
  type ArticlesQueryVariables,
  useArticlesQuery,
} from '@/graph/articles.generated'
import {
  type DeleteArticleMutationVariables,
  useDeleteArticleMutation,
} from '@/graph/deleteArticle.generated'
import type { UpdateArticleInput, Article } from '@/graph/types.generated'
import {
  type UpdateArticleMutationVariables,
  useUpdateArticleMutation,
} from '@/graph/updateArticle.generated'
import type { ArticleEdgeData } from '@/types/pages/view'

type EditableArticleCardProps = {
  blogKey: string
  data: Article | null | undefined
  queryKey: ArticlesQueryVariables
}

export default function EditableArticleCard({ blogKey, data, queryKey }: EditableArticleCardProps) {
  const [editable, setEditable] = useState(false)
  const {
    isOpen: isDeleteDialogOpen,
    onOpen: onDeleteDialogOpen,
    onClose: onDeleteDialogClose,
  } = useDisclosure()

  if (!data) {
    return null
  }

  const onClickEdit = () => {
    setEditable(true)
  }

  const finishEditMode = () => {
    setEditable(false)
  }

  return (
    <>
      <Box borderWidth='1px' borderRadius='lg' p={5}>
        {editable ? (
          <></>
        ) : (
          <Flex justifyContent={'flex-end'}>
            <Menu>
              <MenuButton
                as={IconButton}
                aria-label='Options'
                icon={<HamburgerIcon />}
                variant='outline'
              />
              <MenuList>
                <MenuItem icon={<EditIcon />} onClick={onClickEdit}>
                  編集
                </MenuItem>
                <MenuItem icon={<DeleteIcon />} onClick={onDeleteDialogOpen}>
                  削除
                </MenuItem>
              </MenuList>
            </Menu>
          </Flex>
        )}
        {editable ? (
          <EditForm
            id={data.id}
            content={data.content ?? ''}
            onClose={finishEditMode}
            queryKey={queryKey}
          ></EditForm>
        ) : (
          <ArticleContent content={data.content ?? ''} />
        )}
        {editable ? <></> : <CreatedAtLink blogKey={blogKey} data={data} mt={2} />}
      </Box>
      <DeleteAlertDialog
        id={data.id}
        isOpen={isDeleteDialogOpen}
        onClose={onDeleteDialogClose}
        queryKey={queryKey}
      />
    </>
  )
}

type EditFormProps = {
  id: string
  content: string
  onClose: () => void
  queryKey: ArticlesQueryVariables
}

function EditForm({ id, content, onClose, queryKey }: EditFormProps) {
  const queryClient = useQueryClient()
  const toast = useToast()
  const { mutateAsync, isLoading } = useUpdateArticleMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '保存に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: (data) => {
      toast({
        title: '保存しました',
        status: 'success',
        isClosable: true,
      })
      const updater = (oldData: ArticlesQuery | undefined): ArticlesQuery | undefined => {
        if (!oldData || !oldData.articles || !oldData.articles.edges) {
          return oldData
        }
        const edges: any[] = oldData.articles.edges.map((article: ArticleEdgeData | null) =>
          article && article.node?.id === data.updateArticle?.id
            ? {
                ...article,
                node: {
                  ...article.node,
                  content: data.updateArticle?.content,
                  updatedAt: data.updateArticle?.updatedAt,
                },
              }
            : article,
        )
        const res: ArticlesQuery = {
          ...oldData,
          articles: {
            ...oldData.articles,
            edges,
          },
        }
        return res
      }
      queryClient.setQueryData(useArticlesQuery.getKey(queryKey), updater)
      onClose()
    },
  })
  const mutate = async (variables: UpdateArticleMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })
  const onSubmit = async (content: string) => {
    const input: UpdateArticleInput = {
      id,
      content: content,
    }
    await mutate({ input })
  }

  return (
    <ArticleForm update={onSubmit} isLoading={isLoading} defaultValue={content} onClose={onClose} />
  )
}

type DeleteAlertDialogProps = {
  id: string
  isOpen: boolean
  onClose: () => void
  queryKey: ArticlesQueryVariables
}

function DeleteAlertDialog({ id, isOpen, onClose, queryKey }: DeleteAlertDialogProps) {
  const cancelRef = useRef(null)
  const queryClient = useQueryClient()
  const toast = useToast()
  const { mutateAsync, isLoading } = useDeleteArticleMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '削除に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '削除しました',
        status: 'success',
        isClosable: true,
      })
      queryClient.invalidateQueries(useArticlesQuery.getKey(queryKey))
      onClose()
    },
  })

  const mutate = async (variables: DeleteArticleMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  const onClickDelete = () => {
    mutate({ input: id })
  }

  return (
    <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
      <AlertDialogOverlay>
        <AlertDialogContent>
          <AlertDialogHeader fontSize='lg' fontWeight='bold'>
            記事削除
          </AlertDialogHeader>
          <AlertDialogBody>
            記事を削除すると元に戻すことはできません。
            <br />
            削除しますか？
          </AlertDialogBody>
          <AlertDialogFooter>
            <Button
              colorScheme='red'
              onClick={onClickDelete}
              leftIcon={<DeleteIcon />}
              isLoading={isLoading}
              mr={3}
            >
              削除する
            </Button>
            <Button
              colorScheme={'blue'}
              variant='outline'
              ref={cancelRef}
              leftIcon={<CloseIcon />}
              onClick={onClose}
            >
              キャンセル
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  )
}
