'use client'

import {
  Tabs,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Text,
  useMediaQuery,
  VStack,
  Grid,
  Icon,
  Box,
  Button,
} from '@chakra-ui/react'
import { useEffect, useRef, useState } from 'react'
import { MdPerson } from 'react-icons/md'
import BlogLinks from '@/app/view/[blogKey]/BlogLinks'
import type { BlogLink } from '@/types/pages/view'

type TabsViewProps = {
  author: string
  description: string
  links: BlogLink[]
}

export default function TabsView({ author, description, links }: TabsViewProps) {
  if (description === '') {
    return (
      <Tabs>
        <TabList>
          <Tab><Text letterSpacing={'wider'}>プロフィール</Text></Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <ProfileView author={author} links={links} />
          </TabPanel>
        </TabPanels>
      </Tabs>
    )
  }

  return (
    <Tabs>
      <TabList>
        <Tab><Text letterSpacing={'wider'}>ブログ説明</Text></Tab>
        <Tab><Text letterSpacing={'wider'}>プロフィール</Text></Tab>
      </TabList>
      <TabPanels>
        <TabPanel>
          <DescriptionView description={description} />
        </TabPanel>
        <TabPanel>
          <ProfileView author={author} links={links} />
        </TabPanel>
      </TabPanels>
    </Tabs>
  )
}

type ProfileViewProps = {
  author: string
  links: BlogLink[]
}

function ProfileView({ author, links }: ProfileViewProps) {
  return (
    <VStack spacing={4} align='left'>
      <Grid templateColumns='2em 1fr' gap={4}>
        <Icon as={MdPerson} w={6} h={6} alignSelf={'start'} m={1} />
        <Text letterSpacing={'wider'}>{author}</Text>
      </Grid>
      <BlogLinks data={links} />
    </VStack>
  )
}

type DescriptionViewProps = {
  description: string
}

function DescriptionView({ description }: DescriptionViewProps) {
  const [isPC] = useMediaQuery('(min-width: 769px)')

  if (isPC) {
    return (
      <Text wordBreak={'break-word'} overflowWrap={'anywhere'} whiteSpace={'pre-wrap'} letterSpacing={'wider'}>
        {description}
      </Text>
    )
  }

  return (
    <ShowMore>
      <Text wordBreak={'break-word'} overflowWrap={'anywhere'} whiteSpace={'pre-wrap'} letterSpacing={'wider'}>
        {description}
      </Text>
    </ShowMore>
  )
}

const DEFAULT_HEIGHT = 80

type ShowMoreProps = {
  children: React.ReactNode
}

function ShowMore({ children }: ShowMoreProps) {
  const [showButton, setShowButton] = useState(false)
  const [showMore, setShowMore] = useState(false)
  const contentFieldRef = useRef<HTMLDivElement>(null)
  useEffect(() => {
    if (contentFieldRef.current && contentFieldRef.current.clientHeight >= DEFAULT_HEIGHT) {
      setShowButton(true)
    }
  }, [contentFieldRef])

  return (
    <>
      <Box
        ref={contentFieldRef}
        maxHeight={showMore ? 'fit-content' : `${DEFAULT_HEIGHT}px`}
        overflow='hidden'
      >
        {children}
      </Box>

      {showButton && (
        <>
          {!showMore ? (
            <Button onClick={() => setShowMore(!showMore)}>もっと見る</Button>
          ) : (
            <Button onClick={() => setShowMore(!showMore)}>閉じる</Button>
          )}
        </>
      )}
    </>
  )
}
