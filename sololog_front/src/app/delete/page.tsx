'use client'

import { Spinner } from '@chakra-ui/react'
import DeletePageContent from './DeletePageContent'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

export default function DeletePage() {
  const { isAuthChecking, user: currentUser } = useRequireLogin()

  if (isAuthChecking || !currentUser) {
    return <Spinner size='xl' />
  }

  return <DeletePageContent />
}
