'use client'

import { Spinner } from '@chakra-ui/react'

export default function ReauthenticatePageContent() {
  return (
    <>
      <Spinner size='xl' />
    </>
  )
}
