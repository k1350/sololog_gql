'use client'

import { useToast } from '@chakra-ui/react'
import type { UserCredential } from 'firebase/auth'
import { useRouter } from 'next/navigation'
import { useEffect, useCallback, useContext } from 'react'
import ReauthenticatePageContent from './ReauthenticatePageContent'
import { AuthContext } from '@/lib/auth/authContext'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

export default function ReauthenticatePage() {
  const key = 'delete/loading:uid'
  const authContext = useContext(AuthContext)
  const { isAuthChecking, user: currentUser } = useRequireLogin()
  const router = useRouter()
  const toast = useToast()

  const reauthenticate = useCallback(
    async (beforeUid: string, result: UserCredential) => {
      const uid = result.user.uid
      if (beforeUid !== uid) {
        toast({
          title:
            '元々ログインしていたアカウントと異なります。削除したいアカウントでログインし直してください。',
          status: 'error',
          isClosable: true,
        })
        await authContext.signOut().catch((error) => {
          alert(`ログアウトに失敗しました。エラーコード：${error.code}`)
        })
        router.push('/login')
        return
      }
      const credential = authContext.credentialFromResult(result)
      if (credential) {
        authContext
          .reauthenticateWithCredential(result.user, credential)
          .then(() => {
            router.push('/delete/delete')
          })
          .catch((error) => {
            alert(`再ログインに失敗しました。エラーコード：${error.code}`)
            router.push('/delete')
          })
        return
      }
      alert('再ログインに失敗しました')
      router.push('/delete')
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [router, toast],
  )

  useEffect(() => {
    if (isAuthChecking || !currentUser) {
      return
    }
    const login = async () => {
      const result = await authContext.getRedirectResult()
      if (!result?.user) {
        sessionStorage.setItem(key, currentUser.uid)
        await authContext.signInWithRedirect()
      } else {
        const uid = sessionStorage.getItem(key)
        sessionStorage.removeItem(key)
        reauthenticate(uid ?? '', result)
      }
    }
    login()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthChecking, currentUser, reauthenticate])

  return <ReauthenticatePageContent />
}
