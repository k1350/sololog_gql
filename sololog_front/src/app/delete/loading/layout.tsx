import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'アカウント削除 | ソロログ',
}

export default function Layout({ children }: { children: React.ReactElement }) {
  return children
}
