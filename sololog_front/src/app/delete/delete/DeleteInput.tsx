'use client'

import { DeleteIcon, CloseIcon } from '@chakra-ui/icons'
import { Button, ButtonGroup, Center, FormControl, FormLabel, Input } from '@chakra-ui/react'
import { useState } from 'react'
import NextLinkButton from '@/components/ui/NextLinkButton'

type DeleteInputProps = {
  onClickDelete: () => void
  isLoading: boolean
}

export default function DeleteInput({ onClickDelete, isLoading }: DeleteInputProps) {
  const [text, setText] = useState('')
  const [isDisabledDelete, setIsDisabledDelete] = useState(true)
  const onChangeInput = (value: string) => {
    setText(value)
    if (value === 'delete') {
      setIsDisabledDelete(false)
    } else {
      setIsDisabledDelete(true)
    }
  }
  return (
    <>
      <FormControl>
        <FormLabel>
          アカウントを削除する場合は &quot;delete&quot;
          と入力してから「削除する」ボタンを押してください。
        </FormLabel>
        <Input
          id='confirm-delete'
          value={text}
          onChange={(event) => onChangeInput(event.target.value)}
          placeholder='delete'
        />
      </FormControl>
      <Center>
        <ButtonGroup mt={2}>
          <Button
            colorScheme='red'
            mr={3}
            leftIcon={<DeleteIcon />}
            onClick={onClickDelete}
            isDisabled={isDisabledDelete || isLoading}
            isLoading={isLoading}
          >
            削除する
          </Button>
          <NextLinkButton href={'/home'} outline={true} leftIcon={<CloseIcon />}>
            キャンセル
          </NextLinkButton>
        </ButtonGroup>
      </Center>
    </>
  )
}
