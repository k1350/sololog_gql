'use client'

import { Alert, AlertDescription, AlertIcon, Box, Heading } from '@chakra-ui/react'
import DeleteInput from '@/app/delete/delete/DeleteInput'

type DeleteUserPageContentProps = {
  onClickDelete: () => void
  isLoading: boolean
}
export default function DeleteUserPageContent({
  onClickDelete,
  isLoading,
}: DeleteUserPageContentProps) {
  return (
    <>
      <Heading as='h2' mt={4} mb={4}>
        アカウント削除
      </Heading>
      <Box>
        <Alert status='error' mb={2}>
          <AlertIcon />
          <AlertDescription>
            アカウントを削除すると作成したすべてのブログおよび投稿が削除されます。
            <br />
            削除されたブログ・投稿を元に戻すことはできません。
          </AlertDescription>
        </Alert>
        <DeleteInput onClickDelete={onClickDelete} isLoading={isLoading} />
      </Box>
    </>
  )
}
