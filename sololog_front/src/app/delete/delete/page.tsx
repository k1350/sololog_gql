'use client'

import { Spinner, useToast } from '@chakra-ui/react'
import { useCallback, useContext } from 'react'
import DeleteUserPageContent from './DeleteUserPageContent'
import { CONSTANT } from '@/constants'
import { useDeleteUserMutation } from '@/graph/deleteUser.generated'
import { AuthContext } from '@/lib/auth/authContext'
import { useRequireLogin } from '@/lib/auth/currentUserContext'

export default function DeleteUserPage() {
  const authContext = useContext(AuthContext)
  const { isAuthChecking, user: currentUser } = useRequireLogin()
  const toast = useToast()
  const { mutateAsync, isLoading } = useDeleteUserMutation({
    onError: (e: any) => {
      console.error(e)
      const message =
        e.message === CONSTANT.SERVER_ERROR_MESSAGE.CANNOT_DELETE_ADMIN
          ? '唯一の管理者ユーザーのため削除できません'
          : '削除に失敗しました'
      toast({
        title: message,
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '削除しました',
        status: 'success',
        isClosable: true,
      })
      authContext.signOut().catch((error) => {
        alert(`ログアウトに失敗しました。エラーコード：${error.code}`)
      })
    },
  })

  const onClickDelete = useCallback(async () => {
    await mutateAsync({})
  }, [mutateAsync])

  if (isAuthChecking || !currentUser) {
    return <Spinner size='xl' />
  }

  return <DeleteUserPageContent onClickDelete={onClickDelete} isLoading={isLoading} />
}
