'use client'

import { CloseIcon, LockIcon } from '@chakra-ui/icons'
import { Text, Box, ButtonGroup, Center, Heading } from '@chakra-ui/react'
import NextLinkButton from '@/components/ui/NextLinkButton'

export default function DeletePageContent() {
  return (
    <>
      <Heading as='h2' mt={4} mb={4}>
        アカウント削除
      </Heading>
      <Box>
        <Text>
          アカウント削除前に再ログインが必要です。
          <br />
          現在ログインしているアカウントで再ログインしてください。
        </Text>
        <Center>
          <ButtonGroup mt={2}>
            <NextLinkButton href={'/delete/loading'} leftIcon={<LockIcon />}>
              再ログインする
            </NextLinkButton>
            <NextLinkButton href={'/home'} outline={true} leftIcon={<CloseIcon />}>
              キャンセル
            </NextLinkButton>
          </ButtonGroup>
        </Center>
      </Box>
    </>
  )
}
