'use client'

import { Heading, Spinner, Checkbox, FormControl, FormLabel, Switch } from '@chakra-ui/react'
import { useState } from 'react'
import type {
  UpdateConfigMutationVariables,
  UpdateConfigMutation,
} from '@/graph/updateConfig.generated'

type AdminPageContentProps = {
  isLoading: boolean
  isError: boolean
  mutate: (variables: UpdateConfigMutationVariables) => Promise<UpdateConfigMutation>
  isUpdating: boolean
  allowRegister?: boolean
}

export default function AdminPageContent({
  isLoading,
  isError,
  mutate,
  isUpdating,
  allowRegister,
}: AdminPageContentProps) {
  const [isEditing, setIsEditing] = useState(false)

  return (
    <>
      <Heading as='h2' mt={4} mb={4}>
        管理者メニュー
      </Heading>
      <>
        {isLoading ? (
          <Spinner size='xl' />
        ) : isError || allowRegister == undefined ? (
          <span>読み込みエラーが発生しました</span>
        ) : (
          <>
            <FormControl display='flex' alignItems='center'>
              <FormLabel cursor={isEditing ? 'pointer' : 'default'} htmlFor='allow-register'>
                新規ユーザー登録を許可する
              </FormLabel>
              <Switch
                id='allow-register'
                isChecked={allowRegister}
                isDisabled={!isEditing || isUpdating}
                onChange={async () => {
                  await mutate({
                    input: {
                      allowRegister: !allowRegister,
                    },
                  })
                  setIsEditing(false)
                }}
              />
            </FormControl>
            <Checkbox isChecked={isEditing} onChange={() => setIsEditing(!isEditing)}>
              設定を変更する
            </Checkbox>
          </>
        )}
      </>
    </>
  )
}
