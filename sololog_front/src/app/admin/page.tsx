'use client'

import { Spinner, useToast } from '@chakra-ui/react'
import { useQueryClient } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { useEffect } from 'react'
import AdminPageContent from './AdminPageContent'
import { CONSTANT } from '@/constants'
import { useConfigQuery } from '@/graph/config.generated'
import {
  type UpdateConfigMutationVariables,
  useUpdateConfigMutation,
} from '@/graph/updateConfig.generated'
import { useCurrentUser } from '@/lib/auth/currentUserContext'

export default function AdminPage() {
  const { isAuthChecking, user, role } = useCurrentUser()
  const { data, isLoading, isError } = useConfigQuery(undefined, {
    staleTime: Infinity,
  })
  const router = useRouter()

  useEffect(() => {
    if (isAuthChecking) {
      return
    }
    if (!user) {
      router.push('/login')
    }
    if (role !== CONSTANT.ROLE.ADMIN) {
      router.push('/home')
    }
  }, [isAuthChecking, user, role, router])

  const queryClient = useQueryClient()
  const toast = useToast()
  const { mutateAsync, isLoading: isUpdating } = useUpdateConfigMutation({
    onError: (e: any) => {
      console.error(e)
      toast({
        title: '保存に失敗しました',
        status: 'error',
        isClosable: true,
      })
    },
    onSuccess: () => {
      toast({
        title: '保存しました',
        status: 'success',
        isClosable: true,
      })
      queryClient.invalidateQueries(useConfigQuery.getKey())
    },
  })
  const mutate = async (variables: UpdateConfigMutationVariables) =>
    await mutateAsync({
      input: variables.input,
    })

  if (isAuthChecking || !user || role !== CONSTANT.ROLE.ADMIN) {
    return <Spinner size='xl' />
  }

  return (
    <AdminPageContent
      isLoading={isLoading}
      isError={isError}
      mutate={mutate}
      isUpdating={isUpdating}
      allowRegister={data?.config?.allowRegister}
    />
  )
}
