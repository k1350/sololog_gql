import type { Metadata } from 'next'
import { Layout as BasicLayout } from '@/layout/Layout'

export const metadata: Metadata = {
  title: '管理者メニュー | ソロログ',
}

export default function Layout({ children }: { children: React.ReactElement }) {
  return <BasicLayout>{children}</BasicLayout>
}
