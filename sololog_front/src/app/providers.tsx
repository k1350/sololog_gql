'use client'

import { ChakraProvider, ColorModeScript } from '@chakra-ui/react'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { AuthContext, authContextDefaultValue } from '@/lib/auth/authContext'
import { CurrentUserContext, currentUserContextDefaultValue } from '@/lib/auth/currentUserContext'
import theme from '@/theme'

export function Providers({ children }: { children: React.ReactNode }) {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        staleTime: 1000,
      },
    },
  })
  return (
    <QueryClientProvider client={queryClient}>
      <ChakraProvider theme={theme}>
        <AuthContext.Provider value={authContextDefaultValue}>
          <CurrentUserContext.Provider value={currentUserContextDefaultValue}>
            <ColorModeScript initialColorMode={theme.config.initialColorMode} />
            {children}
          </CurrentUserContext.Provider>
        </AuthContext.Provider>
      </ChakraProvider>
    </QueryClientProvider>
  )
}
