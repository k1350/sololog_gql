import { extendTheme, type ThemeConfig } from '@chakra-ui/react'
import '@fontsource/noto-sans-jp/400.css'
import '@fontsource/noto-sans-jp/700.css'

const config: ThemeConfig = {
  initialColorMode: 'system',
  useSystemColorMode: false,
}

const theme = extendTheme({
  config,
  fonts: {
    heading: `'Noto Sans JP', sans-serif`,
    body: `'Noto Sans JP', sans-serif`,
  },
  colors: {
    mutedTextColor: {
      lightMode: 'gray.500',
      darkMode: 'gray.400',
    },
    alertColor: {
      lightMode: 'red.600',
      darkMode: 'red.200',
    },
    linkColor: {
      lightMode: 'blue.600',
      darkMode: 'blue.200',
    },
    secondaryBgColor: {
      lightMode: 'gray.100',
      darkMode: 'gray.700',
    },
  },
})

export default theme
