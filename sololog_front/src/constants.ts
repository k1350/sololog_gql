export const CONSTANT = {
  ERROR_MESSAGE: {
    REQUIRED: '必須入力です',
    MIN_LENGTH: (min: number) => {
      return `${min} 文字以上で入力してください`
    },
    MAX_LENGTH: (max: number) => {
      return `${max} 文字以内で入力してください`
    },
    PASSWORD: '使用できない文字が含まれています',
    LINK_URL_REQUIRED: 'リンクタイトルを入力した場合は必須入力です',
    LINK_URL_REGEX: 'http:// または https:// から始まる URL を入力してください',
    LINK_NAME_REQUIRED: 'URL を入力した場合は必須入力です',
  },
  ARTICLES_LIMIT: 20,
  ARTICLE_MORE: '[[more]]',
  SERVER_ERROR_MESSAGE: {
    FORBIDDEN: 'Forbidden',
    BLOG_AUTHENTICATION_EXPIRED: 'Blog Authentication Expired',
    CANNOT_DELETE_ADMIN: 'This admin user cannot be deleted',
    REGISTER_NOT_ALLOWED: 'Register is not allowed',
  },
  ROLE: {
    ADMIN: 'ADMIN',
  },
}
