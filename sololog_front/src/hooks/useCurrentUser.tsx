'use client'

import type { User } from 'firebase/auth'
import { useState, useEffect, useContext } from 'react'
import { AuthContext } from '@/lib/auth/authContext'

export type UserType = User | null

function useCurrentUser() {
  const authContext = useContext(AuthContext)
  const [user, setUser] = useState<UserType>(null)
  const [isAuthChecking, setIsAuthChecking] = useState(true)
  const [role, setRole] = useState<string | undefined>(undefined)

  useEffect(() => {
    const authStateChanged = authContext.onAuthStateChanged(async (user) => {
      setUser(user)
      if (user) {
        const claims = (await user.getIdTokenResult())?.claims
        const role = claims?.['role']
        if (role) {
          setRole(role as string)
        }
      } else {
        setRole(undefined)
      }
      setIsAuthChecking(false)
    })
    return () => {
      authStateChanged()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return {
    user,
    isAuthChecking,
    role,
  }
}

export default useCurrentUser
