'use client'

import { useRouter } from 'next/navigation'
import { useEffect } from 'react'
import useCurrentUserBase from '@/hooks/useCurrentUser'

function useRequireLogin() {
  const { user, isAuthChecking } = useCurrentUserBase()
  const router = useRouter()

  useEffect(() => {
    if (isAuthChecking) {
      return
    }
    if (!user) {
      router.push('/login')
    }
  }, [isAuthChecking, user, router])

  return {
    user,
    isAuthChecking,
  }
}

export default useRequireLogin
