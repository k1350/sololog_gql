import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type UpdateBlogMutationVariables = Types.Exact<{
  input: Types.UpdateBlogInput;
}>;


export type UpdateBlogMutation = { __typename?: 'Mutation', updateBlog?: { __typename?: 'Blog', id: string } | null };


export const UpdateBlogDocument = `
    mutation updateBlog($input: UpdateBlogInput!) {
  updateBlog(input: $input) {
    id
  }
}
    `;
export const useUpdateBlogMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateBlogMutation, TError, UpdateBlogMutationVariables, TContext>) =>
    useMutation<UpdateBlogMutation, TError, UpdateBlogMutationVariables, TContext>(
      ['updateBlog'],
      (variables?: UpdateBlogMutationVariables) => requireAuthFetchData<UpdateBlogMutation, UpdateBlogMutationVariables>(UpdateBlogDocument, variables)(),
      options
    );