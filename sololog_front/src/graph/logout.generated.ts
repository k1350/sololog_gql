import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type LogoutMutationVariables = Types.Exact<{ [key: string]: never; }>;


export type LogoutMutation = { __typename?: 'Mutation', logout: boolean };


export const LogoutDocument = `
    mutation logout {
  logout
}
    `;
export const useLogoutMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<LogoutMutation, TError, LogoutMutationVariables, TContext>) =>
    useMutation<LogoutMutation, TError, LogoutMutationVariables, TContext>(
      ['logout'],
      (variables?: LogoutMutationVariables) => requireAuthFetchData<LogoutMutation, LogoutMutationVariables>(LogoutDocument, variables)(),
      options
    );