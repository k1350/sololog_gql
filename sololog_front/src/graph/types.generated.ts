export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Article = Node & {
  __typename?: 'Article';
  articleKey: Scalars['String'];
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  publishOption: PublishOption;
  updatedAt?: Maybe<Scalars['String']>;
  xJwtToken?: Maybe<Scalars['String']>;
};

export type ArticleByArticleKeyInput = {
  articleKey: Scalars['String'];
  blogKey: Scalars['String'];
  password?: InputMaybe<Scalars['String']>;
  xJwtToken?: InputMaybe<Scalars['String']>;
};

export type ArticleConnection = Connection & {
  __typename?: 'ArticleConnection';
  edges?: Maybe<Array<Maybe<ArticleEdge>>>;
  pageInfo: PageInfo;
  xJwtToken?: Maybe<Scalars['String']>;
};

export type ArticleEdge = Edge & {
  __typename?: 'ArticleEdge';
  cursor: Scalars['String'];
  node?: Maybe<Article>;
};

export type ArticlePaginationInput = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};

export type Blog = Node & {
  __typename?: 'Blog';
  author?: Maybe<Scalars['String']>;
  blogKey: Scalars['String'];
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  links?: Maybe<Array<BlogLink>>;
  name?: Maybe<Scalars['String']>;
  passwordHint: Scalars['String'];
  publishOption: PublishOption;
  updatedAt?: Maybe<Scalars['String']>;
  xJwtToken?: Maybe<Scalars['String']>;
};

export type BlogByBlogKeyInput = {
  blogKey: Scalars['String'];
  password?: InputMaybe<Scalars['String']>;
  xJwtToken?: InputMaybe<Scalars['String']>;
};

export type BlogLink = Node & {
  __typename?: 'BlogLink';
  id: Scalars['ID'];
  name: Scalars['String'];
  url: Scalars['String'];
};

export type BlogLinkInput = {
  name?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type Config = {
  __typename?: 'Config';
  allowRegister: Scalars['Boolean'];
};

export type Connection = {
  edges?: Maybe<Array<Maybe<Edge>>>;
  pageInfo: PageInfo;
};

export type CreateArticleInput = {
  blogId: Scalars['ID'];
  content: Scalars['String'];
};

export type CreateBlogInput = {
  author: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
  links?: InputMaybe<Array<BlogLinkInput>>;
  name: Scalars['String'];
  password?: InputMaybe<Scalars['String']>;
  passwordHint?: InputMaybe<Scalars['String']>;
  publishOption: PublishOption;
};

export type Edge = {
  cursor: Scalars['String'];
  node?: Maybe<Node>;
};

export type LoginInput = {
  token: Scalars['String'];
};

export type MasterParameter = {
  __typename?: 'MasterParameter';
  maxBlogLinks: Scalars['Int'];
  maxBlogs: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createArticle?: Maybe<Article>;
  createBlog?: Maybe<Blog>;
  deleteArticle: Scalars['Boolean'];
  deleteBlog: Scalars['Boolean'];
  deleteUser: Scalars['Boolean'];
  login: Scalars['Boolean'];
  logout: Scalars['Boolean'];
  updateArticle?: Maybe<Article>;
  updateBlog?: Maybe<Blog>;
  updateConfig?: Maybe<Config>;
};


export type MutationCreateArticleArgs = {
  input: CreateArticleInput;
};


export type MutationCreateBlogArgs = {
  input: CreateBlogInput;
};


export type MutationDeleteArticleArgs = {
  id: Scalars['ID'];
};


export type MutationDeleteBlogArgs = {
  id: Scalars['ID'];
};


export type MutationLoginArgs = {
  input: LoginInput;
};


export type MutationUpdateArticleArgs = {
  input: UpdateArticleInput;
};


export type MutationUpdateBlogArgs = {
  input: UpdateBlogInput;
};


export type MutationUpdateConfigArgs = {
  input: UpdateConfigInput;
};

export type Node = {
  id: Scalars['ID'];
};

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export enum PublishOption {
  Password = 'PASSWORD',
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type Query = {
  __typename?: 'Query';
  articleByArticleKey?: Maybe<Article>;
  articles?: Maybe<ArticleConnection>;
  blogByBlogKey?: Maybe<Blog>;
  blogById?: Maybe<Blog>;
  blogs: Array<Blog>;
  config?: Maybe<Config>;
  isAuthor: Scalars['Boolean'];
  masterParameter?: Maybe<MasterParameter>;
};


export type QueryArticleByArticleKeyArgs = {
  input: ArticleByArticleKeyInput;
};


export type QueryArticlesArgs = {
  input: BlogByBlogKeyInput;
  paginationInput: ArticlePaginationInput;
};


export type QueryBlogByBlogKeyArgs = {
  input: BlogByBlogKeyInput;
};


export type QueryBlogByIdArgs = {
  id: Scalars['ID'];
};


export type QueryIsAuthorArgs = {
  blogKey: Scalars['String'];
};

export type UpdateArticleInput = {
  content: Scalars['String'];
  id: Scalars['ID'];
};

export type UpdateBlogInput = {
  author?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  links?: InputMaybe<Array<UpdateBlogLinkInput>>;
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  passwordHint?: InputMaybe<Scalars['String']>;
  publishOption?: InputMaybe<PublishOption>;
};

export type UpdateBlogLinkInput = {
  id?: InputMaybe<Scalars['ID']>;
  name: Scalars['String'];
  url: Scalars['String'];
};

export type UpdateConfigInput = {
  allowRegister?: InputMaybe<Scalars['Boolean']>;
};
