import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type UpdateConfigMutationVariables = Types.Exact<{
  input: Types.UpdateConfigInput;
}>;


export type UpdateConfigMutation = { __typename?: 'Mutation', updateConfig?: { __typename?: 'Config', allowRegister: boolean } | null };


export const UpdateConfigDocument = `
    mutation updateConfig($input: UpdateConfigInput!) {
  updateConfig(input: $input) {
    allowRegister
  }
}
    `;
export const useUpdateConfigMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateConfigMutation, TError, UpdateConfigMutationVariables, TContext>) =>
    useMutation<UpdateConfigMutation, TError, UpdateConfigMutationVariables, TContext>(
      ['updateConfig'],
      (variables?: UpdateConfigMutationVariables) => requireAuthFetchData<UpdateConfigMutation, UpdateConfigMutationVariables>(UpdateConfigDocument, variables)(),
      options
    );