import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type DeleteArticleMutationVariables = Types.Exact<{
  input: Types.Scalars['ID'];
}>;


export type DeleteArticleMutation = { __typename?: 'Mutation', deleteArticle: boolean };


export const DeleteArticleDocument = `
    mutation deleteArticle($input: ID!) {
  deleteArticle(id: $input)
}
    `;
export const useDeleteArticleMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<DeleteArticleMutation, TError, DeleteArticleMutationVariables, TContext>) =>
    useMutation<DeleteArticleMutation, TError, DeleteArticleMutationVariables, TContext>(
      ['deleteArticle'],
      (variables?: DeleteArticleMutationVariables) => requireAuthFetchData<DeleteArticleMutation, DeleteArticleMutationVariables>(DeleteArticleDocument, variables)(),
      options
    );