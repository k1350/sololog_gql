import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type MasterParameterQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type MasterParameterQuery = { __typename?: 'Query', masterParameter?: { __typename?: 'MasterParameter', maxBlogs: number, maxBlogLinks: number } | null };


export const MasterParameterDocument = `
    query masterParameter {
  masterParameter {
    maxBlogs
    maxBlogLinks
  }
}
    `;
export const useMasterParameterQuery = <
      TData = MasterParameterQuery,
      TError = any
    >(
      variables?: MasterParameterQueryVariables,
      options?: UseQueryOptions<MasterParameterQuery, TError, TData>
    ) =>
    useQuery<MasterParameterQuery, TError, TData>(
      variables === undefined ? ['masterParameter'] : ['masterParameter', variables],
      requireAuthFetchData<MasterParameterQuery, MasterParameterQueryVariables>(MasterParameterDocument, variables),
      options
    );

useMasterParameterQuery.getKey = (variables?: MasterParameterQueryVariables) => variables === undefined ? ['masterParameter'] : ['masterParameter', variables];
;
