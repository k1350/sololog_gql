import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type CreateBlogMutationVariables = Types.Exact<{
  input: Types.CreateBlogInput;
}>;


export type CreateBlogMutation = { __typename?: 'Mutation', createBlog?: { __typename?: 'Blog', id: string } | null };


export const CreateBlogDocument = `
    mutation createBlog($input: CreateBlogInput!) {
  createBlog(input: $input) {
    id
  }
}
    `;
export const useCreateBlogMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<CreateBlogMutation, TError, CreateBlogMutationVariables, TContext>) =>
    useMutation<CreateBlogMutation, TError, CreateBlogMutationVariables, TContext>(
      ['createBlog'],
      (variables?: CreateBlogMutationVariables) => requireAuthFetchData<CreateBlogMutation, CreateBlogMutationVariables>(CreateBlogDocument, variables)(),
      options
    );