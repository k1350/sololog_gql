import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type ConfigQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type ConfigQuery = { __typename?: 'Query', config?: { __typename?: 'Config', allowRegister: boolean } | null };


export const ConfigDocument = `
    query config {
  config {
    allowRegister
  }
}
    `;
export const useConfigQuery = <
      TData = ConfigQuery,
      TError = any
    >(
      variables?: ConfigQueryVariables,
      options?: UseQueryOptions<ConfigQuery, TError, TData>
    ) =>
    useQuery<ConfigQuery, TError, TData>(
      variables === undefined ? ['config'] : ['config', variables],
      requireAuthFetchData<ConfigQuery, ConfigQueryVariables>(ConfigDocument, variables),
      options
    );

useConfigQuery.getKey = (variables?: ConfigQueryVariables) => variables === undefined ? ['config'] : ['config', variables];
;
