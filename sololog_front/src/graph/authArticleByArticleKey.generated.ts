import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type AuthArticleByArticleKeyQueryVariables = Types.Exact<{
  input: Types.ArticleByArticleKeyInput;
}>;


export type AuthArticleByArticleKeyQuery = { __typename?: 'Query', articleByArticleKey?: { __typename?: 'Article', content?: string | null, publishOption: Types.PublishOption, createdAt?: string | null, updatedAt?: string | null } | null };


export const AuthArticleByArticleKeyDocument = `
    query authArticleByArticleKey($input: ArticleByArticleKeyInput!) {
  articleByArticleKey(input: $input) {
    content
    publishOption
    createdAt
    updatedAt
  }
}
    `;
export const useAuthArticleByArticleKeyQuery = <
      TData = AuthArticleByArticleKeyQuery,
      TError = any
    >(
      variables: AuthArticleByArticleKeyQueryVariables,
      options?: UseQueryOptions<AuthArticleByArticleKeyQuery, TError, TData>
    ) =>
    useQuery<AuthArticleByArticleKeyQuery, TError, TData>(
      ['authArticleByArticleKey', variables],
      requireAuthFetchData<AuthArticleByArticleKeyQuery, AuthArticleByArticleKeyQueryVariables>(AuthArticleByArticleKeyDocument, variables),
      options
    );

useAuthArticleByArticleKeyQuery.getKey = (variables: AuthArticleByArticleKeyQueryVariables) => ['authArticleByArticleKey', variables];
;
