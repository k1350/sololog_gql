import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type MaxBlogsQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type MaxBlogsQuery = { __typename?: 'Query', masterParameter?: { __typename?: 'MasterParameter', maxBlogs: number } | null };


export const MaxBlogsDocument = `
    query maxBlogs {
  masterParameter {
    maxBlogs
  }
}
    `;
export const useMaxBlogsQuery = <
      TData = MaxBlogsQuery,
      TError = any
    >(
      variables?: MaxBlogsQueryVariables,
      options?: UseQueryOptions<MaxBlogsQuery, TError, TData>
    ) =>
    useQuery<MaxBlogsQuery, TError, TData>(
      variables === undefined ? ['maxBlogs'] : ['maxBlogs', variables],
      requireAuthFetchData<MaxBlogsQuery, MaxBlogsQueryVariables>(MaxBlogsDocument, variables),
      options
    );

useMaxBlogsQuery.getKey = (variables?: MaxBlogsQueryVariables) => variables === undefined ? ['maxBlogs'] : ['maxBlogs', variables];
;
