import * as Types from '../types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { guestFetchData } from '../Fetchers';
export type GuestArticlesQueryVariables = Types.Exact<{
  input: Types.BlogByBlogKeyInput;
  paginationInput: Types.ArticlePaginationInput;
}>;


export type GuestArticlesQuery = { __typename?: 'Query', articles?: { __typename?: 'ArticleConnection', xJwtToken?: string | null, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'ArticleEdge', cursor: string, node?: { __typename?: 'Article', id: string, articleKey: string, content?: string | null, publishOption: Types.PublishOption, createdAt?: string | null, updatedAt?: string | null } | null } | null> | null } | null };


export const GuestArticlesDocument = `
    query guestArticles($input: BlogByBlogKeyInput!, $paginationInput: ArticlePaginationInput!) {
  articles(input: $input, paginationInput: $paginationInput) {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      cursor
      node {
        id
        articleKey
        content
        publishOption
        createdAt
        updatedAt
      }
    }
    xJwtToken
  }
}
    `;
export const useGuestArticlesQuery = <
      TData = GuestArticlesQuery,
      TError = any
    >(
      variables: GuestArticlesQueryVariables,
      options?: UseQueryOptions<GuestArticlesQuery, TError, TData>
    ) =>
    useQuery<GuestArticlesQuery, TError, TData>(
      ['guestArticles', variables],
      guestFetchData<GuestArticlesQuery, GuestArticlesQueryVariables>(GuestArticlesDocument, variables),
      options
    );

useGuestArticlesQuery.getKey = (variables: GuestArticlesQueryVariables) => ['guestArticles', variables];
;
