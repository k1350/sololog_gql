import * as Types from '../types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { guestFetchData } from '../Fetchers';
export type GuestBlogByBlogKeyQueryVariables = Types.Exact<{
  input: Types.BlogByBlogKeyInput;
}>;


export type GuestBlogByBlogKeyQuery = { __typename?: 'Query', blogByBlogKey?: { __typename?: 'Blog', blogKey: string, author?: string | null, name?: string | null, description?: string | null, publishOption: Types.PublishOption, passwordHint: string, xJwtToken?: string | null, links?: Array<{ __typename?: 'BlogLink', id: string, name: string, url: string }> | null } | null };


export const GuestBlogByBlogKeyDocument = `
    query guestBlogByBlogKey($input: BlogByBlogKeyInput!) {
  blogByBlogKey(input: $input) {
    blogKey
    author
    name
    description
    publishOption
    passwordHint
    links {
      id
      name
      url
    }
    xJwtToken
  }
}
    `;
export const useGuestBlogByBlogKeyQuery = <
      TData = GuestBlogByBlogKeyQuery,
      TError = any
    >(
      variables: GuestBlogByBlogKeyQueryVariables,
      options?: UseQueryOptions<GuestBlogByBlogKeyQuery, TError, TData>
    ) =>
    useQuery<GuestBlogByBlogKeyQuery, TError, TData>(
      ['guestBlogByBlogKey', variables],
      guestFetchData<GuestBlogByBlogKeyQuery, GuestBlogByBlogKeyQueryVariables>(GuestBlogByBlogKeyDocument, variables),
      options
    );

useGuestBlogByBlogKeyQuery.getKey = (variables: GuestBlogByBlogKeyQueryVariables) => ['guestBlogByBlogKey', variables];
;
