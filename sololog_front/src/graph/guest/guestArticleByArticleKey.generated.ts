import * as Types from '../types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { guestFetchData } from '../Fetchers';
export type GuestArticleByArticleKeyQueryVariables = Types.Exact<{
  input: Types.ArticleByArticleKeyInput;
}>;


export type GuestArticleByArticleKeyQuery = { __typename?: 'Query', articleByArticleKey?: { __typename?: 'Article', content?: string | null, createdAt?: string | null, updatedAt?: string | null, xJwtToken?: string | null } | null };


export const GuestArticleByArticleKeyDocument = `
    query guestArticleByArticleKey($input: ArticleByArticleKeyInput!) {
  articleByArticleKey(input: $input) {
    content
    createdAt
    updatedAt
    xJwtToken
  }
}
    `;
export const useGuestArticleByArticleKeyQuery = <
      TData = GuestArticleByArticleKeyQuery,
      TError = any
    >(
      variables: GuestArticleByArticleKeyQueryVariables,
      options?: UseQueryOptions<GuestArticleByArticleKeyQuery, TError, TData>
    ) =>
    useQuery<GuestArticleByArticleKeyQuery, TError, TData>(
      ['guestArticleByArticleKey', variables],
      guestFetchData<GuestArticleByArticleKeyQuery, GuestArticleByArticleKeyQueryVariables>(GuestArticleByArticleKeyDocument, variables),
      options
    );

useGuestArticleByArticleKeyQuery.getKey = (variables: GuestArticleByArticleKeyQueryVariables) => ['guestArticleByArticleKey', variables];
;
