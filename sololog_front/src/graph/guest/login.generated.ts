import * as Types from '../types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { guestFetchData } from '../Fetchers';
export type LoginMutationVariables = Types.Exact<{
  input: Types.LoginInput;
}>;


export type LoginMutation = { __typename?: 'Mutation', login: boolean };


export const LoginDocument = `
    mutation login($input: LoginInput!) {
  login(input: $input)
}
    `;
export const useLoginMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<LoginMutation, TError, LoginMutationVariables, TContext>) =>
    useMutation<LoginMutation, TError, LoginMutationVariables, TContext>(
      ['login'],
      (variables?: LoginMutationVariables) => guestFetchData<LoginMutation, LoginMutationVariables>(LoginDocument, variables)(),
      options
    );