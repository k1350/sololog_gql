import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type PageBlogsNewQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type PageBlogsNewQuery = { __typename?: 'Query', blogs: Array<{ __typename?: 'Blog', id: string }> };


export const PageBlogsNewDocument = `
    query pageBlogsNew {
  blogs {
    id
  }
}
    `;
export const usePageBlogsNewQuery = <
      TData = PageBlogsNewQuery,
      TError = any
    >(
      variables?: PageBlogsNewQueryVariables,
      options?: UseQueryOptions<PageBlogsNewQuery, TError, TData>
    ) =>
    useQuery<PageBlogsNewQuery, TError, TData>(
      variables === undefined ? ['pageBlogsNew'] : ['pageBlogsNew', variables],
      requireAuthFetchData<PageBlogsNewQuery, PageBlogsNewQueryVariables>(PageBlogsNewDocument, variables),
      options
    );

usePageBlogsNewQuery.getKey = (variables?: PageBlogsNewQueryVariables) => variables === undefined ? ['pageBlogsNew'] : ['pageBlogsNew', variables];
;
