import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type CreateArticleMutationVariables = Types.Exact<{
  input: Types.CreateArticleInput;
}>;


export type CreateArticleMutation = { __typename?: 'Mutation', createArticle?: { __typename?: 'Article', id: string } | null };


export const CreateArticleDocument = `
    mutation createArticle($input: CreateArticleInput!) {
  createArticle(input: $input) {
    id
  }
}
    `;
export const useCreateArticleMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<CreateArticleMutation, TError, CreateArticleMutationVariables, TContext>) =>
    useMutation<CreateArticleMutation, TError, CreateArticleMutationVariables, TContext>(
      ['createArticle'],
      (variables?: CreateArticleMutationVariables) => requireAuthFetchData<CreateArticleMutation, CreateArticleMutationVariables>(CreateArticleDocument, variables)(),
      options
    );