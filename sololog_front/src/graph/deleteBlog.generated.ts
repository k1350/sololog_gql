import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type DeleteBlogMutationVariables = Types.Exact<{
  input: Types.Scalars['ID'];
}>;


export type DeleteBlogMutation = { __typename?: 'Mutation', deleteBlog: boolean };


export const DeleteBlogDocument = `
    mutation deleteBlog($input: ID!) {
  deleteBlog(id: $input)
}
    `;
export const useDeleteBlogMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<DeleteBlogMutation, TError, DeleteBlogMutationVariables, TContext>) =>
    useMutation<DeleteBlogMutation, TError, DeleteBlogMutationVariables, TContext>(
      ['deleteBlog'],
      (variables?: DeleteBlogMutationVariables) => requireAuthFetchData<DeleteBlogMutation, DeleteBlogMutationVariables>(DeleteBlogDocument, variables)(),
      options
    );