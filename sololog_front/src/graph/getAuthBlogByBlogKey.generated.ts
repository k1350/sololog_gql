import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type AuthBlogByBlogKeyQueryVariables = Types.Exact<{
  input: Types.BlogByBlogKeyInput;
}>;


export type AuthBlogByBlogKeyQuery = { __typename?: 'Query', blogByBlogKey?: { __typename?: 'Blog', id: string, blogKey: string, author?: string | null, name?: string | null, description?: string | null, publishOption: Types.PublishOption, passwordHint: string, links?: Array<{ __typename?: 'BlogLink', id: string, name: string, url: string }> | null } | null };


export const AuthBlogByBlogKeyDocument = `
    query authBlogByBlogKey($input: BlogByBlogKeyInput!) {
  blogByBlogKey(input: $input) {
    id
    blogKey
    author
    name
    description
    publishOption
    passwordHint
    links {
      id
      name
      url
    }
  }
}
    `;
export const useAuthBlogByBlogKeyQuery = <
      TData = AuthBlogByBlogKeyQuery,
      TError = any
    >(
      variables: AuthBlogByBlogKeyQueryVariables,
      options?: UseQueryOptions<AuthBlogByBlogKeyQuery, TError, TData>
    ) =>
    useQuery<AuthBlogByBlogKeyQuery, TError, TData>(
      ['authBlogByBlogKey', variables],
      requireAuthFetchData<AuthBlogByBlogKeyQuery, AuthBlogByBlogKeyQueryVariables>(AuthBlogByBlogKeyDocument, variables),
      options
    );

useAuthBlogByBlogKeyQuery.getKey = (variables: AuthBlogByBlogKeyQueryVariables) => ['authBlogByBlogKey', variables];
;
