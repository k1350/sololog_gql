import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type ArticlesQueryVariables = Types.Exact<{
  input: Types.BlogByBlogKeyInput;
  paginationInput: Types.ArticlePaginationInput;
  blogKey: Types.Scalars['String'];
}>;


export type ArticlesQuery = { __typename?: 'Query', isAuthor: boolean, articles?: { __typename?: 'ArticleConnection', pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'ArticleEdge', cursor: string, node?: { __typename?: 'Article', id: string, articleKey: string, content?: string | null, publishOption: Types.PublishOption, createdAt?: string | null, updatedAt?: string | null } | null } | null> | null } | null };


export const ArticlesDocument = `
    query articles($input: BlogByBlogKeyInput!, $paginationInput: ArticlePaginationInput!, $blogKey: String!) {
  articles(input: $input, paginationInput: $paginationInput) {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      cursor
      node {
        id
        articleKey
        content
        publishOption
        createdAt
        updatedAt
      }
    }
  }
  isAuthor(blogKey: $blogKey)
}
    `;
export const useArticlesQuery = <
      TData = ArticlesQuery,
      TError = any
    >(
      variables: ArticlesQueryVariables,
      options?: UseQueryOptions<ArticlesQuery, TError, TData>
    ) =>
    useQuery<ArticlesQuery, TError, TData>(
      ['articles', variables],
      requireAuthFetchData<ArticlesQuery, ArticlesQueryVariables>(ArticlesDocument, variables),
      options
    );

useArticlesQuery.getKey = (variables: ArticlesQueryVariables) => ['articles', variables];
;
