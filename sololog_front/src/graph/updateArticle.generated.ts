import * as Types from './types.generated';

import { useMutation, UseMutationOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type UpdateArticleMutationVariables = Types.Exact<{
  input: Types.UpdateArticleInput;
}>;


export type UpdateArticleMutation = { __typename?: 'Mutation', updateArticle?: { __typename?: 'Article', id: string, content?: string | null, publishOption: Types.PublishOption, createdAt?: string | null, updatedAt?: string | null } | null };


export const UpdateArticleDocument = `
    mutation updateArticle($input: UpdateArticleInput!) {
  updateArticle(input: $input) {
    id
    content
    publishOption
    createdAt
    updatedAt
  }
}
    `;
export const useUpdateArticleMutation = <
      TError = any,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateArticleMutation, TError, UpdateArticleMutationVariables, TContext>) =>
    useMutation<UpdateArticleMutation, TError, UpdateArticleMutationVariables, TContext>(
      ['updateArticle'],
      (variables?: UpdateArticleMutationVariables) => requireAuthFetchData<UpdateArticleMutation, UpdateArticleMutationVariables>(UpdateArticleDocument, variables)(),
      options
    );