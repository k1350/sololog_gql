import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type HomeQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type HomeQuery = { __typename?: 'Query', blogs: Array<{ __typename?: 'Blog', id: string, blogKey: string, name?: string | null, description?: string | null, publishOption: Types.PublishOption }> };


export const HomeDocument = `
    query home {
  blogs {
    id
    blogKey
    name
    description
    publishOption
  }
}
    `;
export const useHomeQuery = <
      TData = HomeQuery,
      TError = any
    >(
      variables?: HomeQueryVariables,
      options?: UseQueryOptions<HomeQuery, TError, TData>
    ) =>
    useQuery<HomeQuery, TError, TData>(
      variables === undefined ? ['home'] : ['home', variables],
      requireAuthFetchData<HomeQuery, HomeQueryVariables>(HomeDocument, variables),
      options
    );

useHomeQuery.getKey = (variables?: HomeQueryVariables) => variables === undefined ? ['home'] : ['home', variables];
;
