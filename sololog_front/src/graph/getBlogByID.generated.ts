import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type GetBlogByIdQueryVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;


export type GetBlogByIdQuery = { __typename?: 'Query', blogById?: { __typename?: 'Blog', id: string, blogKey: string, author?: string | null, name?: string | null, description?: string | null, publishOption: Types.PublishOption, passwordHint: string, createdAt?: string | null, updatedAt?: string | null, links?: Array<{ __typename?: 'BlogLink', id: string, name: string, url: string }> | null } | null };


export const GetBlogByIdDocument = `
    query getBlogByID($id: ID!) {
  blogById(id: $id) {
    id
    blogKey
    author
    name
    description
    publishOption
    passwordHint
    createdAt
    updatedAt
    links {
      id
      name
      url
    }
  }
}
    `;
export const useGetBlogByIdQuery = <
      TData = GetBlogByIdQuery,
      TError = any
    >(
      variables: GetBlogByIdQueryVariables,
      options?: UseQueryOptions<GetBlogByIdQuery, TError, TData>
    ) =>
    useQuery<GetBlogByIdQuery, TError, TData>(
      ['getBlogByID', variables],
      requireAuthFetchData<GetBlogByIdQuery, GetBlogByIdQueryVariables>(GetBlogByIdDocument, variables),
      options
    );

useGetBlogByIdQuery.getKey = (variables: GetBlogByIdQueryVariables) => ['getBlogByID', variables];
;
