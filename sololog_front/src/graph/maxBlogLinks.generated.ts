import * as Types from './types.generated';

import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import { requireAuthFetchData } from './Fetchers';
export type MaxBlogLinksQueryVariables = Types.Exact<{ [key: string]: never; }>;


export type MaxBlogLinksQuery = { __typename?: 'Query', masterParameter?: { __typename?: 'MasterParameter', maxBlogLinks: number } | null };


export const MaxBlogLinksDocument = `
    query maxBlogLinks {
  masterParameter {
    maxBlogLinks
  }
}
    `;
export const useMaxBlogLinksQuery = <
      TData = MaxBlogLinksQuery,
      TError = any
    >(
      variables?: MaxBlogLinksQueryVariables,
      options?: UseQueryOptions<MaxBlogLinksQuery, TError, TData>
    ) =>
    useQuery<MaxBlogLinksQuery, TError, TData>(
      variables === undefined ? ['maxBlogLinks'] : ['maxBlogLinks', variables],
      requireAuthFetchData<MaxBlogLinksQuery, MaxBlogLinksQueryVariables>(MaxBlogLinksDocument, variables),
      options
    );

useMaxBlogLinksQuery.getKey = (variables?: MaxBlogLinksQueryVariables) => variables === undefined ? ['maxBlogLinks'] : ['maxBlogLinks', variables];
;
