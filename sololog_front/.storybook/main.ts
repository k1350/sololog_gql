import type { StorybookConfig } from '@storybook/nextjs'
const config: StorybookConfig = {
  stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    '@chakra-ui/storybook-addon',
  ],
  framework: {
    name: '@storybook/nextjs',
    options: {},
  },
  docs: {
    autodocs: 'tag',
  },
  staticDirs: ["../public"],
}
export default {
  ...config,
  features: {
    ...config.features,
    emotionAlias: false,
  },
  // Fetcher 内で Firebase の初期化をしているのがエラーになるのでファイルごと差し替える
  webpackFinal: async (config) => {
    const c = {
      ...config
    };
    c.resolve.alias['./Fetchers'] = require.resolve('../__mocks__/Fetchers.ts');
    c.resolve.alias['../Fetchers'] = require.resolve('../__mocks__/Fetchers.ts');
    return c;
  },
}
