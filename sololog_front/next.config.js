const { PHASE_PRODUCTION_BUILD } = require('next/constants')
const { PHASE_PRODUCTION_SERVER } = require('next/constants')
const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')

module.exports = (phase) => {
  let contentSecurityPolicy = ''
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    contentSecurityPolicy = `
      default-src 'none';
      img-src 'self' data:;
      font-src 'self';
      frame-src https://${process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN}/;
      style-src 'self' 'unsafe-inline';
      script-src 'self' 'unsafe-eval' 'unsafe-inline' https://apis.google.com/;
      connect-src 'self' https://securetoken.googleapis.com/ https://identitytoolkit.googleapis.com/ ${process.env.NEXT_PUBLIC_API_BASE_URL} ${process.env.NEXT_PUBLIC_FRONT_WS};
      prefetch-src 'self';
    `
  } else if (phase === PHASE_PRODUCTION_BUILD || phase === PHASE_PRODUCTION_SERVER) {
    contentSecurityPolicy = `
      default-src 'none';
      img-src 'self' data:;
      font-src 'self';
      frame-src 'self' https://${process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN}/;
      style-src 'self' 'unsafe-inline';
      script-src 'self' 'unsafe-inline' https://apis.google.com/;
      connect-src 'self' https://securetoken.googleapis.com/ https://identitytoolkit.googleapis.com/ https://www.googleapis.com/ ${process.env.NEXT_PUBLIC_API_BASE_URL} ${process.env.NEXT_PUBLIC_FRONT_WS};
      prefetch-src 'self';
    `
  }
  const securityHeaders = [
    {
      key: 'X-Content-Type-Options',
      value: 'nosniff',
    },
    {
      key: 'Content-Security-Policy',
      value: contentSecurityPolicy.replace(/\s{2,}/g, ' ').trim(),
    },
  ]

  /** @type {import('next').NextConfig} */
  const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    async headers() {
      return [
        {
          // Apply these headers to all routes in your application.
          source: '/:path*',
          headers: securityHeaders,
        },
      ]
    },
    async redirects() {
      return [
        {
          source: '/',
          destination: '/login',
          permanent: false,
        },
      ]
    },
    async rewrites() {
      return [
        {
          source: "/__/auth/:slug*",
          destination: `https://${process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN}/__/auth/:slug*`,
        },
      ];
    },
  }

  return nextConfig
}
