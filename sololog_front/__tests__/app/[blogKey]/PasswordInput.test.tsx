import { render, screen, fireEvent } from '@testing-library/react'
import PasswordInput from '@/app/view/[blogKey]/PasswordInput'

test('パスワード入力欄にセットした値が入力されている', () => {
  let text = 'test'
  let isSend = false
  const dummy = {
    onChangeInput: (value: string) => {
      text = value
    },
    onClickSend: () => {
      isSend = true
    },
  }

  render(
    <PasswordInput
      text={text}
      onChangeInput={dummy.onChangeInput}
      onClickSend={dummy.onClickSend}
    />,
  )
  const input = screen.getByLabelText('パスワードを入力してください')
  expect(input).toHaveValue('test')
})

test('パスワード入力欄に値を入力すると onChangeInput が実行される', () => {
  let text = ''
  let isSend = false
  const dummy = {
    onChangeInput: (value: string) => {
      text = value
    },
    onClickSend: () => {
      isSend = true
    },
  }

  const spy = jest.spyOn(dummy, 'onChangeInput')
  render(
    <PasswordInput
      text={text}
      onChangeInput={dummy.onChangeInput}
      onClickSend={dummy.onClickSend}
    />,
  )
  const input = screen.getByLabelText('パスワードを入力してください')
  fireEvent.change(input, { target: { value: 'test' } })
  expect(spy).toHaveBeenCalled()
})

test('パスワード入力欄にセットされている値が空のとき、送信するボタンを押下できない', () => {
  let text = ''
  let isSend = false
  const dummy = {
    onChangeInput: (value: string) => {
      text = value
    },
    onClickSend: () => {
      isSend = true
    },
  }

  render(
    <PasswordInput
      text={text}
      onChangeInput={dummy.onChangeInput}
      onClickSend={dummy.onClickSend}
    />,
  )
  const button = screen.getByText('送信する').closest('button')
  expect(button).toHaveAttribute('disabled')
})

test('パスワード入力欄にセットされている値が空でないとき、送信するボタンを押下できる', () => {
  let text = 'a'
  let isSend = false
  const dummy = {
    onChangeInput: (value: string) => {
      text = value
    },
    onClickSend: () => {
      isSend = true
    },
  }

  render(
    <PasswordInput
      text={text}
      onChangeInput={dummy.onChangeInput}
      onClickSend={dummy.onClickSend}
    />,
  )
  const button = screen.getByText('送信する').closest('button')
  expect(button).not.toHaveAttribute('disabled')
})

test('送信するボタンを押下したとき onClickSend が実行される', () => {
  let text = 'a'
  let isSend = false
  const dummy = {
    onChangeInput: (value: string) => {
      text = value
    },
    onClickSend: () => {
      isSend = true
    },
  }
  const spy = jest.spyOn(dummy, 'onClickSend')
  render(
    <PasswordInput
      text={text}
      onChangeInput={dummy.onChangeInput}
      onClickSend={dummy.onClickSend}
    />,
  )
  const button = screen.getByText('送信する').closest('button')
  if (button) {
    fireEvent.click(button)
    expect(spy).toHaveBeenCalled()
  }
})
