import { render, screen } from '@testing-library/react'
import Pagination from '@/app/view/[blogKey]/Pagination'

jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: '',
    }
  },
}))

test('hasNextPage = true のとき、次へボタンを押下できる', () => {
  render(<Pagination hasNextPage={true} hasPreviousPage={false} startCursor='a' endCursor='b' />)
  const button = screen.getByText('次へ').closest('button')
  expect(button).not.toHaveAttribute('disabled')
})

test('hasNextPage = false のとき、次へボタンを押下できない', () => {
  render(<Pagination hasNextPage={false} hasPreviousPage={false} startCursor='a' endCursor='b' />)
  const button = screen.getByText('次へ').closest('button')
  expect(button).toHaveAttribute('disabled')
})

test('hasPreviousPage = true のとき、前へボタンを押下できる', () => {
  render(<Pagination hasNextPage={false} hasPreviousPage={true} startCursor='a' endCursor='b' />)
  const button = screen.getByText('前へ').closest('button')
  expect(button).not.toHaveAttribute('disabled')
})

test('hasPreviousPage = false のとき、前へボタンを押下できない', () => {
  render(<Pagination hasNextPage={false} hasPreviousPage={false} startCursor='a' endCursor='b' />)
  const button = screen.getByText('前へ').closest('button')
  expect(button).toHaveAttribute('disabled')
})
