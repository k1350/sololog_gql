import { render } from '@testing-library/react'
import ArticleCard from '@/app/view/[blogKey]/ArticleCard'

test('データが無いとき何も描画されない', () => {
  const { container } = render(<ArticleCard blogKey='test' data={null} />)
  expect(container.firstChild).toBeNull()
})

