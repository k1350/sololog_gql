import { render, screen } from '@testing-library/react'
import ArticleList from '@/app/view/[blogKey]/ArticleList'

jest.mock('@/app/view/[blogKey]/EditableArticleCard', () => () => {
  return <></>
})

test('データが無いとき「記事がありません」と描画される', () => {
  render(<ArticleList blogKey='test' data={null} canEdit={false} />)
  expect(screen.getByText('記事がありません')).toBeInTheDocument()
})

test('データが 0 件のとき「記事がありません」と描画される', () => {
  render(<ArticleList blogKey='test' data={[]} canEdit={false} />)
  expect(screen.getByText('記事がありません')).toBeInTheDocument()
})

test('データが 1 件以上あるとき「記事がありません」と描画されない', () => {
  render(
    <ArticleList
      blogKey='test'
      data={[
        {
          cursor: '1',
          node: {
            id: '1',
            articleKey: 'aaaa',
            content: 'test',
            createdAt: '2022/01/01 00:00:00',
            updatedAt: '2022/01/01 00:00:00',
          },
        },
      ]}
      canEdit={false}
    />,
  )
  expect(screen.queryByText('記事がありません')).not.toBeInTheDocument()
})
