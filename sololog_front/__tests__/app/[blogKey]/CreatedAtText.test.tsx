import { render, screen } from '@testing-library/react'
import CreatedAtText from '@/app/view/[blogKey]/CreatedAtText'

test('データの作成日時と更新日時が異なるとき「（編集済）」と描画される', () => {
  render(<CreatedAtText createdAt='2022/01/01 00:00:00' updatedAt='2022/01/01 01:00:00' />)
  expect(screen.getByText('（編集済）')).toBeInTheDocument()
})

test('データの作成日時と更新日時が同一のとき「（編集済）」と描画されない', () => {
  render(<CreatedAtText createdAt='2022/01/01 00:00:00' updatedAt='2022/01/01 00:00:00' />)
  expect(screen.queryByText('（編集済）')).not.toBeInTheDocument()
})
