import { render, screen } from '@testing-library/react'
import BlogLinks from '@/app/view/[blogKey]/BlogLinks'

test('リンクが 0 件のとき何も描画されない', () => {
  const { container } = render(<BlogLinks data={[]} />)
  expect(container.firstChild).toBeNull()
})

test('リンクにリンクテキストが設定されているとき、リンクテキストでリンクが描画される', () => {
  render(<BlogLinks data={[{ id: '1', name: 'test', url: 'http://example.com' }]} />)
  const text = screen.getByText('test')
  expect(text).toBeInTheDocument()
  expect(text.closest('a')).toHaveAttribute('href', 'http://example.com')
})

test('リンクにリンクテキストが設定されていないとき、URL でリンクが描画される', () => {
  render(<BlogLinks data={[{ id: '1', name: '', url: 'http://example.com' }]} />)
  const text = screen.getByText('http://example.com')
  expect(text).toBeInTheDocument()
  expect(text.closest('a')).toHaveAttribute('href', 'http://example.com')
})

test('リンクが全件描画される', () => {
  render(
    <BlogLinks
      data={[
        { id: '1', name: 'test1', url: 'http://example.com/1' },
        { id: '2', name: 'test2', url: 'http://example.com/2' },
        { id: '3', name: 'test3', url: 'http://example.com/3' },
      ]}
    />,
  )

  expect(screen.getByText('test1')).toBeInTheDocument()
  expect(screen.getByText('test2')).toBeInTheDocument()
  expect(screen.getByText('test3')).toBeInTheDocument()
})
