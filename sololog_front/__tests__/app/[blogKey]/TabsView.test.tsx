import '../../matchMedia.mock'
import { render, screen } from '@testing-library/react'
import TabsView from '@/app/view/[blogKey]/TabsView'

test('description が空のとき、「プロフィール」タブのみ描画される', () => {
  render(<TabsView author='test' description='' links={[]} />)
  expect(screen.queryByText('ブログ説明')).not.toBeInTheDocument()
  expect(screen.queryByText('プロフィール')).toBeInTheDocument()
})

test('description が空でないとき、「ブログ説明」と「プロフィール」タブが描画される', () => {
  render(<TabsView author='test' description='テスト' links={[]} />)
  expect(screen.queryByText('ブログ説明')).toBeInTheDocument()
  expect(screen.queryByText('プロフィール')).toBeInTheDocument()
})

test('ブログ説明の中身が描画される', () => {
  render(<TabsView author='test' description='テストテストテストテスト' links={[]} />)
  expect(screen.queryByText('テストテストテストテスト')).toBeInTheDocument()
})

test('著者名の中身が描画される', () => {
  render(<TabsView author='test' description='テストテストテストテスト' links={[]} />)
  expect(screen.queryByText('test')).toBeInTheDocument()
})

test('ブログリンクの中身が描画される', () => {
  render(
    <TabsView
      author='test'
      description='テストテストテストテスト'
      links={[{ id: '1', name: '', url: 'http://example.com' }]}
    />,
  )
  expect(screen.queryByText('http://example.com')).toBeInTheDocument()
})
