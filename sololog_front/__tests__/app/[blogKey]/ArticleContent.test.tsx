import { render, screen } from '@testing-library/react'
import ArticleContent from '@/app/view/[blogKey]/ArticleContent'

test('[[more]]キーワードが無いとき「続きを読む」が描画されず全文描画される', () => {
  render(<ArticleContent content={'test'} />)
  expect(screen.queryByText('続きを読む')).not.toBeInTheDocument()
  expect(screen.queryByText('test')).toBeInTheDocument()
})

test('[[more]]キーワードがあるとき「続きを読む」が描画され[[more]]以外が描画される', () => {
  render(<ArticleContent content={'before[[more]]after'} />)
  expect(screen.queryByText('続きを読む')).toBeInTheDocument()
  expect(screen.queryByText('before')).toBeInTheDocument()
  expect(screen.queryByText('after')).toBeInTheDocument()
  expect(screen.queryByText('[[more]]')).not.toBeInTheDocument()
})
