import { fireEvent, render, screen } from '@testing-library/react'
import DeleteInput from '@/app/delete/delete/DeleteInput'

const onClickDelete = () => {}

test('初期状態では削除するボタンを押下できない', () => {
  render(<DeleteInput onClickDelete={onClickDelete} isLoading={false} />)
  const button = screen.getByText('削除する').closest('button')
  expect(button).toHaveAttribute('disabled')
})

test('delete と入力しないと削除ボタンを押下できない', () => {
  render(<DeleteInput onClickDelete={onClickDelete} isLoading={false} />)
  const input = screen.getByRole('textbox')
  fireEvent.change(input, { target: { value: 'delet' } })
  const button = screen.getByText('削除する').closest('button')
  expect(button).toHaveAttribute('disabled')
})

test('delete と入力すると削除ボタンを押下できる', () => {
  render(<DeleteInput onClickDelete={onClickDelete} isLoading={false} />)
  const input = screen.getByRole('textbox')
  fireEvent.change(input, { target: { value: 'delete' } })
  const button = screen.getByText('削除する').closest('button')
  expect(button).not.toHaveAttribute('disabled')
})

test('isLoading = true のとき削除ボタンを押下できない', () => {
  render(<DeleteInput onClickDelete={onClickDelete} isLoading={true} />)
  const input = screen.getByRole('textbox')
  fireEvent.change(input, { target: { value: 'delete' } })
  const button = screen.getByText('削除する').closest('button')
  expect(button).toHaveAttribute('disabled')
})
