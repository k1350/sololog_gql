import NewButton from '@/app/home/NewButton'
import { render, screen } from '@testing-library/react'

test('maxBlogs > dataLength のとき、新規作成ボタンに href がセットされている', () => {
  render(<NewButton dataLength={2} maxBlogs={3} />)
  expect(screen.getByText('新規作成').closest('a')).toHaveAttribute('href', '/blogs/new')
})

describe('maxBlogs = dataLength のとき', () => {
  beforeEach(() => {
    render(<NewButton dataLength={3} maxBlogs={3} />)
  })
  test('新規作成ボタンに href がセットされていない', () => {
    expect(screen.getByText('新規作成').closest('a')).not.toHaveAttribute('href')
  })
  test('「作成可能なブログは {maxBlogs} 件までです。」と描画されている', () => {
    expect(screen.getByText('作成可能なブログは 3 件までです。')).toBeInTheDocument()
  })
})

describe('maxBlogs < dataLength のとき', () => {
  beforeEach(() => {
    render(<NewButton dataLength={4} maxBlogs={3} />)
  })
  test('新規作成ボタンに href がセットされていない', () => {
    expect(screen.getByText('新規作成').closest('a')).not.toHaveAttribute('href')
  })
  test('「作成可能なブログは {maxBlogs} 件までです。」と描画されている', () => {
    expect(screen.getByText('作成可能なブログは 3 件までです。')).toBeInTheDocument()
  })
})
