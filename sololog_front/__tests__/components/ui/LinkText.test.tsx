import { render, screen } from '@testing-library/react'
import { LinkText } from '@/components/ui/LinkText'

test('text が描画されている', () => {
  render(<LinkText>http://example.com</LinkText>)
  expect(screen.getByText('http://example.com')).toBeInTheDocument()
})

test('isExternal = true のとき ExternalLinkIcon が描画されている', () => {
  render(<LinkText isExternal={true}>http://example.com</LinkText>)
  expect(screen.queryByTestId('test-external-link-icon')).toBeInTheDocument()
})

test('isExternal = false のとき ExternalLinkIcon が描画されていない', () => {
  render(<LinkText isExternal={false}>http://example.com</LinkText>)
  expect(screen.queryByTestId('test-external-link-icon')).not.toBeInTheDocument()
})

test('isExternal 未指定のとき ExternalLinkIcon が描画されていない', () => {
  render(<LinkText>http://example.com</LinkText>)
  expect(screen.queryByTestId('test-external-link-icon')).not.toBeInTheDocument()
})
