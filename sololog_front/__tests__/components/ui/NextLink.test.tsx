import { render, screen } from '@testing-library/react'
import NextLink from '@/components/ui/NextLink'

test('text が描画されている', () => {
  render(<NextLink href='http://example.com'>テスト</NextLink>)
  expect(screen.getByText('テスト')).toBeInTheDocument()
})

test('isExternal = true のとき ExternalLinkIcon が描画されている', () => {
  render(
    <NextLink href='http://example.com' isExternal={true}>
      テスト
    </NextLink>,
  )
  expect(screen.queryByTestId('test-external-link-icon')).toBeInTheDocument()
})

test('isExternal = false のとき ExternalLinkIcon が描画されていない', () => {
  render(
    <NextLink href='http://example.com' isExternal={false}>
      テスト
    </NextLink>,
  )
  expect(screen.queryByTestId('test-external-link-icon')).not.toBeInTheDocument()
})

test('href がセットされている', () => {
  render(
    <NextLink href='http://example.com' isExternal={false}>
      テスト
    </NextLink>,
  )
  expect(screen.getByText('テスト').closest('a')).toHaveAttribute('href', 'http://example.com')
})
