import { render, screen } from '@testing-library/react'
import NextLinkButton from '@/components/ui/NextLinkButton'
import { EditIcon } from '@chakra-ui/icons'

test('text が描画されている', () => {
  render(
    <NextLinkButton href='http://example.com' leftIcon={<EditIcon />}>
      テスト
    </NextLinkButton>,
  )
  expect(screen.getByText('テスト')).toBeInTheDocument()
})

test('href がセットされている', () => {
  render(
    <NextLinkButton href='http://example.com' leftIcon={<EditIcon />}>
      テスト
    </NextLinkButton>,
  )
  expect(screen.getByText('テスト').closest('a')).toHaveAttribute('href', 'http://example.com')
})
