import { PasswordValidator, LinkUrlValidator } from '@/validate/Blog'
import { CONSTANT } from '@/constants'

describe('PasswordValidator', () => {
  test('showPasswordInput = true のとき、未入力だとエラーになる', () => {
    const validator = PasswordValidator(true)
    expect(validator.validate('')).toBe(CONSTANT.ERROR_MESSAGE.REQUIRED)
  })

  test('showPasswordInput = true のとき、入力済だとエラーにならない', () => {
    const validator = PasswordValidator(true)
    expect(validator.validate('a')).toBe(undefined)
  })

  test('showPasswordInput = false のとき、未入力でもエラーにならない', () => {
    const validator = PasswordValidator(false)
    expect(validator.validate('')).toBe(undefined)
  })
})

describe('validateLinkUrlRequired', () => {
  test('linkName に入力されているとき、未入力だとエラーになる', () => {
    const validator = LinkUrlValidator('a')
    expect(validator.validate('')).toBe(CONSTANT.ERROR_MESSAGE.LINK_URL_REQUIRED)
  })

  test('linkName に入力されているとき、入力済だとエラーにならない', () => {
    const validator = LinkUrlValidator('a')
    expect(validator.validate('a')).toBe(undefined)
  })

  test('linkName に入力されていないとき、未入力でもエラーにならない', () => {
    const validator = LinkUrlValidator('')
    expect(validator.validate('')).toBe(undefined)
  })
})
