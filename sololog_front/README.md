# Development

## Requirements

- Node.js 16+
- yarn
- Firebase プロジェクト
  - Firebase Authentication で Google ログインを可能にしておく

## Run

1. `cd sololog_front`
2. `.env.development` をコピーして `.env.local` にリネームし、中身を自分の環境に合わせて書き換える
  - `NEXT_PUBLIC_FIREBASE_CUSTOM_AUTH_DOMAIN` はローカル開発では Firebase Authentication のデフォルトの authDomain にする
    - `localhost:3000` を指定できないため
3. `yarn dev` で起動

他のコマンドは package.json 参照

# 本番

デプロイ先はどこでもいい。Vercel が楽。

## 事前準備
- Firebase Authentication
  - 「承認済みドメイン」にドメインを追加する
- Google Cloud Console
  - Firebase Authentication によって自動で作られたプロジェクトがあるので選択し、「API とサービス＞認証情報＞OAuth 2.0 クライアント ID」を編集して下記を設定する
    - 承認済みの JavaScript 生成元: 本番環境フロントの URL
    - 承認済みのリダイレクト URI: (本番環境フロントの URL)/__auth/handler
- `.env`（Vercel なら Environment Variables）
  - `NEXT_PUBLIC_FIREBASE_CUSTOM_AUTH_DOMAIN` は本番環境のフロントのドメインにする（rewrites を使って Firebase Authentication の authDomain にリクエストを流す）
  - Safari や Firefox でログインできない問題の対応: https://firebase.google.com/docs/auth/web/redirect-best-practices
