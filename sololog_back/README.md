# Development

## Requirements

- Docker Compose
- Firebase プロジェクト
  - Firebase Authentication で Google ログインを可能にしておく
- Firebase と通信するための Firebase Admin SDK サービス アカウント
- Firebase Admin SDK サービス アカウントの資格情報を含む構成ファイル

## Run

1. `cd sololog_back`
2. `docker/db/env.example` をコピーして `docker/db/.env.dev` にリネームし、DB のユーザー名やパスワードを指定する

   - データベース名も含めた接続情報を丸ごと環境変数の `DNS` で指定するので、データベース名は任意

3. `env.dev.example` をコピーして `.env.dev` にリネームし、内容を書き換える

   - `UID`, `GID` はコンテナを実行するユーザーの UID, GID に合わせる
   - もし sololog_front の起動ポートを変更しているなら `FRONT_ORIGIN` のポート番号を変更する
   - `DSN` に DB への接続情報を [Go-MySQL-Driver の DNS 形式](https://github.com/go-sql-driver/mysql#dsn-data-source-name) で書く。内容は各自の `docker/db/.env.dev` に合わせるが「parseTime=true」オプションは必須
   - `GOOGLE_SERVICE_ACCOUNT_JSON` にはFirebase Admin SDK サービス アカウントの資格情報を含む構成ファイルの中身を base64 エンコードした文字列を設定する
   - `APP_ENV` は local にする
     - local 以外にすると AWS Systems Manager Parameter Store に環境変数を読みに行くが、ローカル開発中は AWS に接続可能な状態にしていないので
   - `SKIP_MIGRATION` は基本は false でいい。DB マイグレーションをスキップしたいときに true にする

4. sololog_back ディレクトリで `docker compose -f compose.dev.yaml --env-file .env.dev up` で起動

   - 初回起動時は DB の初期処理より先にアプリケーションが起動してしまってアプリケーションの起動処理中にエラーとなるので、DB の初期処理が終わったら一旦終了して再度起動する

5. 終了するときは sololog_back ディレクトリで `docker compose -f compose.dev.yaml --env-file .env.dev down`

### Docker Compose を使ってローカル環境で MySQL を動かす場合
本番環境では Lambda の定期実行で鍵をローテーションするが、ローカル環境では Lambda が使えないので手動で鍵を生成する。

```bash
docker compose -f compose.dev.yaml --env-file .env.dev exec db /bin/bash
```

で DB コンテナにログインし、MySQL の sololog データベースを選択してから下記 SQL を流す。

```sql
insert jwk values ('M5CltTKlfgR7ejkj_2QE2', '{ "alg": "HS256", "k": "P2J2Qu5WdByyMrVLuJbvbtQgYgVBGr4eBTKvMz60dw-CjYHbp0Nyc0NXauQ6KIMV-S2-OZydZcR4wiD-J0AR6RkBYkVDLf137BMAcBYv0bbscrcFObbFVW8Xd4e800NseG-GuGak1u0reBhJGALzeQSzrgUyd-fcaI2HiJmtc5UFVT1KTchu-GidEnd3WQ37ZUuHeykqwHisIYz24lcUOIk8ubGOU7yJ1sJgg0XJH-Br4NlYRjBBXI4tcY1vpZ-aLx_Vz88W6hWyuCCbAHQ3_GJpjLZYVXN0JhNl0zyUW--DzmKrD4g6o6z7C_iujm_C_coUyaGp10bwlwbZ7M5QPg", "kid": "M5CltTKlfgR7ejkj_2QE2", "kty": "oct" }', '9999-12-31 23:59:59');
```

# 本番

リージョンは ap-northeast-1 とする。

1. AWS アカウントを作成し、AWS CDK をセットアップする（cdk bootstrap 完了まで実施）
2. Amazon Elastic Container Registry に "sololog-back-repository" というリポジトリを作成する
3. AWS Systems Manager のパラメータストアに下記を追加する。中身はいずれも開発環境で .env.dev に指定した値の本番用の値にする。

   - /sololog/FRONT_ORIGIN
   - /sololog/DSN
   - /sololog/GOOGLE_SERVICE_ACCOUNT_JSON
   - /sololog/SKIP_MIGRATION
     * PlanetScale など、Production 環境に直接 DDL を流せない場合は "true" で登録する。この場合、別途 migration 内容を手動で反映させておく必要がある。
4. AWS Certificate Manager でバックエンドサーバーの URL に証明書を発行する
5. cdk/lib/env/dev.example.ts をコピーして名前を prod.ts に変更し、中身を自分のアカウントの情報で書き換える。CERTIFICATE_ARN は発行した証明書の ARN にする。BACK_APP の設定値はお好みで（DESIRED_COUNT は 2 以上でないと ALB の設定上タイムアウトが発生するかもしれない）。
6. cdk ディレクトリで `yarn deploy:prod --all --profile [AWS CLI のプロファイル名]`

   - AWS CLI でプロファイル名をつけなかった場合は `yarn deploy:prod --all` のみ
7. EC2 > Load balancers から作成された ALB を確認すると DNS name が記載されているので、Cloudflare DNS へ戻ってバックエンドサーバーの URL に CNAME レコードを作る

## デプロイ後

- sololog_lambda をデプロイし、一度 Lambda をテスト実行して jwk テーブルに最初の鍵を生成する
