package directive

import (
	"testing"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func TestValidateNumberValue(t *testing.T) {
	t.Run("*int", func(t *testing.T) {
		min := 1
		max := 3

		t.Run("正常系", func(t *testing.T) {
			tests := []struct {
				name string
				v    int
				min  *int
				max  *int
			}{
				{
					name: "min 以上",
					v:    1,
					min:  &min,
					max:  nil,
				},
				{
					name: "max 以下",
					v:    3,
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run(tt.name, func(t *testing.T) {
					_, err := validateNumberValue(&tt.v, tt.min, tt.max)
					if err != nil {
						t.Errorf("validateNumberValue() error = %v", err)
					}
				})
			}
		})

		t.Run("異常系", func(t *testing.T) {
			tests := []struct {
				name string
				v    int
				min  *int
				max  *int
			}{
				{
					name: "min 未満",
					v:    0,
					min:  &min,
					max:  nil,
				},
				{
					name: "max 超過",
					v:    4,
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run(tt.name, func(t *testing.T) {
					_, err := validateNumberValue(&tt.v, tt.min, tt.max)
					if err == nil {
						t.Errorf("should got error")
					}
				})
			}
		})
	})

	t.Run("*float64", func(t *testing.T) {
		min := 1
		max := 3

		t.Run("正常系", func(t *testing.T) {
			tests := []struct {
				name string
				v    float64
				min  *int
				max  *int
			}{
				{
					name: "min 以上",
					v:    1.1,
					min:  &min,
					max:  nil,
				},
				{
					name: "max 以下",
					v:    3.0,
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run(tt.name, func(t *testing.T) {
					_, err := validateNumberValue(&tt.v, tt.min, tt.max)
					if err != nil {
						t.Errorf("validateNumberValue() error = %v", err)
					}
				})
			}
		})

		t.Run("異常系", func(t *testing.T) {
			tests := []struct {
				name    string
				v       float64
				min     *int
				max     *int
				wantErr bool
			}{
				{
					name: "min 未満",
					v:    0.9,
					min:  &min,
					max:  nil,
				},
				{
					name: "max 超過",
					v:    3.1,
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run(tt.name, func(t *testing.T) {
					_, err := validateNumberValue(&tt.v, tt.min, tt.max)
					if err == nil {
						t.Errorf("should got error")
					}
				})
			}
		})
	})

	t.Run("*int でも *float64 でもない", func(t *testing.T) {
		v := "test"
		min := 1
		_, err := validateNumberValue(&v, &min, nil)
		if err == nil {
			t.Errorf("validateNumberValue() error = %v", err)
		}
	})
}

func TestValidateStringValue(t *testing.T) {
	t.Run("*string と string", func(t *testing.T) {
		min := 1
		max := 3

		t.Run("正常系", func(t *testing.T) {
			tests := []struct {
				name string
				v    string
				min  *int
				max  *int
			}{
				{
					name: "min 以上",
					v:    "あ",
					min:  &min,
					max:  nil,
				},
				{
					name: "max 以下",
					v:    "あいう",
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run("*string: "+tt.name, func(t *testing.T) {
					_, err := validateStringValue(&tt.v, tt.min, tt.max)
					if err != nil {
						t.Errorf("validateStringValue() error = %v", err)
					}
				})

				t.Run("string: "+tt.name, func(t *testing.T) {
					_, err := validateStringValue(tt.v, tt.min, tt.max)
					if err != nil {
						t.Errorf("validateStringValue() error = %v", err)
					}
				})
			}
		})

		t.Run("異常系", func(t *testing.T) {
			tests := []struct {
				name string
				v    string
				min  *int
				max  *int
			}{
				{
					name: "min 未満",
					v:    "",
					min:  &min,
					max:  nil,
				},
				{
					name: "max 超過",
					v:    "あいうえ",
					min:  nil,
					max:  &max,
				},
			}

			for _, tt := range tests {
				t.Run("*string: "+tt.name, func(t *testing.T) {
					_, err := validateStringValue(&tt.v, tt.min, tt.max)
					if err == nil {
						t.Errorf("should got error")
					}
				})

				t.Run("string: "+tt.name, func(t *testing.T) {
					_, err := validateStringValue(tt.v, tt.min, tt.max)
					if err == nil {
						t.Errorf("should got error")
					}
				})
			}
		})
	})

	t.Run("*string でも string でもない", func(t *testing.T) {
		v := 1
		min := 1
		_, err := validateStringValue(v, &min, nil)
		if err == nil {
			t.Errorf("validateStringValue() error = %v", err)
		}
	})
}

func TestValidatePublishOptionValue(t *testing.T) {
	t.Run("*model.PublishOption と model.PublishOption", func(t *testing.T) {
		m := map[string]interface{}{"password": "test"}
		empty := map[string]interface{}{}
		requireIf := "password,PASSWORD"

		t.Run("正常系", func(t *testing.T) {
			tests := []struct {
				name      string
				v         model.PublishOption
				m         map[string]interface{}
				requireIf *string
			}{
				{
					name:      "PUBLIC のときパスワードは無い",
					v:         model.PublishOptionPublic,
					m:         empty,
					requireIf: &requireIf,
				},
				{
					name:      "PASSWORD のときパスワードはある",
					v:         model.PublishOptionPassword,
					m:         m,
					requireIf: &requireIf,
				},
				{
					name:      "PRIVATE のときパスワードは無い",
					v:         model.PublishOptionPrivate,
					m:         empty,
					requireIf: &requireIf,
				},
			}

			for _, tt := range tests {
				t.Run("*model.PublishOption: "+tt.name, func(t *testing.T) {
					_, err := validatePublishOptionValue(&tt.v, tt.m, tt.requireIf)
					if err != nil {
						t.Errorf("validatePublishOptionValue() error = %v", err)
					}
				})

				t.Run("model.PublishOption: "+tt.name, func(t *testing.T) {
					_, err := validatePublishOptionValue(tt.v, tt.m, tt.requireIf)
					if err != nil {
						t.Errorf("validatePublishOptionValue() error = %v", err)
					}
				})
			}
		})

		t.Run("異常系", func(t *testing.T) {
			tests := []struct {
				name      string
				v         model.PublishOption
				m         map[string]interface{}
				requireIf *string
			}{
				{
					name:      "PUBLIC のときパスワードがあるとエラー",
					v:         model.PublishOptionPublic,
					m:         m,
					requireIf: &requireIf,
				},
				{
					name:      "PASSWORD のときパスワードが無いとエラー",
					v:         model.PublishOptionPassword,
					m:         empty,
					requireIf: &requireIf,
				},
				{
					name:      "PRIVATE のときパスワードがあるとエラー",
					v:         model.PublishOptionPrivate,
					m:         m,
					requireIf: &requireIf,
				},
			}

			for _, tt := range tests {
				t.Run("*model.PublishOption: "+tt.name, func(t *testing.T) {
					_, err := validatePublishOptionValue(&tt.v, tt.m, tt.requireIf)
					if err == nil {
						t.Errorf("should got error")
					}
				})

				t.Run("model.PublishOption: "+tt.name, func(t *testing.T) {
					_, err := validatePublishOptionValue(tt.v, tt.m, tt.requireIf)
					if err == nil {
						t.Errorf("should got error")
					}
				})
			}
		})
	})

	t.Run("*model.PublishOption でも model.PublishOption でもない", func(t *testing.T) {
		v := 1
		requireIf := "password,PASSWORD"
		_, err := validatePublishOptionValue(v, nil, &requireIf)
		if err == nil {
			t.Errorf("validatePublishOptionValue() error = %v, wantErr %v", err, true)
		}
	})
}

func TestValidatePasswordValue(t *testing.T) {
	isValidPassword := true

	t.Run("正常系", func(t *testing.T) {
		tests := []struct {
			name            string
			v               string
			isValidPassword *bool
		}{
			{
				name:            "最小文字数",
				v:               "9",
				isValidPassword: &isValidPassword,
			},
			{
				name:            "最大文字数",
				v:               "9!@#%^&*1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
				isValidPassword: &isValidPassword,
			},
		}
		for _, tt := range tests {
			_, err := validatePasswordValue(&tt.v, tt.isValidPassword)
			if err != nil {
				t.Errorf("validatePasswordValue() error = %v", err)
			}
		}
	})

	t.Run("異常系", func(t *testing.T) {
		tests := []struct {
			name            string
			v               string
			isValidPassword *bool
		}{
			{
				name:            "文字数不足",
				v:               "",
				isValidPassword: &isValidPassword,
			},
			{
				name:            "文字数超過",
				v:               "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
				isValidPassword: &isValidPassword,
			},
			{
				name:            "使用できない文字",
				v:               "aA19!@#あいうえお",
				isValidPassword: &isValidPassword,
			},
		}
		for _, tt := range tests {
			_, err := validatePasswordValue(&tt.v, tt.isValidPassword)
			if err == nil {
				t.Errorf("should got error")
			}
		}
	})
}
