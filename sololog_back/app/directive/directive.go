package directive

import (
	"context"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unicode/utf8"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

var passwordPattern = regexp.MustCompile(`^[a-zA-Z\d!@#%\^&\*]{1,255}$`)

func NumberValue(ctx context.Context, obj interface{}, next graphql.Resolver, min *int, max *int) (res interface{}, err error) {
	v, err := next(ctx)
	if err != nil {
		return nil, err
	}

	return validateNumberValue(v, min, max)
}

func StringValue(ctx context.Context, obj interface{}, next graphql.Resolver, min *int, max *int) (res interface{}, err error) {
	v, err := next(ctx)
	if err != nil {
		return nil, err
	}

	return validateStringValue(v, min, max)
}

func PublishOptionValue(ctx context.Context, obj interface{}, next graphql.Resolver, requireIf *string) (res interface{}, err error) {
	v, err := next(ctx)
	if err != nil {
		return nil, err
	}
	m := obj.(map[string]interface{})

	return validatePublishOptionValue(v, m, requireIf)
}

func PasswordValue(ctx context.Context, obj interface{}, next graphql.Resolver, isValidPassword *bool) (res interface{}, err error) {
	v, err := next(ctx)
	if err != nil {
		return nil, err
	}

	return validatePasswordValue(v, isValidPassword)
}

func validateNumberValue(v interface{}, min *int, max *int) (res interface{}, err error) {
	switch v.(type) {
	case *int:
		value := v.(*int)
		if min != nil {
			if *value < *min {
				return nil, fmt.Errorf(strconv.Itoa(*value) + ": is smaller than min value of " + strconv.Itoa(*min))
			}
		}
		if max != nil {
			if *max < *value {
				return nil, fmt.Errorf(strconv.Itoa(*value) + ": is larger than max value of " + strconv.Itoa(*max))
			}
		}
	case *float64:
		value := v.(*float64)
		if min != nil {
			if *value < float64(*min) {
				return nil, fmt.Errorf(strconv.FormatFloat(*value, 'f', -1, 64) + ": is smaller than min value of " + strconv.Itoa(*min))
			}
		}
		if max != nil {
			if float64(*max) < *value {
				return nil, fmt.Errorf(strconv.FormatFloat(*value, 'f', -1, 64) + ": is larger than max value of " + strconv.Itoa(*max))
			}
		}
	default:
		return nil, fmt.Errorf("NumberValue directive is compatible only Int and Float")
	}

	return v, nil
}

func validateStringValue(v interface{}, min *int, max *int) (res interface{}, err error) {
	switch v.(type) {
	case *string:
		value := v.(*string)
		if max != nil {
			if *max < utf8.RuneCountInString(*value) {
				return nil, fmt.Errorf("length is larger than max length of " + strconv.Itoa(*max))
			}
		}
		if min != nil {
			if utf8.RuneCountInString(*value) < *min {
				return nil, fmt.Errorf("length is smaller than min length of " + strconv.Itoa(*min))
			}
		}
	case string:
		if max != nil {
			if *max < utf8.RuneCountInString(v.(string)) {
				return nil, fmt.Errorf("length is larger than max length of " + strconv.Itoa(*max))
			}
		}
		if min != nil {
			if utf8.RuneCountInString(v.(string)) < *min {
				return nil, fmt.Errorf("length is smaller than min length of " + strconv.Itoa(*min))
			}
		}
	default:
		return nil, fmt.Errorf("StringValue directive is compatible only String and ID")
	}

	return v, nil
}

func validatePublishOptionValue(v interface{}, m map[string]interface{}, requireIf *string) (res interface{}, err error) {
	if requireIf != nil {
		constraints := strings.Split(*requireIf, ",")
		if len(constraints) != 2 {
			return nil, fmt.Errorf("RequireIf directive should be '[require field name],[require condition value]")
		}

		var value string

		switch v.(type) {
		case *model.PublishOption:
			value = v.(*model.PublishOption).String()
		case model.PublishOption:
			value = v.(model.PublishOption).String()
		default:
			return nil, fmt.Errorf("PublishOptionValue directive is compatible only PublishOption")
		}
		if value == constraints[1] {
			if _, ok := m[constraints[0]]; !ok {
				return nil, fmt.Errorf(constraints[0] + ": is required when publishOption is " + constraints[1])
			}
		} else {
			if _, ok := m[constraints[0]]; ok {
				return nil, fmt.Errorf(constraints[0] + ": is prohibited when publishOption is not " + constraints[1])
			}
		}
	}

	return v, nil
}

func validatePasswordValue(v interface{}, isValidPassword *bool) (res interface{}, err error) {
	if isValidPassword != nil && *isValidPassword {
		if (v == nil) || reflect.ValueOf(v).IsNil() {
			return nil, fmt.Errorf("password is not valid.")
		}
		value := v.(*string)
		if !passwordPattern.MatchString(*value) {
			return nil, fmt.Errorf("password is not valid.")
		}
	}

	return v, nil
}
