package errors

import (
	"github.com/pkg/errors"
)

var (
	// 認証されていない
	UnauthenticatedError = errors.New("Unauthenticated")
	// パスワード認証のブログでパスワードが間違っている / 権限が不正
	ForbiddenError = errors.New("Forbidden")
	// 対象リソースが存在しない or アクセス権限無し
	NotFoundError = errors.New("Not Found")
	// その他エラー
	InternalServerError = errors.New("InternalServerError")
	// ブログパスワードの認証が切れたエラー
	BlogAuthenticationExpiredError = errors.New("Blog Authentication Expired")
	// 管理者ユーザーが一人しかいないので削除できない
	OnlyAdminCannotDeleteError = errors.New("This admin user cannot be deleted")
	// 新規登録不可エラー
	RegisterNotAllowedError = errors.New("Register is not allowed")
)

func RecreateError(err error) error {
	switch {
	case errors.Is(err, UnauthenticatedError):
		return UnauthenticatedError
	case errors.Is(err, ForbiddenError):
		return ForbiddenError
	case errors.Is(err, NotFoundError):
		return NotFoundError
	case errors.Is(err, BlogAuthenticationExpiredError):
		return BlogAuthenticationExpiredError
	case errors.Is(err, OnlyAdminCannotDeleteError):
		return OnlyAdminCannotDeleteError
	case errors.Is(err, RegisterNotAllowedError):
		return RegisterNotAllowedError
	}
	return InternalServerError
}
