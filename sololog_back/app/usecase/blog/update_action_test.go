package blog

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_blog"
)

func TestUpdateBlog(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	blogId := "000XAL6S41ACTAV9WEVGEMMVR8"
	author := "著者1"
	description := ""
	name := "ブログ名"
	blog := &model.Blog{
		ID:            blogId,
		BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
		Author:        &author,
		Name:          &name,
		Description:   &description,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowstr,
		UpdatedAt:     &nowstr,
	}
	input := &model.UpdateBlogInput{
		ID:     blog.ID,
		Author: blog.Author,
	}

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		mb.
			EXPECT().
			Update(now, *input, 1).
			Return(blog, nil)
		result, err := u.UpdateBlog(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("異常系: 更新に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		mb.
			EXPECT().
			Update(now, *input, 1).
			Return(nil, fmt.Errorf("DB error"))
		_, err := u.UpdateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系: 対象のブログが見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		mb.
			EXPECT().
			Update(now, *input, 1).
			Return(nil, customError.NotFoundError)
		_, err := u.UpdateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
	})
}

func TestUpdateArticle(t *testing.T) {
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	articleId := "000XAL6S41ACTAV9WEVGEMMVR8"
	content := "test"
	article := &model.Article{
		ID:            articleId,
		Content:       &content,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowStr,
		UpdatedAt:     &nowStr,
	}

	input := &model.UpdateArticleInput{
		ID:      article.ID,
		Content: *article.Content,
	}

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		ma.
			EXPECT().
			Update(now, *input, 1).
			Return(article, nil)

		result, err := u.UpdateArticle(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, article) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, article)
		}
	})

	t.Run("異常系：記事更新失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		ma.
			EXPECT().
			Update(now, *input, 1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.UpdateArticle(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：対象の記事が見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)
		ma.
			EXPECT().
			Update(now, *input, 1).
			Return(nil, customError.NotFoundError)

		_, err := u.UpdateArticle(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
	})
}
