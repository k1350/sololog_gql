package blog

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *BlogUsecase) CreateBlog(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error) {
	// ブログ数のチェック
	mp, err := u.mParameterRepository.Find()
	if err != nil {
		return nil, errors.Wrap(err, "Blog.CreateBlog error")
	}
	count, err := u.blogRepository.CountBlogs(userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.CreateBlog error")
	}
	// 不正な操作または複数地点からの同時操作
	if *count >= mp.MaxBlogs {
		return nil, errors.Wrap(errors.New("The number of blogs has exceeded the limit"), "Blog.CreateBlog error")
	}

	result, err := u.blogRepository.Create(now, input, userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.CreateBlog error")
	}
	return result, nil
}

func (u *BlogUsecase) CreateArticle(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error) {
	// 本体の存在チェック＆権限チェックを行う
	_, err := u.blogRepository.FindById(input.BlogID, userId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, errors.Wrap(customError.ForbiddenError, "Blog.CreateArticle error")
		}
		return nil, errors.Wrap(err, "Blog.CreateArticle error")
	}

	// 記事を作成
	result, err := u.articleRepository.Create(now, input, userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.CreateArticle error")
	}
	return result, nil
}
