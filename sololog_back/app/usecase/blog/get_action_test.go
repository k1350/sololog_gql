package blog

import (
	"database/sql"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_blog"
)

func TestGetBlogs(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	author1 := "著者1"
	description1 := ""
	name1 := "ブログ名1"
	author2 := "著者2"
	description2 := "説明"
	name2 := "ブログ名2"

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		expected := []*model.Blog{
			{
				ID:            "1",
				BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
				Author:        &author1,
				Name:          &name1,
				Description:   &description1,
				PublishOption: model.PublishOptionPublic,
				CreatedAt:     &nowstr,
				UpdatedAt:     &nowstr,
			},
			{
				ID:            "3",
				BlogKey:       "ef6583ba-820a-ac6b-5f5b-af97cee16a9a",
				Author:        &author2,
				Name:          &name2,
				Description:   &description2,
				PublishOption: model.PublishOptionPassword,
				CreatedAt:     &nowstr,
				UpdatedAt:     &nowstr,
			},
		}

		mb.
			EXPECT().
			FindByUserId(1).
			Return(expected, nil)

		result, err := u.GetBlogs(1)
		if err != nil {
			t.Error("got err:", err)
		}
		for i, got := range result {
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", result, want)
			}
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindByUserId(1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetBlogs(1)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetBlogByID(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	blogId := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
	author := "著者1"
	description := "説明"
	name := "ブログ名1"

	t.Run("正常系: ブログが見つかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		expected := &model.Blog{
			ID:            blogId,
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(expected, nil)

		result, err := u.GetBlogByID(blogId, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系: ブログが見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(nil, sql.ErrNoRows)

		result, err := u.GetBlogByID(blogId, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetBlogByID(blogId, 1)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetBlogByBlogKey(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	userId := 1
	password := "test"
	author := "著者1"
	description := "説明"
	name := "ブログ名1"
	blog := &model.Blog{
		ID:            "1",
		BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
		Author:        &author,
		Name:          &name,
		Description:   &description,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowstr,
		UpdatedAt:     &nowstr,
	}
	t.Run("正常系：ユーザーIDとパスワードあり、パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: &password,
		}
		passwordInputTime := time.Now().UTC()
		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, &passwordInputTime, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDとパスワードあり、パスワード入力日時なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: &password,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDあり、パスワードなし、パスワード入力日時なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDあり、パスワードなし、パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}
		passwordInputTime := time.Now().UTC()
		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, &passwordInputTime).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, &passwordInputTime, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDなし、パスワードあり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: &password,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: &password,
			}, nil).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, nil, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDとパスワードなし、パスワード入力日時なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, nil).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, nil, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ユーザーIDとパスワードなし、パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}
		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &passwordInputTime).
			Return(blog, nil)

		result, err := u.GetBlogByBlogKey(*input, &passwordInputTime, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, blog) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, blog)
		}
	})

	t.Run("正常系：ブログが見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}
		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &passwordInputTime).
			Return(nil, sql.ErrNoRows)

		result, err := u.GetBlogByBlogKey(*input, &passwordInputTime, nil)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})

	t.Run("異常系：パスワード入力日時なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, nil).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetBlogByBlogKey(*input, nil, nil)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Password: nil,
		}
		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &passwordInputTime).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetBlogByBlogKey(*input, &passwordInputTime, nil)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetArticles(t *testing.T) {
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	password := "test"
	userId := 1
	blogId := "000XAL6S41ACTAV9WEVGEMMVR8"
	author := "著者1"
	description := "説明"
	name := "ブログ名1"
	blog := &model.Blog{
		ID:            blogId,
		BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
		Author:        &author,
		Name:          &name,
		Description:   &description,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowStr,
		UpdatedAt:     &nowStr,
	}
	content1 := "内容1"
	content2 := "内容2"
	findArticlesExpected := []*model.Article{
		{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content1,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		},
		{
			ID:            "01G65Z755AFWAKHE12NY0CQ9FH",
			ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
			Content:       &content2,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		},
	}
	first := 2
	after := "02G65Z755AFWAKHE12NY0CQ9FH"
	last := 2

	t.Run("正常系：前のページと次のページあり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: &password,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
			After: &after,
		}

		startCursor := "000XAL6S41ACTAV9WEVGEMMVR8"
		endCursor := "01G65Z755AFWAKHE12NY0CQ9FH"
		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(true, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(true, nil)

		result, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：前のページなし、次のページあり、ユーザーIDとパスワードあり、パスワード入力日時なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: &password,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		startCursor := "000XAL6S41ACTAV9WEVGEMMVR8"
		endCursor := "01G65Z755AFWAKHE12NY0CQ9FH"
		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(true, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(false, nil)

		result, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：前のページあり、次のページなし、ユーザーIDとパスワードあり、パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: &password,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		startCursor := "000XAL6S41ACTAV9WEVGEMMVR8"
		endCursor := "01G65Z755AFWAKHE12NY0CQ9FH"
		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(false, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(true, nil)

		result, err := u.GetArticles(*input, *paginationInput, &passwordInputTime, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：前のページと次のページなし、ユーザーIDとパスワードなし、パスワード入力日時あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		startCursor := "000XAL6S41ACTAV9WEVGEMMVR8"
		endCursor := "01G65Z755AFWAKHE12NY0CQ9FH"
		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     false,
				HasPreviousPage: false,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &passwordInputTime).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(false, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(false, nil)

		result, err := u.GetArticles(*input, *paginationInput, &passwordInputTime, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：記事が見つからない、パスワードなし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     nil,
				EndCursor:       nil,
				HasNextPage:     false,
				HasPreviousPage: false,
			},
			Edges: nil,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return([]*model.Article{}, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(false, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(false, nil)

		result, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系：本体のブログ取得失敗（パスワード入力日時なし）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：本体のブログ取得失敗（パスワード入力日時あり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		passwordInputTime := time.Now().UTC()

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, &passwordInputTime).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticles(*input, *paginationInput, &passwordInputTime, &userId)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：記事取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：次のページの有無取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(false, fmt.Errorf("DB error"))

		_, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：前のページの有無取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.BlogByBlogKeyInput{
			BlogKey:  blog.BlogKey,
			Password: nil,
		}

		paginationInput := &model.ArticlePaginationInput{
			First: &first,
		}

		mb.
			EXPECT().
			FindByBlogKey(input.BlogKey, &model.BlogByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(blog, nil)

		ma.
			EXPECT().
			FindByBlogId(blogId, *paginationInput).
			Return(findArticlesExpected, nil)

		ma.
			EXPECT().
			HasNextPage(blogId, *paginationInput).
			Return(false, nil)

		ma.
			EXPECT().
			HasPreviousPage(blogId, *paginationInput).
			Return(false, fmt.Errorf("DB error"))

		_, err := u.GetArticles(*input, *paginationInput, nil, &userId)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：first と last 同時指定", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input :=
			&model.BlogByBlogKeyInput{
				BlogKey:  blog.BlogKey,
				Password: nil,
			}
		paginationInput :=
			&model.ArticlePaginationInput{
				First: &first,
				Last:  &last,
			}
		_, err := u.GetArticles(*input, *paginationInput, nil, nil)
		if err == nil {
			t.Error("should got err")
		}
	})

	t.Run("異常系：first と last が両方無い", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input :=
			&model.BlogByBlogKeyInput{
				BlogKey:  blog.BlogKey,
				Password: nil,
			}
		paginationInput :=
			&model.ArticlePaginationInput{
				After: &after,
			}
		_, err := u.GetArticles(*input, *paginationInput, nil, nil)
		if err == nil {
			t.Error("should got err")
		}
	})
}

func TestGetArticleByArticleKey(t *testing.T) {
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	password := "test"
	userId := 1
	content := "内容1"
	expected := &model.Article{
		ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
		ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
		Content:       &content,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowStr,
		UpdatedAt:     &nowStr,
	}

	t.Run("正常系：ユーザーIDあり、パスワードあり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   &password,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   &userId,
				Password: &password,
			}, nil).
			Return(expected, nil)

		result, err := u.GetArticleByArticleKey(*input, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：ユーザーIDなし、パスワードあり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   &password,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: &password,
			}, nil).
			Return(expected, nil)

		result, err := u.GetArticleByArticleKey(*input, nil, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：ユーザーIDあり、パスワードなし、パスワード入力時刻なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   &userId,
				Password: nil,
			}, nil).
			Return(expected, nil)

		result, err := u.GetArticleByArticleKey(*input, nil, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：ユーザーIDなし、パスワードなし、パスワード入力時刻なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, nil).
			Return(expected, nil)

		result, err := u.GetArticleByArticleKey(*input, nil, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：ユーザーIDなし、パスワードなし、パスワード入力時刻あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &now).
			Return(expected, nil)

		result, err := u.GetArticleByArticleKey(*input, &now, nil)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("記事が見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &now).
			Return(nil, sql.ErrNoRows)

		result, err := u.GetArticleByArticleKey(*input, &now, nil)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})

	t.Run("異常系：パスワードあり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   &password,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: &password,
			}, nil).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticleByArticleKey(*input, nil, nil)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：パスワードなし、パスワード入力時刻なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, nil).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticleByArticleKey(*input, nil, nil)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：パスワードなし、パスワード入力時刻あり", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "ITRKESW3kEzNVpW0kuQ2T",
			Password:   nil,
		}

		ma.
			EXPECT().
			FindByArticleKey(input.ArticleKey, &model.ArticleByKeyOpts{
				UserId:   nil,
				Password: nil,
			}, &now).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetArticleByArticleKey(*input, &now, nil)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestIsAuthor(t *testing.T) {
	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			IsAuthor("dccb4797-90c7-ea22-6866-7eb954acbbda", 1).
			Return(true, nil)

		result, err := u.IsAuthor("dccb4797-90c7-ea22-6866-7eb954acbbda", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			IsAuthor("dccb4797-90c7-ea22-6866-7eb954acbbda", 1).
			Return(false, fmt.Errorf("DB error"))

		result, err := u.IsAuthor("dccb4797-90c7-ea22-6866-7eb954acbbda", 1)
		if err == nil {
			t.Error("should got error")
		}
		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}
	})
}
