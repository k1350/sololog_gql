package blog

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *BlogUsecase) GetBlogs(userId int) ([]*model.Blog, error) {
	result, err := u.blogRepository.FindByUserId(userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetBlogs error")
	}
	return result, nil
}

func (u *BlogUsecase) GetBlogByID(blogId string, userId int) (*model.Blog, error) {
	result, err := u.blogRepository.FindById(blogId, userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetBlogByID error")
	}
	return result, nil
}

func (u *BlogUsecase) GetBlogByBlogKey(input model.BlogByBlogKeyInput, passwordInputTime *time.Time, userId *int) (*model.Blog, error) {
	var opts model.BlogByKeyOpts
	if userId != nil {
		opts.UserId = userId
	}
	if input.Password != nil {
		opts.Password = input.Password
	}
	if opts.Password != nil {
		result, err := u.blogRepository.FindByBlogKey(input.BlogKey, &opts, nil)
		if err != nil {
			return nil, errors.Wrap(err, "Blog.GetBlogByBlogKey error")
		}
		return result, nil
	}
	result, err := u.blogRepository.FindByBlogKey(input.BlogKey, &opts, passwordInputTime)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetBlogByBlogKey error")
	}
	return result, nil
}

func (u *BlogUsecase) GetArticles(input model.BlogByBlogKeyInput, paginationInput model.ArticlePaginationInput, passwordInputTime *time.Time, userId *int) (*model.ArticleConnection, error) {
	// first と last を同時に指定できない
	if paginationInput.First != nil && paginationInput.Last != nil {
		return nil, errors.Wrap(errors.New("passing paginationInput.First and paginationInput.Last is not supported: input error"), "Blog.GetArticles error")
	}
	// first と last 両方無い（全件取得）は許可しない
	if paginationInput.First == nil && paginationInput.Last == nil {
		return nil, errors.Wrap(errors.New("paginationInput.First or paginationInput.Last is required: input error"), "Blog.GetArticles error")
	}

	var opts model.BlogByKeyOpts
	if userId != nil {
		opts.UserId = userId
	}
	if input.Password != nil {
		opts.Password = input.Password
	}

	// まずブログ本体を取得する（パスワード認証もここで行う）
	var blog *model.Blog
	var err error
	if opts.Password != nil {
		blog, err = u.blogRepository.FindByBlogKey(input.BlogKey, &opts, nil)
	} else {
		blog, err = u.blogRepository.FindByBlogKey(input.BlogKey, &opts, passwordInputTime)
	}

	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetArticles error")
	}

	result, err := u.articleRepository.FindByBlogId(blog.ID, paginationInput)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetArticles error")
	}

	next, err := u.articleRepository.HasNextPage(blog.ID, paginationInput)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetArticles error")
	}

	previous, err := u.articleRepository.HasPreviousPage(blog.ID, paginationInput)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetArticles error")
	}

	var pageInfo *model.PageInfo

	if len(result) > 0 {
		startCursor := result[0].ID
		endCursor := result[len(result)-1].ID
		pageInfo = &model.PageInfo{
			StartCursor:     &startCursor,
			EndCursor:       &endCursor,
			HasNextPage:     next,
			HasPreviousPage: previous,
		}
	} else {
		pageInfo = &model.PageInfo{
			StartCursor:     nil,
			EndCursor:       nil,
			HasNextPage:     next,
			HasPreviousPage: previous,
		}
	}

	var edges []*model.ArticleEdge
	for i := 0; i < len(result); i++ {
		edges = append(
			edges,
			&model.ArticleEdge{
				Cursor: result[i].ID,
				Node:   result[i],
			},
		)
	}

	return &model.ArticleConnection{
		PageInfo: pageInfo,
		Edges:    edges,
	}, nil
}

func (u *BlogUsecase) GetArticleByArticleKey(input model.ArticleByArticleKeyInput, passwordInputTime *time.Time, userId *int) (*model.Article, error) {
	var opts model.ArticleByKeyOpts
	if userId != nil {
		opts.UserId = userId
	}
	if input.Password != nil {
		opts.Password = input.Password
	}
	if opts.Password != nil {
		result, err := u.articleRepository.FindByArticleKey(input.ArticleKey, &opts, nil)
		if err != nil {
			return nil, errors.Wrap(err, "Blog.GetArticleByArticleKey error")
		}
		return result, nil
	}
	result, err := u.articleRepository.FindByArticleKey(input.ArticleKey, &opts, passwordInputTime)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.GetArticleByArticleKey error")
	}
	return result, nil
}

func (u *BlogUsecase) IsAuthor(blogKey string, userId int) (bool, error) {
	result, err := u.blogRepository.IsAuthor(blogKey, userId)
	if err != nil {
		return false, errors.Wrap(err, "Blog.IsAuthor error")
	}
	return result, nil
}
