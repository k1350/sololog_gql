package blog

import (
	"github.com/pkg/errors"
)

func (u *BlogUsecase) DeleteBlog(blogId string, userId int) error {
	// トランザクション開始
	tx, err := u.blogRepository.BeginTransaction()
	if err != nil {
		return errors.Wrap(err, "Blog.DeleteBlog error")
	}
	defer func() {
		// panicが起きたらロールバック
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p)
		}
	}()

	err = u.blogRepository.DeleteById(tx, blogId, userId)
	if err != nil {
		tx.Rollback()
		return errors.Wrap(err, "Blog.DeleteBlog error")
	}

	tx.Commit()
	return nil
}

func (u *BlogUsecase) DeleteArticle(articleId string, userId int) error {
	err := u.articleRepository.DeleteById(articleId, userId)
	if err != nil {
		return errors.Wrap(err, "Blog.DeleteArticle error")
	}
	return nil
}
