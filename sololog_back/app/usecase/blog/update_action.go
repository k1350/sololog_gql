package blog

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *BlogUsecase) UpdateBlog(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error) {
	result, err := u.blogRepository.Update(now, input, userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.UpdateBlog error")
	}
	return result, nil
}

func (u *BlogUsecase) UpdateArticle(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error) {
	result, err := u.articleRepository.Update(now, input, userId)
	if err != nil {
		return nil, errors.Wrap(err, "Blog.UpdateBlog error")
	}
	return result, nil
}
