package blog

import (
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_blog"
)

func TestDeleteBlog(t *testing.T) {
	blogId := "01ARZ3NDEKTSV4RRFFQ69G5FAV"

	t.Run("正常系", func(t *testing.T) {
		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mb.
			EXPECT().
			DeleteById(tx, blogId, 1).
			Return(nil)

		dbmock.ExpectCommit()

		err = u.DeleteBlog(blogId, 1)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("異常系：ブログ削除に失敗", func(t *testing.T) {
		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mb.
			EXPECT().
			DeleteById(tx, blogId, 1).
			Return(fmt.Errorf("DB error"))

		dbmock.ExpectRollback()

		err = u.DeleteBlog(blogId, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：対象のブログが見つからない", func(t *testing.T) {
		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mb.
			EXPECT().
			DeleteById(tx, blogId, 1).
			Return(customError.NotFoundError)

		dbmock.ExpectRollback()

		err = u.DeleteBlog(blogId, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
	})
}

func TestDeleteArticle(t *testing.T) {
	articleId := "000XAL6S41ACTAV9WEVGEMMVR8"

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		ma.
			EXPECT().
			DeleteById(articleId, 1).
			Return(nil)
		err := u.DeleteArticle(articleId, 1)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("異常系: 記事削除失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		articleId := "000XAL6S41ACTAV9WEVGEMMVR8"

		ma.
			EXPECT().
			DeleteById(articleId, 1).
			Return(fmt.Errorf("DB error"))
		err := u.DeleteArticle(articleId, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系: 対象の記事が見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		articleId := "000XAL6S41ACTAV9WEVGEMMVR8"

		ma.
			EXPECT().
			DeleteById(articleId, 1).
			Return(customError.NotFoundError)
		err := u.DeleteArticle(articleId, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
	})
}
