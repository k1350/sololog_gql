package blog

import (
	"database/sql"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_blog"
)

func TestCreateBlog(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	author := "著者"
	description := ""
	name := "名前"

	input := &model.CreateBlogInput{
		Author:        author,
		Name:          name,
		Description:   nil,
		PublishOption: model.PublishOptionPublic,
		Password:      nil,
		Links:         nil,
	}
	blogId := "000XAL6S41ACTAV9WEVGEMMVR8"

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		paramsExpected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		countExpected := 4

		expected := &model.Blog{
			ID:            blogId,
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		mp.
			EXPECT().
			Find().
			Return(paramsExpected, nil)

		mb.
			EXPECT().
			CountBlogs(1).
			Return(&countExpected, nil)

		mb.
			EXPECT().
			Create(now, *input, 1).
			Return(expected, nil)

		result, err := u.CreateBlog(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：ブログ数が上限超え", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		paramsExpected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		countExpected := 5

		mp.
			EXPECT().
			Find().
			Return(paramsExpected, nil)

		mb.
			EXPECT().
			CountBlogs(1).
			Return(&countExpected, nil)

		_, err := u.CreateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ブログ数の上限値取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mp.
			EXPECT().
			Find().
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.CreateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ブログ数取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		paramsExpected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		mp.
			EXPECT().
			Find().
			Return(paramsExpected, nil)

		mb.
			EXPECT().
			CountBlogs(1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.CreateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ブログ作成失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		paramsExpected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		countExpected := 4

		mp.
			EXPECT().
			Find().
			Return(paramsExpected, nil)

		mb.
			EXPECT().
			CountBlogs(1).
			Return(&countExpected, nil)

		mb.
			EXPECT().
			Create(now, *input, 1).
			Return(nil, fmt.Errorf("DB error"))
		_, err := u.CreateBlog(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestCreateArticle(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	blogId := "000XAL6S41ACTAV9WEVGEMMVR8"
	input := &model.CreateArticleInput{
		BlogID:  blogId,
		Content: "内容",
	}
	articleId := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
	author := "著者1"
	description := ""
	name := "ブログ名1"
	blog := &model.Blog{
		ID:            blogId,
		BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
		Author:        &author,
		Name:          &name,
		Description:   &description,
		PublishOption: model.PublishOptionPublic,
		CreatedAt:     &nowstr,
		UpdatedAt:     &nowstr,
	}

	content := "内容"

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		expected := &model.Article{
			ID:            articleId,
			Content:       &content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(blog, nil)
		ma.
			EXPECT().
			Create(now, *input, 1).
			Return(expected, nil)

		result, err := u.CreateArticle(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系：本体のブログ取得でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.CreateArticle(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：記事が紐づくブログがログインユーザーのブログではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(nil, sql.ErrNoRows)

		_, err := u.CreateArticle(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
	})

	t.Run("異常系：記事作成でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mb := mock_blog.NewMockIBlogRepository(ctrl)
		ma := mock_blog.NewMockIArticleRepository(ctrl)
		mp := mock_blog.NewMockIMParameterRepository(ctrl)
		u := NewBlogUsecase(mb, ma, mp)

		mb.
			EXPECT().
			FindById(blogId, 1).
			Return(blog, nil)
		ma.
			EXPECT().
			Create(now, *input, 1).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.CreateArticle(now, *input, 1)
		if err == nil {
			t.Error("should got error")
		}
	})
}
