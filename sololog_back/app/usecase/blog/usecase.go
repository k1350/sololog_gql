package blog

import (
	"database/sql"
	"time"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IBlogRepository interface {
	BeginTransaction() (*sql.Tx, error)
	FindByUserId(userId int) ([]*model.Blog, error)
	FindById(blogId string, userId int) (*model.Blog, error)
	FindByBlogKey(blogKey string, opts *model.BlogByKeyOpts, passwordInputTime *time.Time) (*model.Blog, error)
	IsAuthor(blogKey string, userId int) (bool, error)
	CountBlogs(userId int) (*int, error)
	Create(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error)
	Update(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error)
	DeleteById(tx *sql.Tx, blogId string, userId int) error
}

type IArticleRepository interface {
	FindByBlogId(blogId string, input model.ArticlePaginationInput) ([]*model.Article, error)
	HasPreviousPage(blogId string, input model.ArticlePaginationInput) (bool, error)
	HasNextPage(blogId string, input model.ArticlePaginationInput) (bool, error)
	FindByArticleKey(articleKey string, opts *model.ArticleByKeyOpts, passwordInputTime *time.Time) (*model.Article, error)
	Create(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error)
	Update(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error)
	DeleteById(articleId string, userId int) error
}

type IMParameterRepository interface {
	Find() (*model.MasterParameter, error)
}

type BlogUsecase struct {
	blogRepository       IBlogRepository
	articleRepository    IArticleRepository
	mParameterRepository IMParameterRepository
}

func NewBlogUsecase(br IBlogRepository, ar IArticleRepository, mr IMParameterRepository) *BlogUsecase {
	return &BlogUsecase{blogRepository: br, articleRepository: ar, mParameterRepository: mr}
}
