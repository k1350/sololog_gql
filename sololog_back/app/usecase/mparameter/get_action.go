package mparameter

import (
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *MParameterUsecase) Get() (*model.MasterParameter, error) {
	result, err := u.mParameterRepository.Find()
	if err != nil {
		return nil, errors.Wrap(err, "MParameter.Get error")
	}
	return result, nil
}
