package mparameter

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_mparameter"
)

func TestGet(t *testing.T) {
	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_mparameter.NewMockIMParameterRepository(ctrl)
		u := NewMParameterUsecase(m)

		expected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		m.
			EXPECT().
			Find().
			Return(expected, nil)
		result, err := u.Get()
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_mparameter.NewMockIMParameterRepository(ctrl)
		u := NewMParameterUsecase(m)

		m.
			EXPECT().
			Find().
			Return(nil, fmt.Errorf("DB error"))
		_, err := u.Get()
		if err == nil {
			t.Error("should got error")
		}
	})
}
