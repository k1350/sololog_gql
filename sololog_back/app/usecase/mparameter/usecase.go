package mparameter

import (
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IMParameterRepository interface {
	Find() (*model.MasterParameter, error)
}

type MParameterUsecase struct {
	mParameterRepository IMParameterRepository
}

func NewMParameterUsecase(r IMParameterRepository) *MParameterUsecase {
	return &MParameterUsecase{mParameterRepository: r}
}
