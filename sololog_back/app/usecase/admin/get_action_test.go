package admin

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_admin"
)

func TestFind(t *testing.T) {
	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		expected := &model.Config{AllowRegister: true}
		mc.
			EXPECT().
			Find().
			Return(expected, nil)

		res, err := u.Find(model.RoleAdmin)
		if err != nil {
			t.Error("got err:", err)
		}
		if res != expected {
			t.Error("should returns true.")
		}
	})

	t.Run("異常系：ユーザーがadmin権限ではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		_, err := u.Find(model.RoleUser)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Errorf("got:\n%v\n\nwant:\n%v", err, customError.ForbiddenError)
		}
	})

	t.Run("異常系：取得失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		mc.
			EXPECT().
			Find().
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.Find(model.RoleAdmin)
		if err == nil {
			t.Error("should got error")
		}
	})
}
