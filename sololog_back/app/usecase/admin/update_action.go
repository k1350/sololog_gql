package admin

import (
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *AdminUsecase) UpdateConfig(role model.Role, input model.UpdateConfigInput) (*model.Config, error) {
	if role != model.RoleAdmin {
		return nil, errors.Wrap(customError.ForbiddenError, "Admin.UpdateConfig error")
	}
	if input.AllowRegister != nil {
		err := u.configRepository.UpdateAllowRegister(*input.AllowRegister)
		if err != nil {
			return nil, errors.Wrap(err, "Admin.UpdateConfig error")
		}
	}

	return &model.Config{AllowRegister: *input.AllowRegister}, nil
}
