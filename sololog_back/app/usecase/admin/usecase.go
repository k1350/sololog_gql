package admin

import (
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IConfigRepository interface {
	Find() (*model.Config, error)
	UpdateAllowRegister(allow bool) error
}

type AdminUsecase struct {
	configRepository IConfigRepository
}

func NewAdminUsecase(con IConfigRepository) *AdminUsecase {
	return &AdminUsecase{configRepository: con}
}
