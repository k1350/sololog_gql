package admin

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_admin"
)

func TestUpdateConfig(t *testing.T) {
	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		expected := true
		mc.
			EXPECT().
			UpdateAllowRegister(expected).
			Return(nil)

		input := &model.UpdateConfigInput{
			AllowRegister: &expected,
		}
		res, err := u.UpdateConfig(model.RoleAdmin, *input)
		if err != nil {
			t.Error("got err:", err)
		}
		if res == nil {
			t.Error("should returns true.")
		} else if !res.AllowRegister {
			t.Error("should returns true.")
		}
	})

	t.Run("異常系：ユーザーがadmin権限ではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		expected := true
		input := &model.UpdateConfigInput{
			AllowRegister: &expected,
		}
		_, err := u.UpdateConfig(model.RoleUser, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Errorf("got:\n%v\n\nwant:\n%v", err, customError.ForbiddenError)
		}
	})

	t.Run("異常系：更新失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		mc := mock_admin.NewMockIConfigRepository(ctrl)
		u := NewAdminUsecase(mc)

		expected := false
		mc.
			EXPECT().
			UpdateAllowRegister(false).
			Return(fmt.Errorf("DB error"))

		input := &model.UpdateConfigInput{
			AllowRegister: &expected,
		}
		_, err := u.UpdateConfig(model.RoleAdmin, *input)
		if err == nil {
			t.Error("should got error")
		}
	})
}
