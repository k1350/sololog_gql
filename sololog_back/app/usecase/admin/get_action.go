package admin

import (
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *AdminUsecase) Find(role model.Role) (*model.Config, error) {
	if role != model.RoleAdmin {
		return nil, errors.Wrap(customError.ForbiddenError, "Admin.Find error")
	}
	result, err := u.configRepository.Find()
	if err != nil {
		return nil, errors.Wrap(err, "Admin.Find error")
	}
	return result, nil
}
