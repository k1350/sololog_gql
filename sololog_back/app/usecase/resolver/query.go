package resolver

import (
	"database/sql"
	"time"

	perrors "github.com/pkg/errors"
	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

func (u *ResolverUsecase) Blogs(userId int) ([]*model.Blog, error) {
	result, err := u.blogUsecase.GetBlogs(userId)
	if err != nil {
		clog.Error(err)
		return []*model.Blog{}, customError.RecreateError(err)
	}

	return result, nil
}

func (u *ResolverUsecase) BlogByID(id string, userId int) (*model.Blog, error) {
	result, err := u.blogUsecase.GetBlogByID(id, userId)
	if err != nil {
		if !perrors.Is(err, sql.ErrNoRows) {
			clog.Error(err)
			return nil, customError.RecreateError(err)
		}
		return nil, nil
	}

	return result, nil
}

func (u *ResolverUsecase) BlogByBlogKey(now time.Time, input model.BlogByBlogKeyInput, userId *int) (*model.Blog, error) {
	var passwordInputTime *time.Time
	if input.XJwtToken != nil {
		passwordInputTime = u.jWKUsecase.GetIssuedAt(now, *input.XJwtToken, input.BlogKey)
	}
	result, err := u.blogUsecase.GetBlogByBlogKey(input, passwordInputTime, userId)
	if err != nil {
		if !perrors.Is(err, customError.ForbiddenError) && !perrors.Is(err, customError.BlogAuthenticationExpiredError) && !perrors.Is(err, sql.ErrNoRows) {
			clog.Error(err)
		}
		if perrors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, customError.RecreateError(err)
	}
	if input.Password != nil {
		// パスワード入力でブログを取得した場合、JWTを作成する
		newToken, err := u.jWKUsecase.New(now, input.BlogKey)
		// エラーが起きても処理は継続する
		if err != nil {
			clog.Error(err)
		} else {
			result.XJwtToken = newToken
		}
	}

	return result, nil
}

func (u *ResolverUsecase) IsAuthor(blogKey string, userId int) (bool, error) {
	result, err := u.blogUsecase.IsAuthor(blogKey, userId)
	if err != nil {
		clog.Error(err)
		return false, customError.RecreateError(err)
	}
	return result, nil
}

func (u *ResolverUsecase) Articles(now time.Time, input model.BlogByBlogKeyInput, paginationInput model.ArticlePaginationInput, userId *int) (*model.ArticleConnection, error) {
	var passwordInputTime *time.Time
	if input.XJwtToken != nil {
		passwordInputTime = u.jWKUsecase.GetIssuedAt(now, *input.XJwtToken, input.BlogKey)
	}

	result, err := u.blogUsecase.GetArticles(input, paginationInput, passwordInputTime, userId)
	if err != nil {
		if !perrors.Is(err, customError.ForbiddenError) && !perrors.Is(err, customError.BlogAuthenticationExpiredError) {
			clog.Error(err)
		}
		return nil, customError.RecreateError(err)
	}
	if input.Password != nil {
		// パスワード入力でブログを取得した場合、JWTを作成する
		newToken, err := u.jWKUsecase.New(now, input.BlogKey)
		// エラーが起きても処理は継続する
		if err != nil {
			clog.Error(err)
		} else {
			result.XJwtToken = newToken
		}
	}

	return result, nil
}

func (u *ResolverUsecase) ArticleByArticleKey(now time.Time, input model.ArticleByArticleKeyInput, userId *int) (*model.Article, error) {
	var passwordInputTime *time.Time
	if input.XJwtToken != nil {
		passwordInputTime = u.jWKUsecase.GetIssuedAt(now, *input.XJwtToken, input.BlogKey)
	}
	result, err := u.blogUsecase.GetArticleByArticleKey(input, passwordInputTime, userId)
	if err != nil {
		if !perrors.Is(err, customError.ForbiddenError) && !perrors.Is(err, customError.BlogAuthenticationExpiredError) && !perrors.Is(err, sql.ErrNoRows) {
			clog.Error(err)
		}
		if perrors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, customError.RecreateError(err)
	}
	if input.Password != nil {
		// パスワード入力でブログを取得した場合、JWTを作成する
		newToken, err := u.jWKUsecase.New(now, input.BlogKey)
		// エラーが起きても処理は継続する
		if err != nil {
			clog.Error(err)
		} else {
			result.XJwtToken = newToken
		}
	}

	return result, nil
}

func (u *ResolverUsecase) MasterParameter() (*model.MasterParameter, error) {
	result, err := u.mParameterUsecase.Get()

	if err != nil {
		clog.Error(err)
		return nil, customError.RecreateError(err)
	}

	return result, nil
}

func (u *ResolverUsecase) Config(role model.Role) (*model.Config, error) {
	result, err := u.adminUsecase.Find(role)
	if err != nil {
		clog.Error(err)
		return result, customError.RecreateError(err)
	}
	return result, nil
}
