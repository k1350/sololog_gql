package resolver

import (
	"context"
	"time"

	perrors "github.com/pkg/errors"
	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

func (u *ResolverUsecase) Login(ctx context.Context, now time.Time, input model.LoginInput) (bool, error) {
	err := u.authUsecase.Login(ctx, now, input)
	if err != nil {
		if !perrors.Is(err, customError.RegisterNotAllowedError) {
			clog.Error(err)
		}
		return false, customError.RecreateError(err)
	}
	return true, nil
}

func (r *ResolverUsecase) Logout(now time.Time, userId int) (bool, error) {
	err := r.authUsecase.Logout(now, userId)
	if err != nil {
		clog.Error(err)
		return false, customError.RecreateError(err)
	}
	return true, nil
}

func (r *ResolverUsecase) DeleteUser(ctx context.Context, now time.Time, userId int, user model.UserInfoByToken) (bool, error) {
	err := r.authUsecase.DeleteUser(ctx, now, userId, user)
	if err != nil {
		if !perrors.Is(err, customError.OnlyAdminCannotDeleteError) {
			clog.Error(err)
		}
		return false, customError.RecreateError(err)
	}

	return true, nil
}

func (r *ResolverUsecase) CreateBlog(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error) {
	result, err := r.blogUsecase.CreateBlog(now, input, userId)
	if err != nil {
		clog.Error(err)
		return nil, customError.RecreateError(err)
	}

	return result, nil
}

func (r *ResolverUsecase) CreateArticle(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error) {
	result, err := r.blogUsecase.CreateArticle(now, input, userId)
	if err != nil {
		clog.Error(err)
		return nil, customError.RecreateError(err)
	}
	return result, nil
}

func (r *ResolverUsecase) UpdateBlog(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error) {
	result, err := r.blogUsecase.UpdateBlog(now, input, userId)
	if err != nil {
		clog.Error(err)
		return nil, customError.RecreateError(err)
	}
	return result, nil
}

func (r *ResolverUsecase) UpdateArticle(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error) {
	result, err := r.blogUsecase.UpdateArticle(now, input, userId)
	if err != nil {
		clog.Error(err)
		return nil, customError.RecreateError(err)
	}
	return result, nil
}

func (r *ResolverUsecase) DeleteBlog(id string, userId int) (bool, error) {
	err := r.blogUsecase.DeleteBlog(id, userId)
	if err != nil {
		clog.Error(err)
		return false, customError.RecreateError(err)
	}
	return true, nil
}

func (r *ResolverUsecase) DeleteArticle(id string, userId int) (bool, error) {
	err := r.blogUsecase.DeleteArticle(id, userId)
	if err != nil {
		clog.Error(err)
		return false, customError.RecreateError(err)
	}
	return true, nil
}

func (r *ResolverUsecase) UpdateConfig(role model.Role, input model.UpdateConfigInput) (*model.Config, error) {
	result, err := r.adminUsecase.UpdateConfig(role, input)
	if err != nil {
		clog.Error(err)
		return result, customError.RecreateError(err)
	}
	return result, nil
}
