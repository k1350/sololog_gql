package resolver

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_resolver"
)

func TestLogin(t *testing.T) {
	now := time.Now().UTC()
	var ctx context.Context
	input := &model.LoginInput{
		Token: "test",
	}

	t.Run("ログイン/新規登録成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := true

		auth.
			EXPECT().
			Login(ctx, now, *input).
			Return(nil)
		result, err := u.Login(ctx, now, *input)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("ログイン失敗: IDトークンが無効", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := false

		auth.
			EXPECT().
			Login(ctx, now, *input).
			Return(fmt.Errorf("Invalid IdToken"))
		result, err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.InternalServerError) {
			t.Error("got unexpected err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("新規登録失敗: 新規登録が許可されていない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := false

		auth.
			EXPECT().
			Login(ctx, now, *input).
			Return(errors.Wrap(customError.RegisterNotAllowedError, "error"))
		result, err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.RegisterNotAllowedError) {
			t.Error("got unexpected err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestLogout(t *testing.T) {
	now := time.Now().UTC()

	t.Run("ログアウト成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		auth.
			EXPECT().
			Logout(now, 1).
			Return(nil)

		expected := true

		result, err := u.Logout(now, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestDeleteUser(t *testing.T) {
	now := time.Now().UTC()
	var ctx context.Context

	user := &model.UserInfoByToken{
		UID:      "test",
		Provider: "google",
		Role:     model.RoleUser,
	}

	t.Run("ユーザー削除成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		auth.
			EXPECT().
			DeleteUser(ctx, now, 1, *user).
			Return(nil)

		expected := true

		result, err := u.DeleteUser(ctx, now, 1, *user)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("ユーザー削除失敗: 唯一の管理者ユーザーは削除できない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := false

		auth.
			EXPECT().
			DeleteUser(ctx, now, 1, *user).
			Return(errors.Wrap(customError.OnlyAdminCannotDeleteError, "error"))
		result, err := u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.OnlyAdminCannotDeleteError) {
			t.Error("got unexpected err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestCreateBlog(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	input := &model.CreateBlogInput{
		Author:        "著者",
		Name:          "名前",
		Description:   nil,
		PublishOption: model.PublishOptionPublic,
		Password:      nil,
		Links:         []*model.BlogLinkInput{},
	}

	t.Run("ブログ作成成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Blog{
			ID:            "id",
			BlogKey:       "key",
			Author:        &input.Author,
			Name:          &input.Name,
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			CreateBlog(now, *input, 1).
			Return(expected, nil)

		result, err := u.CreateBlog(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("ブログ作成失敗: ブログ作成数の上限超過", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			CreateBlog(now, *input, 1).
			Return(nil, errors.Wrap(errors.New("The number of blogs has exceeded the limit"), "error"))

		result, err := u.CreateBlog(now, *input, 1)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.InternalServerError) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}

func TestCreateArticle(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	input := &model.CreateArticleInput{
		BlogID:  "id",
		Content: "内容",
	}

	t.Run("記事作成成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Article{
			ID:            "id",
			ArticleKey:    "key",
			Content:       &input.Content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			CreateArticle(now, *input, 1).
			Return(expected, nil)

		result, err := u.CreateArticle(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("記事作成失敗: 記事が紐づくブログがログインユーザーのブログではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			CreateArticle(now, *input, 1).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))

		result, err := u.CreateArticle(now, *input, 1)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}

func TestUpdateBlog(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	dummy := "著者"
	input := &model.UpdateBlogInput{
		Author: &dummy,
	}

	t.Run("ブログ更新成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Blog{
			ID:            "id",
			BlogKey:       "key",
			Author:        &dummy,
			Name:          &dummy,
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			UpdateBlog(now, *input, 1).
			Return(expected, nil)

		result, err := u.UpdateBlog(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系: 対象のブログが見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			UpdateBlog(now, *input, 1).
			Return(nil, errors.Wrap(customError.NotFoundError, "error"))

		result, err := u.UpdateBlog(now, *input, 1)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}

func TestUpdateArticle(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	input := &model.UpdateArticleInput{
		ID:      "id",
		Content: "内容",
	}

	t.Run("記事更新成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Article{
			ID:            "id",
			ArticleKey:    "key",
			Content:       &input.Content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			UpdateArticle(now, *input, 1).
			Return(expected, nil)

		result, err := u.UpdateArticle(now, *input, 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("記事更新失敗: 対象の記事が見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			UpdateArticle(now, *input, 1).
			Return(nil, errors.Wrap(customError.NotFoundError, "error"))

		result, err := u.UpdateArticle(now, *input, 1)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}

func TestDeleteBlog(t *testing.T) {
	t.Run("ブログ削除成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			DeleteBlog("id", 1).
			Return(nil)
		expected := true

		result, err := u.DeleteBlog("id", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("ブログ削除失敗: 対象のブログが見つからない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			DeleteBlog("id", 1).
			Return(errors.Wrap(customError.NotFoundError, "error"))

		result, err := u.DeleteBlog("id", 1)
		expected := false

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestDeleteArticle(t *testing.T) {
	t.Run("記事削除成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			DeleteArticle("id", 1).
			Return(nil)
		expected := true

		result, err := u.DeleteArticle("id", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("記事削除失敗: 対象の記事が存在しない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			DeleteArticle("id", 1).
			Return(errors.Wrap(customError.NotFoundError, "error"))

		result, err := u.DeleteArticle("id", 1)
		expected := false

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestUpdateConfig(t *testing.T) {
	allow := true
	input := &model.UpdateConfigInput{
		AllowRegister: &allow,
	}

	t.Run("Config更新成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Config{
			AllowRegister: allow,
		}

		admin.
			EXPECT().
			UpdateConfig(model.RoleAdmin, *input).
			Return(expected, nil)

		result, err := u.UpdateConfig(model.RoleAdmin, *input)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("Config更新失敗: 管理者ユーザーではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		admin.
			EXPECT().
			UpdateConfig(model.RoleUser, *input).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))

		result, err := u.UpdateConfig(model.RoleUser, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}
