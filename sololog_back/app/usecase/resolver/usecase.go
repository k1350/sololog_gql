package resolver

import (
	"context"
	"time"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IAuthUsecase interface {
	Login(ctx context.Context, now time.Time, input model.LoginInput) error
	Logout(now time.Time, userId int) error
	DeleteUser(ctx context.Context, now time.Time, userId int, user model.UserInfoByToken) error
}

type IBlogUsecase interface {
	CreateBlog(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error)
	CreateArticle(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error)
	GetBlogs(userId int) ([]*model.Blog, error)
	GetBlogByID(blogId string, userId int) (*model.Blog, error)
	GetBlogByBlogKey(input model.BlogByBlogKeyInput, passwordInputTime *time.Time, userId *int) (*model.Blog, error)
	IsAuthor(blogKey string, userId int) (bool, error)
	GetArticles(input model.BlogByBlogKeyInput, paginationInput model.ArticlePaginationInput, passwordInputTime *time.Time, userId *int) (*model.ArticleConnection, error)
	GetArticleByArticleKey(input model.ArticleByArticleKeyInput, passwordInputTime *time.Time, userId *int) (*model.Article, error)
	UpdateBlog(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error)
	UpdateArticle(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error)
	DeleteBlog(blogId string, userId int) error
	DeleteArticle(articleId string, userId int) error
}

type IMParameterUsecase interface {
	Get() (*model.MasterParameter, error)
}

type IAdminUsecase interface {
	Find(role model.Role) (*model.Config, error)
	UpdateConfig(role model.Role, input model.UpdateConfigInput) (*model.Config, error)
}

type IJWKUsecase interface {
	GetIssuedAt(now time.Time, token string, blogKey string) *time.Time
	New(now time.Time, blogKey string) (*string, error)
}

type ResolverUsecase struct {
	authUsecase       IAuthUsecase
	blogUsecase       IBlogUsecase
	mParameterUsecase IMParameterUsecase
	adminUsecase      IAdminUsecase
	jWKUsecase        IJWKUsecase
}

func NewResolverUsecase(auth IAuthUsecase, blog IBlogUsecase, mParameter IMParameterUsecase, admin IAdminUsecase, jwk IJWKUsecase) *ResolverUsecase {
	return &ResolverUsecase{
		authUsecase:       auth,
		blogUsecase:       blog,
		mParameterUsecase: mParameter,
		adminUsecase:      admin,
		jWKUsecase:        jwk,
	}
}
