package resolver

import (
	"database/sql"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_resolver"
)

func TestBlogs(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	t.Run("ブログ取得成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		author := "Author"
		name := "Name"

		expected := []*model.Blog{
			{
				ID:            "1",
				BlogKey:       "key",
				Author:        &author,
				Name:          &name,
				Description:   nil,
				PublishOption: model.PublishOptionPublic,
				CreatedAt:     &nowstr,
				UpdatedAt:     &nowstr,
			},
		}

		blog.
			EXPECT().
			GetBlogs(1).
			Return(expected, nil)
		result, err := u.Blogs(1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestBlogByID(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	t.Run("ブログ取得成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		author := "Author"
		name := "Name"

		expected := &model.Blog{
			ID:            "1",
			BlogKey:       "key",
			Author:        &author,
			Name:          &name,
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			GetBlogByID("1", 1).
			Return(expected, nil)
		result, err := u.BlogByID("1", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("ブログが見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			GetBlogByID("1", 1).
			Return(nil, sql.ErrNoRows)
		result, err := u.BlogByID("1", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}
	})
}

func TestBlogByBlogKey(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	userId := 1

	t.Run("ブログ取得成功（トークンあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		token := "token"
		input := &model.BlogByBlogKeyInput{
			BlogKey:   "key",
			XJwtToken: &token,
		}

		author := "Author"
		name := "Name"
		expected := &model.Blog{
			ID:            "1",
			BlogKey:       "key",
			Author:        &author,
			Name:          &name,
			Description:   nil,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		passwordInputTime := time.Now().UTC()
		jwk.
			EXPECT().
			GetIssuedAt(now, *input.XJwtToken, input.BlogKey).
			Return(&passwordInputTime)

		blog.
			EXPECT().
			GetBlogByBlogKey(*input, &passwordInputTime, &userId).
			Return(expected, nil)

		result, err := u.BlogByBlogKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("ブログ取得成功（トークンなし、パスワードあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.BlogByBlogKeyInput{
			BlogKey:  "key",
			Password: &password,
		}

		author := "Author"
		name := "Name"
		newToken := "newToken"
		expected := &model.Blog{
			ID:            "1",
			BlogKey:       "key",
			Author:        &author,
			Name:          &name,
			Description:   nil,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			GetBlogByBlogKey(*input, nil, &userId).
			Return(expected, nil)
		jwk.
			EXPECT().
			New(now, input.BlogKey).
			Return(&newToken, nil)

		result, err := u.BlogByBlogKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != &newToken {
			t.Errorf("token should be \n%v", newToken)
		}
	})

	t.Run("ブログ取得成功（トークンなし、パスワードなし）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		input := &model.BlogByBlogKeyInput{
			BlogKey: "key",
		}

		author := "Author"
		name := "Name"
		expected := &model.Blog{
			ID:            "1",
			BlogKey:       "key",
			Author:        &author,
			Name:          &name,
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			GetBlogByBlogKey(*input, nil, &userId).
			Return(expected, nil)

		result, err := u.BlogByBlogKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("ブログが見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		input := &model.BlogByBlogKeyInput{
			BlogKey: "key",
		}

		blog.
			EXPECT().
			GetBlogByBlogKey(*input, nil, &userId).
			Return(nil, sql.ErrNoRows)

		result, err := u.BlogByBlogKey(now, *input, &userId)
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("ブログ取得失敗: パスワードが一致しなかった/閲覧権限なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.BlogByBlogKeyInput{
			BlogKey:  "key",
			Password: &password,
		}

		blog.
			EXPECT().
			GetBlogByBlogKey(*input, nil, &userId).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))

		result, err := u.BlogByBlogKey(now, *input, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})

	t.Run("ブログ取得失敗: トークンの有効期限が切れた", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		token := "token"
		input := &model.BlogByBlogKeyInput{
			BlogKey:   "key",
			XJwtToken: &token,
		}

		passwordInputTime := time.Now().UTC()
		jwk.
			EXPECT().
			GetIssuedAt(now, *input.XJwtToken, input.BlogKey).
			Return(&passwordInputTime)
		blog.
			EXPECT().
			GetBlogByBlogKey(*input, &passwordInputTime, &userId).
			Return(nil, errors.Wrap(customError.BlogAuthenticationExpiredError, "error"))

		result, err := u.BlogByBlogKey(now, *input, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})
}

func TestIsAuthor(t *testing.T) {
	t.Run("著者判定成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		blog.
			EXPECT().
			IsAuthor("key", 1).
			Return(true, nil)
		result, err := u.IsAuthor("key", 1)
		if err != nil {
			t.Error("got err:", err)
		}
		if result != true {
			t.Error("got result:", result)
		}
	})
}

func TestArticles(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	userId := 1
	content1 := "内容1"
	content2 := "内容2"
	findArticlesExpected := []*model.Article{
		{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content1,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		},
		{
			ID:            "01G65Z755AFWAKHE12NY0CQ9FH",
			ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
			Content:       &content2,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		},
	}
	first := 2
	after := "02G65Z755AFWAKHE12NY0CQ9FH"
	paginationInput := &model.ArticlePaginationInput{
		First: &first,
		After: &after,
	}

	startCursor := "000XAL6S41ACTAV9WEVGEMMVR8"
	endCursor := "01G65Z755AFWAKHE12NY0CQ9FH"

	t.Run("記事取得成功（トークンあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		token := "token"
		input := &model.BlogByBlogKeyInput{
			BlogKey:   "key",
			XJwtToken: &token,
		}

		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		passwordInputTime := time.Now().UTC()
		jwk.
			EXPECT().
			GetIssuedAt(now, *input.XJwtToken, input.BlogKey).
			Return(&passwordInputTime)

		blog.
			EXPECT().
			GetArticles(*input, *paginationInput, &passwordInputTime, &userId).
			Return(expected, nil)

		result, err := u.Articles(now, *input, *paginationInput, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("記事取得成功（トークンなし、パスワードあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.BlogByBlogKeyInput{
			BlogKey:  "key",
			Password: &password,
		}

		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}
		newToken := "newToken"

		blog.
			EXPECT().
			GetArticles(*input, *paginationInput, nil, &userId).
			Return(expected, nil)
		jwk.
			EXPECT().
			New(now, input.BlogKey).
			Return(&newToken, nil)

		result, err := u.Articles(now, *input, *paginationInput, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != &newToken {
			t.Errorf("token should be \n%v", newToken)
		}
	})

	t.Run("記事取得成功（トークンなし、パスワードなし）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		input := &model.BlogByBlogKeyInput{
			BlogKey: "key",
		}

		expected := &model.ArticleConnection{
			PageInfo: &model.PageInfo{
				StartCursor:     &startCursor,
				EndCursor:       &endCursor,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			Edges: []*model.ArticleEdge{
				{
					Cursor: findArticlesExpected[0].ID,
					Node:   findArticlesExpected[0],
				},
				{
					Cursor: findArticlesExpected[1].ID,
					Node:   findArticlesExpected[1],
				},
			},
		}

		blog.
			EXPECT().
			GetArticles(*input, *paginationInput, nil, &userId).
			Return(expected, nil)

		result, err := u.Articles(now, *input, *paginationInput, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("記事取得失敗: パスワードが一致しなかった/閲覧権限なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.BlogByBlogKeyInput{
			BlogKey:  "key",
			Password: &password,
		}

		blog.
			EXPECT().
			GetArticles(*input, *paginationInput, nil, &userId).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))

		result, err := u.Articles(now, *input, *paginationInput, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})

	t.Run("記事取得失敗: トークンの有効期限が切れた", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.BlogByBlogKeyInput{
			BlogKey:  "key",
			Password: &password,
		}

		blog.
			EXPECT().
			GetArticles(*input, *paginationInput, nil, &userId).
			Return(nil, errors.Wrap(customError.BlogAuthenticationExpiredError, "error"))

		result, err := u.Articles(now, *input, *paginationInput, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})
}

func TestArticleByArticleKey(t *testing.T) {
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	userId := 1
	content := "content"

	t.Run("記事取得成功（トークンあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		token := "token"
		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
			XJwtToken:  &token,
		}

		expected := &model.Article{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		passwordInputTime := time.Now().UTC()
		jwk.
			EXPECT().
			GetIssuedAt(now, *input.XJwtToken, input.BlogKey).
			Return(&passwordInputTime)

		blog.
			EXPECT().
			GetArticleByArticleKey(*input, &passwordInputTime, &userId).
			Return(expected, nil)

		result, err := u.ArticleByArticleKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("記事取得成功（トークンなし、パスワードあり）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
			Password:   &password,
		}

		expected := &model.Article{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}
		newToken := "newToken"

		blog.
			EXPECT().
			GetArticleByArticleKey(*input, nil, &userId).
			Return(expected, nil)
		jwk.
			EXPECT().
			New(now, input.BlogKey).
			Return(&newToken, nil)

		result, err := u.ArticleByArticleKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != &newToken {
			t.Errorf("token should be \n%v", newToken)
		}
	})

	t.Run("記事取得成功（トークンなし、パスワードなし）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
		}

		expected := &model.Article{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		blog.
			EXPECT().
			GetArticleByArticleKey(*input, nil, &userId).
			Return(expected, nil)

		result, err := u.ArticleByArticleKey(now, *input, &userId)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
		if expected.XJwtToken != nil {
			t.Error("token should be nil")
		}
	})

	t.Run("記事が見つからなかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
		}

		blog.
			EXPECT().
			GetArticleByArticleKey(*input, nil, &userId).
			Return(nil, sql.ErrNoRows)

		result, err := u.ArticleByArticleKey(now, *input, &userId)
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("記事取得失敗: パスワードが一致しなかった/閲覧権限なし", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		password := "password"
		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
			Password:   &password,
		}

		blog.
			EXPECT().
			GetArticleByArticleKey(*input, nil, &userId).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))

		result, err := u.ArticleByArticleKey(now, *input, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})

	t.Run("記事取得失敗: トークンの有効期限が切れた", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		token := "token"
		input := &model.ArticleByArticleKeyInput{
			ArticleKey: "key",
			XJwtToken:  &token,
		}

		passwordInputTime := time.Now().UTC()
		jwk.
			EXPECT().
			GetIssuedAt(now, *input.XJwtToken, input.BlogKey).
			Return(&passwordInputTime)
		blog.
			EXPECT().
			GetArticleByArticleKey(*input, &passwordInputTime, &userId).
			Return(nil, errors.Wrap(customError.BlogAuthenticationExpiredError, "error"))

		result, err := u.ArticleByArticleKey(now, *input, &userId)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})
}

func TestMasterParameter(t *testing.T) {
	t.Run("取得成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		mp.
			EXPECT().
			Get().
			Return(expected, nil)
		result, err := u.MasterParameter()
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})
}

func TestConfig(t *testing.T) {
	t.Run("取得成功", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		expected := &model.Config{AllowRegister: true}
		admin.
			EXPECT().
			Find(model.RoleAdmin).
			Return(expected, nil)
		result, err := u.Config(model.RoleAdmin)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("管理者権限ではない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		auth := mock_resolver.NewMockIAuthUsecase(ctrl)
		blog := mock_resolver.NewMockIBlogUsecase(ctrl)
		mp := mock_resolver.NewMockIMParameterUsecase(ctrl)
		admin := mock_resolver.NewMockIAdminUsecase(ctrl)
		jwk := mock_resolver.NewMockIJWKUsecase(ctrl)
		u := NewResolverUsecase(auth, blog, mp, admin, jwk)

		admin.
			EXPECT().
			Find(model.RoleUser).
			Return(nil, errors.Wrap(customError.ForbiddenError, "error"))
		result, err := u.Config(model.RoleUser)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}
		if result != nil {
			t.Errorf("got:\n%v", result)
		}
	})
}
