package auth

import (
	"context"
	"fmt"
	"testing"
	"time"

	firebaseAuth "firebase.google.com/go/v4/auth"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_auth"
)

func TestDeleteUser(t *testing.T) {
	now := time.Now().UTC()
	var ctx context.Context

	admin := &model.UserInfoByToken{
		UID:      "test1",
		Provider: "google",
		Role:     model.RoleAdmin,
	}

	user := &model.UserInfoByToken{
		UID:      "test2",
		Provider: "facebook",
		Role:     model.RoleUser,
	}

	t.Run("正常系：管理者ユーザーの削除", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		mu.
			EXPECT().
			CountAdminUser().
			Return(2, nil)

		users := []*model.ProviderUserForDB{
			{
				UID:       "test1",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test11",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test1", "test11"}).
			Return(nil)

		mu.
			EXPECT().
			DeleteUserById(tx, 1).
			Return(nil)

		mb.
			EXPECT().
			DeleteByUserId(tx, 1).
			Return(nil)

		dbmock.ExpectCommit()

		ma.
			EXPECT().
			DeleteUsers(ctx, []string{"test1", "test11"}).
			Return(nil, nil)

		err = u.DeleteUser(ctx, now, 1, *admin)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("正常系：通常ユーザーの削除", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test2", "test22"}).
			Return(nil)

		mu.
			EXPECT().
			DeleteUserById(tx, 1).
			Return(nil)

		mb.
			EXPECT().
			DeleteByUserId(tx, 1).
			Return(nil)

		dbmock.ExpectCommit()

		ma.
			EXPECT().
			DeleteUsers(ctx, []string{"test2", "test22"}).
			Return(nil, nil)

		err = u.DeleteUser(ctx, now, 1, *user)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("異常系：管理者ユーザー数カウントでエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		mu.
			EXPECT().
			CountAdminUser().
			Return(0, fmt.Errorf("DB error"))

		err := u.DeleteUser(ctx, now, 1, *admin)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：管理者ユーザーが一人しかいない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		mu.
			EXPECT().
			CountAdminUser().
			Return(1, nil)

		err := u.DeleteUser(ctx, now, 1, *admin)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.OnlyAdminCannotDeleteError) {
			t.Errorf("got:\n%v\n\nwant:\n%v", err, customError.OnlyAdminCannotDeleteError)
		}
	})

	t.Run("異常系：削除対象のUID取得でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(nil, fmt.Errorf("DB error"))

		err := u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：トランザクション開始でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(nil, fmt.Errorf("DB error"))

		err := u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ProviderUser 削除でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test2", "test22"}).
			Return(fmt.Errorf("DB error"))

		dbmock.ExpectRollback()

		err = u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：User 削除でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test2", "test22"}).
			Return(nil)

		mu.
			EXPECT().
			DeleteUserById(tx, 1).
			Return(fmt.Errorf("DB error"))

		dbmock.ExpectRollback()

		err = u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：Blog 削除でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test2", "test22"}).
			Return(nil)

		mu.
			EXPECT().
			DeleteUserById(tx, 1).
			Return(nil)

		mb.
			EXPECT().
			DeleteByUserId(tx, 1).
			Return(fmt.Errorf("DB error"))

		dbmock.ExpectRollback()

		err = u.DeleteUser(ctx, now, 1, *user)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：Firebase からのユーザー削除でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		db, dbmock, err := sqlmock.New()
		if err != nil {
			t.Error("failed to open sqlmock database:", err)
		}
		defer db.Close()

		dbmock.ExpectBegin()
		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		users := []*model.ProviderUserForDB{
			{
				UID:       "test2",
				Provider:  "google",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test22",
				Provider:  "facebook",
				UserId:    1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		mu.
			EXPECT().
			FindProviderUsersByUserId(1).
			Return(users, nil)

		mb.
			EXPECT().
			BeginTransaction().
			Return(tx, nil)

		mu.
			EXPECT().
			DeleteProviderUserByUIDs(tx, []string{"test2", "test22"}).
			Return(nil)

		mu.
			EXPECT().
			DeleteUserById(tx, 1).
			Return(nil)

		mb.
			EXPECT().
			DeleteByUserId(tx, 1).
			Return(nil)

		dbmock.ExpectCommit()

		fbErrorInfo := &firebaseAuth.DeleteUsersErrorInfo{
			Index:  1,
			Reason: "error",
		}

		fbError := &firebaseAuth.DeleteUsersResult{
			SuccessCount: 1,
			FailureCount: 1,
			Errors: []*firebaseAuth.DeleteUsersErrorInfo{
				fbErrorInfo,
			},
		}

		ma.
			EXPECT().
			DeleteUsers(ctx, []string{"test2", "test22"}).
			Return(fbError, fmt.Errorf("Delete user error"))

		err = u.DeleteUser(ctx, now, 1, *user)
		if err != nil {
			t.Error("got err:", err)
		}
	})
}
