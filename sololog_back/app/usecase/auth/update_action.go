package auth

import (
	"context"
	"database/sql"
	"strconv"
	"time"

	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

func (u *AuthUsecase) Login(ctx context.Context, now time.Time, input model.LoginInput) error {
	info, err := u.authRepository.VerifyToken(ctx, input.Token)
	if err != nil {
		return errors.Wrap(err, "Auth.Login error")
	}
	user, err := u.userRepository.FindUserByUID(info.UID, info.Provider)
	switch {
	case errors.Is(err, sql.ErrNoRows):
		// 新規登録可能かどうか確認する
		config, err := u.configRepository.Find()
		if err != nil {
			return errors.Wrap(err, "Auth.Login error")
		}
		if !config.AllowRegister {
			// Firebase 側に登録されてしまったユーザーを削除する
			uids := []string{info.UID}
			deleteUsersResult, err := u.authRepository.DeleteUsers(ctx, uids)
			if err != nil {
				// Firebase からのユーザー削除に失敗してもエラーログに書き出すだけにする。ユーザー削除は後でコンソールで対応する。
				clog.Error("Failure Count: " + strconv.Itoa(deleteUsersResult.FailureCount))
				for _, v := range deleteUsersResult.Errors {
					clog.Error("UID: " + uids[v.Index] + ", Reason: " + v.Reason)
				}
			}
			return errors.Wrap(customError.RegisterNotAllowedError, "Auth.Login error")
		}

		// ユーザー数を数える
		count, err := u.userRepository.CountUser()
		if err != nil {
			return errors.Wrap(err, "Auth.Login error")
		}
		var role model.Role
		if count == 0 {
			role = model.RoleAdmin
		} else {
			role = model.RoleUser
		}
		// 新規登録
		_, err = u.userRepository.Create(now, info.UID, info.Provider, role)
		if err != nil {
			return errors.Wrap(err, "Auth.Login error")
		}
		// カスタムクレームをセット
		err = u.authRepository.SetRole(ctx, info.UID, role)
		if err != nil {
			return errors.Wrap(err, "Auth.Login error")
		}
		// もし最初の一人の登録だったら新規登録不可に変更
		if count == 0 {
			return u.configRepository.UpdateAllowRegister(false)
		}
		return nil
	case err != nil:
		return errors.Wrap(err, "Auth.Login error")
	default:
		// ログイン
		loginInput := &model.UpdateUserInput{
			ID:          user.ID,
			LastLogined: &now,
		}
		err := u.userRepository.Update(now, *loginInput)
		if err != nil {
			return errors.Wrap(err, "Auth.Login error")
		}
		return nil
	}
}

func (u *AuthUsecase) Logout(now time.Time, userId int) error {
	logoutInput := &model.UpdateUserInput{
		ID:           userId,
		LastLogouted: &now,
	}
	err := u.userRepository.Update(now, *logoutInput)
	if err != nil {
		return errors.Wrap(err, "Auth.Logout error")
	}
	return nil
}
