package auth

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_auth"
)

func TestGetFirebaseUser(t *testing.T) {
	var ctx context.Context
	now := time.Now()

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleAdmin,
			IssuedAt: now,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)

		result, err := u.GetFirebaseUser(ctx, "token")
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, info) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, info)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(nil, fmt.Errorf("Invalid IdToken"))

		_, err := u.GetFirebaseUser(ctx, "token")
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetUserId(t *testing.T) {
	now := time.Now().UTC()
	after := now.Add(1 * time.Minute)

	t.Run("正常系：ユーザーが見つかった", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		user := &model.UserForDB{
			ID:           1,
			Role:         model.RoleAdmin,
			LastLogined:  now,
			LastLogouted: &now,
			CreatedAt:    now,
			UpdatedAt:    now,
		}

		mu.
			EXPECT().
			FindUserByUID("user", "provider").
			Return(user, nil)

		result, err := u.GetUserId("user", "provider", after)
		if err != nil {
			t.Error("got err:", err)
		}
		if *result != user.ID {
			t.Errorf("got:\n%v\n\nwant:\n%v", *result, user.ID)
		}
	})

	t.Run("異常系：ユーザーの最終ログアウト日時<=トークンの発行日時", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		mu.
			EXPECT().
			FindUserByUID("user", "provider").
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetUserId("user", "provider", now)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ユーザーが存在しない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		mu.
			EXPECT().
			FindUserByUID("user", "provider").
			Return(nil, sql.ErrNoRows)

		_, err := u.GetUserId("user", "provider", after)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}
	})
}
