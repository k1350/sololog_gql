package auth

import (
	"context"
	"strconv"
	"time"

	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

func (u *AuthUsecase) DeleteUser(ctx context.Context, now time.Time, userId int, user model.UserInfoByToken) error {
	if user.Role == model.RoleAdmin {
		cnt, err := u.userRepository.CountAdminUser()
		if err != nil {
			return errors.Wrap(err, "Auth.DeleteUser error")
		}
		if cnt < 2 {
			return errors.Wrap(customError.OnlyAdminCannotDeleteError, "Auth.DeleteUser error")
		}
	}

	// 削除対象のUID取得
	users, err := u.userRepository.FindProviderUsersByUserId(userId)
	if err != nil {
		return errors.Wrap(err, "Auth.DeleteUser error")
	}
	var uids []string
	for i := range users {
		uids = append(uids, users[i].UID)
	}

	// トランザクション開始
	tx, err := u.blogRepository.BeginTransaction()
	if err != nil {
		return errors.Wrap(err, "Auth.DeleteUser error")
	}
	defer func() {
		// panicが起きたらロールバック
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p)
		}
	}()

	err = u.userRepository.DeleteProviderUserByUIDs(tx, uids)
	if err != nil {
		tx.Rollback()
		return errors.Wrap(err, "Auth.DeleteUser error")
	}

	err = u.userRepository.DeleteUserById(tx, userId)
	if err != nil {
		tx.Rollback()
		return errors.Wrap(err, "Auth.DeleteUser error")
	}

	err = u.blogRepository.DeleteByUserId(tx, userId)
	if err != nil {
		tx.Rollback()
		return errors.Wrap(err, "Auth.DeleteUser error")
	}

	tx.Commit()

	deleteUsersResult, err := u.authRepository.DeleteUsers(ctx, uids)
	if err != nil {
		// Firebase からのユーザー削除に失敗しても、アプリケーション的にはユーザーは消せている状態なのでエラーログに書き出すだけにする。ユーザー削除は後でコンソールで対応する。
		clog.Error("Failure Count: " + strconv.Itoa(deleteUsersResult.FailureCount))
		for _, v := range deleteUsersResult.Errors {
			clog.Error("UID: " + uids[v.Index] + ", Reason: " + v.Reason)
		}
	}

	return nil
}
