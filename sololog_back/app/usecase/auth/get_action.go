package auth

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func (u *AuthUsecase) GetFirebaseUser(ctx context.Context, token string) (*model.UserInfoByToken, error) {
	result, err := u.authRepository.VerifyToken(ctx, token)
	if err != nil {
		return nil, errors.Wrap(err, "Auth.GetFirebaseUser error")
	}
	return result, nil
}

func (u *AuthUsecase) GetUserId(uid string, provider string, issuedAt time.Time) (*int, error) {
	user, err := u.userRepository.FindUserByUID(uid, provider)
	if err != nil {
		return nil, errors.Wrap(err, "Auth.GetUserId error")
	}
	if user.LastLogouted != nil {
		if issuedAt.Before(*user.LastLogouted) {
			return nil, errors.Wrap(errors.New("Expired IdToken"), "Auth.GetUserId error")
		}
	}
	return &user.ID, nil
}
