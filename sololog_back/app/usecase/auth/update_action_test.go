package auth

import (
	"context"
	"database/sql"
	"fmt"
	"testing"
	"time"

	firebaseAuth "firebase.google.com/go/v4/auth"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_auth"
)

func TestLogin(t *testing.T) {
	now := time.Now().UTC()
	var ctx context.Context
	input := &model.LoginInput{
		Token: "token",
	}

	t.Run("正常系：ログイン", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleAdmin,
		}

		user := &model.UserForDB{
			ID:          1,
			Role:        model.RoleAdmin,
			LastLogined: now,
			CreatedAt:   now,
			UpdatedAt:   now,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(user, nil)

		loginInput := &model.UpdateUserInput{
			ID:          user.ID,
			LastLogined: &now,
		}
		mu.
			EXPECT().
			Update(now, *loginInput).
			Return(nil)
		err := u.Login(ctx, now, *input)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("正常系：新規登録（一人目）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)
		var id int64 = 1

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(0, nil)
		mu.
			EXPECT().
			Create(now, info.UID, info.Provider, model.RoleAdmin).
			Return(&id, nil)
		ma.
			EXPECT().
			SetRole(ctx, info.UID, model.RoleAdmin).
			Return(nil)
		con.
			EXPECT().
			UpdateAllowRegister(false).
			Return(nil)
		err := u.Login(ctx, now, *input)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("正常系：新規登録（二人目以降登録許可時）", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)
		var id int64 = 1

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(1, nil)
		mu.
			EXPECT().
			Create(now, info.UID, info.Provider, model.RoleUser).
			Return(&id, nil)
		ma.
			EXPECT().
			SetRole(ctx, info.UID, model.RoleUser).
			Return(nil)
		err := u.Login(ctx, now, *input)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("正常系：新規登録不許可", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: false}, nil)

		ma.
			EXPECT().
			DeleteUsers(ctx, []string{info.UID}).
			Return(nil, nil)

		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.RegisterNotAllowedError) {
			t.Error("got unexpected err:", err)
		}
	})

	t.Run("異常系：トークンの検証に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(nil, fmt.Errorf("Invalid IdToken"))

		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ユーザーの取得に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleAdmin,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, fmt.Errorf("DB error"))

		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：最終ログイン日時の更新に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleAdmin,
		}

		user := &model.UserForDB{
			ID:          1,
			Role:        model.RoleAdmin,
			LastLogined: now,
			CreatedAt:   now,
			UpdatedAt:   now,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(user, nil)

		loginInput := &model.UpdateUserInput{
			ID:          user.ID,
			LastLogined: &now,
		}
		mu.
			EXPECT().
			Update(now, *loginInput).
			Return(fmt.Errorf("DB error"))
		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：登録許可モード取得に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(nil, fmt.Errorf("DB error"))

		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ユーザー数カウントに失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(0, fmt.Errorf("DB error"))
		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：新規登録不許可時にユーザー削除に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: false}, nil)

		fbErrorInfo := &firebaseAuth.DeleteUsersErrorInfo{
			Index:  0,
			Reason: "error",
		}

		fbError := &firebaseAuth.DeleteUsersResult{
			SuccessCount: 0,
			FailureCount: 1,
			Errors: []*firebaseAuth.DeleteUsersErrorInfo{
				fbErrorInfo,
			},
		}
		ma.
			EXPECT().
			DeleteUsers(ctx, []string{info.UID}).
			Return(fbError, fmt.Errorf("Delete user error"))

		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.RegisterNotAllowedError) {
			t.Error("got unexpected err:", err)
		}
	})

	t.Run("異常系：新規登録に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(0, nil)
		mu.
			EXPECT().
			Create(now, info.UID, info.Provider, model.RoleAdmin).
			Return(nil, fmt.Errorf("DB error"))
		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：ロールセットに失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)
		var id int64 = 1

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(0, nil)
		mu.
			EXPECT().
			Create(now, info.UID, info.Provider, model.RoleAdmin).
			Return(&id, nil)
		ma.
			EXPECT().
			SetRole(ctx, info.UID, model.RoleAdmin).
			Return(fmt.Errorf("Setting custom claims error"))
		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：新規登録許可モード変更に失敗", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)
		var id int64 = 1

		info := &model.UserInfoByToken{
			UID:      "uid",
			Provider: "provider",
			Role:     model.RoleUndefined,
		}

		ma.
			EXPECT().
			VerifyToken(ctx, "token").
			Return(info, nil)
		mu.
			EXPECT().
			FindUserByUID(info.UID, info.Provider).
			Return(nil, sql.ErrNoRows)
		con.
			EXPECT().
			Find().
			Return(&model.Config{AllowRegister: true}, nil)
		mu.
			EXPECT().
			CountUser().
			Return(0, nil)
		mu.
			EXPECT().
			Create(now, info.UID, info.Provider, model.RoleAdmin).
			Return(&id, nil)
		ma.
			EXPECT().
			SetRole(ctx, info.UID, model.RoleAdmin).
			Return(nil)
		con.
			EXPECT().
			UpdateAllowRegister(false).
			Return(fmt.Errorf("DB error"))
		err := u.Login(ctx, now, *input)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestLogout(t *testing.T) {
	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		input := &model.UpdateUserInput{
			ID:           -1,
			LastLogouted: &now,
		}
		mu.
			EXPECT().
			Update(now, *input).
			Return(nil)

		err := u.Logout(now, -1)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		ma := mock_auth.NewMockIAuthRepository(ctrl)
		mu := mock_auth.NewMockIUserRepository(ctrl)
		mb := mock_auth.NewMockIBlogRepository(ctrl)
		con := mock_auth.NewMockIConfigRepository(ctrl)
		u := NewAuthUsecase(ma, mu, mb, con)

		input := &model.UpdateUserInput{
			ID:           -1,
			LastLogouted: &now,
		}
		mu.
			EXPECT().
			Update(now, *input).
			Return(fmt.Errorf("DB error"))

		err := u.Logout(now, -1)
		if err == nil {
			t.Error("should got error")
		}
	})
}
