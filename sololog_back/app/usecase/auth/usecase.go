package auth

import (
	"context"
	"database/sql"
	"time"

	firebaseAuth "firebase.google.com/go/v4/auth"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IAuthRepository interface {
	VerifyToken(ctx context.Context, idtoken string) (*model.UserInfoByToken, error)
	SetRole(ctx context.Context, uid string, role model.Role) error
	DeleteUsers(ctx context.Context, uids []string) (*firebaseAuth.DeleteUsersResult, error)
}

type IUserRepository interface {
	FindUserByUID(uid string, provider string) (*model.UserForDB, error)
	FindProviderUsersByUserId(userId int) ([]*model.ProviderUserForDB, error)
	CountUser() (int, error)
	CountAdminUser() (int, error)
	Create(now time.Time, uid string, provider string, role model.Role) (*int64, error)
	Update(now time.Time, input model.UpdateUserInput) error
	DeleteUserById(tx *sql.Tx, id int) error
	DeleteProviderUserByUIDs(tx *sql.Tx, uids []string) error
}

type IBlogRepository interface {
	BeginTransaction() (*sql.Tx, error)
	FindByUserId(userId int) ([]*model.Blog, error)
	DeleteByUserId(tx *sql.Tx, userId int) error
}

type IConfigRepository interface {
	Find() (*model.Config, error)
	UpdateAllowRegister(allow bool) error
}

type AuthUsecase struct {
	authRepository   IAuthRepository
	userRepository   IUserRepository
	blogRepository   IBlogRepository
	configRepository IConfigRepository
}

func NewAuthUsecase(ar IAuthRepository, ur IUserRepository, br IBlogRepository, con IConfigRepository) *AuthUsecase {
	return &AuthUsecase{authRepository: ar, userRepository: ur, blogRepository: br, configRepository: con}
}
