package jwk

import (
	"time"

	"github.com/pkg/errors"
)

func (u *JWKUsecase) RotateKey(now time.Time) error {
	// 新規鍵作成
	err := u.jwkRepository.Create(now)
	if err != nil {
		return errors.Wrap(err, "JWK.RotateKey error")
	}

	// 古い鍵削除
	err = u.jwkRepository.DeleteOldKey(now)
	if err != nil {
		return errors.Wrap(err, "JWK.RotateKey error")
	}
	return nil
}
