package jwk

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/golang/mock/gomock"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_jwk"
)

func TestRotateKey(t *testing.T) {
	cache, _ := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	defer cache.Close()
	now := time.Now().UTC()

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			Create(now).
			Return(nil)
		m.
			EXPECT().
			DeleteOldKey(now).
			Return(nil)

		err := u.RotateKey(now)
		if err != nil {
			t.Error("got err:", err)
		}
	})

	t.Run("異常系：新規鍵作成でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			Create(now).
			Return(fmt.Errorf("DB error"))

		err := u.RotateKey(now)
		if err == nil {
			t.Error("should got error")
		}
	})

	t.Run("異常系：古い鍵削除でエラー", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			Create(now).
			Return(nil)

		m.
			EXPECT().
			DeleteOldKey(now).
			Return(fmt.Errorf("DB error"))

		err := u.RotateKey(now)
		if err == nil {
			t.Error("should got error")
		}
	})
}
