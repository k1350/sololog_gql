package jwk

import (
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"gitlab.com/k1350/jwx_repository"
)

const (
	cacheKey = "jwk"
)

type IJWKRepository interface {
	Create(now time.Time) error
	GetLatestKey(now time.Time) (*jwx_repository.JWK, error)
	GetValidKeySet(now time.Time) (jwk.Set, error)
	DeleteOldKey(now time.Time) error
}

type JWKUsecase struct {
	cache         *bigcache.BigCache
	jwkRepository IJWKRepository
}

func NewJWKUsecase(c *bigcache.BigCache, r IJWKRepository) *JWKUsecase {
	return &JWKUsecase{cache: c, jwkRepository: r}
}
