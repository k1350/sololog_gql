package jwk

import (
	"bytes"
	"context"
	"database/sql/driver"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/golang/mock/gomock"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"

	"gitlab.com/k1350/jwx_repository"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/mock/usecase/mock_jwk"
)

type AnyTime struct{}

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func TestGetLatestKey(t *testing.T) {
	cache, _ := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	defer cache.Close()
	now := time.Now().UTC()
	t.Run("正常系：キャッシュにない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		key, _ := jwx_repository.CreateKey()
		raw, _ := json.MarshalIndent(key, "", "  ")
		expected := &jwx_repository.JWK{
			ID:        key.KeyID(),
			JWK:       raw,
			ExpiredAt: now.AddDate(0, 1, 14),
		}

		m.
			EXPECT().
			GetLatestKey(now).
			Return(expected, nil)

		result, err := u.GetLatestKey(now)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, key) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("正常系：キャッシュにあり、キーローテーション不要", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		key, _ := jwx_repository.CreateKey()
		raw, _ := json.MarshalIndent(key, "", "  ")
		expected := &jwx_repository.JWK{
			ID:        key.KeyID(),
			JWK:       raw,
			ExpiredAt: now.AddDate(0, 0, 1),
		}
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		enc.Encode(*expected)
		cache.Set(cacheKey, buf.Bytes())

		result, err := u.GetLatestKey(now)
		cache.Delete(cacheKey)

		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, key) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, key)
		}
	})

	t.Run("正常系：キャッシュにあり、キーローテーション必要", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		key1, _ := jwx_repository.CreateKey()
		key2, _ := jwx_repository.CreateKey()
		raw1, _ := json.MarshalIndent(key1, "", "  ")
		raw2, _ := json.MarshalIndent(key2, "", "  ")
		expected := &jwx_repository.JWK{
			ID:        key1.KeyID(),
			JWK:       raw1,
			ExpiredAt: now.AddDate(0, 1, 14),
		}
		cached := &jwx_repository.JWK{
			ID:        key2.KeyID(),
			JWK:       raw2,
			ExpiredAt: now.Add(time.Hour * 23).Add(time.Minute * 59).Add(time.Second * 59),
		}
		var buf bytes.Buffer
		enc := gob.NewEncoder(&buf)
		enc.Encode(*cached)
		cache.Set(cacheKey, buf.Bytes())

		m.
			EXPECT().
			GetLatestKey(now).
			Return(expected, nil)

		result, err := u.GetLatestKey(now)
		cache.Delete(cacheKey)

		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, key1) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, key1)
		}
	})

	t.Run("異常系：キャッシュにない", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			GetLatestKey(now).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetLatestKey(now)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetValidKeySet(t *testing.T) {
	cache, _ := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	defer cache.Close()
	now := time.Now().UTC()
	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		key1, _ := jwx_repository.CreateKey()
		key2, _ := jwx_repository.CreateKey()
		expected := jwk.NewSet()
		expected.AddKey(key1)
		expected.AddKey(key2)

		m.
			EXPECT().
			GetValidKeySet(now).
			Return(expected, nil)

		result, err := u.GetValidKeySet(now)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			GetValidKeySet(now).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.GetValidKeySet(now)
		if err == nil {
			t.Error("should got error")
		}
	})
}

func TestGetIssuedAt(t *testing.T) {
	cache, _ := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	defer cache.Close()
	now := time.Now().UTC().Truncate(time.Second)

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		tok, _ := jwt.NewBuilder().
			Issuer(issuer).
			IssuedAt(now).
			Expiration(now.Add(time.Minute*blogAuthMinute)).
			Claim(blogKeyClaimName, "test1234").
			Build()

		key, _ := jwx_repository.CreateKey()
		signed, _ := jwt.Sign(tok, jwt.WithKey(jwa.HS256, key))
		str := string(signed)

		expected := jwk.NewSet()
		expected.AddKey(key)

		m.
			EXPECT().
			GetValidKeySet(now).
			Return(expected, nil)

		result := u.GetIssuedAt(now, str, "test1234")
		if !reflect.DeepEqual(*result, now) {
			t.Errorf("got:\n%v\n\nwant:\n%v", *result, now)
		}
	})

	t.Run("異常系：期限切れ", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		tok, _ := jwt.NewBuilder().
			Issuer(issuer).
			IssuedAt(now).
			Expiration(now.Add(time.Minute*-1)).
			Claim(blogKeyClaimName, "test1234").
			Build()

		key, _ := jwx_repository.CreateKey()
		signed, _ := jwt.Sign(tok, jwt.WithKey(jwa.HS256, key))
		str := string(signed)

		expected := jwk.NewSet()
		expected.AddKey(key)

		m.
			EXPECT().
			GetValidKeySet(now).
			Return(expected, nil)

		result := u.GetIssuedAt(now, str, "test1234")
		if result != nil {
			t.Errorf("result should be nil\n%v", result)
		}
	})

	t.Run("異常系：ブログキー不一致", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		tok, _ := jwt.NewBuilder().
			Issuer(issuer).
			IssuedAt(now).
			Expiration(now.Add(time.Minute*blogAuthMinute)).
			Claim(blogKeyClaimName, "test12345").
			Build()

		key, _ := jwx_repository.CreateKey()
		signed, _ := jwt.Sign(tok, jwt.WithKey(jwa.HS256, key))
		str := string(signed)

		expected := jwk.NewSet()
		expected.AddKey(key)

		m.
			EXPECT().
			GetValidKeySet(now).
			Return(expected, nil)

		result := u.GetIssuedAt(now, str, "test1234")
		if result != nil {
			t.Errorf("result should be nil\n%v", result)
		}
	})
}

func TestNew(t *testing.T) {
	cache, _ := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	defer cache.Close()
	now := time.Now().UTC().Truncate(time.Second)

	t.Run("正常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		tok, _ := jwt.NewBuilder().
			Issuer(issuer).
			IssuedAt(now).
			Expiration(now.Add(time.Minute*blogAuthMinute)).
			Claim(blogKeyClaimName, "test1234").
			Build()

		key, _ := jwx_repository.CreateKey()
		raw, _ := json.MarshalIndent(key, "", "  ")
		signed, _ := jwt.Sign(tok, jwt.WithKey(jwa.HS256, key))
		str := string(signed)
		j := &jwx_repository.JWK{
			ID:        key.KeyID(),
			JWK:       raw,
			ExpiredAt: now.AddDate(0, 0, 1),
		}

		m.
			EXPECT().
			GetLatestKey(now).
			Return(j, nil)

		result, err := u.New(now, "test1234")
		cache.Delete(cacheKey)
		if err != nil {
			t.Error("got err:", err)
		}
		if !reflect.DeepEqual(*result, str) {
			t.Errorf("got:\n%v\n\nwant:\n%v", *result, str)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		m := mock_jwk.NewMockIJWKRepository(ctrl)
		u := NewJWKUsecase(cache, m)

		m.
			EXPECT().
			GetLatestKey(now).
			Return(nil, fmt.Errorf("DB error"))

		_, err := u.New(now, "test1234")
		cache.Delete(cacheKey)
		if err == nil {
			t.Error("should got error")
		}
	})
}
