package jwk

import (
	"bytes"
	"context"
	"encoding/gob"
	"errors"
	"time"

	perrors "github.com/pkg/errors"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"gitlab.com/k1350/jwx_repository"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

const (
	blogKeyClaimName = "blogKey"
	blogAuthMinute   = 30
	issuer           = "Sololog"
)

func (u *JWKUsecase) GetLatestKey(now time.Time) (jwk.Key, error) {
	raw, err := u.cache.Get(cacheKey)
	var key jwk.Key
	if err != nil {
		// キャッシュに無いのでDBから取得
		key, err = u.getAndCacheKey(now)
		if err != nil {
			return nil, perrors.Wrap(err, "JWK.GetLatestKey error")
		}
	} else {
		// キャッシュにあるので復元
		var buf bytes.Buffer
		buf.Write(raw)
		dec := gob.NewDecoder(&buf)
		var j jwx_repository.JWK
		err = dec.Decode(&j)
		if err != nil {
			return nil, perrors.Wrap(err, "JWK.GetLatestKey error")
		}

		// 残り有効期限が一日未満なら最新のキーを取る
		tomorrow := now.AddDate(0, 0, 1)
		if j.ExpiredAt.Before(tomorrow) {
			// キャッシュ削除
			u.cache.Delete(cacheKey)
			key, err = u.getAndCacheKey(now)
			if err != nil {
				return nil, perrors.Wrap(err, "JWK.GetLatestKey error")
			}
		} else {
			key, err = j.ConvertToJwkKey()
			if err != nil {
				return nil, perrors.Wrap(err, "JWK.GetLatestKey error")
			}
		}
	}

	return key, nil
}

func (u *JWKUsecase) GetValidKeySet(now time.Time) (jwk.Set, error) {
	result, err := u.jwkRepository.GetValidKeySet(now)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.GetLaGetValidKeySettestKey error")
	}
	return result, nil
}

func (u *JWKUsecase) GetIssuedAt(now time.Time, token string, blogKey string) *time.Time {
	tok, err := u.parseAndValidate(now, token)
	if err != nil {
		if errors.Is(err, jwt.ErrTokenExpired()) {
			clog.Error(err)
		}
		return nil
	}

	if tok != nil {
		blogKeyClaim, ok := tok.Get(blogKeyClaimName)
		if ok && blogKeyClaim == blogKey {
			t := tok.IssuedAt()
			return &t
		}
	}
	return nil
}

func (u *JWKUsecase) New(now time.Time, blogKey string) (*string, error) {
	tok, err := jwt.NewBuilder().
		Issuer(issuer).
		IssuedAt(now).
		Expiration(now.Add(time.Minute*blogAuthMinute)).
		Claim(blogKeyClaimName, blogKey).
		Build()
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.New error")
	}

	key, err := u.GetLatestKey(now)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.New error")
	}
	signed, err := jwt.Sign(tok, jwt.WithKey(jwa.HS256, key))
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.New error")
	}

	str := string(signed)

	return &str, nil
}

func (u *JWKUsecase) getAndCacheKey(now time.Time) (jwk.Key, error) {
	j, err := u.jwkRepository.GetLatestKey(now)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.getAndCacheKey error")
	}
	key, err := j.ConvertToJwkKey()
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.getAndCacheKey error")
	}

	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(*j)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.getAndCacheKey error")
	}
	// キャッシュにセット
	u.cache.Set(cacheKey, buf.Bytes())

	return key, nil
}

func (u *JWKUsecase) parseAndValidate(now time.Time, token string) (jwt.Token, error) {
	keys, err := u.GetValidKeySet(now)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.parseAndValidate error")
	}

	// デフォルトで "iat", "exp", "nbf" はバリデーションチェックされる
	options := []jwt.ParseOption{
		jwt.WithKeySet(keys),
		jwt.WithIssuer(issuer),
		jwt.WithValidator(customClaimValidator),
	}
	tok, err := jwt.ParseString(token, options...)
	if err != nil {
		return nil, perrors.Wrap(err, "JWK.parseAndValidate error")
	}
	return tok, nil
}

var customClaimValidator = jwt.ValidatorFunc(func(_ context.Context, t jwt.Token) jwt.ValidationError {
	_, isExist := t.Get(blogKeyClaimName)
	if !isExist {
		return jwt.NewValidationError(errors.New("tokens should have blogKey"))
	}
	return nil
})
