package parameters

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ssm"
	"os"
	"strings"
)

func FetchParameterStore() error {
	// ローカル環境だったらスキップ
	environment := os.Getenv("APP_ENV")
	if environment == "local" {
		return nil
	}

	// パラメータストアから環境変数を読んでセットする
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		return err
	}
	svc := ssm.NewFromConfig(cfg, func(o *ssm.Options) {
		o.Region = "ap-northeast-1"
	})

	parameters, err := svc.GetParametersByPath(context.Background(), &ssm.GetParametersByPathInput{
		Path:           aws.String("/sololog"),
		WithDecryption: aws.Bool(true),
	})
	if err != nil {
		return err
	}

	for _, v := range parameters.Parameters {
		names := strings.Split(*v.Name, "/")
		name := names[len(names)-1]
		err = os.Setenv(name, *v.Value)
		if err != nil {
			return err
		}
	}
	return nil
}
