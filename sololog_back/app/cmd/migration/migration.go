package main

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"

	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/migration"
)

func main() {
	dsn := os.Getenv("DSN")

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		clog.Emergency(err)
		panic(err)
	}
	defer db.Close()
	if err = db.Ping(); err != nil {
		clog.Emergency(err)
		panic(err)
	}

	m := migration.NewMigration(db)
	err = m.MigrateUp()
	if err != nil {
		clog.Emergency(err)
	}
}
