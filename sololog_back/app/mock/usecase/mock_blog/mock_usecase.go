// Code generated by MockGen. DO NOT EDIT.
// Source: ./usecase/blog/usecase.go

// Package mock_blog is a generated GoMock package.
package mock_blog

import (
	sql "database/sql"
	reflect "reflect"
	time "time"

	gomock "github.com/golang/mock/gomock"
	model "gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

// MockIBlogRepository is a mock of IBlogRepository interface.
type MockIBlogRepository struct {
	ctrl     *gomock.Controller
	recorder *MockIBlogRepositoryMockRecorder
}

// MockIBlogRepositoryMockRecorder is the mock recorder for MockIBlogRepository.
type MockIBlogRepositoryMockRecorder struct {
	mock *MockIBlogRepository
}

// NewMockIBlogRepository creates a new mock instance.
func NewMockIBlogRepository(ctrl *gomock.Controller) *MockIBlogRepository {
	mock := &MockIBlogRepository{ctrl: ctrl}
	mock.recorder = &MockIBlogRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockIBlogRepository) EXPECT() *MockIBlogRepositoryMockRecorder {
	return m.recorder
}

// BeginTransaction mocks base method.
func (m *MockIBlogRepository) BeginTransaction() (*sql.Tx, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "BeginTransaction")
	ret0, _ := ret[0].(*sql.Tx)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// BeginTransaction indicates an expected call of BeginTransaction.
func (mr *MockIBlogRepositoryMockRecorder) BeginTransaction() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "BeginTransaction", reflect.TypeOf((*MockIBlogRepository)(nil).BeginTransaction))
}

// CountBlogs mocks base method.
func (m *MockIBlogRepository) CountBlogs(userId int) (*int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CountBlogs", userId)
	ret0, _ := ret[0].(*int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CountBlogs indicates an expected call of CountBlogs.
func (mr *MockIBlogRepositoryMockRecorder) CountBlogs(userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CountBlogs", reflect.TypeOf((*MockIBlogRepository)(nil).CountBlogs), userId)
}

// Create mocks base method.
func (m *MockIBlogRepository) Create(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", now, input, userId)
	ret0, _ := ret[0].(*model.Blog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Create indicates an expected call of Create.
func (mr *MockIBlogRepositoryMockRecorder) Create(now, input, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockIBlogRepository)(nil).Create), now, input, userId)
}

// DeleteById mocks base method.
func (m *MockIBlogRepository) DeleteById(tx *sql.Tx, blogId string, userId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteById", tx, blogId, userId)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteById indicates an expected call of DeleteById.
func (mr *MockIBlogRepositoryMockRecorder) DeleteById(tx, blogId, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteById", reflect.TypeOf((*MockIBlogRepository)(nil).DeleteById), tx, blogId, userId)
}

// FindByBlogKey mocks base method.
func (m *MockIBlogRepository) FindByBlogKey(blogKey string, opts *model.BlogByKeyOpts, passwordInputTime *time.Time) (*model.Blog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindByBlogKey", blogKey, opts, passwordInputTime)
	ret0, _ := ret[0].(*model.Blog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FindByBlogKey indicates an expected call of FindByBlogKey.
func (mr *MockIBlogRepositoryMockRecorder) FindByBlogKey(blogKey, opts, passwordInputTime interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindByBlogKey", reflect.TypeOf((*MockIBlogRepository)(nil).FindByBlogKey), blogKey, opts, passwordInputTime)
}

// FindById mocks base method.
func (m *MockIBlogRepository) FindById(blogId string, userId int) (*model.Blog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindById", blogId, userId)
	ret0, _ := ret[0].(*model.Blog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FindById indicates an expected call of FindById.
func (mr *MockIBlogRepositoryMockRecorder) FindById(blogId, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindById", reflect.TypeOf((*MockIBlogRepository)(nil).FindById), blogId, userId)
}

// FindByUserId mocks base method.
func (m *MockIBlogRepository) FindByUserId(userId int) ([]*model.Blog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindByUserId", userId)
	ret0, _ := ret[0].([]*model.Blog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FindByUserId indicates an expected call of FindByUserId.
func (mr *MockIBlogRepositoryMockRecorder) FindByUserId(userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindByUserId", reflect.TypeOf((*MockIBlogRepository)(nil).FindByUserId), userId)
}

// IsAuthor mocks base method.
func (m *MockIBlogRepository) IsAuthor(blogKey string, userId int) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsAuthor", blogKey, userId)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IsAuthor indicates an expected call of IsAuthor.
func (mr *MockIBlogRepositoryMockRecorder) IsAuthor(blogKey, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsAuthor", reflect.TypeOf((*MockIBlogRepository)(nil).IsAuthor), blogKey, userId)
}

// Update mocks base method.
func (m *MockIBlogRepository) Update(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", now, input, userId)
	ret0, _ := ret[0].(*model.Blog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update.
func (mr *MockIBlogRepositoryMockRecorder) Update(now, input, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockIBlogRepository)(nil).Update), now, input, userId)
}

// MockIArticleRepository is a mock of IArticleRepository interface.
type MockIArticleRepository struct {
	ctrl     *gomock.Controller
	recorder *MockIArticleRepositoryMockRecorder
}

// MockIArticleRepositoryMockRecorder is the mock recorder for MockIArticleRepository.
type MockIArticleRepositoryMockRecorder struct {
	mock *MockIArticleRepository
}

// NewMockIArticleRepository creates a new mock instance.
func NewMockIArticleRepository(ctrl *gomock.Controller) *MockIArticleRepository {
	mock := &MockIArticleRepository{ctrl: ctrl}
	mock.recorder = &MockIArticleRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockIArticleRepository) EXPECT() *MockIArticleRepositoryMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockIArticleRepository) Create(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", now, input, userId)
	ret0, _ := ret[0].(*model.Article)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Create indicates an expected call of Create.
func (mr *MockIArticleRepositoryMockRecorder) Create(now, input, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockIArticleRepository)(nil).Create), now, input, userId)
}

// DeleteById mocks base method.
func (m *MockIArticleRepository) DeleteById(articleId string, userId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteById", articleId, userId)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteById indicates an expected call of DeleteById.
func (mr *MockIArticleRepositoryMockRecorder) DeleteById(articleId, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteById", reflect.TypeOf((*MockIArticleRepository)(nil).DeleteById), articleId, userId)
}

// FindByArticleKey mocks base method.
func (m *MockIArticleRepository) FindByArticleKey(articleKey string, opts *model.ArticleByKeyOpts, passwordInputTime *time.Time) (*model.Article, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindByArticleKey", articleKey, opts, passwordInputTime)
	ret0, _ := ret[0].(*model.Article)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FindByArticleKey indicates an expected call of FindByArticleKey.
func (mr *MockIArticleRepositoryMockRecorder) FindByArticleKey(articleKey, opts, passwordInputTime interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindByArticleKey", reflect.TypeOf((*MockIArticleRepository)(nil).FindByArticleKey), articleKey, opts, passwordInputTime)
}

// FindByBlogId mocks base method.
func (m *MockIArticleRepository) FindByBlogId(blogId string, input model.ArticlePaginationInput) ([]*model.Article, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindByBlogId", blogId, input)
	ret0, _ := ret[0].([]*model.Article)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FindByBlogId indicates an expected call of FindByBlogId.
func (mr *MockIArticleRepositoryMockRecorder) FindByBlogId(blogId, input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindByBlogId", reflect.TypeOf((*MockIArticleRepository)(nil).FindByBlogId), blogId, input)
}

// HasNextPage mocks base method.
func (m *MockIArticleRepository) HasNextPage(blogId string, input model.ArticlePaginationInput) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HasNextPage", blogId, input)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// HasNextPage indicates an expected call of HasNextPage.
func (mr *MockIArticleRepositoryMockRecorder) HasNextPage(blogId, input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HasNextPage", reflect.TypeOf((*MockIArticleRepository)(nil).HasNextPage), blogId, input)
}

// HasPreviousPage mocks base method.
func (m *MockIArticleRepository) HasPreviousPage(blogId string, input model.ArticlePaginationInput) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HasPreviousPage", blogId, input)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// HasPreviousPage indicates an expected call of HasPreviousPage.
func (mr *MockIArticleRepositoryMockRecorder) HasPreviousPage(blogId, input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HasPreviousPage", reflect.TypeOf((*MockIArticleRepository)(nil).HasPreviousPage), blogId, input)
}

// Update mocks base method.
func (m *MockIArticleRepository) Update(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", now, input, userId)
	ret0, _ := ret[0].(*model.Article)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update.
func (mr *MockIArticleRepositoryMockRecorder) Update(now, input, userId interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockIArticleRepository)(nil).Update), now, input, userId)
}

// MockIMParameterRepository is a mock of IMParameterRepository interface.
type MockIMParameterRepository struct {
	ctrl     *gomock.Controller
	recorder *MockIMParameterRepositoryMockRecorder
}

// MockIMParameterRepositoryMockRecorder is the mock recorder for MockIMParameterRepository.
type MockIMParameterRepositoryMockRecorder struct {
	mock *MockIMParameterRepository
}

// NewMockIMParameterRepository creates a new mock instance.
func NewMockIMParameterRepository(ctrl *gomock.Controller) *MockIMParameterRepository {
	mock := &MockIMParameterRepository{ctrl: ctrl}
	mock.recorder = &MockIMParameterRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockIMParameterRepository) EXPECT() *MockIMParameterRepositoryMockRecorder {
	return m.recorder
}

// Find mocks base method.
func (m *MockIMParameterRepository) Find() (*model.MasterParameter, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Find")
	ret0, _ := ret[0].(*model.MasterParameter)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Find indicates an expected call of Find.
func (mr *MockIMParameterRepositoryMockRecorder) Find() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Find", reflect.TypeOf((*MockIMParameterRepository)(nil).Find))
}
