package log

import (
	"io"
	"log"
	"os"
)

func init() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC)
}

func Debug(v ...any) {
	writeLog("DEBUG", v)
}

func Error(v ...any) {
	writeLog("ERROR", v)
}

func Emergency(v ...any) {
	writeLog("EMERGENCY", v)
}

func writeLog(level string, v ...any) {
	var out io.Writer
	if level == "DEBUG" {
		out = os.Stdout
	} else {
		out = os.Stderr
	}
	log.SetPrefix("[" + level + "] ")
	log.SetOutput(out)
	log.Printf("%+v", v)
}
