package mparameter

import (
	"database/sql"
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type MParameterRepository struct {
	*sql.DB
}

func NewMParameterRepository(db *sql.DB) *MParameterRepository {
	return &MParameterRepository{db}
}

func (r *MParameterRepository) Find() (*model.MasterParameter, error) {
	s := "SELECT max_blogs, max_blog_links FROM m_parameter LIMIT 1"
	var master model.MasterParameter
	stmt, err := r.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	err = stmt.QueryRow().Scan(&master.MaxBlogs, &master.MaxBlogLinks)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return &master, nil
}
