package mparameter

import (
	"fmt"
	"reflect"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func TestFind(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewMParameterRepository(db)
	s := regexp.QuoteMeta("SELECT max_blogs, max_blog_links FROM m_parameter LIMIT 1")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"max_blogs", "max_blog_links"}).
			AddRow(5, 3)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnRows(rows)
		expected := &model.MasterParameter{
			MaxBlogs:     5,
			MaxBlogLinks: 3,
		}

		result, err := repo.Find()

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.Find()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
