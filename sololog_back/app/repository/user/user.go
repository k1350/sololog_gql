package user

import (
	"database/sql"
	"github.com/pkg/errors"
	"strings"
	"time"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type UserRepository struct {
	*sql.DB
}

func NewUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{db}
}

const (
	adminRole = "1"
)

var roleField = "CASE user.role WHEN 1 THEN \"" + model.RoleAdmin.String() + "\" ELSE \"" + model.RoleUser.String() + "\" END AS role"

func (r *UserRepository) FindUserByUID(uid string, provider string) (*model.UserForDB, error) {
	s := "SELECT user.id, " + roleField + ", user.last_logined, user.last_logouted, user.created_at, user.updated_at FROM user INNER JOIN provider_user ON user.id = provider_user.user_id WHERE provider_user.uid = ? AND provider_user.provider = ? LIMIT 1"
	stmt, err := r.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	u := &model.UserForDB{}
	err = stmt.QueryRow(uid, provider).Scan(&u.ID, &u.Role, &u.LastLogined, &u.LastLogouted, &u.CreatedAt, &u.UpdatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return u, nil
}

func (r *UserRepository) FindProviderUsersByUserId(userId int) ([]*model.ProviderUserForDB, error) {
	s := "SELECT uid, provider, user_id, created_at, updated_at FROM provider_user WHERE user_id = ?"
	stmt, err := r.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	rows, err := stmt.Query(userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer rows.Close()

	var result []*model.ProviderUserForDB
	for rows.Next() {
		u := &model.ProviderUserForDB{}
		err = rows.Scan(&u.UID, &u.Provider, &u.UserId, &u.CreatedAt, &u.UpdatedAt)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}

		result = append(result, u)
	}

	err = rows.Err()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	return result, nil
}

func (r *UserRepository) CountUser() (int, error) {
	s := "SELECT count(id) FROM user"
	stmt, err := r.Prepare(s)
	if err != nil {
		return 0, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	var count int
	err = stmt.QueryRow().Scan(&count)
	switch {
	case err != nil:
		return 0, errors.Wrap(err, "DB error")
	default:
		return count, nil
	}
}

func (r *UserRepository) CountAdminUser() (int, error) {
	s := "SELECT count(id) FROM user WHERE role = " + adminRole
	stmt, err := r.Prepare(s)
	if err != nil {
		return 0, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	var count int
	err = stmt.QueryRow().Scan(&count)
	switch {
	case err != nil:
		return 0, errors.Wrap(err, "DB error")
	default:
		return count, nil
	}
}

func (r *UserRepository) Create(now time.Time, uid string, provider string, role model.Role) (*int64, error) {
	su := "INSERT INTO user (role, last_logined, created_at, updated_at) VALUES (?, ?, ?, ?)"

	spu := "INSERT INTO provider_user (uid, provider, user_id, created_at, updated_at) VALUES (?, ?, ?, ?, ?)"

	intRole := convertRoleToInt(role)

	// トランザクション開始
	tx, err := r.Begin()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer func() {
		// panicが起きたらロールバック
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p)
		}
	}()

	// User 作成
	stmtu, err := tx.Prepare(su)
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmtu.Close()

	res, err := stmtu.Exec(intRole, now, now, now)
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrap(err, "DB error")
	}
	insertId, err := res.LastInsertId()
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrap(err, "DB error")
	}

	// ProviderUser 作成
	stmtpu, err := tx.Prepare(spu)
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmtpu.Close()

	_, err = stmtpu.Exec(uid, provider, insertId, now, now)
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrap(err, "DB error")
	}

	tx.Commit()
	return &insertId, nil
}

func (r *UserRepository) Update(now time.Time, input model.UpdateUserInput) error {
	var inputFields []string
	var args []interface{}

	if input.Role != nil {
		inputFields = append(inputFields, "role")
		args = append(args, *input.Role)
	}
	if input.LastLogined != nil {
		inputFields = append(inputFields, "last_logined")
		args = append(args, *input.LastLogined)
	}
	if input.LastLogouted != nil {
		inputFields = append(inputFields, "last_logouted")
		args = append(args, *input.LastLogouted)
	}

	if len(inputFields) == 0 {
		return nil
	}

	inputFields = append(inputFields, "updated_at")
	args = append(args, now, input.ID)
	var sb strings.Builder
	sb.WriteString("UPDATE user SET ")
	for i, v := range inputFields {
		sb.WriteString(v + " = ?")
		if i < len(inputFields)-1 {
			sb.WriteString(",")
		}
	}
	sb.WriteString(" WHERE id = ?")
	stmt, err := r.Prepare(sb.String())
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(args...)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.ForbiddenError, "DB error")
	}
	return nil
}

func (r *UserRepository) DeleteUserById(tx *sql.Tx, id int) error {
	s := "DELETE from user WHERE id = ?"
	stmt, err := tx.Prepare(s)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(id)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.ForbiddenError, "DB error")
	}

	return nil
}

func (r *UserRepository) DeleteProviderUserByUIDs(tx *sql.Tx, uids []string) error {
	if len(uids) == 0 {
		return nil
	}

	var args []interface{}
	for i := range uids {
		args = append(args, uids[i])
	}

	s := "DELETE from provider_user WHERE uid in (?" + strings.Repeat(",?", len(uids)-1) + ")"

	stmt, err := tx.Prepare(s)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(args...)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.ForbiddenError, "DB error")
	}

	return nil
}

func convertRoleToInt(opt model.Role) int {
	switch opt {
	case model.RoleAdmin:
		return 1
	case model.RoleUser:
		return 2
	}
	// 到達しないが return を書かないとエラーになるので書く
	return 99
}
