package user

import (
	"database/sql"
	"fmt"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func TestFindUserByUID(t *testing.T) {
	now := time.Now().UTC()

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("SELECT user.id, CASE user.role WHEN 1 THEN \"ADMIN\" ELSE \"USER\" END AS role, user.last_logined, user.last_logouted, user.created_at, user.updated_at FROM user INNER JOIN provider_user ON user.id = provider_user.user_id WHERE provider_user.uid = ? AND provider_user.provider = ? LIMIT 1")

	t.Run("正常系：ユーザーが存在する", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "role", "last_logined", "last_logouted", "created_at", "updated_at"}).
			AddRow(5, model.RoleAdmin, now, now, now, now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("uid", "provider").
			WillReturnRows(rows)
		expected := &model.UserForDB{
			ID:           5,
			Role:         model.RoleAdmin,
			LastLogined:  now,
			LastLogouted: &now,
			CreatedAt:    now,
			UpdatedAt:    now,
		}

		result, err := repo.FindUserByUID("uid", "provider")

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：ユーザーが存在しない", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("uid", "provider").
			WillReturnError(sql.ErrNoRows)

		result, err := repo.FindUserByUID("uid", "provider")

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, nil)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("uid", "provider").
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.FindUserByUID("uid", "provider")

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestFindByUserId(t *testing.T) {
	now := time.Now().UTC()

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)

	s := regexp.QuoteMeta("SELECT uid, provider, user_id, created_at, updated_at FROM provider_user WHERE user_id = ?")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"uid", "provider", "user_id", "created_at", "updated_at"}).
			AddRow("test1", "google", -1, now, now).
			AddRow("test2", "facebook", -1, now, now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(-1).
			WillReturnRows(rows)
		expected := []*model.ProviderUserForDB{
			{
				UID:       "test1",
				Provider:  "google",
				UserId:    -1,
				CreatedAt: now,
				UpdatedAt: now,
			},
			{
				UID:       "test2",
				Provider:  "facebook",
				UserId:    -1,
				CreatedAt: now,
				UpdatedAt: now,
			},
		}

		result, err := repo.FindProviderUsersByUserId(-1)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(-1).
			WillReturnError(fmt.Errorf("some error"))

		_, err = repo.FindProviderUsersByUserId(-1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCountUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("SELECT count(id) FROM user")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow(10)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnRows(rows)

		result, err := repo.CountUser()

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, 10) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, 10)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.CountUser()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCountAdminUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("SELECT count(id) FROM user WHERE role = 1")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow(10)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnRows(rows)

		result, err := repo.CountAdminUser()

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, 10) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, 10)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.CountAdminUser()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCreate(t *testing.T) {
	now := time.Now().UTC()

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("INSERT INTO user (role, last_logined, created_at, updated_at) VALUES (?, ?, ?, ?)")
	spu := regexp.QuoteMeta("INSERT INTO provider_user (uid, provider, user_id, created_at, updated_at) VALUES (?, ?, ?, ?, ?)")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(2, now, now, now).
			WillReturnResult(sqlmock.NewResult(-1, 1))
		mock.ExpectPrepare(spu)
		mock.ExpectExec(spu).
			WithArgs("uid", "provider", -1, now, now).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectCommit()
		var expected int64 = -1

		result, err := repo.Create(now, "uid", "provider", model.RoleUser)

		if err != nil {
			t.Error("got err:", err)
		}

		if *result != expected {
			t.Errorf("got:\n%v\n\nwant:\n%v", *result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：user 作成失敗", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(2, now, now, now).
			WillReturnError(fmt.Errorf("some error"))
		mock.ExpectRollback()

		_, err := repo.Create(now, "uid", "provider", model.RoleUser)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：user_provider 作成失敗", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(2, now, now, now).
			WillReturnResult(sqlmock.NewResult(-1, 1))
		mock.ExpectPrepare(spu)
		mock.ExpectExec(spu).
			WithArgs("uid", "provider", -1, now, now).
			WillReturnError(fmt.Errorf("some error"))
		mock.ExpectRollback()

		_, err := repo.Create(now, "uid", "provider", model.RoleUser)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestUpdate(t *testing.T) {
	now := time.Now().UTC()
	login := now.Add(1 * time.Minute)
	logout := now.Add(2 * time.Minute)
	role := model.RoleUser
	id := -1

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)

	t.Run("正常系：全フィールドあり", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE user SET role = ?,last_logined = ?,last_logouted = ?,updated_at = ? WHERE id = ?")
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(role, login, logout, now, id).
			WillReturnResult(sqlmock.NewResult(1, 1))

		input := &model.UpdateUserInput{
			ID:           id,
			Role:         &role,
			LastLogined:  &login,
			LastLogouted: &logout,
		}

		err := repo.Update(now, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：全フィールドなし", func(t *testing.T) {
		input := &model.UpdateUserInput{
			ID: id,
		}

		err := repo.Update(now, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE user SET role = ?,updated_at = ? WHERE id = ?")
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(role, now, -1).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateUserInput{
			ID:   id,
			Role: &role,
		}
		err := repo.Update(now, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

}

func TestDeleteUserById(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("DELETE from user WHERE id = ?")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(-1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteUserById(tx, -1)

		tx.Rollback()

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(-1).
			WillReturnError(fmt.Errorf("some error"))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteUserById(tx, -1)

		tx.Rollback()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestDeleteProviderUserByUIDs(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewUserRepository(db)
	s := regexp.QuoteMeta("DELETE from provider_user WHERE uid in (?,?)")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("user1", "user2").
			WillReturnResult(sqlmock.NewResult(1, 1))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		input := []string{"user1", "user2"}
		err = repo.DeleteProviderUserByUIDs(tx, input)

		tx.Rollback()

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("user1", "user2").
			WillReturnError(fmt.Errorf("some error"))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		input := []string{"user1", "user2"}
		err = repo.DeleteProviderUserByUIDs(tx, input)

		tx.Rollback()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
