package blog

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type AnyString struct{}

// Match satisfies sqlmock.Argument interface
func (a AnyString) Match(v driver.Value) bool {
	_, ok := v.(string)
	return ok
}

type AnyBytes struct{}

func (a AnyBytes) Match(v driver.Value) bool {
	_, ok := v.([]byte)
	return ok
}

func TestBeginTransaction(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		_, err = repo.BeginTransaction()

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectBegin().
			WillReturnError(fmt.Errorf("some error"))

		_, err = repo.BeginTransaction()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestFindByUserId(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	sqlstr := "SELECT id, blog_key, user_id, author, name, description, CASE publish_option WHEN 1 THEN \"" +
		model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() +
		"\" END AS publsh_option, password_hint, links, created_at, updated_at FROM blog WHERE user_id = ? ORDER BY id DESC"

	s := regexp.QuoteMeta(sqlstr)

	author1 := "著者1"
	description1 := ""
	name1 := "ブログ名1"
	author3 := "著者3"
	description3 := "説明"
	name2 := "ブログ名2"

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author1, name1, description1, "PUBLIC", "", json.RawMessage("[{\"id\": \"1\", \"url\": \"http://example.com/\", \"name\": \"test\"}]"), now, now).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "ef6583ba-820a-ac6b-5f5b-af97cee16a9a", -1, author3, name2, description3, "PASSWORD", "ヒント", json.RawMessage("[]"), now, now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(-1).
			WillReturnRows(rows)
		var links1 []*model.BlogLink
		links1 = append(links1, &model.BlogLink{
			ID:   "1",
			Name: "test",
			URL:  "http://example.com/",
		})
		var links2 []*model.BlogLink
		expected := []*model.Blog{
			{
				ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
				BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
				Author:        &author1,
				Name:          &name1,
				Description:   &description1,
				PublishOption: model.PublishOptionPublic,
				PasswordHint:  "",
				Links:         links1,
				CreatedAt:     &nowstr,
				UpdatedAt:     &nowstr,
			},
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
				BlogKey:       "ef6583ba-820a-ac6b-5f5b-af97cee16a9a",
				Author:        &author3,
				Name:          &name2,
				Description:   &description3,
				PublishOption: model.PublishOptionPassword,
				PasswordHint:  "ヒント",
				Links:         links2,
				CreatedAt:     &nowstr,
				UpdatedAt:     &nowstr,
			},
		}

		result, err := repo.FindByUserId(-1)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(-1).
			WillReturnError(fmt.Errorf("some error"))

		_, err = repo.FindByUserId(-1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestFindById(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	sqlstr := "SELECT id, blog_key, user_id, author, name, description, CASE publish_option WHEN 1 THEN \"" +
		model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() +
		"\" END AS publsh_option, password_hint, links, created_at, updated_at FROM blog WHERE id = ? AND user_id = ? LIMIT 1"
	s := regexp.QuoteMeta(sqlstr)

	author1 := "著者1"
	description1 := "説明"
	name1 := "ブログ名1"

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author1, name1, description1, "PUBLIC", "", json.RawMessage("[{\"id\": \"1\", \"url\": \"http://example.com/\", \"name\": \"test\"}]"), now, now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(rows)

		var links1 []*model.BlogLink
		links1 = append(links1, &model.BlogLink{
			ID:   "1",
			Name: "test",
			URL:  "http://example.com/",
		})
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPublic,
			PasswordHint:  "",
			Links:         links1,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		result, err := repo.FindById("000XAL6S41ACTAV9WEVGEMMVR8", -1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系: 取得失敗", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnError(fmt.Errorf("some error"))

		_, err = repo.FindById("000XAL6S41ACTAV9WEVGEMMVR8", -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系: ブログが見つからなかった", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnError(sql.ErrNoRows)

		result, err := repo.FindById("000XAL6S41ACTAV9WEVGEMMVR8", -1)

		if result != nil {
			t.Error("should be nil:", result)
		}

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestFindByBlogKey(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	rawLinks := json.RawMessage("[{\"id\": \"1\", \"url\": \"http://example.com/\", \"name\": \"test\"}]")
	var links []*model.BlogLink
	links = append(links, &model.BlogLink{
		ID:   "1",
		Name: "test",
		URL:  "http://example.com/",
	})

	author1 := "著者1"
	description1 := "説明"
	name1 := "ブログ名1"
	sqlbase := "SELECT id, blog_key, user_id, author, name, description, CASE publish_option WHEN 1 THEN \"" +
		model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() +
		"\" END AS publsh_option, password_hint, links, created_at, updated_at, password, password_updated_at FROM blog WHERE blog_key = ? AND (publish_option = 1 OR publish_option = 2 "

	rowbase := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at", "password", "password_updated_at"})

	t.Run("正常系：公開", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author1, name1, description1, "PUBLIC", "", rawLinks, now, now, "", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPublic,
			PasswordHint:  "",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態でパスワード保護されていて正しいパスワードを入力された", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPassword,
			Links:         links,
			PasswordHint:  "ヒント",
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		password := "password"
		opts := &model.BlogByKeyOpts{UserId: nil, Password: &password}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系（パスワード入力日時あり）：非ログイン状態でパスワード保護されていてパスワード更新日時<パスワード入力日時", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PASSWORD", "ヒント", rawLinks, now, now, "dummy", time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC))

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  "ヒント",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}
		pit := time.Date(2000, 1, 1, 0, 0, 0, 1, time.UTC)
		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, &pit)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でありパスワード保護されている", func(t *testing.T) {
		sqlstr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -2).
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  "ヒント",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		userId := -2
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でなくパスワード保護されていて、パスワード未入力", func(t *testing.T) {
		sqlStr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlStr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -3).
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        nil,
			Name:          nil,
			Description:   nil,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  "ヒント",
			Links:         nil,
			CreatedAt:     nil,
			UpdatedAt:     nil,
		}

		userId := -3
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態でパスワード保護されていて、パスワード未入力", func(t *testing.T) {
		sqlStr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlStr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        nil,
			Name:          nil,
			Description:   nil,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  "ヒント",
			Links:         nil,
			CreatedAt:     nil,
			UpdatedAt:     nil,
		}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者であり非公開", func(t *testing.T) {
		sqlstr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, name1, description1, "PRIVATE", "", rawLinks, now, now, "", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -2).
			WillReturnRows(rows)
		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author1,
			Name:          &name1,
			Description:   &description1,
			PublishOption: model.PublishOptionPrivate,
			PasswordHint:  "",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		userId := -2
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でなく非公開", func(t *testing.T) {
		sqlstr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -3).
			WillReturnError(sql.ErrNoRows)

		userId := -3
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Error("should got nil")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態で非公開", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnError(sql.ErrNoRows)

		result, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Error("should got nil")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：著者でなくパスワード保護されていて誤ったパスワードを入力された", func(t *testing.T) {
		sqlstr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, "ブログ名1", description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -3).
			WillReturnRows(rows)

		userId := -3
		password := "password2"
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: &password}

		_, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：非ログイン状態でパスワード保護されていて誤ったパスワードを入力された", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, "ブログ名1", description1, "PASSWORD", "ヒント", rawLinks, now, now, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)

		password := "password2"
		opts := &model.BlogByKeyOpts{UserId: nil, Password: &password}

		_, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系（パスワード入力日時あり）：著者でなくパスワード保護されていてパスワード更新日時=パスワード入力日時", func(t *testing.T) {
		sqlstr := sqlbase + "OR publish_option = 99 AND user_id = ?) LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, author1, "ブログ名1", description1, "PASSWORD", "ヒント", rawLinks, now, now, "dummy", now)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda", -3).
			WillReturnRows(rows)

		userId := -3
		opts := &model.BlogByKeyOpts{UserId: &userId, Password: nil}

		_, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", opts, &now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系（パスワード入力日時あり）：非ログイン状態でパスワード保護されていてパスワード更新日時>パスワード入力日時", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)
		rows := rowbase.
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -2, "著者1", "ブログ名1", "", "PASSWORD", "ヒント", rawLinks, now, now, "dummy", time.Date(2000, 1, 1, 0, 0, 0, 1, time.UTC))

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnRows(rows)

		pit := time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
		_, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, &pit)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		sqlstr := sqlbase + ") LIMIT 1"
		s := regexp.QuoteMeta(sqlstr)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("dccb4797-90c7-ea22-6866-7eb954acbbda").
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.FindByBlogKey("dccb4797-90c7-ea22-6866-7eb954acbbda", nil, nil)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestIsAuthor(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	s := regexp.QuoteMeta("SELECT count(id) FROM blog WHERE blog_key = ? AND user_id = ?")

	t.Run("正常系：著者である", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow(1)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("a", 1).
			WillReturnRows(rows)
		result, err := repo.IsAuthor("a", 1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でない", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow(0)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("a", 1).
			WillReturnRows(rows)
		result, err := repo.IsAuthor("a", 1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("a", 1).
			WillReturnError(fmt.Errorf("some error"))
		result, err := repo.IsAuthor("a", 1)

		if err == nil {
			t.Error("should got error")
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCountBlogs(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	s := regexp.QuoteMeta("SELECT count(id) FROM blog where user_id = ?")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow(5)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(1).
			WillReturnRows(rows)
		expected := 5

		result, err := repo.CountBlogs(1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(*result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", *result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs(1).
			WillReturnError(fmt.Errorf("some error"))
		_, err := repo.CountBlogs(1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCreate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)

	selectSql := "SELECT id, blog_key, user_id, author, name, description, CASE publish_option WHEN 1 THEN \"" +
		model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() +
		"\" END AS publsh_option, password_hint, links, created_at, updated_at FROM blog WHERE id = ? AND user_id = ? LIMIT 1"
	ss := regexp.QuoteMeta(selectSql)

	t.Run("全フィールドあり正常系", func(t *testing.T) {
		author := "著者"
		description := "説明"
		name := "名前"
		password := "password"
		passwordHint := "ヒント"
		link1Name := "テスト"
		link1URL := "http://example.com"
		link2Name := "テスト2"
		link2URL := "http://2.example.com"

		var inputLinks []*model.BlogLinkInput
		inputLinks = append(inputLinks,
			&model.BlogLinkInput{Name: &link1Name, URL: link1URL},
			&model.BlogLinkInput{Name: &link2Name, URL: link2URL},
		)
		input := &model.CreateBlogInput{
			Author:        author,
			Name:          name,
			Description:   &description,
			PublishOption: model.PublishOptionPassword,
			Password:      &password,
			PasswordHint:  &passwordHint,
			Links:         inputLinks,
		}

		var links []*model.BlogLink
		links = append(links,
			&model.BlogLink{
				ID:   "1",
				Name: *inputLinks[0].Name,
				URL:  inputLinks[0].URL,
			},
			&model.BlogLink{
				ID:   "2",
				Name: *inputLinks[1].Name,
				URL:  inputLinks[1].URL,
			})

		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  passwordHint,
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		rawLinks, _ := json.Marshal(links)

		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author, name, description, "PASSWORD", passwordHint, rawLinks, now, now)

		smax := regexp.QuoteMeta("INSERT INTO blog (id,blog_key,user_id,author,name,publish_option,password_updated_at,created_at,updated_at,description,password,password_hint,links) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)")

		mock.ExpectPrepare(smax)
		mock.ExpectExec(smax).
			WithArgs(AnyString{}, AnyString{}, -1, author, name, 2, now, now, now, description, AnyString{}, passwordHint, AnyBytes{}).
			WillReturnResult(sqlmock.NewResult(2, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs(AnyString{}, -1).
			WillReturnRows(rows)

		result, err := repo.Create(now, *input, -1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("最小フィールド正常系", func(t *testing.T) {
		var links []*model.BlogLink
		author := "著者"
		description := ""
		name := "名前"

		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author, name, description, "PUBLIC", "", json.RawMessage("[]"), now, now)

		s := regexp.QuoteMeta("INSERT INTO blog (id,blog_key,user_id,author,name,publish_option,password_updated_at,created_at,updated_at,links) VALUES (?,?,?,?,?,?,?,?,?,?)")

		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, AnyString{}, -1, author, name, 1, now, now, now, "[]").
			WillReturnResult(sqlmock.NewResult(2, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs(AnyString{}, -1).
			WillReturnRows(rows)

		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPublic,
			PasswordHint:  "",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		input := &model.CreateBlogInput{
			Author:        author,
			Name:          name,
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			Password:      nil,
			Links:         []*model.BlogLinkInput{},
		}

		result, err := repo.Create(now, *input, -1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ作成失敗", func(t *testing.T) {
		s := regexp.QuoteMeta("INSERT INTO blog (id,blog_key,user_id,author,name,publish_option,password_updated_at,created_at,updated_at,links) VALUES (?,?,?,?,?,?,?,?,?,?)")

		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, AnyString{}, -1, "著者", "名前", 1, now, now, now, "[]").
			WillReturnError(fmt.Errorf("some error"))

		input := &model.CreateBlogInput{
			Author:        "著者",
			Name:          "名前",
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			Password:      nil,
			Links:         []*model.BlogLinkInput{},
		}

		_, err = repo.Create(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ取得失敗", func(t *testing.T) {
		s := regexp.QuoteMeta("INSERT INTO blog (id,blog_key,user_id,author,name,publish_option,password_updated_at,created_at,updated_at,links) VALUES (?,?,?,?,?,?,?,?,?,?)")

		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, AnyString{}, -1, "著者", "名前", 1, now, now, now, "[]").
			WillReturnResult(sqlmock.NewResult(2, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs(AnyString{}, -1).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.CreateBlogInput{
			Author:        "著者",
			Name:          "名前",
			Description:   nil,
			PublishOption: model.PublishOptionPublic,
			Password:      nil,
			Links:         []*model.BlogLinkInput{},
		}

		_, err := repo.Create(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestUpdate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)
	now := time.Now().UTC()
	nowstr := now.Format(time.RFC3339)
	optsql := regexp.QuoteMeta("SELECT " + PublishOptionField + " FROM blog WHERE id = ? AND user_id = ?")
	selectSql := "SELECT id, blog_key, user_id, author, name, description, CASE publish_option WHEN 1 THEN \"" +
		model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() +
		"\" END AS publsh_option, password_hint, links, created_at, updated_at FROM blog WHERE id = ? AND user_id = ? LIMIT 1"
	ss := regexp.QuoteMeta(selectSql)

	author := "著者"
	name := "名前"
	description := "説明"
	password := "password"
	passwordHint := "ヒント"
	link1ID := "3"
	link1Name := "test"
	link1URL := "http://example.com"
	link2Name := "test2"
	link2URL := "http://2.example.com"

	var links []*model.BlogLink
	links = append(links,
		&model.BlogLink{
			ID:   "1",
			Name: link1Name,
			URL:  link1URL,
		},
		&model.BlogLink{
			ID:   "2",
			Name: link2Name,
			URL:  link2URL,
		},
	)
	rawLinks, _ := json.Marshal(links)

	t.Run("正常系：全フィールドあり（パスワード変更あり）", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE blog SET author = ?,name = ?,description = ?,publish_option = ?,links = ?,password = ?,password_updated_at = ?,password_hint = ?,updated_at = ? WHERE id = ? AND user_id = ?")

		pubRows := sqlmock.NewRows([]string{"publsh_option"}).
			AddRow("PASSWORD")
		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author, name, description, "PASSWORD", "ヒント", rawLinks, now, now)

		publishOption := model.PublishOptionPassword

		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(pubRows)
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(author, name, description, 2, AnyBytes{}, AnyString{}, now, passwordHint, now, "000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(rows)

		input := &model.UpdateBlogInput{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: &publishOption,
			Password:      &password,
			PasswordHint:  &passwordHint,
			Links:         []*model.UpdateBlogLinkInput{{ID: &link1ID, Name: link1Name, URL: link1URL}, {ID: nil, Name: link2Name, URL: link2URL}},
		}

		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  passwordHint,
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		result, err := repo.Update(now, *input, -1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：ブログ更新なし", func(t *testing.T) {
		pubRows := sqlmock.NewRows([]string{"publsh_option"}).
			AddRow("PASSWORD")
		rows := sqlmock.NewRows([]string{"id", "blog_key", "user_id", "author", "name", "description", "publsh_option", "password_hint", "links", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "dccb4797-90c7-ea22-6866-7eb954acbbda", -1, author, name, description, "PASSWORD", "ヒント", rawLinks, now, now)

		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(pubRows)
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(rows)

		input := &model.UpdateBlogInput{
			ID: "000XAL6S41ACTAV9WEVGEMMVR8",
		}

		expected := &model.Blog{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			BlogKey:       "dccb4797-90c7-ea22-6866-7eb954acbbda",
			Author:        &author,
			Name:          &name,
			Description:   &description,
			PublishOption: model.PublishOptionPassword,
			PasswordHint:  "ヒント",
			Links:         links,
			CreatedAt:     &nowstr,
			UpdatedAt:     &nowstr,
		}

		result, err := repo.Update(now, *input, -1)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：公開オプション取得失敗", func(t *testing.T) {
		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateBlogInput{
			ID: "000XAL6S41ACTAV9WEVGEMMVR8",
		}

		_, err := repo.Update(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ更新失敗", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE blog SET publish_option = ?,password = ?,password_updated_at = ?,password_hint = ?,updated_at = ? WHERE id = ? AND user_id = ?")

		publishOption := model.PublishOptionPublic

		pubRows := sqlmock.NewRows([]string{"publsh_option"}).
			AddRow("PASSWORD")

		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(pubRows)
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(1, "", now, "", now, "000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateBlogInput{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			PublishOption: &publishOption,
		}

		_, err := repo.Update(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログが見つからない", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE blog SET publish_option = ?,password = ?,password_updated_at = ?,password_hint = ?,updated_at = ? WHERE id = ? AND user_id = ?")

		publishOption := model.PublishOptionPublic

		pubRows := sqlmock.NewRows([]string{"publsh_option"}).
			AddRow("PASSWORD")

		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(pubRows)
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(1, "", now, "", now, "000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnResult(sqlmock.NewResult(0, 0))

		input := &model.UpdateBlogInput{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			PublishOption: &publishOption,
		}

		_, err := repo.Update(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ取得失敗", func(t *testing.T) {
		s := regexp.QuoteMeta("UPDATE blog SET publish_option = ?,password = ?,password_updated_at = ?,password_hint = ?,updated_at = ? WHERE id = ? AND user_id = ?")

		publishOption := model.PublishOptionPublic

		pubRows := sqlmock.NewRows([]string{"publsh_option"}).
			AddRow("PASSWORD")

		mock.ExpectPrepare(optsql)
		mock.ExpectQuery(optsql).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnRows(pubRows)
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(1, "", now, "", now, "000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -1).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateBlogInput{
			ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
			PublishOption: &publishOption,
		}

		_, err := repo.Update(now, *input, -1)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestDeleteById(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)

	s := regexp.QuoteMeta("DELETE from blog WHERE id = ? AND user_id = ?")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -2).
			WillReturnResult(sqlmock.NewResult(1, 1))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteById(tx, "000XAL6S41ACTAV9WEVGEMMVR8", -2)

		tx.Rollback()

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系: ブログ削除失敗", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -2).
			WillReturnError(fmt.Errorf("some error"))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteById(tx, "000XAL6S41ACTAV9WEVGEMMVR8", -2)

		tx.Rollback()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系: 対象のブログが見つからない", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("000XAL6S41ACTAV9WEVGEMMVR8", -2).
			WillReturnResult(sqlmock.NewResult(0, 0))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteById(tx, "000XAL6S41ACTAV9WEVGEMMVR8", -2)

		tx.Rollback()

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestDeleteByUserId(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewBlogRepository(db)

	s := regexp.QuoteMeta("DELETE FROM blog WHERE user_id = ?")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(-1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteByUserId(tx, -1)

		tx.Rollback()

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectBegin()
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(-1).
			WillReturnError(fmt.Errorf("some error"))

		tx, err := db.Begin()
		if err != nil {
			fmt.Println("failed to begin transaction:", err)
		}

		err = repo.DeleteByUserId(tx, -1)

		tx.Rollback()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
