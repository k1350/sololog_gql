package blog

import (
	"database/sql"
	"encoding/json"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	"strings"
	"time"

	"github.com/matoous/go-nanoid/v2"
	ulid "github.com/oklog/ulid/v2"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type BlogRepository struct {
	db *sql.DB
}

var PublishOptionField = "CASE publish_option WHEN 1 THEN \"" + model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() + "\" END AS publsh_option"
var fields = "id, blog_key, user_id, author, name, description, " + PublishOptionField + ", password_hint, links, created_at, updated_at"

func NewBlogRepository(db *sql.DB) *BlogRepository {
	return &BlogRepository{db}
}

func (r *BlogRepository) BeginTransaction() (*sql.Tx, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return tx, err
}

func (r *BlogRepository) FindByUserId(userId int) ([]*model.Blog, error) {
	s := "SELECT " + fields + " FROM blog WHERE user_id = ? ORDER BY id DESC"
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	rows, err := stmt.Query(userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer rows.Close()

	var result []*model.Blog
	for rows.Next() {
		b := &model.BlogForDB{}
		err = rows.Scan(&b.ID, &b.BlogKey, &b.UserId, &b.Author, &b.Name, &b.Description, &b.PublishOption, &b.PasswordHint, &b.Links, &b.CreatedAt, &b.UpdatedAt)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}

		result = append(result, b.ConvertBlogForDBToBlog())
	}

	err = rows.Err()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	return result, nil
}

func (r *BlogRepository) FindById(blogId string, userId int) (*model.Blog, error) {
	result, err := r.getBlog(blogId, userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	return result, nil
}

func (r *BlogRepository) FindByBlogKey(blogKey string, opts *model.BlogByKeyOpts, passwordInputTime *time.Time) (*model.Blog, error) {
	var sb strings.Builder
	sb.WriteString("SELECT " + fields + ", password, password_updated_at FROM blog WHERE blog_key = ? AND (publish_option = 1 OR publish_option = 2 ")

	var args []interface{}
	args = append(args, blogKey)
	if opts != nil {
		if opts.UserId != nil {
			sb.WriteString("OR publish_option = 99 AND user_id = ?")
			args = append(args, opts.UserId)
		}
	}
	sb.WriteString(") LIMIT 1")

	b := &model.BlogForDB{}
	var password string
	stmt, err := r.db.Prepare(sb.String())
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.
		QueryRow(args...).
		Scan(&b.ID, &b.BlogKey, &b.UserId, &b.Author, &b.Name, &b.Description, &b.PublishOption, &b.PasswordHint, &b.Links, &b.CreatedAt, &b.UpdatedAt, &password, &b.PasswordUpdatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	// 公開オプションが「公開」ならそのまま返す
	if b.PublishOption == model.PublishOptionPublic {
		return b.ConvertBlogForDBToBlog(), nil
	}
	// 公開オプションが「パスワード制限」か「非公開」の場合、閲覧権限をチェックする
	if opts != nil {
		// ユーザー自身が作ったブログなら閲覧可能
		if opts.UserId != nil {
			if *opts.UserId == b.UserId {
				return b.ConvertBlogForDBToBlog(), nil
			}
		}
		// 公開オプションが「パスワード制限」で正しいパスワードを入力された場合は閲覧可能
		if b.PublishOption == model.PublishOptionPassword && opts.Password != nil {
			err := bcrypt.CompareHashAndPassword([]byte(password), []byte(*opts.Password))
			switch {
			case err == bcrypt.ErrMismatchedHashAndPassword:
				// パスワードが一致しない
				return nil, errors.Wrap(customError.ForbiddenError, "DB error")
			case err != nil:
				// パスワードが一致しない以外のエラー
				return nil, errors.Wrap(err, "DB error")
			default:
				return b.ConvertBlogForDBToBlog(), nil
			}
		}
	}
	if b.PublishOption == model.PublishOptionPassword {
		if passwordInputTime == nil {
			// パスワード未入力の場合、閲覧不可の要素を削除
			b.Author = nil
			b.Name = nil
			b.Description = nil
			b.Links = nil
			b.CreatedAt = nil
			b.UpdatedAt = nil
			return b.ConvertBlogForDBToBlog(), nil
		} else {
			// 公開オプションが「パスワード制限」でパスワード更新日時<パスワード入力日時なら閲覧可能
			if b.PasswordUpdatedAt.Before(*passwordInputTime) {
				return b.ConvertBlogForDBToBlog(), nil
			}
			return nil, customError.BlogAuthenticationExpiredError
		}
	}

	// 閲覧権限なし
	return nil, errors.Wrap(customError.ForbiddenError, "DB error")
}

func (r *BlogRepository) IsAuthor(blogKey string, userId int) (bool, error) {
	s := "SELECT count(id) FROM blog WHERE blog_key = ? AND user_id = ?"
	var count *int
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return false, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.QueryRow(blogKey, userId).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "DB error")
	}
	return *count > 0, nil
}

func (r *BlogRepository) CountBlogs(userId int) (*int, error) {
	s := "SELECT count(id) FROM blog where user_id = ?"
	var count *int
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.QueryRow(userId).Scan(&count)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return count, nil
}

func (r *BlogRepository) Create(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error) {
	blogKey, err := generateId()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	var inputFields []string
	var args []interface{}

	id := ulid.Make().String()

	// Blog の sql 構築
	inputFields = append(inputFields, "id", "blog_key", "user_id", "author", "name", "publish_option", "password_updated_at", "created_at", "updated_at")
	args = append(args, id, blogKey, userId, input.Author, input.Name, convertPublishOptionToInt(input.PublishOption), now, now, now)

	if input.Description != nil {
		inputFields = append(inputFields, "description")
		args = append(args, *input.Description)
	}
	if input.PublishOption == model.PublishOptionPassword {
		inputFields = append(inputFields, "password")
		hashed, err := bcrypt.GenerateFromPassword([]byte(*input.Password), 12)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}
		args = append(args, string(hashed))
		if input.PasswordHint != nil {
			inputFields = append(inputFields, "password_hint")
			args = append(args, *input.PasswordHint)
		}
	}

	inputFields = append(inputFields, "links")
	if input.Links != nil && len(input.Links) > 0 {
		var links []model.BlogLink
		for _, v := range input.Links {
			var name string
			if v.Name != nil {
				name = *v.Name
			} else {
				name = ""
			}
			links = append(links, model.BlogLink{ID: ulid.Make().String(), Name: name, URL: v.URL})
		}
		json_links, err := json.Marshal(links)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}
		args = append(args, json_links)
	} else {
		args = append(args, "[]")
	}

	s := "INSERT INTO blog (" + strings.Join(inputFields, ",") + ") VALUES (?" + strings.Repeat(",?", len(args)-1) + ")"

	// Blog 作成
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	_, err = stmt.Exec(args...)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	result, err := r.getBlog(id, userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return result, nil
}

func (r *BlogRepository) Update(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error) {
	// 更新前の公開オプションを取得する
	option, err := r.getPublishOption(input.ID, userId)
	if err != nil {
		return nil, err
	}

	// Blog 更新 SQL 構築
	var inputFields []string
	var args []interface{}

	if input.Author != nil {
		inputFields = append(inputFields, "author")
		args = append(args, *input.Author)
	}
	if input.Name != nil {
		inputFields = append(inputFields, "name")
		args = append(args, *input.Name)
	}
	if input.Description != nil {
		inputFields = append(inputFields, "description")
		args = append(args, *input.Description)
	}
	if input.PublishOption != nil {
		inputFields = append(inputFields, "publish_option")
		args = append(args, convertPublishOptionToInt(*input.PublishOption))
	}
	if input.Links != nil {
		inputFields = append(inputFields, "links")
		if len(input.Links) > 0 {
			var links []model.BlogLink
			for _, v := range input.Links {
				if v.ID != nil {
					links = append(links, model.BlogLink{ID: *v.ID, Name: v.Name, URL: v.URL})
				} else {
					links = append(links, model.BlogLink{ID: ulid.Make().String(), Name: v.Name, URL: v.URL})
				}
			}
			json_links, err := json.Marshal(links)
			if err != nil {
				return nil, errors.Wrap(err, "DB error")
			}
			args = append(args, json_links)
		} else {
			args = append(args, "[]")
		}
	}
	// 更新後の公開オプションが「パスワード」かどうか
	isPassword := false
	if input.PublishOption != nil {
		if *input.PublishOption == model.PublishOptionPassword {
			isPassword = true
		}
	} else {
		if *option == model.PublishOptionPassword {
			isPassword = true
		}
	}
	if isPassword {
		// 更新後の公開オプションが「パスワード」で、パスワードの入力値がある場合は更新
		if input.Password != nil {
			inputFields = append(inputFields, "password", "password_updated_at")
			hashed, err := bcrypt.GenerateFromPassword([]byte(*input.Password), 12)
			if err != nil {
				return nil, errors.Wrap(err, "DB error")
			}
			args = append(args, string(hashed), now)
		}
		// パスワードヒントも更新
		if input.PasswordHint != nil {
			inputFields = append(inputFields, "password_hint")
			args = append(args, *input.PasswordHint)
		}
	} else {
		// 更新後の公開オプションが「パスワード」ではなく、従来は「パスワード」だった場合はパスワードとパスワードヒントを削除
		if *option == model.PublishOptionPassword {
			inputFields = append(inputFields, "password", "password_updated_at", "password_hint")
			args = append(args, "", now, "")
		}
	}
	// Blog に更新があるかどうか
	if len(inputFields) > 0 {
		inputFields = append(inputFields, "updated_at")
		args = append(args, now)

		var sb strings.Builder
		sb.WriteString("UPDATE blog SET ")

		for i, v := range inputFields {
			sb.WriteString(v + " = ?")
			if i < len(inputFields)-1 {
				sb.WriteString(",")
			}
		}
		sb.WriteString(" WHERE id = ? AND user_id = ?")
		args = append(args, input.ID, userId)
		stmt, err := r.db.Prepare(sb.String())
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}
		defer stmt.Close()

		res, err := stmt.Exec(args...)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}
		rowsAffected, err := res.RowsAffected()
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}
		if rowsAffected == 0 {
			return nil, errors.Wrap(customError.NotFoundError, "DB error")
		}
	}
	result, err := r.getBlog(input.ID, userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return result, nil
}

func (r *BlogRepository) DeleteById(tx *sql.Tx, blogId string, userId int) error {
	stmt, err := tx.Prepare("DELETE from blog WHERE id = ? AND user_id = ?")
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	res, err := stmt.Exec(blogId, userId)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.NotFoundError, "DB error")
	}

	return nil
}

func (r *BlogRepository) DeleteByUserId(tx *sql.Tx, userId int) error {
	stmt, err := tx.Prepare("DELETE FROM blog WHERE user_id = ?")
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	_, err = stmt.Exec(userId)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	return nil
}

func convertPublishOptionToInt(opt model.PublishOption) int {
	switch opt {
	case model.PublishOptionPublic:
		return 1
	case model.PublishOptionPassword:
		return 2
	case model.PublishOptionPrivate:
		return 99
	}
	// 到達しないが return を書かないとエラーになるので書く
	return 99
}

func generateId() (string, error) {
	return gonanoid.Generate("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 21)
}

func (r *BlogRepository) getBlog(id string, userId int) (*model.Blog, error) {
	s := "SELECT " + fields + " FROM blog WHERE id = ? AND user_id = ? LIMIT 1"
	stmt, err := r.db.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	b := &model.BlogForDB{}
	err = stmt.QueryRow(id, userId).Scan(&b.ID, &b.BlogKey, &b.UserId, &b.Author, &b.Name, &b.Description, &b.PublishOption, &b.PasswordHint, &b.Links, &b.CreatedAt, &b.UpdatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return b.ConvertBlogForDBToBlog(), nil
}

func (r *BlogRepository) getPublishOption(id string, userId int) (*model.PublishOption, error) {
	// 更新前の公開オプションを取得する
	optsql := "SELECT " + PublishOptionField + " FROM blog WHERE id = ? AND user_id = ?"
	optstmt, err := r.db.Prepare(optsql)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer optstmt.Close()
	var option model.PublishOption
	err = optstmt.QueryRow(id, userId).Scan(&option)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return &option, nil
}
