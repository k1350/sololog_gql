package article

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type AnyString struct{}

func (a AnyString) Match(v driver.Value) bool {
	_, ok := v.(string)
	return ok
}

func TestFindByBlogId(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	content1 := "内容1"
	content2 := "内容2"

	s := "SELECT article.id, article.article_key, article.content," +
		"CASE publish_option WHEN 1 THEN \"" + model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() + "\" END AS publsh_option" +
		", article.created_at, article.updated_at FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?"

	t.Run("正常系：Forward pagination 1ページ目", func(t *testing.T) {
		var sb strings.Builder
		sb.WriteString(s)
		sb.WriteString(" ORDER BY article.id ASC LIMIT ?")

		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "publish_option", "created_at", "updated_at"}).
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "VMSFvmh8e4UlwUQbFKREl", content1, model.PublishOptionPublic, now, now).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "ITRKESW3kEzNVpW0kuQ2T", content2, model.PublishOptionPublic, now, now)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		first := 2
		input := &model.ArticlePaginationInput{
			First: &first,
		}

		s := regexp.QuoteMeta(sb.String())
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnRows(rows)
		expected := []*model.Article{
			{
				ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
				ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
				Content:       &content1,
				PublishOption: model.PublishOptionPublic,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
				ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
				Content:       &content2,
				PublishOption: model.PublishOptionPublic,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
		}

		result, err := repo.FindByBlogId(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Forward pagination 2ページ目", func(t *testing.T) {
		var sb strings.Builder
		sb.WriteString(s)
		sb.WriteString(" AND article.id > ? ORDER BY article.id ASC LIMIT ?")

		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "publish_option", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR9", "VMSFvmh8e4UlwUQbFKREl", content1, model.PublishOptionPrivate, now, now).
			AddRow("000XAL6S41ACTAV9WEVGEMMVRC", "ITRKESW3kEzNVpW0kuQ2T", content2, model.PublishOptionPrivate, now, now)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
		first := 2
		input := &model.ArticlePaginationInput{
			After: &after,
			First: &first,
		}

		s := regexp.QuoteMeta(sb.String())
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01ARZ3NDEKTSV4RRFFQ69G5FAV", 2).
			WillReturnRows(rows)
		expected := []*model.Article{
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVR9",
				ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
				Content:       &content1,
				PublishOption: model.PublishOptionPrivate,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVRC",
				ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
				Content:       &content2,
				PublishOption: model.PublishOptionPrivate,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
		}

		result, err := repo.FindByBlogId(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Backward pagination 1ページ目", func(t *testing.T) {
		var sb strings.Builder
		sb.WriteString(s)
		sb.WriteString(" ORDER BY article.id DESC LIMIT ?")

		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "publish_option", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVRC", "VMSFvmh8e4UlwUQbFKREl", content2, model.PublishOptionPassword, now, now).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "ITRKESW3kEzNVpW0kuQ2T", content1, model.PublishOptionPassword, now, now)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		last := 2
		input := &model.ArticlePaginationInput{
			Last: &last,
		}

		s := regexp.QuoteMeta(sb.String())
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnRows(rows)
		expected := []*model.Article{
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
				ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
				Content:       &content1,
				PublishOption: model.PublishOptionPassword,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVRC",
				ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
				Content:       &content2,
				PublishOption: model.PublishOptionPassword,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
		}

		result, err := repo.FindByBlogId(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Backward pagination 2ページ目", func(t *testing.T) {
		var sb strings.Builder
		sb.WriteString(s)
		sb.WriteString(" AND article.id < ? ORDER BY article.id DESC LIMIT ?")

		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "publish_option", "created_at", "updated_at"}).
			AddRow("000XAL6S41ACTAV9WEVGEMMVR8", "VMSFvmh8e4UlwUQbFKREl", content2, model.PublishOptionPrivate, now, now).
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content1, model.PublishOptionPrivate, now, now)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "000XAL6S41ACTAV9WEVGEMMVR8"
		last := 2
		input := &model.ArticlePaginationInput{
			Before: &before,
			Last:   &last,
		}

		s := regexp.QuoteMeta(sb.String())
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "000XAL6S41ACTAV9WEVGEMMVR8", 2).
			WillReturnRows(rows)
		expected := []*model.Article{
			{
				ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
				ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
				Content:       &content1,
				PublishOption: model.PublishOptionPrivate,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
			{
				ID:            "000XAL6S41ACTAV9WEVGEMMVR8",
				ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
				Content:       &content2,
				PublishOption: model.PublishOptionPrivate,
				CreatedAt:     &nowStr,
				UpdatedAt:     &nowStr,
			},
		}

		result, err := repo.FindByBlogId(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		for i := 0; i < 2; i++ {
			got := result[i]
			want := expected[i]
			if !reflect.DeepEqual(got, want) {
				t.Errorf("got:\n%v\n\nwant:\n%v", got, want)
			}
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		var sb strings.Builder
		sb.WriteString(s)
		sb.WriteString(" ORDER BY article.id ASC LIMIT ?")

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		first := 2
		input := &model.ArticlePaginationInput{
			First: &first,
		}

		s := regexp.QuoteMeta(sb.String())
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.FindByBlogId(blogId, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestHasPreviousPage(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)

	t.Run("正常系：Last 指定あり, 1 ページ目, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		last := 2
		input := &model.ArticlePaginationInput{
			Last: &last,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Last 指定あり, 2 ページ目, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id < ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01ARZ3NDEKTSV4RRFFQ69G5FAV").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
		last := 2
		input := &model.ArticlePaginationInput{
			Before: &before,
			Last:   &last,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Last 指定あり, 2 ページ目, 次のページなし", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id < ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("2")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01ARZ3NDEKTSV4RRFFQ69G5FAV").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
		last := 2
		input := &model.ArticlePaginationInput{
			Before: &before,
			Last:   &last,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：After 指定あり, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id <= ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			After: &after,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：After 指定あり, 次のページなし", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id <= ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnError(sql.ErrNoRows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			After: &after,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Last も After も指定なし", func(t *testing.T) {
		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			Before: &before,
		}
		result, err := repo.HasPreviousPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：Last 指定あり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH").
			WillReturnError(fmt.Errorf("some error"))

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		last := 2
		input := &model.ArticlePaginationInput{
			Last: &last,
		}
		_, err := repo.HasPreviousPage(blogId, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：After 指定あり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id <= ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnError(fmt.Errorf("some error"))

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			After: &after,
		}
		_, err := repo.HasPreviousPage(blogId, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestHasNextPage(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)

	t.Run("正常系：First 指定あり, 1 ページ目, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		first := 2
		input := &model.ArticlePaginationInput{
			First: &first,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：First 指定あり, 2 ページ目, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id > ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01ARZ3NDEKTSV4RRFFQ69G5FAV").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
		first := 2
		input := &model.ArticlePaginationInput{
			After: &after,
			First: &first,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：First 指定あり, 2 ページ目, 次のページなし", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id > ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("2")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01ARZ3NDEKTSV4RRFFQ69G5FAV").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01ARZ3NDEKTSV4RRFFQ69G5FAV"
		first := 2
		input := &model.ArticlePaginationInput{
			After: &after,
			First: &first,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Before 指定あり, 次のページあり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id >= ?")
		rows := sqlmock.NewRows([]string{"count"}).
			AddRow("3")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnRows(rows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			Before: &before,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, true) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, true)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：Before 指定あり, 次のページなし", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id >= ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnError(sql.ErrNoRows)

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			Before: &before,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：First も Before も指定なし", func(t *testing.T) {
		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		after := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			After: &after,
		}
		result, err := repo.HasNextPage(blogId, *input)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, false) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, false)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：First 指定あり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH").
			WillReturnError(fmt.Errorf("some error"))

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		first := 2
		input := &model.ArticlePaginationInput{
			First: &first,
		}
		_, err := repo.HasNextPage(blogId, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：Before 指定あり", func(t *testing.T) {
		s := regexp.QuoteMeta("SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id >= ?")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", "01D78XZ44G0000000000000000").
			WillReturnError(fmt.Errorf("some error"))

		blogId := "01G65Z755AFWAKHE12NY0CQ9FH"
		before := "01D78XZ44G0000000000000000"
		input := &model.ArticlePaginationInput{
			Before: &before,
		}
		_, err := repo.HasNextPage(blogId, *input)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestFindByArticleKey(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	sfield := "SELECT article.id, article.article_key, article.content, article.created_at, article.updated_at, blog.user_id, blog.password, blog.password_updated_at, CASE publish_option WHEN 1 THEN \"" + model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() + "\" END AS publsh_option"
	swhere := ` FROM article 
		INNER JOIN blog ON blog.id = article.blog_id 
		WHERE article.article_key = ? AND (blog.publish_option = 1 OR blog.publish_option = 2 `

	sbase := sfield + swhere
	rowbase := sqlmock.NewRows([]string{"id", "article_key", "content", "created_at", "updated_at", "user_id", "password", "password_updated_at", "publsh_option"})
	content := "内容"

	t.Run("正常系：公開", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "", now, "PUBLIC")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", nil, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態でパスワード保護されていて正しいパスワードを入力された", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		password := "password"
		opts := &model.ArticleByKeyOpts{UserId: nil, Password: &password}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系（パスワード入力日時あり）：非ログイン状態でパスワード保護されていてパスワード更新日時<パスワード入力日時", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "dummy", time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC), "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		opts := &model.ArticleByKeyOpts{UserId: nil, Password: nil}

		pit := time.Date(2000, 1, 1, 0, 0, 0, 1, time.UTC)
		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, &pit)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でありパスワード保護されている", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -1).
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		userId := -1
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でなくパスワード保護されていて、パスワード未入力", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -2).
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       nil,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     nil,
			UpdatedAt:     nil,
		}

		userId := -2
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系（パスワード入力日時あり）：著者でなくパスワード保護されていてパスワード更新日時=パスワード入力日時", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "dummy", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -2).
			WillReturnRows(rows)

		userId := -2
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: nil}

		_, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, &now)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態でパスワード保護されていて、パスワード未入力", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       nil,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     nil,
			UpdatedAt:     nil,
		}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", nil, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系（パスワード入力日時あり）：非ログイン状態でパスワード保護されていてパスワード更新日時>パスワード入力日時", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "dummy", time.Date(2000, 1, 1, 0, 0, 0, 1, time.UTC), "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		opts := &model.ArticleByKeyOpts{UserId: nil, Password: nil}

		pit := time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
		_, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, &pit)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.BlogAuthenticationExpiredError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者であり非公開", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", content, now, now, -1, "", now, "PRIVATE")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -1).
			WillReturnRows(rows)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "ITRKESW3kEzNVpW0kuQ2T",
			Content:       &content,
			PublishOption: model.PublishOptionPrivate,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		userId := -1
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：著者でなく非公開", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -2).
			WillReturnError(sql.ErrNoRows)

		userId := -2
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: nil}

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Error("should got nil")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("正常系：非ログイン状態で非公開", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnError(sql.ErrNoRows)

		result, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", nil, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, sql.ErrNoRows) {
			t.Error("got unexpected err:", err)
		}

		if result != nil {
			t.Error("should got nil")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：著者でなくパスワード保護されていて誤ったパスワードを入力された", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + "OR blog.publish_option = 99 AND blog.user_id = ?) LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", "内容", now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T", -2).
			WillReturnRows(rows)

		userId := -2
		password := "password2"
		opts := &model.ArticleByKeyOpts{UserId: &userId, Password: &password}

		_, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：非ログイン状態でパスワード保護されていて誤ったパスワードを入力された", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")
		rows := rowbase.
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "ITRKESW3kEzNVpW0kuQ2T", "内容", now, now, -1, "$2a$10$P9Zr9LES1Yv/n6k77pDy0OVwCRBeHRhHsFMQyU6GfkfpOXfHOPjgG", now, "PASSWORD")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnRows(rows)

		password := "password2"
		opts := &model.ArticleByKeyOpts{UserId: nil, Password: &password}

		_, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", opts, nil)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.ForbiddenError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：取得時エラー", func(t *testing.T) {
		s := regexp.QuoteMeta(sbase + ") LIMIT 1")

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs("ITRKESW3kEzNVpW0kuQ2T").
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.FindByArticleKey("ITRKESW3kEzNVpW0kuQ2T", nil, nil)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCreate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)
	selectSql := "SELECT article.id, article.article_key, article.content," +
		"CASE publish_option WHEN 1 THEN \"" + model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() + "\" END AS publsh_option" +
		", article.created_at, article.updated_at FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE article.id = ? AND blog.user_id = ?"
	ss := regexp.QuoteMeta(selectSql)

	s := regexp.QuoteMeta("INSERT INTO article (id, blog_id, article_key, content, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?)")

	content := "内容"

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "created_at", "publish_option", "updated_at"}).
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "VMSFvmh8e4UlwUQbFKREl", content, model.PublishOptionPublic, now, now)

		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, "01G65Z755AFWAKHE12NY0CQ9FH", AnyString{}, "内容", now, now).
			WillReturnResult(sqlmock.NewResult(3, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs(AnyString{}, 2).
			WillReturnRows(rows)

		input := &model.CreateArticleInput{
			BlogID:  "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容",
		}

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
			Content:       &content,
			PublishOption: model.PublishOptionPublic,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		result, err := repo.Create(now, *input, 2)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：記事作成失敗", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, "01G65Z755AFWAKHE12NY0CQ9FH", AnyString{}, "内容", now, now).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.CreateArticleInput{
			BlogID:  "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容",
		}

		_, err := repo.Create(now, *input, 2)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：記事取得失敗", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(AnyString{}, "01G65Z755AFWAKHE12NY0CQ9FH", AnyString{}, "内容", now, now).
			WillReturnResult(sqlmock.NewResult(3, 1))
		mock.ExpectPrepare(ss)
		mock.ExpectQuery(ss).
			WithArgs(AnyString{}, 2).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.CreateArticleInput{
			BlogID:  "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容",
		}

		_, err := repo.Create(now, *input, 2)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestUpdate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)
	now := time.Now().UTC()
	nowStr := now.Format(time.RFC3339)

	selectSql := "SELECT article.id, article.article_key, article.content," +
		"CASE publish_option WHEN 1 THEN \"" + model.PublishOptionPublic.String() + "\" WHEN 2 THEN \"" + model.PublishOptionPassword.String() + "\" ELSE \"" + model.PublishOptionPrivate.String() + "\" END AS publsh_option" +
		", article.created_at, article.updated_at FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE article.id = ? AND blog.user_id = ?"
	sas := regexp.QuoteMeta(selectSql)
	uas := `UPDATE article 
		INNER JOIN blog ON article.blog_id = blog.id 
		SET article.content = ?, article.updated_at = ? 
		WHERE article.id = ? AND blog.user_id = ?`
	s := regexp.QuoteMeta(uas)

	content := "内容2"

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "article_key", "content", "publish_option", "created_at", "updated_at"}).
			AddRow("01ARZ3NDEKTSV4RRFFQ69G5FAV", "VMSFvmh8e4UlwUQbFKREl", content, model.PublishOptionPassword, now, now)

		expected := &model.Article{
			ID:            "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			ArticleKey:    "VMSFvmh8e4UlwUQbFKREl",
			Content:       &content,
			PublishOption: model.PublishOptionPassword,
			CreatedAt:     &nowStr,
			UpdatedAt:     &nowStr,
		}

		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("内容2", now, "01ARZ3NDEKTSV4RRFFQ69G5FAV", 2).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectPrepare(sas)
		mock.ExpectQuery(sas).
			WithArgs("01ARZ3NDEKTSV4RRFFQ69G5FAV", 2).
			WillReturnRows(rows)

		input := &model.UpdateArticleInput{
			ID:      "01ARZ3NDEKTSV4RRFFQ69G5FAV",
			Content: "内容2",
		}

		result, err := repo.Update(now, *input, 2)

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：Not Found", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("内容", now, "01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnResult(sqlmock.NewResult(0, 0))

		input := &model.UpdateArticleInput{
			ID:      "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容",
		}

		_, err := repo.Update(now, *input, 2)

		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ更新失敗", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("内容", now, "01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateArticleInput{
			ID:      "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容",
		}

		_, err := repo.Update(now, *input, 2)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：ブログ取得失敗", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("内容2", now, "01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectPrepare(sas)
		mock.ExpectQuery(sas).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnError(fmt.Errorf("some error"))

		input := &model.UpdateArticleInput{
			ID:      "01G65Z755AFWAKHE12NY0CQ9FH",
			Content: "内容2",
		}

		_, err := repo.Update(now, *input, 2)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestDeleteById(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewArticleRepository(db)
	as := `DELETE article FROM article 
		INNER JOIN blog ON article.blog_id = blog.id 
		WHERE article.id = ? AND blog.user_id = ?`
	s := regexp.QuoteMeta(as)

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := repo.DeleteById("01G65Z755AFWAKHE12NY0CQ9FH", 2)
		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：Not Found", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnResult(sqlmock.NewResult(0, 0))

		err := repo.DeleteById("01G65Z755AFWAKHE12NY0CQ9FH", 2)
		if err == nil {
			t.Error("should got error")
		} else if !errors.Is(err, customError.NotFoundError) {
			t.Error("got unexpected err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：SQLエラー", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs("01G65Z755AFWAKHE12NY0CQ9FH", 2).
			WillReturnError(fmt.Errorf("some error"))

		err := repo.DeleteById("01G65Z755AFWAKHE12NY0CQ9FH", 2)
		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
