package article

import (
	"database/sql"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"strings"
	"time"

	"github.com/matoous/go-nanoid/v2"
	ulid "github.com/oklog/ulid/v2"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/repository/blog"
)

type ArticleRepository struct {
	db *sql.DB
}

func NewArticleRepository(db *sql.DB) *ArticleRepository {
	return &ArticleRepository{db}
}

func (r *ArticleRepository) FindByBlogId(blogId string, input model.ArticlePaginationInput) ([]*model.Article, error) {
	var sb strings.Builder
	var args []interface{}
	s := `SELECT article.id, article.article_key, article.content,` + blog.PublishOptionField + `, article.created_at, article.updated_at 
		FROM article INNER JOIN blog ON blog.id = article.blog_id 
		WHERE blog.id = ?`
	args = append(args, blogId)
	sb.WriteString(s)

	if input.After != nil {
		sb.WriteString(" AND article.id > ?")
		args = append(args, *input.After)
	}
	if input.Before != nil {
		sb.WriteString(" AND article.id < ?")
		args = append(args, *input.Before)
	}
	if input.First != nil {
		sb.WriteString(" ORDER BY article.id ASC LIMIT ?")
		args = append(args, *input.First)
	} else if input.Last != nil {
		sb.WriteString(" ORDER BY article.id DESC LIMIT ?")
		args = append(args, *input.Last)
	}

	stmt, err := r.db.Prepare(sb.String())
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	rows, err := stmt.Query(args...)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer rows.Close()

	var result []*model.Article
	for rows.Next() {

		a := &model.ArticleForDB{}
		err = rows.Scan(&a.ID, &a.ArticleKey, &a.Content, &a.PublishOption, &a.CreatedAt, &a.UpdatedAt)
		if err != nil {
			return nil, errors.Wrap(err, "DB error")
		}

		result = append(result, a.ConvertArticleForDBToArticle())
	}

	err = rows.Err()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	if input.Last != nil {
		// id 昇順に直す
		for i := 0; i < len(result)/2; i++ {
			result[i], result[len(result)-i-1] = result[len(result)-i-1], result[i]
		}
	}

	return result, nil
}

func (r *ArticleRepository) HasPreviousPage(blogId string, input model.ArticlePaginationInput) (bool, error) {
	if input.Last != nil {
		count, err := r.countApplyCursorsToEdges(blogId, input)
		if err != nil {
			return false, errors.Wrap(err, "DB error")
		}
		if count > *input.Last {
			return true, nil
		}
		return false, nil
	}
	if input.After != nil {
		var args []interface{}
		s := "SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id <= ?"
		args = append(args, blogId, *input.After)

		var countAfter string
		stmt, err := r.db.Prepare(s)
		if err != nil {
			return false, errors.Wrap(err, "DB error")
		}
		defer stmt.Close()
		err = stmt.QueryRow(args...).Scan(&countAfter)
		switch {
		case err == sql.ErrNoRows:
			return false, nil
		case err != nil:
			return false, errors.Wrap(err, "DB error")
		}

		countAfterInt, _ := strconv.Atoi(countAfter)
		if countAfterInt > 0 {
			return true, nil
		}
	}
	return false, nil
}

func (r *ArticleRepository) HasNextPage(blogId string, input model.ArticlePaginationInput) (bool, error) {
	if input.First != nil {
		count, err := r.countApplyCursorsToEdges(blogId, input)
		if err != nil {
			return false, errors.Wrap(err, "DB error")
		}
		if count > *input.First {
			return true, nil
		}
		return false, nil
	}
	if input.Before != nil {
		var args []interface{}
		s := "SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ? AND article.id >= ?"
		args = append(args, blogId, *input.Before)

		var countBefore string
		stmt, err := r.db.Prepare(s)
		if err != nil {
			return false, errors.Wrap(err, "DB error")
		}
		defer stmt.Close()
		err = stmt.QueryRow(args...).Scan(&countBefore)
		switch {
		case err == sql.ErrNoRows:
			return false, nil
		case err != nil:
			return false, errors.Wrap(err, "DB error")
		}

		countBeforeInt, _ := strconv.Atoi(countBefore)
		if countBeforeInt > 0 {
			return true, nil
		}
	}
	return false, nil
}

func (r *ArticleRepository) FindByArticleKey(articleKey string, opts *model.ArticleByKeyOpts, passwordInputTime *time.Time) (*model.Article, error) {
	var sb strings.Builder
	sb.WriteString("SELECT article.id, article.article_key, article.content, article.created_at, article.updated_at, blog.user_id, blog.password, blog.password_updated_at, " + blog.PublishOptionField)

	s := ` FROM article 
		INNER JOIN blog ON blog.id = article.blog_id 
		WHERE article.article_key = ? AND (blog.publish_option = 1 OR blog.publish_option = 2 `

	sb.WriteString(s)

	var args []interface{}
	args = append(args, articleKey)
	if opts != nil {
		if opts.UserId != nil {
			sb.WriteString("OR blog.publish_option = 99 AND blog.user_id = ?")
			args = append(args, opts.UserId)
		}
	}
	sb.WriteString(") LIMIT 1")

	a := &model.ArticleForDB{}
	b := &model.BlogForDB{}
	var password string

	stmt, err := r.db.Prepare(sb.String())
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.QueryRow(args...).Scan(&a.ID, &a.ArticleKey, &a.Content, &a.CreatedAt, &a.UpdatedAt, &b.UserId, &password, &b.PasswordUpdatedAt, &a.PublishOption)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	// 公開オプションが「公開」ならそのまま返す
	if a.PublishOption == model.PublishOptionPublic {
		return a.ConvertArticleForDBToArticle(), nil
	}
	// 公開オプションが「パスワード制限」か「非公開」の場合、閲覧権限をチェックする
	if opts != nil {
		// ユーザー自身が作ったブログなら閲覧可能
		if opts.UserId != nil {
			if *opts.UserId == b.UserId {
				return a.ConvertArticleForDBToArticle(), nil
			}
		}
		// 公開オプションが「パスワード制限」で正しいパスワードを入力された場合は閲覧可能
		if a.PublishOption == model.PublishOptionPassword && opts.Password != nil {
			err := bcrypt.CompareHashAndPassword([]byte(password), []byte(*opts.Password))
			switch {
			case err == bcrypt.ErrMismatchedHashAndPassword:
				// パスワードが一致しない
				return nil, errors.Wrap(customError.ForbiddenError, "DB error")
			case err != nil:
				// パスワードが一致しない以外のエラー
				return nil, errors.Wrap(err, "DB error")
			default:
				return a.ConvertArticleForDBToArticle(), nil
			}
		}
	}
	if a.PublishOption == model.PublishOptionPassword {
		if passwordInputTime == nil {
			// パスワードが未入力の場合、閲覧不可の要素を削除
			a.Content = nil
			a.CreatedAt = nil
			a.UpdatedAt = nil
			return a.ConvertArticleForDBToArticle(), nil
		} else {
			if b.PasswordUpdatedAt.Before(*passwordInputTime) {
				return a.ConvertArticleForDBToArticle(), nil
			}
			return nil, errors.Wrap(customError.BlogAuthenticationExpiredError, "DB error")
		}
	}
	// 閲覧権限なし
	return nil, errors.Wrap(customError.ForbiddenError, "DB error")
}

func (r *ArticleRepository) Create(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error) {
	articleKey, err := generateId()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}

	id := ulid.Make().String()

	cas := "INSERT INTO article (id, blog_id, article_key, content, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?)"
	stmt, err := r.db.Prepare(cas)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	_, err = stmt.Exec(id, input.BlogID, articleKey, input.Content, now, now)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return r.getArticle(id, userId)
}

func (r *ArticleRepository) Update(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error) {
	uas := `UPDATE article 
		INNER JOIN blog ON article.blog_id = blog.id 
		SET article.content = ?, article.updated_at = ? 
		WHERE article.id = ? AND blog.user_id = ?`
	stmt, err := r.db.Prepare(uas)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	res, err := stmt.Exec(input.Content, now, input.ID, userId)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return nil, errors.Wrap(customError.NotFoundError, "DB error")
	}
	return r.getArticle(input.ID, userId)
}

func (r *ArticleRepository) DeleteById(articleId string, userId int) error {
	// 削除
	as := `DELETE article FROM article 
		INNER JOIN blog ON article.blog_id = blog.id 
		WHERE article.id = ? AND blog.user_id = ?`
	stmt, err := r.db.Prepare(as)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	res, err := stmt.Exec(articleId, userId)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.NotFoundError, "DB error")
	}
	return nil
}

func (r *ArticleRepository) countApplyCursorsToEdges(blogId string, input model.ArticlePaginationInput) (int, error) {
	var sb strings.Builder
	var args []interface{}
	s := "SELECT count(article.id) FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE blog.id = ?"
	sb.WriteString(s)
	args = append(args, blogId)

	if input.After != nil {
		sb.WriteString(" AND article.id > ?")
		args = append(args, *input.After)
	}
	if input.Before != nil {
		sb.WriteString(" AND article.id < ?")
		args = append(args, *input.Before)
	}
	var count string
	stmt, err := r.db.Prepare(sb.String())
	if err != nil {
		return 0, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.QueryRow(args...).Scan(&count)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		return 0, errors.Wrap(err, "DB error")
	}

	return strconv.Atoi(count)
}

func (r *ArticleRepository) getArticle(id string, userId int) (*model.Article, error) {
	sas := "SELECT article.id, article.article_key, article.content," + blog.PublishOptionField +
		", article.created_at, article.updated_at FROM article INNER JOIN blog ON blog.id = article.blog_id WHERE article.id = ? AND blog.user_id = ?"
	a := &model.ArticleForDB{}
	stmt, err := r.db.Prepare(sas)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()
	err = stmt.QueryRow(id, userId).Scan(&a.ID, &a.ArticleKey, &a.Content, &a.PublishOption, &a.CreatedAt, &a.UpdatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return a.ConvertArticleForDBToArticle(), nil
}

func generateId() (string, error) {
	return gonanoid.Generate("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 21)
}
