package config

import (
	"fmt"
	"reflect"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

func TestFind(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewConfigRepository(db)
	s := regexp.QuoteMeta("SELECT allow_register FROM config LIMIT 1")

	t.Run("正常系", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"allow_register"}).
			AddRow(1)

		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnRows(rows)
		expected := &model.Config{
			AllowRegister: true,
		}

		result, err := repo.Find()

		if err != nil {
			t.Error("got err:", err)
		}

		if !reflect.DeepEqual(result, expected) {
			t.Errorf("got:\n%v\n\nwant:\n%v", result, expected)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectQuery(s).
			WithArgs().
			WillReturnError(fmt.Errorf("some error"))

		_, err := repo.Find()

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestUpdateAllowRegister(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Error("failed to open sqlmock database:", err)
	}
	defer db.Close()

	repo := NewConfigRepository(db)
	s := regexp.QuoteMeta("UPDATE config SET allow_register = ?")

	t.Run("正常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := repo.UpdateAllowRegister(true)

		if err != nil {
			t.Error("got err:", err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系：対象なし", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(0).
			WillReturnResult(sqlmock.NewResult(0, 0))

		err := repo.UpdateAllowRegister(false)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("異常系", func(t *testing.T) {
		mock.ExpectPrepare(s)
		mock.ExpectExec(s).
			WithArgs(0).
			WillReturnError(fmt.Errorf("some error"))

		err := repo.UpdateAllowRegister(false)

		if err == nil {
			t.Error("should got error")
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
