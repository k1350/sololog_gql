package config

import (
	"database/sql"
	"github.com/pkg/errors"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type ConfigRepository struct {
	*sql.DB
}

func NewConfigRepository(db *sql.DB) *ConfigRepository {
	return &ConfigRepository{db}
}

func (r *ConfigRepository) Find() (*model.Config, error) {
	s := "SELECT allow_register FROM config LIMIT 1"
	var c model.ConfigForDB
	stmt, err := r.Prepare(s)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	err = stmt.QueryRow().Scan(&c.AllowRegister)
	if err != nil {
		return nil, errors.Wrap(err, "DB error")
	}
	return c.ConvertConfigForDBToConfig(), nil
}

func (r *ConfigRepository) UpdateAllowRegister(allow bool) error {
	s := "UPDATE config SET allow_register = ?"
	stmt, err := r.Prepare(s)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	defer stmt.Close()

	var intAllow int
	if allow {
		intAllow = 1
	} else {
		intAllow = 0
	}
	res, err := stmt.Exec(intAllow)
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "DB error")
	}
	if rowsAffected == 0 {
		return errors.Wrap(customError.NotFoundError, "DB error")
	}
	return nil
}
