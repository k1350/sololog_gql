package auth

import (
	"context"
	"time"

	firebaseAuth "firebase.google.com/go/v4/auth"
	"github.com/pkg/errors"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type AuthRepository struct {
	client *firebaseAuth.Client
}

func NewAuthRepository(c *firebaseAuth.Client) *AuthRepository {
	return &AuthRepository{client: c}
}

func (r *AuthRepository) VerifyToken(ctx context.Context, idtoken string) (*model.UserInfoByToken, error) {
	token, err := r.client.VerifyIDToken(ctx, idtoken)
	if err != nil {
		return nil, errors.Wrap(err, "Invalid IdToken")
	}
	fclaims := token.Claims["firebase"].(map[string]interface{})
	roleStr := token.Claims["role"]
	var role model.Role
	if roleStr == nil {
		role = model.RoleUndefined
	} else {
		role = model.ConvertToRole(roleStr.(string))
	}
	return &model.UserInfoByToken{
		UID:      token.UID,
		Provider: fclaims["sign_in_provider"].(string),
		Role:     role,
		IssuedAt: time.Unix(token.IssuedAt, 0).UTC(),
	}, nil
}

func (r *AuthRepository) SetRole(ctx context.Context, uid string, role model.Role) error {
	// カスタムクレームをセット
	claims := map[string]interface{}{"role": role}
	err := r.client.SetCustomUserClaims(ctx, uid, claims)
	if err != nil {
		return errors.Wrap(err, "Setting custom claims error")
	}
	return nil
}

func (r *AuthRepository) DeleteUsers(ctx context.Context, uids []string) (*firebaseAuth.DeleteUsersResult, error) {
	deleteUsersResult, err := r.client.DeleteUsers(ctx, uids)
	if err != nil {
		return deleteUsersResult, errors.Wrap(err, "Delete user error")
	}
	return deleteUsersResult, nil
}
