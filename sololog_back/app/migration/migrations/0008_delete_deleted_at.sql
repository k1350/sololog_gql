-- +migrate Up
-- 今まで論理削除されていた行を物理削除する
delete from user where deleted_at is not null;
delete from provider_user where deleted_at is not null;
delete from blog where deleted_at is not null;
delete from article where deleted_at is not null;

alter table user drop column deleted_at;
alter table provider_user drop column deleted_at;
alter table blog drop column deleted_at;
alter table article drop column deleted_at;

-- +migrate Down
alter table user add column deleted_at datetime default null comment "削除日時";
alter table provider_user add column deleted_at datetime default null comment "削除日時";
alter table blog add column deleted_at datetime default null comment "削除日時";
alter table article add column deleted_at datetime default null comment "削除日時";
