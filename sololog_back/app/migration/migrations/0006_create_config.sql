-- +migrate Up
create table config (
    id int primary key comment "設定ID",
    allow_register tinyint not null comment "新規登録可能フラグ"
) comment = "設定";
insert into config (id, allow_register) values (1, 1);

-- +migrate Down
drop table config;
