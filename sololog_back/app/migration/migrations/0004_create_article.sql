-- +migrate Up
create table article (
    id char(26) primary key comment "記事ID",
    blog_id char(26) not null comment "ブログID",
    article_key char(21) unique not null comment "記事キー",
    content mediumtext not null comment "内容",
    created_at datetime not null comment "作成日時",
    updated_at datetime not null comment "更新日時",
    deleted_at datetime default null comment "削除日時",
    index blog_id_index (blog_id),
    index article_key_index (article_key)
) comment = "記事";

-- +migrate Down
drop table article;
