-- +migrate Up
create table provider_user (
    uid varchar(255) not null comment "連携ユーザーID",
    provider varchar(50) not null comment "連携プロバイダ",
    user_id int not null comment "ユーザーID",
    created_at datetime not null comment "作成日時",
    updated_at datetime not null comment "更新日時",
    deleted_at datetime default null comment "削除日時",
    primary key(uid, provider),
    index user_id_index (user_id)
) comment = "連携ユーザー";

-- +migrate Down
drop table provider_user;
