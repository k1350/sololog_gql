-- +migrate Up
-- ロール 1：管理者、2：ユーザー
create table user (
    id int auto_increment primary key comment "ユーザーID",
    role tinyint not null comment "ロール",
    last_logined datetime not null comment "最終ログイン日時",
    last_logouted datetime default null comment "最終ログアウト日時",
    created_at datetime not null comment "作成日時",
    updated_at datetime not null comment "更新日時",
    deleted_at datetime default null comment "削除日時",
    index role_index (role)
) comment = "ユーザー";

-- +migrate Down
drop table user;
