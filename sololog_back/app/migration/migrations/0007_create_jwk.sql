-- +migrate Up
create table jwk (
    id char(21) primary key comment "キーID",
    jwk blob not null comment "JWK",
    expired_at datetime not null comment "有効期限"
) comment = "JWK";

-- +migrate Down
drop table jwk;
