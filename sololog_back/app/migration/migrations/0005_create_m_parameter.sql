-- +migrate Up
create table m_parameter (
    id int primary key comment "マスタID",
    max_blogs tinyint not null comment "最大ブログ数",
    max_blog_links tinyint not null comment "最大ブログリンク数"
) comment = "マスタパラメータ";
insert into m_parameter (id, max_blogs, max_blog_links) values (1, 5, 3);

-- +migrate Down
drop table m_parameter;
