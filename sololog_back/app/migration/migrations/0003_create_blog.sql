-- +migrate Up
-- 公開オプション 1: 公開、2: パスワード制限、99: 非公開
create table blog (
    id char(26) primary key comment "ブログID",
    blog_key char(21) unique not null comment "ブログキー",
    user_id int not null comment "ユーザーID",
    author varchar(50) not null comment "著者",
    name varchar(255) not null comment "ブログ名",
    description varchar(500) not null default "" comment "説明",
    publish_option tinyint not null comment "公開オプション",
    password varchar(255) not null default "" comment "パスワード",
    password_hint varchar(100) not null default "" comment "パスワードのヒント",
    password_updated_at datetime not null comment "パスワード更新日時",
    links json not null comment "ブログリンク",
    created_at datetime not null comment "作成日時",
    updated_at datetime not null comment "更新日時",
    deleted_at datetime default null comment "削除日時",
    index blog_key_index (blog_key),
    index user_id_index (user_id)
) comment = "ブログ";

-- +migrate Down
drop table blog;
