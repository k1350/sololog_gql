package migration

import (
	"database/sql"
	"embed"
	"fmt"

	"github.com/rubenv/sql-migrate"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

//go:embed migrations/*.sql
var static embed.FS

type Migration struct {
	db *sql.DB
}

func NewMigration(db *sql.DB) *Migration {
	return &Migration{db}
}

func (m *Migration) MigrateUp() error {
	migrations := &migrate.AssetMigrationSource{
		Asset: static.ReadFile,
		AssetDir: func() func(string) ([]string, error) {
			return func(path string) ([]string, error) {
				return assetDir(path)
			}
		}(),
		Dir: "migrations",
	}

	n, err := migrate.Exec(m.db, "mysql", migrations, migrate.Up)
	clog.Debug(fmt.Sprintf("Applied %d migrations!", n))
	return err
}

func (m *Migration) MigrateDown() error {
	migrations := &migrate.AssetMigrationSource{
		Asset: static.ReadFile,
		AssetDir: func() func(string) ([]string, error) {
			return func(path string) ([]string, error) {
				return assetDir(path)
			}
		}(),
		Dir: "migrations",
	}

	n, err := migrate.Exec(m.db, "mysql", migrations, migrate.Down)
	clog.Debug(fmt.Sprintf("Applied %d migrations!", n))
	return err
}

func assetDir(path string) ([]string, error) {
	dirEntry, err := static.ReadDir(path)
	if err != nil {
		return nil, err
	}
	entries := make([]string, 0)
	for _, e := range dirEntry {
		entries = append(entries, e.Name())
	}

	return entries, nil
}
