package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/base64"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	firebase "firebase.google.com/go/v4"
	firebaseAuth "firebase.google.com/go/v4/auth"
	"github.com/99designs/gqlgen/graphql/handler"
	// "github.com/99designs/gqlgen/graphql/playground"
	"github.com/allegro/bigcache/v3"
	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/k1350/jwx_repository"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/directive"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/generated"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/middleware/auth"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/middleware/header"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/migration"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/parameters"
	ar "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/article"
	authr "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/auth"
	br "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/blog"
	con "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/config"
	mpr "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/mparameter"
	ur "gitlab.com/k1350/sololog_gql/sololog_back/app/repository/user"
	adu "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/admin"
	au "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/auth"
	bu "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/blog"
	ju "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/jwk"
	mpu "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/mparameter"
	resol "gitlab.com/k1350/sololog_gql/sololog_back/app/usecase/resolver"
)

const (
	defaultPort     = "8080"
	credentialFile  = "credentials.json"
	maxOpenConns    = 10
	maxIdleConns    = 5
	connMaxLifeTime = time.Second * 10
	shutdownTimeout = time.Second * 90
)

func main() {
	// 環境変数の読み取り
	err := parameters.FetchParameterStore()
	if err != nil {
		clog.Emergency(err)
		panic(err)
	}

	dsn := os.Getenv("DSN")
	db, err := connectDb(dsn)
	if err != nil {
		clog.Emergency(err)
		panic(err)
	}
	defer db.Close()
	if err = db.Ping(); err != nil {
		clog.Emergency(err)
		panic(err)
	}

	skipMigration := os.Getenv("SKIP_MIGRATION")
	if skipMigration == "true" {
		clog.Debug("skip migration.")
	} else {
		mig := migration.NewMigration(db)
		err = mig.MigrateUp()
		if err != nil {
			clog.Emergency(err)
			panic(err)
		}
	}

	client, err := setupFirebase()
	if err != nil {
		clog.Emergency(err)
		panic(err)
	}

	// 一週間キャッシュする
	cache, err := bigcache.New(context.Background(), bigcache.DefaultConfig(30*24*time.Hour))
	if err != nil {
		clog.Emergency(err)
	}
	defer cache.Close()

	config, au := buildConfig(db, client, cache)

	srv := handler.NewDefaultServer(
		generated.NewExecutableSchema(config),
	)

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	origin := os.Getenv("FRONT_ORIGIN")

	http.HandleFunc("/healthcheck", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
	})
	// http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query",
		auth.Auth(au,
			header.ResponseHeader(origin, srv),
		),
	)

	server := &http.Server{
		Addr:    ":" + port,
		Handler: nil,
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGTERM)
	defer stop()
	go func() {
		<-ctx.Done()
		clog.Debug("Caught SIGTERM, shutting down")
		ctx2, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()
		err = server.Shutdown(ctx2)
		if err != nil {
			clog.Emergency(err)
		}
	}()

	clog.Debug("Start server")
	err = server.ListenAndServe()
	if err != http.ErrServerClosed {
		clog.Emergency("Server closed with error:", err)
		panic(err)
	}
	clog.Debug("Main end")
}

func connectDb(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(maxOpenConns)
	db.SetMaxIdleConns(maxIdleConns)
	db.SetConnMaxLifetime(connMaxLifeTime)
	return db, nil
}

func setupFirebase() (*firebaseAuth.Client, error) {
	// 環境変数から base64 エンコードされた JSON を読み取ってファイルとして保存する
	serviceAccount := os.Getenv("GOOGLE_SERVICE_ACCOUNT_JSON")
	buf := bytes.NewBufferString(serviceAccount)
	dst := make([]byte, base64.StdEncoding.DecodedLen(buf.Len()))
	_, err := base64.StdEncoding.Decode(dst, buf.Bytes())
	if err != nil {
		return nil, err
	}
	f, err := os.Create(credentialFile)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := f.Close()
		if err != nil {
			clog.Emergency(err)
		}
	}()
	_, err = f.Write(dst)
	if err != nil {
		return nil, err
	}
	// Firebase SDK セットアップのために環境変数に JSON へのパスをセット
	err = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", credentialFile)
	if err != nil {
		return nil, err
	}

	// Firebase SDK のセットアップ
	app, err := firebase.NewApp(context.Background(), nil)
	if err != nil {
		return nil, err
	}
	return app.Auth(context.Background())
}

func buildConfig(db *sql.DB, client *firebaseAuth.Client, cache *bigcache.BigCache) (generated.Config, *au.AuthUsecase) {
	b := br.NewBlogRepository(db)
	a := ar.NewArticleRepository(db)
	u := ur.NewUserRepository(db)
	aut := authr.NewAuthRepository(client)
	mp := mpr.NewMParameterRepository(db)
	con := con.NewConfigRepository(db)
	jwkr := jwx_repository.NewJWKRepository(db)
	au := au.NewAuthUsecase(aut, u, b, con)
	bu := bu.NewBlogUsecase(b, a, mp)
	mpu := mpu.NewMParameterUsecase(mp)
	adu := adu.NewAdminUsecase(con)
	jwku := ju.NewJWKUsecase(cache, jwkr)
	resolu := resol.NewResolverUsecase(au, bu, mpu, adu, jwku)

	c := generated.Config{
		Resolvers: &graph.Resolver{
			ResolverUsecase: resolu,
		},
	}
	c.Directives.NumberValue = directive.NumberValue
	c.Directives.StringValue = directive.StringValue
	c.Directives.PublishOptionValue = directive.PublishOptionValue
	c.Directives.PasswordValue = directive.PasswordValue

	return c, au
}
