package graph

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

//go:generate go run github.com/99designs/gqlgen generate

import (
	"context"
	"time"

	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
)

type IResolverUsecase interface {
	Login(ctx context.Context, now time.Time, input model.LoginInput) (bool, error)
	Logout(now time.Time, userId int) (bool, error)
	DeleteUser(ctx context.Context, now time.Time, userId int, user model.UserInfoByToken) (bool, error)
	CreateBlog(now time.Time, input model.CreateBlogInput, userId int) (*model.Blog, error)
	CreateArticle(now time.Time, input model.CreateArticleInput, userId int) (*model.Article, error)
	UpdateBlog(now time.Time, input model.UpdateBlogInput, userId int) (*model.Blog, error)
	UpdateArticle(now time.Time, input model.UpdateArticleInput, userId int) (*model.Article, error)
	DeleteBlog(id string, userId int) (bool, error)
	DeleteArticle(id string, userId int) (bool, error)
	UpdateConfig(role model.Role, input model.UpdateConfigInput) (*model.Config, error)
	Blogs(userId int) ([]*model.Blog, error)
	BlogByID(id string, userId int) (*model.Blog, error)
	BlogByBlogKey(now time.Time, input model.BlogByBlogKeyInput, userId *int) (*model.Blog, error)
	IsAuthor(blogKey string, userId int) (bool, error)
	Articles(now time.Time, input model.BlogByBlogKeyInput, paginationInput model.ArticlePaginationInput, userId *int) (*model.ArticleConnection, error)
	ArticleByArticleKey(now time.Time, input model.ArticleByArticleKeyInput, userId *int) (*model.Article, error)
	MasterParameter() (*model.MasterParameter, error)
	Config(role model.Role) (*model.Config, error)
}

type Resolver struct {
	ResolverUsecase IResolverUsecase
}
