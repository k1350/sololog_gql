package model

import (
	"encoding/json"
	"time"
)

type BlogForDB struct {
	ID                string           `json:"id"`
	BlogKey           string           `json:"blogKey"`
	UserId            int              `json:"user"`
	Author            *string          `json:"author"`
	Name              *string          `json:"name"`
	Description       *string          `json:"description"`
	PublishOption     PublishOption    `json:"publishOption"`
	PasswordUpdatedAt time.Time        `json:"passwordUpdatedAt"`
	PasswordHint      string           `json:"passwordHint"`
	Links             *json.RawMessage `json:"links"`
	CreatedAt         *time.Time       `json:"createdAt"`
	UpdatedAt         *time.Time       `json:"updatedAt"`
}

type QueryArgs struct {
	Query string
	Args  []interface{}
}

type BlogByKeyOpts struct {
	UserId   *int
	Password *string
}

type PublishOptionAndPasswordUpdatedAt struct {
	PublishOption     PublishOption
	PasswordUpdatedAt time.Time
}

func (bfdb *BlogForDB) ConvertBlogForDBToBlog() *Blog {
	b := &Blog{
		ID:            bfdb.ID,
		BlogKey:       bfdb.BlogKey,
		Author:        bfdb.Author,
		Name:          bfdb.Name,
		Description:   bfdb.Description,
		PublishOption: bfdb.PublishOption,
		PasswordHint:  bfdb.PasswordHint,
	}
	if bfdb.CreatedAt != nil {
		createdAt := (*bfdb.CreatedAt).Format(time.RFC3339)
		b.CreatedAt = &createdAt
	}
	if bfdb.UpdatedAt != nil {
		updatedAt := (*bfdb.UpdatedAt).Format(time.RFC3339)
		b.UpdatedAt = &updatedAt
	}
	if bfdb.Links != nil {
		var links []BlogLink
		json.Unmarshal(*bfdb.Links, &links)
		var blogLinks []*BlogLink
		for i := range links {
			blogLinks = append(blogLinks, &links[i])
		}
		b.Links = blogLinks
	} else {
		b.Links = nil
	}
	return b
}

func ConvertUnixTimeToTime(unix interface{}) *time.Time {
	var t *time.Time
	if unix == nil {
		t = nil
	} else {
		dtFromUnix := time.Unix(unix.(int64), 0)
		t = &dtFromUnix
	}
	return t
}
