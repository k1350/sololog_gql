package model

import (
	"time"
)

type ArticleForDB struct {
	ID            string        `json:"id"`
	ArticleKey    string        `json:"articleKey"`
	Content       *string       `json:"content"`
	PublishOption PublishOption `json:"publishOption"`
	CreatedAt     *time.Time    `json:"createdAt"`
	UpdatedAt     *time.Time    `json:"updatedAt"`
}

type ArticleByKeyOpts struct {
	UserId   *int
	Password *string
}

func (afdb *ArticleForDB) ConvertArticleForDBToArticle() *Article {
	a := &Article{
		ID:            afdb.ID,
		ArticleKey:    afdb.ArticleKey,
		Content:       afdb.Content,
		PublishOption: afdb.PublishOption,
	}
	if afdb.CreatedAt != nil {
		createdAt := (*afdb.CreatedAt).Format(time.RFC3339)
		a.CreatedAt = &createdAt
	}
	if afdb.UpdatedAt != nil {
		updatedAt := (*afdb.UpdatedAt).Format(time.RFC3339)
		a.UpdatedAt = &updatedAt
	}
	return a
}
