package model

import (
	"fmt"
	"io"
	"strconv"
	"time"
)

type UserForDB struct {
	ID           int
	Role         Role
	LastLogined  time.Time
	LastLogouted *time.Time
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

type ProviderUserForDB struct {
	UID       string
	Provider  string
	UserId    int
	CreatedAt time.Time
	UpdatedAt time.Time
}

type UserInfoByToken struct {
	UID      string
	Provider string
	Role     Role
	IssuedAt time.Time
}

type Role string

const (
	RoleAdmin     Role = "ADMIN"
	RoleUser      Role = "USER"
	RoleUndefined Role = "UNDEFINED"
)

var AllRole = []Role{
	RoleAdmin,
	RoleUser,
}

type UpdateUserInput struct {
	ID           int
	Role         *Role
	LastLogined  *time.Time
	LastLogouted *time.Time
}

func ConvertToRole(v string) Role {
	if v == "ADMIN" {
		return RoleAdmin
	}
	if v == "USER" {
		return RoleUser
	}
	return RoleUndefined
}

func (e Role) IsValid() bool {
	switch e {
	case RoleAdmin, RoleUser:
		return true
	}
	return false
}

func (e Role) String() string {
	return string(e)
}

func (e *Role) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = Role(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid Role", str)
	}
	return nil
}

func (e Role) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}
