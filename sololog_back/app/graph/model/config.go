package model

type ConfigForDB struct {
	ID            int
	AllowRegister int
}

func (cfdb *ConfigForDB) ConvertConfigForDBToConfig() *Config {
	var allowRegister bool
	if cfdb.AllowRegister == 0 {
		allowRegister = false
	} else {
		allowRegister = true
	}
	return &Config{
		AllowRegister: allowRegister,
	}
}
