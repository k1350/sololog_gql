package header

import (
	"net/http"
)

const xRequestedWith = "XMLHttpRequest"
const contentType = "application/json"

func ResponseHeader(origin string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !isAllowedMethod(r.Method) {
			w.Header().Set("Allow", "GET, POST, OPTIONS, HEAD")
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			return
		}

		if r.Method == http.MethodPost {
			if isAllowedOrigin(r.Header["Origin"], origin) {
				if isAllowedPostAction(r.Header) {
					w.Header().Set("Access-Control-Allow-Origin", origin)
					w.Header().Set("X-Content-Type-Options", "nosniff")
					next.ServeHTTP(w, r)
					return
				}
			}
		} else if r.Method == http.MethodOptions {
			if isAllowedOrigin(r.Header["Origin"], origin) {
				w.Header().Set("Access-Control-Allow-Origin", origin)
				w.Header().Set("Access-Control-Allow-Credentials", "POST, OPTIONS")
				w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With")
				w.Header().Set("Access-Control-Allow-Credentials", "true")
				w.Header().Set("Access-Control-Max-Age", "7200")
				next.ServeHTTP(w, r)
				return
			}
		}

		http.Error(w, "Forbidden", http.StatusForbidden)
	})
}

// 許可メソッドの確認
// GET と HEAD は未使用だが405を返してはならないので含めている
func isAllowedMethod(method string) bool {
	allowedMethods := [4]string{http.MethodPost, http.MethodOptions, http.MethodGet, http.MethodHead}
	for _, v := range allowedMethods {
		if method == v {
			return true
		}
	}
	return false
}

// オリジンの確認
func isAllowedOrigin(header []string, origin string) bool {
	return len(header) > 0 && header[0] == origin
}

func isAllowedPostAction(header http.Header) bool {
	xRequestedWithHeader := header["X-Requested-With"]
	if len(xRequestedWithHeader) == 0 || xRequestedWithHeader[0] != xRequestedWith {
		return false
	}
	contentTypeHeader := header["Content-Type"]
	if len(contentTypeHeader) == 0 || contentTypeHeader[0] != contentType {
		return false
	}
	return true
}
