package auth

import (
	"context"
	"net/http"
	"time"

	customError "gitlab.com/k1350/sololog_gql/sololog_back/app/errors"
	"gitlab.com/k1350/sololog_gql/sololog_back/app/graph/model"
	clog "gitlab.com/k1350/sololog_gql/sololog_back/app/log"
)

type AuthContext struct {
	userId *int
	user   *model.UserInfoByToken
}

type IAuthUsecase interface {
	GetFirebaseUser(ctx context.Context, token string) (*model.UserInfoByToken, error)
	GetUserId(uid string, provider string, issuedAt time.Time) (*int, error)
}

func Auth(u IAuthUsecase, next http.Handler) http.Handler {
	authUsecase = u
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var userId *int
		var user *model.UserInfoByToken
		h := r.Header["Authorization"]
		if len(h) > 0 && len(h[0]) > 6 {
			token := h[0][7:]
			var err error
			user, err = authUsecase.GetFirebaseUser(r.Context(), token)
			if err != nil {
				clog.Error(err)
			}
			if user != nil {
				userId, err = authUsecase.GetUserId(user.UID, user.Provider, user.IssuedAt)
				if err != nil {
					clog.Error(err)
				}
			} else {
				userId = nil
			}
		} else {
			userId = nil
			user = nil
		}
		ctx := context.WithValue(r.Context(), auth, &AuthContext{
			userId: userId,
			user:   user,
		})
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

type ctxKey int

const (
	auth ctxKey = iota
)

var authUsecase IAuthUsecase

func GetUserId(ctx context.Context) (*int, error) {
	userId := ctx.Value(auth).(*AuthContext).userId
	if userId == nil {
		return nil, customError.UnauthenticatedError
	}
	return userId, nil
}

func GetFirebaseUser(ctx context.Context) (*model.UserInfoByToken, error) {
	user := ctx.Value(auth).(*AuthContext).user
	if user == nil {
		return nil, customError.UnauthenticatedError
	}
	return user, nil
}
