#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { AppStack } from "../lib/stacks/app-stack";
import { getProperties } from "../lib/env/getProperties";
import { ImageStack } from "../lib/stacks/image-stack";

const app = new cdk.App();
const envName = app.node.tryGetContext("CDK_ENV");
const properties = getProperties(envName);

const imageStack = new ImageStack(app, "SolologBackImageStack", {
  env: {
    account: properties.CDK_ACCOUNT,
    region: properties.CDK_REGION,
  },
});

const appStack = new AppStack(app, "SolologBackAppStack", {
  env: {
    account: properties.CDK_ACCOUNT,
    region: properties.CDK_REGION,
  },
  CERTIFICATE_ARN: properties.CERTIFICATE_ARN,
  BACK_APP: properties.BACK_APP,
  REPOSITORY: imageStack.REPOSITORY,
});

appStack.addDependency(imageStack);
