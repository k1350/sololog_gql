import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import {
  Cluster,
  ContainerImage,
  FargatePlatformVersion,
  FargateTaskDefinition,
  FargateService,
  AwsLogDriver,
} from "aws-cdk-lib/aws-ecs";
import {
  Peer,
  Port,
  SecurityGroup,
  SubnetType,
  Vpc,
} from "aws-cdk-lib/aws-ec2";
import {
  ApplicationLoadBalancer,
  ApplicationProtocol,
  ApplicationTargetGroup,
  TargetType,
  SslPolicy,
} from "aws-cdk-lib/aws-elasticloadbalancingv2";
import { Role, ServicePrincipal, ManagedPolicy } from "aws-cdk-lib/aws-iam";
import { LogGroup } from "aws-cdk-lib/aws-logs";
import { Certificate } from "aws-cdk-lib/aws-certificatemanager";
import crypto = require("crypto");
import { Duration } from "aws-cdk-lib";

type BackAppProps = {
  CPU: number;
  MEMORY_LIMIT_MB: number;
  DESIRED_COUNT: number;
};

type AppStackProps = cdk.StackProps & {
  CERTIFICATE_ARN: string;
  BACK_APP: BackAppProps;
  REPOSITORY: cdk.aws_ecr.IRepository;
};

export class AppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: AppStackProps) {
    super(scope, id, props);

    // VPC
    const vpc = new Vpc(this, "SolologBackVPC", {
      maxAzs: 2,
      subnetConfiguration: [
        {
          name: "SolologBackPublicSubnet",
          cidrMask: 24,
          subnetType: SubnetType.PUBLIC,
        },
      ],
      natGateways: 0,
    });

    // AWS Certificate Manager
    const certificate = Certificate.fromCertificateArn(
      this,
      "SolologBackDomainCert",
      props.CERTIFICATE_ARN
    );

    // LoadBalancer
    const albSg = new SecurityGroup(this, "SolologBackALBSecurityGroup", {
      vpc,
      allowAllOutbound: true,
    });
    albSg.addIngressRule(Peer.anyIpv4(), Port.tcp(80));
    albSg.addIngressRule(Peer.anyIpv4(), Port.tcp(443));
    const alb = new ApplicationLoadBalancer(
      this,
      "SolologBackApplicationLoadBalancer",
      {
        vpc: vpc,
        internetFacing: true,
        securityGroup: albSg,
      }
    );
    const targetGroup = new ApplicationTargetGroup(
      this,
      "SolologBackListenerTarget",
      {
        protocol: ApplicationProtocol.HTTP,
        port: 8080,
        vpc,
        targetType: TargetType.IP,
        healthCheck: {
          path: "/healthcheck",
          healthyHttpCodes: "204",
          timeout: cdk.Duration.seconds(120),
          interval: cdk.Duration.seconds(300),
        },
        // FARGATE_SPOT に関連付けられているターゲットグループの登録解除の遅延を 2 分未満の値に設定する必要がある
        deregistrationDelay: cdk.Duration.seconds(10),
      }
    );
    alb.addListener("SolologBack443Listener", {
      certificates: [certificate],
      defaultTargetGroups: [targetGroup],
      port: 443,
      sslPolicy: SslPolicy.TLS12_EXT,
    });
    // redirects HTTP port 80 to HTTPS port 443
    alb.addRedirect();

    // Fargate Task Definition
    const taskExecutionRole = new Role(this, "SolologBackTaskExecutionRole", {
      roleName: "sololog-back-task-execution-role",
      assumedBy: new ServicePrincipal("ecs-tasks.amazonaws.com"),
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName(
          "service-role/AmazonECSTaskExecutionRolePolicy"
        ),
      ],
    });
    const taskRole = new Role(this, "SolologBackTaskRole", {
      roleName: "sololog-back-task-role",
      assumedBy: new ServicePrincipal("ecs-tasks.amazonaws.com"),
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName("AmazonSSMReadOnlyAccess"),
      ],
    });
    const taskDefinition = new FargateTaskDefinition(
      this,
      "SolologBackTaskDef",
      {
        taskRole,
        executionRole: taskExecutionRole,
        cpu: props.BACK_APP.CPU,
        memoryLimitMiB: props.BACK_APP.MEMORY_LIMIT_MB,
      }
    );
    const logGroup = new LogGroup(this, "ServiceLogGroup", {
      logGroupName: "sololog-back-logs",
    });
    const hash = crypto
      .createHash("md5")
      .update(Math.random().toString())
      .digest("hex")
      .toString();
    taskDefinition.addContainer(`SolologBackAppContainer${hash}`, {
      image: ContainerImage.fromEcrRepository(props.REPOSITORY, "latest"),
      portMappings: [
        {
          containerPort: 8080,
        },
      ],
      logging: new AwsLogDriver({
        streamPrefix: "app",
        logGroup,
        multilinePattern: "^\\[(DEBUG|ERROR|EMERGENCY|mysql)\\]",
      }),
      stopTimeout: Duration.seconds(120),
    });

    // Fargate デプロイ
    const cluster = new Cluster(this, "SolologBackEcsCluster", {
      vpc,
    });
    const serviceSecurityGroup = new SecurityGroup(
      this,
      "SolologBackServiceSecurityGroup",
      {
        vpc: vpc,
      }
    );
    const fargateService = new FargateService(this, "SolologBackService", {
      cluster,
      taskDefinition: taskDefinition,
      vpcSubnets: vpc.selectSubnets({
        subnetType: SubnetType.PUBLIC,
      }),
      platformVersion: FargatePlatformVersion.VERSION1_4,
      securityGroups: [serviceSecurityGroup],
      desiredCount: props.BACK_APP.DESIRED_COUNT,
      assignPublicIp: true,
      capacityProviderStrategies: [
        {
          capacityProvider: "FARGATE",
          base: 0,
          weight: 1,
        },
        {
          capacityProvider: "FARGATE_SPOT",
          base: 0,
          weight: props.BACK_APP.DESIRED_COUNT - 1,
        },
      ],
    });
    targetGroup.addTarget(fargateService);
  }
}
