import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import { DockerImageAsset } from "aws-cdk-lib/aws-ecr-assets";
import { ECRDeployment, DockerImageName } from "cdk-ecr-deployment";
import path = require("path");
import { Repository } from "aws-cdk-lib/aws-ecr";
import crypto = require("crypto");

export class ImageStack extends cdk.Stack {
  public readonly REPOSITORY: cdk.aws_ecr.IRepository;

  constructor(scope: Construct, id: string, props: cdk.StackProps) {
    super(scope, id, props);

    // Docker イメージを ECR にデプロイ
    const repositoryName = "sololog-back-repository";
    const hash = crypto
      .createHash("md5")
      .update(Math.random().toString())
      .digest("hex")
      .toString();
    const image = new DockerImageAsset(this, "SolologBackImageAsset", {
      directory: path.join(__dirname, "..", "..", ".."),
      file: path.join("docker", "go", "Dockerfile.prod"),
    });

    new ECRDeployment(this, "SolologBackImage", {
      src: new DockerImageName(image.imageUri),
      dest: new DockerImageName(
        `${props.env?.account}.dkr.ecr.${props.env?.region}.amazonaws.com/${repositoryName}:latest`
      ),
    });

    this.REPOSITORY = Repository.fromRepositoryName(
      this,
      "SolologBackRepository",
      repositoryName
    );
  }
}
