const config = {
  CDK_ACCOUNT: "123456789012",
  CDK_REGION: "ap-northeast-1",
  CERTIFICATE_ARN:
    "arn:aws:acm:ap-northeast-1:123456789012:certificate/xxxxxxxx-3450-4663-83a8-6318f094b1b1",
  BACK_APP: {
    CPU: 256,
    MEMORY_LIMIT_MB: 512,
    DESIRED_COUNT: 1,
  },
};

export default config;
