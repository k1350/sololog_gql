import DevProperties from "./dev";
import ProdProperties from "./prod";

export const getProperties = (env: string) => {
  switch (env) {
    case "dev":
      return DevProperties;
    case "prod":
      return ProdProperties;
    default:
      throw new Error("No Support environment");
  }
};
