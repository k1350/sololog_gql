package main

import (
	"database/sql"
	"encoding/json"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitlab.com/k1350/jwx_repository"
)

const (
	dsnParameterName = "/sololog/DSN"
)

type Parameter struct {
	ARN              string
	DataType         string
	LastModifiedDate string
	Name             string
	Selector         *string
	SourceResult     *string
	Type             string
	Value            string
	Version          int
}

type ParameterResponse struct {
	Parameter      Parameter
	ResultMetaData map[string]interface{}
}

func fetchDSN() (string, error) {
	token := os.Getenv("AWS_SESSION_TOKEN")
	req, err := http.NewRequest(
		http.MethodGet,
		"http://localhost:2773/systemsmanager/parameters/get?name="+url.QueryEscape(dsnParameterName)+"&withDecryption=true",
		nil,
	)
	if err != nil {
		return "", err
	}
	req.Header.Set("X-Aws-Parameters-Secrets-Token", token)

	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var result ParameterResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return "", err
	}
	return result.Parameter.Value, nil
}

func rotateKey() error {
	dsn, err := fetchDSN()
	if err != nil {
		return err
	}

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return err
	}
	defer db.Close()
	if err = db.Ping(); err != nil {
		return err
	}

	jwkr := jwx_repository.NewJWKRepository(db)

	now := time.Now().UTC()
	// 新規鍵作成
	err = jwkr.Create(now)
	if err != nil {
		return err
	}

	// 古い鍵削除
	return jwkr.DeleteOldKey(now)
}

func main() {
	lambda.Start(rotateKey)
}
