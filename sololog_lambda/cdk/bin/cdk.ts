#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { AppStack } from "../lib/stacks/app-stack";
import { getProperties } from "../lib/env/getProperties";

const app = new cdk.App();
const envName = app.node.tryGetContext("CDK_ENV");
const properties = getProperties(envName);

new AppStack(app, "SolologLambdaAppStack", {
  env: {
    account: properties.CDK_ACCOUNT,
    region: properties.CDK_REGION,
  },
});
