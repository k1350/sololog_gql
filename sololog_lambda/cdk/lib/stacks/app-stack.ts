import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import { GoFunction } from "@aws-cdk/aws-lambda-go-alpha";
import { Rule, Schedule } from "aws-cdk-lib/aws-events";
import { LambdaFunction } from "aws-cdk-lib/aws-events-targets";
import { PolicyStatement } from "aws-cdk-lib/aws-iam";
import { LayerVersion } from "aws-cdk-lib/aws-lambda";

const AWS_PARAMETERS_AND_SECRETS_LAMBDA_EXTENSION_LAYER =
  "arn:aws:lambda:ap-northeast-1:133490724326:layer:AWS-Parameters-and-Secrets-Lambda-Extension:2";
const DSN = "/sololog/DSN";

export class AppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: cdk.StackProps) {
    super(scope, id, props);

    const lambdaPolicyStatement = new PolicyStatement({
      sid: "GetParameterStore",
      actions: ["ssm:GetParameter"],
      resources: [
        `arn:aws:ssm:${props.env?.region}:${props.env?.account}:parameter${DSN}`,
      ],
    });

    const lambda = new GoFunction(this, "handler", {
      entry: "../app",
      layers: [
        LayerVersion.fromLayerVersionArn(
          this,
          `AWSParametersAndSecretsLambdaExtensionLayer`,
          AWS_PARAMETERS_AND_SECRETS_LAMBDA_EXTENSION_LAYER
        ),
      ],
    });
    lambda.addToRolePolicy(lambdaPolicyStatement);

    new Rule(this, "SolologLambdaScheduleRule", {
      schedule: Schedule.cron({ minute: "0", hour: "16", day: "1" }),
      targets: [new LambdaFunction(lambda, { retryAttempts: 3 })],
    });
  }
}
