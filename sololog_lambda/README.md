# ローカル開発

Lambda Extensions を使う都合でローカル開発環境構築が難しいのでデプロイして動作確認する。

# Lambda デプロイ

## 事前準備

AWS Systems Manager > パラメータストアで "/sololog/DSN" というパスで MySQL への接続文字列を保存する。

## デプロイ

cdk ディレクトリで `cdk deploy`

## 削除

cdk ディレクトリで `cdk destroy`

以下は消えないので手動で削除する

- S3 バケットに残っている Lambda デプロイ時の zip と json
- CloudWatch のロググループ

# 本番環境

リージョンは ap-northeast-1 とする。

1. 先に sololog_back をデプロイする
2. cdk/lib/env/dev.example.ts をコピーして名前を prod.ts に変更し、中身を自分のアカウントの情報で書き換える
3. cdk ディレクトリで `yarn deploy:prod --profile [AWS CLI のプロファイル名]`

   - AWS CLI でプロファイル名をつけなかった場合は `yarn deploy:prod` のみ
